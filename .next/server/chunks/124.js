"use strict";
exports.id = 124;
exports.ids = [124];
exports.modules = {

/***/ 1124:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "LP": () => (/* binding */ Phone),
  "NZ": () => (/* binding */ Profile),
  "c6": () => (/* binding */ SwitchAccount)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: external "react-query"
var external_react_query_ = __webpack_require__(2585);
// EXTERNAL MODULE: ./components/Site/Modal/index.ts + 1 modules
var Modal = __webpack_require__(3117);
// EXTERNAL MODULE: ./mod/api/withGa/user.ts + 1 modules
var user = __webpack_require__(7134);
// EXTERNAL MODULE: ./pages/user.state.tsx
var user_state = __webpack_require__(2298);
// EXTERNAL MODULE: ./mod/utils/index.ts + 1 modules
var utils = __webpack_require__(3863);
// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__(2034);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./pages/member/CodeBtn.tsx
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






class CodeBtn extends external_react_.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      wait: 0,
      sending: false
    });

    _defineProperty(this, "timer", null);

    _defineProperty(this, "sendCode", () => {
      if (this.state.sending) {
        return;
      }

      this.setState(s => _objectSpread(_objectSpread({}, s), {}, {
        sending: true
      }));
      Promise.resolve(1).then(() => this.props.sendCode()).then(() => {
        this.setState(s => _objectSpread(_objectSpread({}, s), {}, {
          wait: 60
        }));
        this.timer = setInterval(() => {
          this.setState(s => {
            let nwait = s.wait - 1;

            if (nwait <= 0) {
              clearInterval(this.timer);
            }

            return _objectSpread(_objectSpread({}, s), {}, {
              wait: nwait
            });
          });
        }, 1e3);
      }).then(() => {
        external_react_toastify_.toast.success('验证码发送成功, 请查收');
      }).catch(err => {
        external_react_toastify_.toast.error((0,utils/* isRespError */.Vn)(err) ? err.message : '网络波动请重试');
      }).finally(() => {
        this.setState(s => _objectSpread(_objectSpread({}, s), {}, {
          sending: false
        }));
      });
    });
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    return /*#__PURE__*/jsx_runtime_.jsx("div", {
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("button", {
        type: "button",
        className: "btn p-2",
        disabled: this.state.sending || this.state.wait > 0,
        onClick: this.sendCode,
        children: [this.timer !== null ? '重新发送' : '发送验证码', this.state.wait > 0 && `${Math.max(this.state.wait, 0)}s`]
      })
    });
  }

}
// EXTERNAL MODULE: external "react-ga"
var external_react_ga_ = __webpack_require__(9831);
var external_react_ga_default = /*#__PURE__*/__webpack_require__.n(external_react_ga_);
;// CONCATENATED MODULE: ./pages/member/Settings.tsx
function Settings_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function Settings_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { Settings_ownKeys(Object(source), true).forEach(function (key) { Settings_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { Settings_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function Settings_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }













const registerUser = (0,utils/* withGAEvent */.v)(user_state/* registerUser */.a$)({
  category: '用户注册',
  action: '自动注册',
  label: '登录出错'
});
const Profile = ({
  info
}) => {
  var _info$nickName, _info$introduction;

  const [_x, dispatch] = Modal/* ModalState.useContainer */.XK.useContainer();
  const {
    refetch
  } = user_state/* UserQuery.useContainer */.fE.useContainer();
  const {
    0: state,
    1: setState
  } = (0,external_react_.useState)({
    nickName: (_info$nickName = info.nickName) !== null && _info$nickName !== void 0 ? _info$nickName : '',
    introduction: (_info$introduction = info.introduction) !== null && _info$introduction !== void 0 ? _info$introduction : ''
  });
  (0,external_react_.useEffect)(() => {
    setState(s => {
      var _info$nickName2, _info$introduction2;

      return Settings_objectSpread(Settings_objectSpread({}, s), {}, {
        nickName: (_info$nickName2 = info.nickName) !== null && _info$nickName2 !== void 0 ? _info$nickName2 : '',
        introduction: (_info$introduction2 = info.introduction) !== null && _info$introduction2 !== void 0 ? _info$introduction2 : ''
      });
    });
  }, [info.nickName, info.introduction]);
  const updateProfile = (0,external_react_query_.useMutation)(params => {
    return user/* updateInfo */.rn(params);
  });

  const updateProfileFn = () => {
    dispatch({
      type: 'loading'
    });
    updateProfile.mutateAsync({
      id: (0,user_state/* getUid */.sq)(),
      nickName: state.nickName,
      introduction: state.introduction
    }).then(async r => {
      await refetch();
      external_react_toastify_.toast.success('用户资料更新成功');
      dispatch({
        type: 'close'
      });
    }, err => {
      external_react_toastify_.toast.error((0,utils/* isRespError */.Vn)(err) ? err.message : '网络波动请重试');
      dispatch({
        type: 'loaded'
      });
    });
  };

  return /*#__PURE__*/jsx_runtime_.jsx(Modal/* Form */.l0, {
    title: "\u500B\u4EBA\u8CC7\u6599",
    subtitle: "\u60A8\u7684\u540D\u7A31\u53CA\u982D\u50CF\u5C07\u5728\u7559\u8A00\u6642\u5C55\u793A\uFF0C\u81EA\u6211\u4ECB\u7D39\u5247\u53EF\u5728\u500B\u4EBA\u4E3B\u9801\u88E1\u770B\u5230\u3002",
    close: () => dispatch({
      type: 'close'
    }),
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("form", {
      onSubmit: e => {
        e.preventDefault();
        updateProfileFn();
      },
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "alert alert-danger w-100",
        style: {
          display: 'none'
        },
        role: "alert"
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "form-group-attached",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "form-group required",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            htmlFor: "display_name",
            children: "\u6635\u79F0"
          }), /*#__PURE__*/jsx_runtime_.jsx("input", {
            id: "display_name",
            name: "display_name",
            className: "form-control",
            type: "text",
            maxLength: 20,
            value: state.nickName,
            onChange: e => {
              setState(s => Settings_objectSpread(Settings_objectSpread({}, s), {}, {
                nickName: e.target.value
              }));
            }
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "form-group",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            htmlFor: "about_me",
            children: "\u81EA\u6211\u4ECB\u7D39"
          }), /*#__PURE__*/jsx_runtime_.jsx("input", {
            id: "about_me",
            name: "about_me",
            className: "form-control",
            type: "text",
            value: state.introduction,
            onChange: e => {
              setState(s => Settings_objectSpread(Settings_objectSpread({}, s), {}, {
                introduction: e.target.value
              }));
            }
          })]
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("button", {
        className: "btn btn-submit btn-block",
        type: "submit",
        children: "\u66F4\u65B0\u8D44\u6599"
      })]
    })
  });
};
/**绑定手机 */

const Phone = props => {
  var _props$oldPhone;

  const [_x, dispatch] = Modal/* ModalState.useContainer */.XK.useContainer();
  const {
    0: state,
    1: setState
  } = (0,external_react_.useState)({
    phone: (_props$oldPhone = props === null || props === void 0 ? void 0 : props.oldPhone) !== null && _props$oldPhone !== void 0 ? _props$oldPhone : '',
    code: ''
  });
  (0,external_react_.useEffect)(() => {
    setState(s => {
      var _props$oldPhone2;

      return Settings_objectSpread(Settings_objectSpread({}, s), {}, {
        phone: (_props$oldPhone2 = props === null || props === void 0 ? void 0 : props.oldPhone) !== null && _props$oldPhone2 !== void 0 ? _props$oldPhone2 : ''
      });
    });
  }, [props.oldPhone]);

  const sendCode = params => {
    return Promise.resolve(1).then(() => {
      return user/* sendCode */.Z3({
        type: user/* SendCodeType.bindPhone */.yf.bindPhone,
        mobile: params.phone
      });
    }).then(r => r.data.result);
  };

  const bindPhone = params => {
    return user/* bindPhone */.eF({
      userId: (0,user_state/* getUid */.sq)(),
      mobile: params.phone,
      code: params.code
    }).then(r => r.data.result);
  };

  const {
    refetch: refetchUser
  } = user_state/* UserQuery.useContainer */.fE.useContainer();

  const sendCodeFn = () => {
    return sendCode({
      phone: state.phone
    });
  };

  const bindPhoneFn = () => {
    dispatch({
      type: 'loading'
    });
    Promise.resolve(1).then(() => bindPhone({
      phone: state.phone,
      code: state.code
    })).then(async r => {
      await refetchUser();
      external_react_toastify_.toast.success('手机号绑定成功');
      dispatch({
        type: 'close'
      });
    }, err => {
      external_react_toastify_.toast.error((0,utils/* isRespError */.Vn)(err) ? err.message : '网络波动请重试');
      dispatch({
        type: 'loaded'
      });
    });
  };

  return /*#__PURE__*/jsx_runtime_.jsx(Modal/* Form */.l0, {
    title: "\u7ED1\u5B9A\u624B\u673A",
    subtitle: "\u7ED1\u5B9A\u624B\u673A\u540E\u4F60\u5C06\u53EF\u4EE5\u5728\u4E0D\u540C\u8BBE\u5907\u4E2D\u540C\u6B65\u7528\u6237",
    close: () => dispatch({
      type: 'close'
    }),
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("form", {
      onSubmit: e => {
        e.preventDefault();
        bindPhoneFn();
      },
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "alert alert-danger w-100",
        style: {
          display: 'none'
        },
        role: "alert"
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "form-group-attached",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "form-group required",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            htmlFor: "bindphone",
            children: "\u624B\u673A\u53F7"
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "d-flex",
            children: [/*#__PURE__*/jsx_runtime_.jsx("input", {
              id: "bindphone",
              className: "form-control",
              type: "tel",
              maxLength: 20,
              value: state.phone,
              required: true,
              onChange: e => setState(s => Settings_objectSpread(Settings_objectSpread({}, s), {}, {
                phone: e.target.value
              }))
            }), /*#__PURE__*/jsx_runtime_.jsx("div", {
              children: /*#__PURE__*/jsx_runtime_.jsx(CodeBtn, {
                sendCode: sendCodeFn
              })
            })]
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "form-group required",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            htmlFor: "vierfy_code",
            children: "\u9A8C\u8BC1\u7801"
          }), /*#__PURE__*/jsx_runtime_.jsx("input", {
            id: "vierfy_code",
            className: "form-control",
            type: "text",
            value: state.code,
            required: true,
            onChange: e => setState(s => Settings_objectSpread(Settings_objectSpread({}, s), {}, {
              code: e.target.value
            }))
          })]
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        children: /*#__PURE__*/jsx_runtime_.jsx("button", {
          className: "btn btn-submit btn-block",
          type: "submit",
          children: "\u7ED1\u5B9A"
        })
      })]
    })
  });
};
/**切换帐号 */

const SwitchAccount = props => {
  var _props$login;

  const [_x, dispatch] = Modal/* ModalState.useContainer */.XK.useContainer();
  const {
    0: state,
    1: setState
  } = (0,external_react_.useState)({
    phone: '',
    code: ''
  });

  const sendCode = params => {
    return user/* sendCode */.Z3({
      type: user/* SendCodeType.findAccount */.yf.findAccount,
      mobile: params.phone
    }).then(r => r.data.result);
  };

  const switchAccount = (0,external_react_query_.useMutation)(params => {
    return user/* switchAccount */.yD(params).then(r => r.data.result);
  });
  const userQ = user_state/* UserQuery.useContainer */.fE.useContainer();

  const sendCodeFn = () => {
    return sendCode({
      phone: state.phone
    });
  };

  const isLoading = switchAccount.isLoading || userQ.isLoading;

  const switchAccountFn = () => {
    dispatch({
      type: 'loading'
    });
    Promise.resolve(1).then(() => switchAccount.mutateAsync({
      mobile: state.phone,
      code: state.code
    })).then(r => {
      (0,user_state/* setUid */.fs)(r.id);
      return userQ.refetch();
    }).then(r => {
      external_react_toastify_.toast.success('切换帐号成功');
      dispatch({
        type: 'close'
      });
    }, err => {
      external_react_toastify_.toast.error((0,utils/* isRespError */.Vn)(err) ? err.message : '网络波动请重试');
      dispatch({
        type: 'loaded'
      });
    });
  };

  return /*#__PURE__*/jsx_runtime_.jsx(Modal/* Form */.l0, {
    title: props.login ? '登陆帐号' : '切换帐号',
    subtitle: (_props$login = props.login) !== null && _props$login !== void 0 ? _props$login : '通过已绑定的手机号登陆帐号',
    close: () => {
      if (!props.login) {
        dispatch({
          type: 'close'
        }); // ga

        external_react_ga_default().event({
          category: '切换账号',
          action: '取消切换'
        });
        return;
      }

      (0,user_state/* setUid */.fs)('');
      dispatch({
        type: 'loading'
      });
      registerUser().then(() => {
        return userQ.refetch();
      }).then(() => {
        dispatch({
          type: 'close'
        });
      }).catch((0,utils/* tapError */.sn)(err => {
        external_react_toastify_.toast.error((0,utils/* isRespError */.Vn)(err) ? err.message : '网络波动请重试');
        dispatch({
          type: 'loaded'
        });
      }));
      return;
    },
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("form", {
      onSubmit: e => {
        e.preventDefault();
        switchAccountFn();
      },
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "alert alert-danger w-100",
        style: {
          display: 'none'
        },
        role: "alert"
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "form-group-attached",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "form-group required",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            htmlFor: "bindphone",
            children: "\u624B\u673A\u53F7"
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "d-flex",
            children: [/*#__PURE__*/jsx_runtime_.jsx("input", {
              id: "bindphone",
              className: "form-control",
              required: true,
              type: "tel",
              maxLength: 20,
              value: state.phone,
              disabled: isLoading,
              onChange: e => setState(s => Settings_objectSpread(Settings_objectSpread({}, s), {}, {
                phone: e.target.value
              }))
            }), /*#__PURE__*/jsx_runtime_.jsx("div", {
              children: /*#__PURE__*/jsx_runtime_.jsx(CodeBtn, {
                sendCode: sendCodeFn
              })
            })]
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "form-group required",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            htmlFor: "vierfy_code",
            children: "\u9A8C\u8BC1\u7801"
          }), /*#__PURE__*/jsx_runtime_.jsx("input", {
            id: "vierfy_code",
            className: "form-control",
            type: "text",
            required: true,
            value: state.code,
            disabled: isLoading,
            onChange: e => setState(s => Settings_objectSpread(Settings_objectSpread({}, s), {}, {
              code: e.target.value
            }))
          })]
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        children: /*#__PURE__*/jsx_runtime_.jsx("button", {
          className: "btn btn-submit btn-block",
          type: "submit",
          disabled: isLoading,
          children: "\u5207\u6362\u5E10\u53F7"
        })
      })]
    })
  });
};

/***/ })

};
;