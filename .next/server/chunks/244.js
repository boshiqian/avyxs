exports.id = 244;
exports.ids = [244];
exports.modules = {

/***/ 8229:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "z": () => (/* reexport */ SiteContent)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./components/Site/Content/SiteContent.module.css
var SiteContent_module = __webpack_require__(766);
var SiteContent_module_default = /*#__PURE__*/__webpack_require__.n(SiteContent_module);
// EXTERNAL MODULE: ./components/Site/Content/AppDownload.module.css
var AppDownload_module = __webpack_require__(6445);
var AppDownload_module_default = /*#__PURE__*/__webpack_require__.n(AppDownload_module);
// EXTERNAL MODULE: ./components/iconfont/IconClose.tsx
var IconClose = __webpack_require__(9205);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/Site/Content/AppDownload.tsx





const k = 'close-bottom-app-download';
const AppDownload = () => {
  const {
    0: show,
    1: setShow
  } = (0,external_react_.useState)(true);
  (0,external_react_.useEffect)(() => {
    setShow(sessionStorage.getItem(k) !== '0');
  }, []);
  const sC = show ? '' : 'd-none';
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: `${(AppDownload_module_default()).root} ${sC} d-lg-none`,
    children: /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "container",
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "d-flex py-2 align-items-center",
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "no-font p-1 mr-1 pointer",
          onClick: () => {
            sessionStorage.setItem(k, '0');
            setShow(false);
          },
          children: /*#__PURE__*/jsx_runtime_.jsx(IconClose/* default */.Z, {
            size: 1.5,
            color: "currentColor"
          })
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "no-font",
          children: /*#__PURE__*/jsx_runtime_.jsx("img", {
            className: "rounded",
            height: "50",
            src: "/img/ico.png",
            alt: ""
          })
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "mx-2",
          children: [/*#__PURE__*/jsx_runtime_.jsx("h5", {
            className: `mb-0 ${(AppDownload_module_default()).title}`,
            children: "\u4E0B\u8F7D\u5168\u65B0APP"
          }), /*#__PURE__*/jsx_runtime_.jsx("p", {
            className: `mb-0 ${(AppDownload_module_default())["sub-title"]}`,
            children: "\u4EAB\u53D7\u66F4\u591A\u4F18\u60E0"
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "flex-grow-1"
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "ml-2",
          children: /*#__PURE__*/jsx_runtime_.jsx("a", {
            href: "http://st1.hhhtlntx.com/hhsp93p.apk",
            download: true,
            children: /*#__PURE__*/jsx_runtime_.jsx("button", {
              className: `btn btn-submit ${(AppDownload_module_default()).btn}`,
              children: "\u4E0B\u8F7D"
            })
          })
        })]
      })
    })
  });
};
;// CONCATENATED MODULE: ./components/Site/Content/SiteContent.tsx






const SiteContent = props => {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
      id: "site-content",
      className: (SiteContent_module_default())["site-content"],
      children: props.children
    }), /*#__PURE__*/jsx_runtime_.jsx(AppDownload, {})]
  });
};
;// CONCATENATED MODULE: ./components/Site/Content/index.ts


/***/ }),

/***/ 3752:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "$": () => (/* reexport */ Footer),
  "a": () => (/* reexport */ footerRightLinks)
});

// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: ./components/Site/Modal/index.ts + 1 modules
var Modal = __webpack_require__(3117);
// EXTERNAL MODULE: ./components/Site/Modal/Form.tsx
var Form = __webpack_require__(2915);
// EXTERNAL MODULE: ./components/Site/Footer/ContactForm.module.css
var ContactForm_module = __webpack_require__(1037);
var ContactForm_module_default = /*#__PURE__*/__webpack_require__.n(ContactForm_module);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/Site/Footer/ContactForm.tsx





const ContactForm = () => {
  const [_x, dispatch] = Modal/* ModalState.useContainer */.XK.useContainer();
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: (ContactForm_module_default()).root,
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(Form/* Form */.l, {
      title: "\u8054\u7CFB\u6211\u4EEC",
      subtitle: "\u65E0\u8BBA\u60A8\u9047\u5230\u4EFB\u4F55\u95EE\u9898\uFF0C\u6216\u8005\u60F3\u6295\u653E\u5E7F\u544A\uFF0C\u7686\u6B22\u8FCE\u4F7F\u7528\u4E0B\u65B9\u8054\u7CFB\u65B9\u5F0F\u8054\u7EDC\u6211\u4EEC\u3002",
      close: () => dispatch({
        type: 'close'
      }),
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "form-group-attached",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "form-group required",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            children: "Telegram\u641C\u7D22"
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "form-control",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              href: "https://t.me/heiheishipin",
              target: "_blank",
              children: "@heiheishipin"
            })
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "form-group required",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            children: "TG\u4E0B\u8F7D"
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "form-control",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              href: "https://telegram.org/",
              target: "_blank",
              children: "https://telegram.org/"
            })
          })]
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        children: /*#__PURE__*/jsx_runtime_.jsx("a", {
          href: "https://t.me/heiheishipin",
          target: "_blank",
          children: /*#__PURE__*/jsx_runtime_.jsx("button", {
            className: "btn btn-block btn-submit",
            children: "\u70B9\u51FB\u6DFB\u52A0 @heiheishipin"
          })
        })
      })]
    })
  });
};
;// CONCATENATED MODULE: ./components/Site/Footer/Footer.tsx





const footerRightLinks = [{
  title: '18 U.S.C. 2257',
  link: '/h5/2257',
  stitle: '2257'
}, {
  title: '使用條款',
  link: '/h5/terms'
}, {
  title: '濫用報告',
  link: '/h5/dmca'
}];
const Footer = () => {
  return /*#__PURE__*/jsx_runtime_.jsx("footer", {
    id: "site-footer",
    className: "site-footer py-5",
    children: /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "container",
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "row",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "col-lg-8 order-2 order-lg-1",
          children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
            src: "/img/logo.png",
            className: "mb-4",
            height: "28"
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            children: [/*#__PURE__*/jsx_runtime_.jsx("a", {
              href: "//instagram.com",
              target: "_blank",
              children: /*#__PURE__*/jsx_runtime_.jsx("svg", {
                height: "18",
                width: "18",
                children: /*#__PURE__*/jsx_runtime_.jsx("use", {
                  "xlink-href": "#icon-ig"
                })
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("a", {
              href: "#",
              target: "_blank",
              children: /*#__PURE__*/jsx_runtime_.jsx("svg", {
                height: "18",
                width: "18",
                children: /*#__PURE__*/jsx_runtime_.jsx("use", {
                  "xlink-href": "#icon-fb"
                })
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("p", {
              className: "pt-2 m-0",
              children: "Copyright \xA9 2021 93P.com All rights reserved"
            })]
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "col-lg-4 order-1",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "row",
            children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
              className: "col-6",
              children: /*#__PURE__*/jsx_runtime_.jsx(LeftLinks, {})
            }), /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: "col-6",
              children: /*#__PURE__*/jsx_runtime_.jsx(RightLinks, {})
            })]
          })
        })]
      })
    })
  });
};
/* harmony default export */ const Footer_Footer = ((/* unused pure expression or super */ null && (Footer)));

const LeftLinks = () => {
  const [_x, dispatch] = Modal/* ModalState.useContainer */.XK.useContainer();

  const showContactForm = e => {
    e.preventDefault();
    dispatch({
      type: 'show',
      content: /*#__PURE__*/jsx_runtime_.jsx(ContactForm, {})
    });
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "widget",
    children: [/*#__PURE__*/jsx_runtime_.jsx("h5", {
      className: "widget-title",
      children: "\u95DC\u65BC"
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
      className: "list-inline vertical-list",
      children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
        children: /*#__PURE__*/jsx_runtime_.jsx("a", {
          href: "#",
          onClick: showContactForm,
          children: "\u6295\u653E\u5E7F\u544A"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("li", {
        children: /*#__PURE__*/jsx_runtime_.jsx("a", {
          href: "#",
          onClick: showContactForm,
          children: "\u8054\u7CFB\u6211\u4EEC"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("li", {
        children: /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
          href: "/h5/backurl",
          passHref: true,
          children: /*#__PURE__*/jsx_runtime_.jsx("a", {
            children: "\u5907\u7528\u7F51\u5740"
          })
        })
      })]
    })]
  });
};

const RightLinks = () => {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "widget",
    children: [/*#__PURE__*/jsx_runtime_.jsx("h5", {
      className: "widget-title",
      children: "\u653F\u7B56"
    }), /*#__PURE__*/jsx_runtime_.jsx("ul", {
      className: "list-inline vertical-list",
      children: footerRightLinks.map(t => {
        return /*#__PURE__*/jsx_runtime_.jsx("li", {
          children: /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: t.link,
            passHref: true,
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              children: t.title
            })
          })
        }, t.title);
      })
    })]
  });
};
;// CONCATENATED MODULE: ./components/Site/Footer/index.ts


/***/ }),

/***/ 6445:
/***/ ((module) => {

// Exports
module.exports = {
	"root": "AppDownload_root__1Xna2",
	"title": "AppDownload_title__1VKnN",
	"sub-title": "AppDownload_sub-title__2dvZ2",
	"btn": "AppDownload_btn__1fKsY"
};


/***/ }),

/***/ 766:
/***/ ((module) => {

// Exports
module.exports = {
	"site-content": "SiteContent_site-content__3pt8O"
};


/***/ }),

/***/ 1037:
/***/ ((module) => {

// Exports
module.exports = {
	"root": "ContactForm_root__3gj9H"
};


/***/ })

};
;