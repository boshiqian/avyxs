exports.id = 284;
exports.ids = [284];
exports.modules = {

/***/ 9284:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "K9": () => (/* reexport */ StoreProvider),
  "BR": () => (/* reexport */ Store_state/* StoreState */.J0)
});

// UNUSED EXPORTS: Pay

// EXTERNAL MODULE: ./components/Site/Store/Store.state.ts
var Store_state = __webpack_require__(5332);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./components/Site/Modal/index.ts + 1 modules
var Modal = __webpack_require__(3117);
// EXTERNAL MODULE: ./components/Site/Store/Store.module.css
var Store_module = __webpack_require__(9077);
var Store_module_default = /*#__PURE__*/__webpack_require__.n(Store_module);
// EXTERNAL MODULE: external "react-query"
var external_react_query_ = __webpack_require__(2585);
// EXTERNAL MODULE: ./pages/api.ts + 1 modules
var api = __webpack_require__(3919);
// EXTERNAL MODULE: ./mod/api/store.ts
var store = __webpack_require__(6482);
// EXTERNAL MODULE: ./components/Site/Store/Buy.module.css
var Buy_module = __webpack_require__(3854);
var Buy_module_default = /*#__PURE__*/__webpack_require__.n(Buy_module);
// EXTERNAL MODULE: ./mod/ec.ts
var ec = __webpack_require__(4280);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/Site/Store/Buy.tsx










const Buy = () => {
  const {
    state,
    dispatch
  } = Store_state/* StoreState.useContainer */.J0.useContainer();
  const {
    isLoading,
    data
  } = (0,external_react_query_.useQuery)('payways', () => {
    return store/* payChannels */.RX().then(r => r.data.result);
  });
  (0,external_react_.useEffect)(() => {
    ec.ec.addProduct({
      id: state.params.gid,
      name: state.params.gtitle,
      price: state.params.gprice
    });
    ec.ec.detail({
      step: 1
    });
  }, []);
  (0,Store_state/* useStoreLoading */.wG)(isLoading);

  if (isLoading) {
    return null;
  } // ReactGA.event({})


  return /*#__PURE__*/jsx_runtime_.jsx(Modal/* Form */.l0, {
    title: "\u8D2D\u4E70",
    subtitle: "",
    close: () => dispatch({
      type: 'close'
    }),
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("form", {
      onSubmit: e => e.preventDefault(),
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "form-group-attached",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "form-group required",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            htmlFor: "p-name",
            children: "\u4EA7\u54C1"
          }), /*#__PURE__*/jsx_runtime_.jsx("input", {
            readOnly: true,
            id: "p-name",
            className: "form-control",
            value: state.params.gtitle
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "form-group required",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            htmlFor: "p-price",
            children: "\u4EF7\u683C"
          }), /*#__PURE__*/jsx_runtime_.jsx("input", {
            readOnly: true,
            id: "p-price",
            className: "form-control",
            value: state.params.gprice
          })]
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (Buy_module_default())["buy-btns"],
        children: data.map(item => {
          return /*#__PURE__*/jsx_runtime_.jsx("div", {
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("button", {
              className: "btn btn-block",
              onClick: () => {
                dispatch({
                  type: 'order',
                  dispatch: dispatch,
                  params: {
                    cid: item.id
                  }
                }); // ga

                ec.ec.checkout_option({
                  option: item.title,
                  step: 2
                });
              },
              children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
                width: "20",
                className: "mr-2",
                src: (0,api/* ImageBase */.v)(item.imgUrl)
              }), /*#__PURE__*/jsx_runtime_.jsx("span", {
                children: item.title
              })]
            })
          }, item.id);
        })
      })]
    })
  });
};
// EXTERNAL MODULE: ./pages/user.state.tsx
var user_state = __webpack_require__(2298);
// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__(2034);
;// CONCATENATED MODULE: ./components/Site/Store/Order.tsx










const Order = () => {
  const {
    state,
    dispatch
  } = Store_state/* StoreState.useContainer */.J0.useContainer();
  const orderid = state.params.orderid;
  const {
    refetch: refetchUser
  } = user_state/* UserQuery.useContainer */.fE.useContainer();
  const {
    isLoading,
    data,
    refetch,
    isFetching,
    isFetched
  } = (0,external_react_query_.useQuery)(['order-check', orderid], ({
    queryKey: [_n, orderid]
  }) => {
    return store/* checkOrder */.Gd({
      orderNum: orderid
    }).then(r => r.data.result).then(async paid => {
      if (paid) {
        await refetchUser();
      }

      return paid;
    });
  });
  (0,external_react_.useEffect)(() => {
    if (!isFetched) {
      return;
    }

    let goNext = true;

    const f = () => {
      Promise.resolve(1).then(() => refetch()).then(r => {
        if (r.data) {
          goNext = false;
          external_react_toastify_.toast.success('支付成功');
          return Promise.reject(0);
        }
      }).then(() => new Promise(rl => setTimeout(rl, 2e3))).then(() => {
        if (goNext) {
          f();
        }
      });
    };

    f();
    return () => {
      goNext = false;
    };
  }, [isFetched]); // ga

  (0,external_react_.useEffect)(() => {
    ec.ec.checkout({
      id: orderid,
      step: 3
    });
  }, []);
  (0,external_react_.useEffect)(() => {
    if (!data) {
      return;
    }

    ec.ec.purchase({
      id: orderid,
      step: 4
    });
  }, [!!data]);
  (0,Store_state/* useStoreLoading */.wG)(isLoading);

  const pay = () => {
    store/* pay */.c6({
      orderNum: orderid
    });
  };

  let orderStatus = '加载中';

  if (typeof data !== 'undefined') {
    orderStatus = data ? '支付完成' : '待支付';
  }

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Modal/* Form */.l0, {
    title: "\u8BA2\u5355\u72B6\u6001",
    subtitle: "",
    close: () => dispatch({
      type: 'close'
    }),
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "form-group-attached",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "form-group required",
        children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
          htmlFor: "p-name",
          children: "\u4EA7\u54C1"
        }), /*#__PURE__*/jsx_runtime_.jsx("input", {
          readOnly: true,
          id: "p-name",
          className: "form-control",
          value: state.params.gtitle
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "form-group required",
        children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
          htmlFor: "p-price",
          children: "\u4EF7\u683C"
        }), /*#__PURE__*/jsx_runtime_.jsx("input", {
          readOnly: true,
          id: "p-price",
          className: "form-control",
          value: state.params.gprice
        })]
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "form-group-attached",
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "form-group required",
        children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
          htmlFor: "p-price",
          children: "\u8BA2\u5355\u72B6\u6001"
        }), /*#__PURE__*/jsx_runtime_.jsx("input", {
          readOnly: true,
          id: "p-price",
          className: "form-control",
          value: orderStatus
        })]
      })
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      children: data ? /*#__PURE__*/jsx_runtime_.jsx("button", {
        className: "btn btn-submit btn-block",
        onClick: () => dispatch({
          type: 'close'
        }),
        children: "\u5173\u95ED"
      }) : /*#__PURE__*/jsx_runtime_.jsx("a", {
        href: store/* pay */.c6({
          orderNum: orderid
        }),
        target: "_blank",
        children: /*#__PURE__*/jsx_runtime_.jsx("button", {
          className: "btn btn-submit btn-block",
          children: "\u53BB\u652F\u4ED8"
        })
      })
    })]
  });
};
;// CONCATENATED MODULE: ./components/Site/Store/Store.tsx








const Store = () => {
  const {
    state
  } = Store_state/* StoreState.useContainer */.J0.useContainer();
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Modal/* Modal */.u_, {
    className: (Store_module_default()).root,
    show: state.show,
    loading: state.loading,
    children: [state.step === Store_state/* OrderStep.Buy */.Rd.Buy && /*#__PURE__*/jsx_runtime_.jsx(Buy, {}), state.step === Store_state/* OrderStep.Order */.Rd.Order && /*#__PURE__*/jsx_runtime_.jsx(Order, {}), state.step === Store_state/* OrderStep.Goods */.Rd.Goods && state.goods]
  });
};
const StoreProvider = props => {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Store_state/* StoreState.Provider */.J0.Provider, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(Store, {}), props.children]
  });
};
;// CONCATENATED MODULE: ./components/Site/Store/index.ts



/***/ }),

/***/ 3854:
/***/ ((module) => {

// Exports
module.exports = {
	"buy-btns": "Buy_buy-btns__-wapt"
};


/***/ }),

/***/ 9077:
/***/ ((module) => {

// Exports
module.exports = {
	"root": "Store_root__3xeGu"
};


/***/ })

};
;