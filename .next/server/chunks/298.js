"use strict";
exports.id = 298;
exports.ids = [298];
exports.modules = {

/***/ 7134:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "yf": () => (/* reexport */ SendCodeType),
  "eF": () => (/* binding */ user_bindPhone),
  "x4": () => (/* binding */ user_login),
  "z2": () => (/* binding */ user_register),
  "Z3": () => (/* binding */ user_sendCode),
  "yD": () => (/* binding */ user_switchAccount),
  "rn": () => (/* binding */ user_updateInfo)
});

// UNUSED EXPORTS: uploadAvatar

// EXTERNAL MODULE: ./mod/utils/index.ts + 1 modules
var utils = __webpack_require__(3863);
// EXTERNAL MODULE: ./mod/api/api.ts + 1 modules
var api = __webpack_require__(2529);
;// CONCATENATED MODULE: ./mod/api/user.ts

const register = params => {
  params.devType = 3;
  params.regType = 'h5';
  let link = new URL(window.location.search, 'http://noop');
  let tgCode = link.searchParams.get('tgCode');

  if (tgCode) {
    params.tgCode = tgCode;
  }

  return api/* client.post */.L.post('/user/regUser', params);
};
const login = params => {
  params.isUpdate = true;
  return api/* client.post */.L.post('/user/getUserInfo', params);
};
const updateInfo = params => {
  return api/* client.post */.L.post('/user/updateUserInfo', params);
};
const uploadAvatar = params => {
  let token = params.token;
  delete params.token;
  return api/* client.post */.L.post('/upload/uploadUserHead', params, {
    headers: {
      token
    }
  });
};
let SendCodeType;

(function (SendCodeType) {
  SendCodeType[SendCodeType["bindPhone"] = 1] = "bindPhone";
  SendCodeType[SendCodeType["findAccount"] = 2] = "findAccount";
})(SendCodeType || (SendCodeType = {}));

const sendCode = params => {
  return api/* client.post */.L.post('/user/sendCode', params);
};
const bindPhone = params => {
  return api/* client.post */.L.post('/user/bindPhone', params);
};
const switchAccount = params => {
  return api/* client.post */.L.post('/user/findUserByPhone', params);
};
;// CONCATENATED MODULE: ./mod/api/withGa/user.ts



const user_register = (0,utils/* withGAEvent */.v)(register)({
  category: '用户自动注册',
  action: '注册'
});
const user_login = (0,utils/* withGAEvent */.v)(login)({
  category: '用户登录',
  action: 'token登录'
});
const user_updateInfo = (0,utils/* withGAEvent */.v)(updateInfo)({
  category: '修改资料',
  action: '修改'
});
const user_uploadAvatar = (0,utils/* withGAEvent */.v)(uploadAvatar)({
  category: '上传头像',
  action: '上传'
});
const user_sendCode = (0,utils/* withGAEvent */.v)(sendCode)({
  category: '验证码',
  label: '绑定手机号',
  action: '发送'
});
const user_bindPhone = (0,utils/* withGAEvent */.v)(bindPhone)({
  category: '用户资料',
  action: '绑定手机号'
});
const user_switchAccount = (0,utils/* withGAEvent */.v)(switchAccount)({
  category: '用户登录',
  action: '切换账号'
});

/***/ }),

/***/ 3863:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "IU": () => (/* reexport */ beforeNow),
  "Vn": () => (/* binding */ isRespError),
  "sn": () => (/* binding */ tapError),
  "Qh": () => (/* binding */ tapOk),
  "v": () => (/* binding */ withGAEvent)
});

// UNUSED EXPORTS: tapRespErr

// EXTERNAL MODULE: external "react-ga"
var external_react_ga_ = __webpack_require__(9831);
var external_react_ga_default = /*#__PURE__*/__webpack_require__.n(external_react_ga_);
// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__(2470);
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);
;// CONCATENATED MODULE: ./mod/utils/time.ts

const beforeNow = (t, n = external_moment_default()()) => {
  let r;
  r = n.diff(t, 'years');

  if (r > 0) {
    return r + '年前';
  }

  r = n.diff(t, 'months');

  if (r > 0) {
    return r + '月前';
  }

  r = n.diff(t, 'weeks');

  if (r > 0) {
    return r + '星期前';
  }

  r = n.diff(t, 'days');

  if (r > 0) {
    return r + '天前';
  }

  r = n.diff(t, 'hours');

  if (r > 0) {
    return r + '小时前';
  }

  r = Math.max(1, n.diff(t, 'minutes'));

  if (r > 0) {
    return r + '分钟前';
  }

  return r;
};
;// CONCATENATED MODULE: ./mod/utils/index.ts
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



const isRespError = err => {
  if (err.success == false) {
    return true;
  }

  return false;
};
const tapError = fn => err => {
  fn(err);
  return Promise.reject(err);
};
const tapRespErr = fn => err => {
  let l = isRespError(err) ? err.message : '未知错误';
  fn(l);
  return Promise.reject(err);
};
const tapOk = fn => r => {
  fn(r);
  return r;
};
const withGAEvent = f => ec => {
  // @ts-ignore
  return (...r) => {
    external_react_ga_default().event(ec); // @ts-ignore

    return f(...r).then(tapOk(() => {
      external_react_ga_default().event(_objectSpread(_objectSpread({}, ec), {}, {
        action: ec.action + '成功'
      }));
    }), tapRespErr(l => {
      if (ec.label) {
        l = ec.label + '-' + l;
      }

      external_react_ga_default().event(_objectSpread(_objectSpread({}, ec), {}, {
        action: ec.action + '失败',
        label: l
      }));
    }));
  };
};

/***/ }),

/***/ 2298:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "a$": () => (/* binding */ registerUser),
/* harmony export */   "sq": () => (/* binding */ getUid),
/* harmony export */   "fs": () => (/* binding */ setUid),
/* harmony export */   "Wv": () => (/* binding */ placeholderUser),
/* harmony export */   "fE": () => (/* binding */ UserQuery),
/* harmony export */   "dr": () => (/* binding */ UserProvider),
/* harmony export */   "wT": () => (/* binding */ UserPrice)
/* harmony export */ });
/* harmony import */ var _mod_api_withGa_user__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7134);
/* harmony import */ var unstated_next__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9892);
/* harmony import */ var unstated_next__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(unstated_next__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2585);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_query__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__);




const UserIDStorageKey = 'user-id';
const hourKey = (/* unused pure expression or super */ null && ([0, 1, 2, 3, 4, 5, 6, 7, 8]));

const getHourKey = () => {
  const h = new Date().getHours();
  return hourKey[Math.floor(h / 3)];
};

async function registerUser() {
  let userId = localStorage.getItem(UserIDStorageKey);

  if (!userId) {
    // let d = await import('@fingerprintjs/fingerprintjs')
    // let fp = await d.load()
    // let r = await fp.get()
    // let deviceId = r.visitorId + getHourKey()
    userId = await (0,_mod_api_withGa_user__WEBPACK_IMPORTED_MODULE_0__/* .register */ .z2)({}).then(r => r.data.result).then(r => r.id);
    localStorage.setItem(UserIDStorageKey, userId);
  }

  return userId;
}
const getUid = () => {
  return localStorage.getItem(UserIDStorageKey);
};
const setUid = uid => {
  if (uid === '') {
    return localStorage.removeItem(UserIDStorageKey);
  }

  return localStorage.setItem(UserIDStorageKey, uid);
};



const placeholderUser = {
  account: 'guest',
  balance: 0,
  devCode: '',
  devType: 3,
  headUrl: 'user/def_head.png',
  id: '',
  isVip: false,
  nickName: '游客',
  sex: 0
};

const useUserQuery = () => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_3__.useQuery)(['user-info'], () => {
    return _mod_api_withGa_user__WEBPACK_IMPORTED_MODULE_0__/* .login */ .x4({
      userId: getUid()
    }).then(r => r.data.result).then(u => {
      if (!u) {
        return Promise.reject('login fail');
      }

      return u;
    });
  }, {
    placeholderData: placeholderUser,
    retry: false
  });
};

const UserQuery = (0,unstated_next__WEBPACK_IMPORTED_MODULE_1__.createContainer)(useUserQuery);
const UserProvider = props => {
  var _q$data;

  const q = UserQuery.useContainer();

  if (!(q !== null && q !== void 0 && (_q$data = q.data) !== null && _q$data !== void 0 && _q$data.id)) {
    return null;
  }

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment, {
    children: props.children
  });
};
const UserPrice = user => price => {
  let discount = user.discount / 10;
  price = price * discount;
  price = Math.ceil(price);
  return price;
};

/***/ })

};
;