"use strict";
exports.id = 332;
exports.ids = [332];
exports.modules = {

/***/ 5332:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Rd": () => (/* binding */ OrderStep),
/* harmony export */   "J0": () => (/* binding */ StoreState),
/* harmony export */   "wG": () => (/* binding */ useStoreLoading)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var unstated_next__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9892);
/* harmony import */ var unstated_next__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(unstated_next__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _pages_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3919);
/* harmony import */ var _mod_api_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6482);
/* harmony import */ var _mod_utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3863);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2034);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _pages_user_state__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(2298);
/* harmony import */ var _mod_ec__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(4280);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









let OrderStep;

(function (OrderStep) {
  OrderStep[OrderStep["Hidden"] = 0] = "Hidden";
  OrderStep[OrderStep["Goods"] = 1] = "Goods";
  OrderStep[OrderStep["Buy"] = 2] = "Buy";
  OrderStep[OrderStep["Order"] = 3] = "Order";
  OrderStep[OrderStep["Pay"] = 4] = "Pay";
})(OrderStep || (OrderStep = {}));

const defaultState = {
  show: false,
  loading: false,
  // @ts-ignore
  step: null,
  params: {}
};

function StateReducer(s, a) {
  let ns = _objectSpread({}, s);

  switch (a.type) {
    case 'loading':
      ns.loading = true;
      break;

    case 'loaded':
      ns.loading = false;
      break;

    case 'close':
      ns.show = false;
      ns.step = OrderStep.Hidden;
      _mod_ec__WEBPACK_IMPORTED_MODULE_7__.ec.clear();
      break;

    case 'goods':
      ns.show = true;
      ns.step = OrderStep.Goods;
      ns.goods = a.goods;
      break;

    case 'buy':
      ns.loading = true;
      ns.show = true;
      ns.step = OrderStep.Buy;
      ns.params = _objectSpread(_objectSpread({}, ns.params), a.params);
      break;

    case 'order':
      ns.show = true;
      ns.loading = true;
      Promise.resolve().then(() => {
        return _mod_api_store__WEBPACK_IMPORTED_MODULE_3__/* .buy */ .b7({
          userId: (0,_pages_user_state__WEBPACK_IMPORTED_MODULE_6__/* .getUid */ .sq)(),
          channelId: a.params.cid,
          itemId: ns.params.gid,
          type: ns.params.gtype
        });
      }).then(r => r.data.result).then(orderNum => {
        a.dispatch({
          type: 'order-pay',
          params: {
            orderid: orderNum
          }
        });
      }, err => {
        react_toastify__WEBPACK_IMPORTED_MODULE_5__.toast.error((0,_mod_utils__WEBPACK_IMPORTED_MODULE_4__/* .isRespError */ .Vn)(err) ? err.message : '网络波动请重试');
        a.dispatch({
          type: 'loaded'
        });
      });
      break;

    case 'order-pay':
      ns.show = true;
      ns.loading = false;
      ns.step = OrderStep.Order;
      ns.params = _objectSpread(_objectSpread({}, ns.params), a.params);
      break;
  }

  return ns;
}

const useStoreState = () => {
  const api = _pages_api__WEBPACK_IMPORTED_MODULE_2__/* .APIClient.useContainer */ .lP.useContainer();
  const {
    0: state,
    1: dispatch
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useReducer)(StateReducer, defaultState);
  return {
    state,
    dispatch
  };
};

const StoreState = (0,unstated_next__WEBPACK_IMPORTED_MODULE_1__.createContainer)(useStoreState);
const useStoreLoading = loading => {
  const {
    dispatch
  } = StoreState.useContainer();
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    dispatch({
      type: loading ? 'loading' : 'loaded'
    });
  }, [loading]);
};

/***/ }),

/***/ 6482:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "xn": () => (/* binding */ GoodType),
/* harmony export */   "kT": () => (/* binding */ vipGoodList),
/* harmony export */   "Wb": () => (/* binding */ coinGoodList),
/* harmony export */   "RX": () => (/* binding */ payChannels),
/* harmony export */   "b7": () => (/* binding */ buy),
/* harmony export */   "c6": () => (/* binding */ pay),
/* harmony export */   "Gd": () => (/* binding */ checkOrder),
/* harmony export */   "jB": () => (/* binding */ buyVideo)
/* harmony export */ });
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2529);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


let GoodType;

(function (GoodType) {
  GoodType[GoodType["VIP"] = 1] = "VIP";
  GoodType[GoodType["Coin"] = 2] = "Coin";
})(GoodType || (GoodType = {}));

/**VIP商品列表 */
const vipGoodList = (params = {}) => {
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/getVipItem', params).then(r => {
    let nr = r.data.result.map(i => {
      let params = JSON.parse(i.params);
      return _objectSpread(_objectSpread({}, i), {}, {
        params
      });
    });
    r.data.result = nr;
    return r;
  });
};

/**金币商品列表 */
const coinGoodList = (params = {}) => {
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/getCoinItem', params);
};
/**支付通道 */

/**支付通道列表 */
const payChannels = (params = {}) => {
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/getChannelList', params);
};

/**商品支付下单 */
const buy = params => {
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/pay/toPay', params);
};

/**支付订单 */
const pay = params => {
  return `${_api__WEBPACK_IMPORTED_MODULE_0__/* .BaseAPI */ .T}/pay/page?token=${params.orderNum}`;
};

/**检查订单是否完成 */
const checkOrder = params => {
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post(`/pay/checkOrderStatus`, params);
};

/**视频购买 */
const buyVideo = params => {
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post(`/user/coinPay`, params);
};

/***/ }),

/***/ 4280:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Ec": () => (/* binding */ Ec),
/* harmony export */   "ec": () => (/* binding */ ec)
/* harmony export */ });
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9831);
/* harmony import */ var react_ga__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_ga__WEBPACK_IMPORTED_MODULE_0__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


/**
 * 展示数据
 * > 表示被查看的产品的相关信息
 */

class Ec {
  constructor() {
    _defineProperty(this, "addImpression", items => {
      react_ga__WEBPACK_IMPORTED_MODULE_0___default().plugin.execute(Ec.PluginName, 'addImpression', items);
    });

    _defineProperty(this, "addProduct", items => {
      react_ga__WEBPACK_IMPORTED_MODULE_0___default().plugin.execute(Ec.PluginName, 'addProduct', items);
    });

    _defineProperty(this, "clear", () => {
      react_ga__WEBPACK_IMPORTED_MODULE_0___default().plugin.execute(Ec.PluginName, 'clear', {});
    });

    _defineProperty(this, "_action", (action, params) => {
      react_ga__WEBPACK_IMPORTED_MODULE_0___default().plugin.execute(Ec.PluginName, 'setAction', action, params);
    });

    _defineProperty(this, "click", (action = {}) => {
      this._action('click', action);
    });

    _defineProperty(this, "detail", (action = {}) => {
      this._action('detail', action);
    });

    _defineProperty(this, "add", (action = {}) => {
      this._action('add', action);
    });

    _defineProperty(this, "remove", (action = {}) => {
      this._action('remove', action);
    });

    _defineProperty(this, "checkout", action => {
      this._action('checkout', action);
    });

    _defineProperty(this, "checkout_option", action => {
      this._action('checkout_option', action);
    });

    _defineProperty(this, "purchase", action => {
      this._action('click', action);
    });

    _defineProperty(this, "refund", (action = {}) => {
      this._action('click', action);
    });

    _defineProperty(this, "promo_click", (action = {}) => {
      this._action('click', action);
    });
  }

}

_defineProperty(Ec, "PluginName", 'ec');

_defineProperty(Ec, "init", () => {
  react_ga__WEBPACK_IMPORTED_MODULE_0___default().plugin.require('ec');
});

const ec = new Ec();

/***/ })

};
;