exports.id = 384;
exports.ids = [384];
exports.modules = {

/***/ 5501:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "b1": () => (/* binding */ getStaticProps),
/* harmony export */   "JX": () => (/* binding */ ASideWrapper)
/* harmony export */ });
/* unused harmony export ASide */
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1664);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6731);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_Site_Content__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8229);
/* harmony import */ var _components_Site_Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3752);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__);







const ASide = ({
  links
}) => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx("aside", {
    className: "col-lg-2 left-aside menu d-none d-lg-block",
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx("nav", {
      className: "navbar navbar-expand pl-3 pl-xl-5",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx("div", {
        className: "collapse navbar-collapse",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx("ul", {
          className: "navbar-nav flex-column",
          children: links.map(({
            title,
            link,
            stitle
          }) => {
            if (link.startsWith('http')) {
              return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx("li", {
                className: "nav-item",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsxs)("a", {
                  className: "nav-link",
                  target: "_blank",
                  href: link,
                  children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx("span", {
                    className: "separator",
                    children: "\u2022"
                  }), stitle !== null && stitle !== void 0 ? stitle : title]
                })
              }, title);
            }

            let aC = router.pathname === link ? 'active-color' : '';
            return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx("li", {
              className: "nav-item",
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx(next_link__WEBPACK_IMPORTED_MODULE_1__.default, {
                href: link,
                passHref: true,
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsxs)("a", {
                  className: `nav-link ${aC}`,
                  children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx("span", {
                    className: "separator",
                    children: "\u2022"
                  }), stitle !== null && stitle !== void 0 ? stitle : title]
                })
              })
            }, title);
          })
        })
      })
    })
  });
};
const getStaticProps = () => ({
  props: {
    noWrap: true
  }
});
const ASideWrapper = props => {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx(_components_Site_Content__WEBPACK_IMPORTED_MODULE_3__/* .SiteContent */ .z, {
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx("div", {
      className: "container",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsxs)("div", {
        className: "row with-left-aside",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx(ASide, {
          links: props.links
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsxs)("div", {
          className: "col-12 col-lg-10",
          children: [props.children, /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx(_components_Site_Footer__WEBPACK_IMPORTED_MODULE_4__/* .Footer */ .$, {})]
        })]
      })
    })
  });
};

/***/ }),

/***/ 1664:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(2167)


/***/ }),

/***/ 2431:
/***/ (() => {

/* (ignored) */

/***/ })

};
;