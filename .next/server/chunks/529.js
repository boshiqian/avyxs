"use strict";
exports.id = 529;
exports.ids = [529];
exports.modules = {

/***/ 2529:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "T": () => (/* binding */ BaseAPI),
  "L": () => (/* binding */ client)
});

// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2376);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "crypto-js"
var external_crypto_js_ = __webpack_require__(419);
var external_crypto_js_default = /*#__PURE__*/__webpack_require__.n(external_crypto_js_);
;// CONCATENATED MODULE: ./mod/api/crypto.ts

const KEY = external_crypto_js_default().enc.Utf8.parse('56+0x4d8=8c56fd5');
const IV = external_crypto_js_default().enc.Utf8.parse('');
function Encrypt(word, key = KEY, iv = IV) {
  let srcs = external_crypto_js_default().enc.Utf8.parse(word);
  var encrypted = external_crypto_js_default().AES.encrypt(srcs, key, {
    iv: iv,
    mode: (external_crypto_js_default()).mode.ECB,
    padding: (external_crypto_js_default()).pad.Pkcs7
  }); // console.log("-=-=-=-", encrypted.ciphertext)

  return external_crypto_js_default().enc.Base64.stringify(encrypted.ciphertext);
}
function Decrypt(word, key = KEY, iv = IV) {
  let base64 = external_crypto_js_default().enc.Base64.parse(word);
  let src = external_crypto_js_default().enc.Base64.stringify(base64);
  var decrypt = external_crypto_js_default().AES.decrypt(src, key, {
    iv: iv,
    mode: (external_crypto_js_default()).mode.ECB,
    padding: (external_crypto_js_default()).pad.Pkcs7
  });
  var decryptedStr = decrypt.toString((external_crypto_js_default()).enc.Utf8);
  return decryptedStr.toString();
}
;// CONCATENATED MODULE: ./mod/api/api.ts


const BaseAPI =  false ? 0 : '//vapi.pipe-welding.cn/api';
const client = external_axios_default().create({
  baseURL: BaseAPI
});
client.interceptors.request.use(function (req) {
  var _req$data;

  let d = (_req$data = req.data) !== null && _req$data !== void 0 ? _req$data : {};
  let w = JSON.stringify(d);
  w = Encrypt(w);
  req.data = {
    data: w
  };
  return req;
});
client.interceptors.response.use(function (res) {
  if (!res.data.success) {
    return Promise.reject(res.data);
  }

  return res;
});
client.interceptors.response.use(function (res) {
  let d = res.data;

  if (d.result === null) {
    return res;
  }

  if (typeof d.result !== 'string') {
    return Promise.reject(d);
  }

  let result = Decrypt(d.result);
  res.data.result = JSON.parse(result);
  return res;
});

/***/ })

};
;