exports.id = 594;
exports.ids = [594];
exports.modules = {

/***/ 2594:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ StoreCoinPage),
/* harmony export */   "CoinGoods": () => (/* binding */ CoinGoods)
/* harmony export */ });
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7692);
/* harmony import */ var _mod_api_store__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6482);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2585);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_query__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_Site_Store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9284);
/* harmony import */ var _coin_module_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(4670);
/* harmony import */ var _coin_module_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_coin_module_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _mod_ec__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4280);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6731);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__);











function StoreCoinPage() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsxs)("div", {
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx(_Header__WEBPACK_IMPORTED_MODULE_0__/* .Header */ .h, {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx("div", {
      className: "container",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx("div", {
        id: "list_videos_my_favourite_videos",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx("section", {
          className: "pb-3 pb-e-lg-40",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx("div", {
            className: "row gutter-20",
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx(CoinGoods, {})
          })
        })
      })
    })]
  });
}
const CoinGoods = props => {
  const {
    isLoading,
    data
  } = (0,react_query__WEBPACK_IMPORTED_MODULE_2__.useQuery)(['store-coin'], () => {
    return _mod_api_store__WEBPACK_IMPORTED_MODULE_1__/* .coinGoodList */ .Wb().then(r => r.data.result);
  });
  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(() => {
    if (props.dispatch) {
      props.dispatch({
        type: isLoading ? 'loading' : 'loaded'
      });
    }
  }, [isLoading]);
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_6__.useRouter)();
  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(() => {
    if (!data) {
      return;
    }

    let i = 0;

    for (let t of data) {
      _mod_ec__WEBPACK_IMPORTED_MODULE_5__.ec.addImpression({
        id: t.id,
        name: `${t.coins} 金币`,
        position: i++,
        list: router.pathname.startsWith('/video') ? '视频页弹窗购买列表' : '金币页购买列表'
      });
    }
  }, [isLoading]);

  if (isLoading) {
    return null;
  }

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.Fragment, {
    children: data.map((item, i) => {
      var _props$className;

      return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx("div", {
        className: (_props$className = props === null || props === void 0 ? void 0 : props.className) !== null && _props$className !== void 0 ? _props$className : 'col-6 col-sm-4 col-lg-2',
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx(Item, {
          good: item,
          onBuy: () => {
            _mod_ec__WEBPACK_IMPORTED_MODULE_5__.ec.addProduct({
              id: item.id,
              name: `${item.coins} 金币`,
              position: i
            });
            _mod_ec__WEBPACK_IMPORTED_MODULE_5__.ec.click();
          }
        })
      }, item.id);
    })
  });
};

const Item = ({
  good: item,
  onBuy
}) => {
  const {
    dispatch
  } = _components_Site_Store__WEBPACK_IMPORTED_MODULE_4__/* .PayState.useContainer */ .BR.useContainer();

  const buy = () => {
    if (onBuy) {
      onBuy();
    }

    dispatch({
      type: 'buy',
      params: {
        gid: item.id,
        gtype: _mod_api_store__WEBPACK_IMPORTED_MODULE_1__/* .GoodType.Coin */ .xn.Coin,
        gtitle: `${item.coins} 金币`,
        gprice: item.price
      }
    });
  };

  let bgColor = 50 + Math.ceil(Number(item.price) / 1000 * 280);
  bgColor = Math.ceil(bgColor);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsxs)("div", {
    className: `rounded p-2 mb-3 ${(_coin_module_css__WEBPACK_IMPORTED_MODULE_8___default().item)}`,
    style: {
      backgroundColor: `hsl(${bgColor}, 38%, 35%)`
    },
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsxs)("div", {
      className: "p-2",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsxs)("h4", {
        className: "mb-1",
        children: ["\u91D1\u5E01: ", /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx("span", {
          className: "float-right",
          children: item.coins
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsxs)("h4", {
        className: "mb-1",
        children: ["\u4EF7\u683C: ", /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx("span", {
          className: "float-right",
          children: item.price
        })]
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx("div", {
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx("button", {
        className: "btn btn-block mb-0",
        onClick: buy,
        children: "\u8D2D\u4E70"
      })
    })]
  });
};

/***/ }),

/***/ 4670:
/***/ ((module) => {

// Exports
module.exports = {
	"item": "coin_item__BDiJ0",
	"scolor": "coin_scolor__2FABr"
};


/***/ })

};
;