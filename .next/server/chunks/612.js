"use strict";
exports.id = 612;
exports.ids = [612];
exports.modules = {

/***/ 8612:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "yC": () => (/* binding */ search),
/* harmony export */   "pk": () => (/* binding */ listCategory),
/* harmony export */   "Se": () => (/* binding */ listByCategoryID),
/* harmony export */   "Hv": () => (/* binding */ listByColID),
/* harmony export */   "um": () => (/* binding */ info),
/* harmony export */   "zM": () => (/* binding */ randRecommend),
/* harmony export */   "vo": () => (/* binding */ zan)
/* harmony export */ });
/* unused harmony export listTags */
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2529);

const search = params => {
  var _params$limit, _params$page;

  params.limit = (_params$limit = params.limit) !== null && _params$limit !== void 0 ? _params$limit : 24;
  params.page = (_params$page = params.page) !== null && _params$page !== void 0 ? _params$page : 1;
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/searchVideo', params);
};
const listTags = client => (params = {}) => {
  return client.post('/getTagList', params);
};
const listCategory = (params = {}) => {
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/getVideoCategory', params);
};
const listByCategoryID = params => {
  var _params$limit2, _params$page2;

  params.limit = (_params$limit2 = params.limit) !== null && _params$limit2 !== void 0 ? _params$limit2 : 24;
  params.page = (_params$page2 = params.page) !== null && _params$page2 !== void 0 ? _params$page2 : 1;
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/h5/getListByCategory', params);
};
const listByColID = params => {
  var _params$limit3, _params$page3;

  params.limit = (_params$limit3 = params.limit) !== null && _params$limit3 !== void 0 ? _params$limit3 : 24;
  params.page = (_params$page3 = params.page) !== null && _params$page3 !== void 0 ? _params$page3 : 1;
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/listByIndexCol', params);
};
const info = params => {
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/getVideoInfo', params);
};
const randRecommend = params => {
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/h5/listVideoByRand', params);
};
const zan = params => {
  return _api__WEBPACK_IMPORTED_MODULE_0__/* .client.post */ .L.post('/updateVideoZanCount', params);
};

/***/ })

};
;