"use strict";
exports.id = 668;
exports.ids = [668];
exports.modules = {

/***/ 5657:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "h": () => (/* binding */ Header)
/* harmony export */ });
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1664);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6731);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _mod_api_types__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(1080);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__);







function makeHref(router, query) {
  let q2 = {};

  for (let k in router.query) {
    if (k === 'page') {
      continue;
    }

    q2[k] = router.query[k];
  }

  Object.assign(q2, query);
  return {
    query: q2
  };
}

const Header = ({
  tag,
  noSort
}) => {
  var _router$query$sort, _router$query;

  const sorts = (0,react__WEBPACK_IMPORTED_MODULE_2__.useMemo)(() => {
    return [{
      name: '默认排序',
      key: _mod_api_types__WEBPACK_IMPORTED_MODULE_3__/* .SortType.Default */ .ER.Default
    }, {
      name: '近期最佳',
      key: _mod_api_types__WEBPACK_IMPORTED_MODULE_3__/* .SortType.Top */ .ER.Top
    }, {
      name: '最近更新',
      key: _mod_api_types__WEBPACK_IMPORTED_MODULE_3__/* .SortType.Newest */ .ER.Newest
    }, {
      name: '推荐',
      key: _mod_api_types__WEBPACK_IMPORTED_MODULE_3__/* .SortType.Recommend */ .ER.Recommend
    }];
  }, []);
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_1__.useRouter)();
  const sortBy = (_router$query$sort = (_router$query = router.query) === null || _router$query === void 0 ? void 0 : _router$query.sort) !== null && _router$query$sort !== void 0 ? _router$query$sort : sorts[0].key;

  const isActive = sort => {
    if (sort === sortBy) {
      return 'active';
    }

    return '';
  };

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("section", {
    className: "content-header pt-0",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
      className: "container",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("div", {
        className: "title-with-avatar center",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
          className: "title-box",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("h6", {
            className: "sub-title mb-1",
            children: tag.subtitle
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("h2", {
            className: "h3-md mb-1",
            children: tag.title
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("span", {
            className: "inactive-color fs-2 mb-0",
            children: [tag.count, " \u90E8\u5F71\u7247"]
          })]
        })
      }), !noSort && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("nav", {
        className: "sort-nav",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("ul", {
          id: "list_videos_common_videos_list_sort_list",
          children: sorts.map(item => {
            return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("li", {
              className: isActive(item.key),
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx(next_link__WEBPACK_IMPORTED_MODULE_0__.default, {
                href: makeHref(router, {
                  sort: item.key.toString()
                }),
                passHref: true,
                shallow: true,
                children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx("a", {
                  children: item.name
                })
              })
            }, item.key);
          })
        })
      })]
    })
  });
};

/***/ }),

/***/ 1575:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "t": () => (/* binding */ Pagination)
/* harmony export */ });
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6731);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1664);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const Pagination = ({
  page
}) => {
  var _router$query$page, _router$query, _router$query2;

  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_0__.useRouter)();
  const nowPage = (_router$query$page = (_router$query = router.query) === null || _router$query === void 0 ? void 0 : _router$query.page) !== null && _router$query$page !== void 0 ? _router$query$page : '1';
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    jQuery('html').animate({
      scrollTop: 0
    });
  }, [(_router$query2 = router.query) === null || _router$query2 === void 0 ? void 0 : _router$query2.page]);
  const pages = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(() => {
    let r = [];

    for (let i = page.current - 2; i <= page.current + 2; i++) {
      r.push(i);
    }

    r = r.filter(n => n > 0);
    r = r.filter(n => n <= page.pages);

    if (r.length < 5) {
      if (r[r.length - 1] == page.pages) {
        let r0 = r[0];
        r = [r0 - 2, r0 - 1].concat(r);
        r = r.filter(n => n > 0);
        r = r.slice(-5);
        return r;
      }

      if (r[0] == 1) {
        let r0 = r[r.length - 1];
        r = r.concat([r0 + 1, r0 + 2]);
        r = r.filter(n => n <= page.pages);
        r = r.slice(0, 5);
        return r;
      }
    }

    return r;
  }, [page.pages, page.current]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("ul", {
    className: "pagination",
    children: [pages[0] > 1 && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx("li", {
      className: "page-item",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__.default, {
        href: {
          query: _objectSpread(_objectSpread({}, router.query), {}, {
            page: '1'
          })
        },
        passHref: true,
        shallow: true,
        replace: true,
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx("a", {
          className: "page-link",
          children: "\xAB \u9996\u9801"
        })
      })
    }), pages.map(n => {
      let nn = '' + n;

      if (n < 10) {
        nn = '0' + nn;
      }

      if (page.current === n) {
        return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx("li", {
          className: "page-item",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx("span", {
            className: "page-link active disabled",
            children: nn
          })
        }, n);
      }

      return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx("li", {
        className: "page-item",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__.default, {
          href: {
            query: _objectSpread(_objectSpread({}, router.query), {}, {
              page: n
            })
          },
          passHref: true,
          shallow: true,
          replace: true,
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx("a", {
            className: "page-link",
            children: nn
          })
        })
      }, n);
    }), pages[pages.length - 1] < page.pages && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx("li", {
      className: "page-item",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__.default, {
        href: {
          query: _objectSpread(_objectSpread({}, router.query), {}, {
            page: page.pages
          })
        },
        passHref: true,
        shallow: true,
        replace: true,
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx("a", {
          className: "page-link",
          children: "\u6700\u5F8C \xBB"
        })
      })
    })]
  });
};

/***/ })

};
;