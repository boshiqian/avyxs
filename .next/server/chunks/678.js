"use strict";
exports.id = 678;
exports.ids = [678];
exports.modules = {

/***/ 3678:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "X": () => (/* binding */ ModalState),
/* harmony export */   "k": () => (/* binding */ usePageLoading)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var unstated_next__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9892);
/* harmony import */ var unstated_next__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(unstated_next__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




function dispatch(s, a) {
  var _a$loading;

  let ns = _objectSpread({}, s);

  switch (a.type) {
    case 'loading':
      ns.loading = true;
      ns.show = true;
      break;

    case 'loaded':
      ns.loading = false;
      ns.show = true;
      break;

    case 'close':
      ns.show = false;
      break;

    case 'show':
      ns.show = true;
      ns.content = a.content;
      ns.loading = (_a$loading = a === null || a === void 0 ? void 0 : a.loading) !== null && _a$loading !== void 0 ? _a$loading : false;
      break;
  }

  return ns;
}

const defaultState = {
  loading: false,
  show: false,
  content: null
};

const useModalState = () => {
  return (0,react__WEBPACK_IMPORTED_MODULE_0__.useReducer)(dispatch, defaultState);
};

const ModalState = (0,unstated_next__WEBPACK_IMPORTED_MODULE_1__.createContainer)(useModalState);
const usePageLoading = loading => {
  const [_x, dispatch] = ModalState.useContainer();
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    dispatch({
      type: loading ? 'loading' : 'close'
    });
  }, [loading]);
};

/***/ })

};
;