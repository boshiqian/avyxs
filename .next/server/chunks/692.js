"use strict";
exports.id = 692;
exports.ids = [692];
exports.modules = {

/***/ 7692:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "h": () => (/* binding */ Header)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./components/iconfont/helper.ts
var helper = __webpack_require__(4531);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/iconfont/IconPlay.tsx
const _excluded = ["size", "color", "style"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconPlay = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = _objectWithoutProperties(_ref, _excluded);

  const style = _style ? _objectSpread(_objectSpread({}, DEFAULT_STYLE), _style) : DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", _objectSpread(_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M512 149.333333c200.298667 0 362.666667 162.368 362.666667 362.666667s-162.368 362.666667-362.666667 362.666667S149.333333 712.298667 149.333333 512 311.701333 149.333333 512 149.333333z m0 64c-164.949333 0-298.666667 133.717333-298.666667 298.666667s133.717333 298.666667 298.666667 298.666667 298.666667-133.717333 298.666667-298.666667-133.717333-298.666667-298.666667-298.666667z m-64 141.312a42.666667 42.666667 0 0 1 22.741333 6.570667l176.533334 111.232a42.666667 42.666667 0 0 1 0.32 71.978667l-176.533334 113.429333A42.666667 42.666667 0 0 1 405.333333 621.973333v-224.64a42.666667 42.666667 0 0 1 42.666667-42.666666z m21.333333 81.322667v146.922667l115.456-74.176L469.333333 435.968z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconPlay.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconPlay = (IconPlay);
;// CONCATENATED MODULE: ./components/iconfont/IconSet.tsx
const IconSet_excluded = ["size", "color", "style"];

function IconSet_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function IconSet_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { IconSet_ownKeys(Object(source), true).forEach(function (key) { IconSet_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { IconSet_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function IconSet_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function IconSet_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = IconSet_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function IconSet_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const IconSet_DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconSet = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = IconSet_objectWithoutProperties(_ref, IconSet_excluded);

  const style = _style ? IconSet_objectSpread(IconSet_objectSpread({}, IconSet_DEFAULT_STYLE), _style) : IconSet_DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", IconSet_objectSpread(IconSet_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M448.362667 166.826667l113.6 0.170666a64 64 0 0 1 63.893333 63.914667l0.042667 18.517333a301.461333 301.461333 0 0 1 62.101333 34.88l15.210667-8.746666a64 64 0 0 1 87.296 23.381333l56.938666 98.304a64 64 0 0 1-19.989333 85.397333l-3.477333 2.133334-15.274667 8.810666c2.624 24.234667 2.304 48.853333-1.130667 73.322667l10.794667 6.250667a64 64 0 0 1 25.216 84.117333l-1.770667 3.306667-53.333333 92.373333a64 64 0 0 1-84.117333 25.216l-3.328-1.792-14.741334-8.533333a298.538667 298.538667 0 0 1-59.626666 33.28v25.386666a64 64 0 0 1-59.989334 63.957334l-4.074666 0.128-113.6-0.170667a64 64 0 0 1-63.893334-63.893333l-0.064-30.613334a302.613333 302.613333 0 0 1-50.069333-29.696l-27.221333 15.658667a64 64 0 0 1-87.296-23.402667l-56.938667-98.282666a64 64 0 0 1 19.989333-85.418667l3.477334-2.133333 27.690666-15.936c-2.133333-20.266667-2.24-40.768-0.192-61.226667l-30.741333-17.770667A64 64 0 0 1 158.506667 393.6l1.792-3.306667 53.333333-92.373333a64 64 0 0 1 84.117333-25.216l3.306667 1.792 26.794667 15.466667a297.984 297.984 0 0 1 56.426666-34.666667v-24.362667a64 64 0 0 1 59.989334-63.978666l4.074666-0.128z m-0.085334 64l0.064 65.066666-36.778666 17.301334c-15.744 7.402667-30.613333 16.533333-44.309334 27.221333l-34.005333 26.538667-62.570667-36.138667-1.6-0.896-53.333333 92.373333 66.56 38.421334-4.138667 41.152c-1.6 15.978667-1.536 32.106667 0.149334 48.085333l4.394666 41.429333-63.786666 36.736 56.917333 98.282667 63.338667-36.416 33.6 24.597333a237.994667 237.994667 0 0 0 39.466666 23.424l36.736 17.258667 0.128 71.168 113.578667 0.170667-0.064-68.16 39.466667-16.426667a234.538667 234.538667 0 0 0 46.826666-26.112l33.578667-24.128 50.56 29.184 53.290667-92.394667-48.128-27.818666 5.973333-42.688c2.666667-19.093333 2.965333-38.421333 0.896-57.6l-4.48-41.450667 51.456-29.696-56.938667-98.282667-51.2 29.504-33.621333-24.512a238.037333 238.037333 0 0 0-48.938667-27.498666l-39.381333-16.341334-0.128-61.184-113.578667-0.170666z m127.381334 183.722666a128.170667 128.170667 0 0 1 46.890666 174.933334 127.829333 127.829333 0 0 1-174.762666 46.848 128.170667 128.170667 0 0 1-46.869334-174.933334 127.829333 127.829333 0 0 1 174.741334-46.848z m-119.317334 78.805334a64.170667 64.170667 0 0 0 23.466667 87.573333 63.829333 63.829333 0 0 0 87.296-23.402667 64.170667 64.170667 0 0 0-20.266667-85.589333l-3.2-1.984-3.306666-1.770667a63.829333 63.829333 0 0 0-83.989334 25.173334z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconSet.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconSet = (IconSet);
// EXTERNAL MODULE: ./components/iconfont/IconConsumption.tsx
var IconConsumption = __webpack_require__(3269);
// EXTERNAL MODULE: ./components/iconfont/IconVip.tsx
var IconVip = __webpack_require__(4754);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
// EXTERNAL MODULE: ./pages/api.ts + 1 modules
var api = __webpack_require__(3919);
// EXTERNAL MODULE: ./mod/utils/index.ts + 1 modules
var utils = __webpack_require__(3863);
// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__(2470);
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);
// EXTERNAL MODULE: ./components/Site/Modal/index.ts + 1 modules
var Modal = __webpack_require__(3117);
// EXTERNAL MODULE: ./pages/member/Settings.tsx + 1 modules
var Settings = __webpack_require__(1124);
// EXTERNAL MODULE: ./pages/user.state.tsx
var user_state = __webpack_require__(2298);
// EXTERNAL MODULE: external "react-ga"
var external_react_ga_ = __webpack_require__(9831);
var external_react_ga_default = /*#__PURE__*/__webpack_require__.n(external_react_ga_);
;// CONCATENATED MODULE: ./pages/member/Header.tsx
















const placeholderUser = {
  account: 'guest',
  balance: 0,
  devCode: '',
  devType: 3,
  headUrl: 'user/def_head.png',
  id: '',
  isVip: false,
  nickName: '游客',
  sex: 0
};
const Header = () => {
  const router = (0,router_.useRouter)();
  const nowTab = router.pathname;

  const isActive = tab => {
    if (nowTab === tab) {
      return 'active';
    }

    return '';
  };

  const [_x1, dispatch] = Modal/* ModalState.useContainer */.XK.useContainer();
  const {
    isLoading,
    data
  } = user_state/* UserQuery.useContainer */.fE.useContainer();
  const isSelf = true;
  const joinBefore = (0,utils/* beforeNow */.IU)(external_moment_default()(data.createTime));
  return /*#__PURE__*/jsx_runtime_.jsx("section", {
    className: "content-header pt-0",
    style: {
      background: '#10121d',
      backgroundImage: 'linear-gradient(to bottom, #090812, #111520)'
    },
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "container",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "profile-info title-with-avatar",
        children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
          className: "avatar",
          src: (0,api/* ImageBase */.v)(data.headUrl),
          width: "100"
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "title-box",
          children: [/*#__PURE__*/jsx_runtime_.jsx("h3", {
            children: data.nickName
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "inactive-color",
            children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("span", {
              children: ["\u52A0\u5165\u65BC ", joinBefore]
            }), data.isVip && /*#__PURE__*/(0,jsx_runtime_.jsxs)("span", {
              className: "ml-2",
              children: ["VIP\u8FC7\u671F\u65F6\u95F4:", external_moment_default()(data.vipTimp).format('YYYY-MM-DD HH:MM')]
            })]
          })]
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("nav", {
        className: "profile-nav",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
          children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
            className: isActive('/member'),
            children: /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
              href: "/member",
              passHref: true,
              children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
                children: [/*#__PURE__*/jsx_runtime_.jsx(iconfont_IconPlay, {
                  className: "ml-2 ml-sm-0 mr-2",
                  color: "currentColor"
                }), /*#__PURE__*/jsx_runtime_.jsx("span", {
                  children: "\u4E0A\u50B3\u4F5C\u54C1"
                }), /*#__PURE__*/jsx_runtime_.jsx("span", {
                  className: "count",
                  children: "0"
                })]
              })
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("li", {
            className: isActive('/member/store/coin'),
            children: /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
              href: "/member/store/coin",
              passHref: true,
              children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
                children: [/*#__PURE__*/jsx_runtime_.jsx(IconConsumption/* default */.Z, {
                  className: "ml-2 ml-sm-0 mr-2",
                  color: "currentColor"
                }), /*#__PURE__*/jsx_runtime_.jsx("span", {
                  children: "\u91D1\u5E01\u8D2D\u4E70"
                }), /*#__PURE__*/jsx_runtime_.jsx("span", {
                  className: "count"
                })]
              })
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("li", {
            className: isActive('/member/store/vip2'),
            children: /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
              href: "/member/store/vip2",
              passHref: true,
              children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
                children: [/*#__PURE__*/jsx_runtime_.jsx(IconVip/* default */.Z, {
                  className: "ml-2 ml-sm-0 mr-2",
                  color: "currentColor"
                }), /*#__PURE__*/jsx_runtime_.jsx("span", {
                  children: "\u4F1A\u5458\u8D2D\u4E70"
                }), /*#__PURE__*/jsx_runtime_.jsx("span", {
                  className: "count"
                })]
              })
            })
          })]
        }), isSelf && /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
            className: "right text-nowrap",
            href: "#",
            "data-toggle": "dropdown",
            children: [/*#__PURE__*/jsx_runtime_.jsx(iconfont_IconSet, {
              className: "mr-2",
              color: "currentColor"
            }), /*#__PURE__*/jsx_runtime_.jsx("span", {
              children: "\u8A2D\u5B9A"
            })]
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "dropdown-menu dropdown-menu-right",
            children: [/*#__PURE__*/jsx_runtime_.jsx("a", {
              className: "dropdown-item",
              href: "#",
              onClick: e => {
                e.preventDefault();
                external_react_ga_default().event({
                  category: '用户资料',
                  action: '修改资料',
                  label: '用户中心'
                });
                dispatch({
                  type: 'show',
                  content: /*#__PURE__*/jsx_runtime_.jsx(Settings/* Profile */.NZ, {
                    info: data
                  })
                });
              },
              children: "\u500B\u4EBA\u8CC7\u6599"
            }), /*#__PURE__*/jsx_runtime_.jsx("a", {
              className: "dropdown-item",
              href: "#",
              onClick: e => {
                e.preventDefault();
                external_react_ga_default().event({
                  category: '用户资料',
                  action: '绑定手机',
                  label: '用户中心'
                });
                dispatch({
                  type: 'show',
                  content: /*#__PURE__*/jsx_runtime_.jsx(Settings/* Phone */.LP, {
                    oldPhone: data.phone
                  })
                });
              },
              children: "\u7ED1\u5B9A\u624B\u673A"
            })]
          })]
        })]
      })]
    })
  });
};

/***/ })

};
;