"use strict";
exports.id = 77;
exports.ids = [77];
exports.modules = {

/***/ 2077:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "J": () => (/* reexport */ SiteTitle)
});

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(701);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./mod/app.title.ts
var app_title = __webpack_require__(8011);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/Site/Title/SiteTitle.tsx





const SiteTitle = props => {
  const router = (0,router_.useRouter)();
  let title = (0,app_title/* genTitle */.mQ)(router.pathname, props.title);
  return /*#__PURE__*/jsx_runtime_.jsx((head_default()), {
    children: /*#__PURE__*/jsx_runtime_.jsx("title", {
      children: title
    })
  });
};
;// CONCATENATED MODULE: ./components/Site/Title/index.ts


/***/ }),

/***/ 8011:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "mQ": () => (/* binding */ genTitle)
/* harmony export */ });
/* unused harmony exports appname, title */
const appname = '嘿嘿视频';
const title = {
  '/': '首页',
  '/video': '视频播放',
  '/search': '搜索',
  '/tag/category': '分类',
  '/member': '我的主页',
  '/member/store': '商店',
  '/member/store/coin': '金币购买',
  '/member/store/vip': '会员购买',
  '/member/store/vip2': '会员购买'
};
const genTitle = (pathname, extitle = '') => {
  let names = [appname];
  return appname;

  if (pathname === '/') {
    names.push(title['/']);
  } else {
    pathname.split('/').slice(1).reduce((t, e) => {
      t = t + '/' + e;
      let v = title[t];

      if (v) {
        names.push(v);
      }

      return t;
    }, '');
  }

  if (extitle) {
    names.push(extitle);
  }

  names.reverse();
  return names.join(' - ');
};

/***/ })

};
;