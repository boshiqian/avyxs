"use strict";
exports.id = 788;
exports.ids = [788];
exports.modules = {

/***/ 6788:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "j": () => (/* binding */ WeekTop)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./pages/home/VideoCard.tsx + 1 modules
var VideoCard = __webpack_require__(5525);
// EXTERNAL MODULE: ./components/iconfont/helper.ts
var helper = __webpack_require__(4531);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/iconfont/IconRightarrow.tsx
const _excluded = ["size", "color", "style"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconRightarrow = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = _objectWithoutProperties(_ref, _excluded);

  const style = _style ? _objectSpread(_objectSpread({}, DEFAULT_STYLE), _style) : DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", _objectSpread(_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M214.677333 542.122667l0.042667-64.405334 477.653333-0.298666-225.301333-225.322667 45.568-45.568 303.424 303.424L512.213333 813.781333l-45.504-45.504 226.453334-226.453333-478.485334 0.298667z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconRightarrow.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconRightarrow = (IconRightarrow);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
;// CONCATENATED MODULE: ./pages/home/WeekTop.tsx






const WeekTop = ({
  col: props
}) => {
  var _props$subtitle;

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("section", {
    className: "pb-3 pb-e-lg-40",
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "title-with-more",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "title-box",
        children: [/*#__PURE__*/jsx_runtime_.jsx("h6", {
          className: "sub-title inactive-color",
          children: (_props$subtitle = props === null || props === void 0 ? void 0 : props.subtitle) !== null && _props$subtitle !== void 0 ? _props$subtitle : props.title
        }), /*#__PURE__*/jsx_runtime_.jsx("h2", {
          className: "h3-md",
          children: props.title
        })]
      }), props.id && /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "more",
        children: /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
          href: {
            pathname: '/tag/indexcol',
            query: {
              id: props.id
            }
          },
          passHref: true,
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
            children: ["\u66F4\u591A", /*#__PURE__*/jsx_runtime_.jsx(iconfont_IconRightarrow, {
              size: 1.3,
              color: "currentColor"
            })]
          })
        })
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "row gutter-20",
      children: props.list.map(item => {
        return /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "col-6 col-sm-4 col-lg-3",
          children: /*#__PURE__*/jsx_runtime_.jsx(VideoCard/* VideoCard */.c, {
            item: item
          })
        }, item.id);
      })
    })]
  });
};

/***/ })

};
;