"use strict";
exports.id = 829;
exports.ids = [829];
exports.modules = {

/***/ 1829:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ StaticCategory)
});

// EXTERNAL MODULE: external "js-base64"
var external_js_base64_ = __webpack_require__(5385);
;// CONCATENATED MODULE: ./components/Site/Nav/api.fake.ts

const category = JSON.parse((0,external_js_base64_.decode)(`W3sibmFtZSI6Iuiho+edgCIsImNoaWxkcmVuIjpbIum7keS4nSIsIueZveS4nSIsIuiVvuS4nSIsIuagoeacjSIsIuaDhei2oyIsIuWFlOWls+mDjiIsIuWls+S7hiIsIuWSjOacjSJdfSx7Im5hbWUiOiLouqvmnZAiLCJjaGlsZHJlbiI6WyLnhp/lpbMiLCLlt6jkubMiLCLlsJHlpbMiLCLlsJHlpociLCLnmb3omY4iLCLnvo7lsLsiLCLotKvkubMiLCLnvo7ohb8iXX0seyJuYW1lIjoi5Lqk5ZCIIiwiY2hpbGRyZW4iOlsi6aKc5bCEIiwi6ISa5LqkIiwi6IKb5LqkIiwi5r2u5ZC5Iiwi5rex5ZaJIiwi5Y+j54iGIiwi5Lmz5LqkIiwi5Lit5Ye6Il19LHsibmFtZSI6IueOqeazlSIsImNoaWxkcmVuIjpbIumcsuWHuiIsIui9ruWluCIsIuiHquaFsCIsIuiwg+aVmSIsIuaNhue7kSIsIueUt00iLCLkubHkvKYiLCLmjInmkakiLCLlvLrlpbgiLCLlpJpQIiwi5YiR5YW3IiwiM1AiXX0seyJuYW1lIjoi5Ymn5oOFIiwiY2hpbGRyZW4iOlsi6K+x5oORIiwi5pe26Ze05YGc5q2iIiwi5bm06b6E5beuIiwi5Ye66L2oIiwi5YKs55ygIiwi5YG35ouNIiwi5LiN5LymIiwiTlRSIl19LHsibmFtZSI6IuinkuiJsiIsImNoaWxkcmVuIjpbIuWMu+eUnyIsIuaKpOWjqyIsIuiAgeW4iCIsIuepuuWnkCIsIuacquS6oeS6uiIsIuaDheS+oyIsIuWutuaUv+WmhyIsIuWutuW6reaVmeW4iCIsIuS6uuWmuyIsIuS4u+aSrSIsIk9MIl19LHsibmFtZSI6IuWcsOeCuSIsImNoaWxkcmVuIjpbIueUtei9piIsIua4qeaziSIsIua0l+a1tOWcuiIsIuazs+axoCIsIuWOleaJgCIsIuWtpuagoSIsIuWBpei6q+aIvyIsIuS+v+WIqeW6lyJdfV0=`));
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/Site/Nav/StaticCategory.tsx






/* harmony default export */ const StaticCategory = (() => {
  return /*#__PURE__*/jsx_runtime_.jsx(jsx_runtime_.Fragment, {
    children: category.map(c => {
      return /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_.Fragment, {
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "title-box",
          children: /*#__PURE__*/jsx_runtime_.jsx("h2", {
            className: "h3-md",
            children: c.name
          })
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "row gutter-20 pb-3",
          children: c.children.map(c2 => {
            return /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: "col-6 col-sm-4 col-lg-3 mb-3",
              children: /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                href: {
                  pathname: '/search',
                  query: {
                    q: c2
                  }
                },
                passHref: true,
                children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                  className: "tag text-light",
                  children: c2
                })
              })
            }, c2);
          })
        })]
      }, c.name);
    })
  });
});

/***/ })

};
;