"use strict";
exports.id = 85;
exports.ids = [85];
exports.modules = {

/***/ 9933:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "K": () => (/* binding */ HomeState)
});

// EXTERNAL MODULE: ./pages/api.ts + 1 modules
var pages_api = __webpack_require__(3919);
// EXTERNAL MODULE: ./mod/api/api.ts + 1 modules
var api = __webpack_require__(2529);
;// CONCATENATED MODULE: ./mod/api/home.ts

const info = params => {
  return api/* client.post */.L.post('/h5/getIndexData', params);
};
// EXTERNAL MODULE: external "react-query"
var external_react_query_ = __webpack_require__(2585);
// EXTERNAL MODULE: external "unstated-next"
var external_unstated_next_ = __webpack_require__(9892);
;// CONCATENATED MODULE: ./pages/home/home.state.ts





const useHomeState = () => {
  const api = pages_api/* APIClient.useContainer */.lP.useContainer();
  return (0,external_react_query_.useQuery)('home', () => {
    return info().then(res => res.data.result).then(d => {
      (0,pages_api/* setAppConfig */.ib)(d.config);
      (0,pages_api/* setImageBase */.u1)(d.config.resUrl);
      return d;
    });
  });
};

const HomeState = (0,external_unstated_next_.createContainer)(useHomeState);

/***/ }),

/***/ 3085:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ HomePage)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: external "jquery"
var external_jquery_ = __webpack_require__(1273);
;// CONCATENATED MODULE: ./mod/owl.carousel.ts





if (false) {}
// EXTERNAL MODULE: ./mod/layzload.ts
var layzload = __webpack_require__(9927);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./pages/home/TopSlide.tsx






const TopSlide = ({
  items,
  config
}) => {
  const ref = (0,external_react_.useRef)();
  (0,external_react_.useEffect)(() => {
    if (!ref.current) {
      return;
    }

    const lazyload = () => {
      // @ts-ignore
      jQuery('#home-owl .lazyload').lazyload();
    };

    let o = jQuery('#home-owl');
    o.one('initialized.owl.carousel', lazyload);
    o.on('resized.owl.carousel', lazyload); // @ts-ignore

    o.owlCarousel({
      autoplay: true,
      autoWidth: false,
      dots: false,
      loop: true,
      center: true,
      responsive: {
        0: {
          items: 2
        },
        992: {
          items: 4
        }
      }
    });
    return () => {
      o.trigger('destroy.owl.carousel');
      o = null;
    };
  }, [ref]);
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "jable-carousel jable-animate overflow-h",
    "data-animation": "slideRight",
    "data-animation-item": ".item",
    children: /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "gutter-20 gutter-xl-30 pb-3",
      children: /*#__PURE__*/jsx_runtime_.jsx("div", {
        id: "home-owl",
        className: "owl-carousel",
        ref: ref,
        children: items.map((item, index) => {
          return /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "item",
            children: /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: "video-img-box",
              children: /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: "img-box",
                children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
                  href: item.hrefUrl,
                  target: "_blank",
                  children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
                    className: "lazyload",
                    width: "690",
                    height: "300",
                    src: "/img/placeholder-lg.jpg",
                    "data-src": config.resUrl + item.imgUrl
                  }), (item === null || item === void 0 ? void 0 : item.re) && /*#__PURE__*/jsx_runtime_.jsx("div", {
                    className: "ribbon-top-left",
                    children: "\u7CBE\u9078"
                  })]
                })
              })
            })
          }, item.id);
        })
      })
    })
  });
};
// EXTERNAL MODULE: ./pages/home/WeekTop.tsx + 1 modules
var WeekTop = __webpack_require__(6788);
// EXTERNAL MODULE: ./pages/api.ts + 1 modules
var pages_api = __webpack_require__(3919);
// EXTERNAL MODULE: ./components/Site/Modal/index.ts + 1 modules
var Modal = __webpack_require__(3117);
// EXTERNAL MODULE: ./pages/home/home.state.ts + 1 modules
var home_state = __webpack_require__(9933);
;// CONCATENATED MODULE: ./pages/home/index.page.tsx









function HomePage() {
  const api = pages_api/* APIClient.useContainer */.lP.useContainer();
  const {
    isLoading,
    isError,
    data
  } = home_state/* HomeState.useContainer */.K.useContainer();
  (0,Modal/* usePageLoading */.kP)(isLoading);

  if (isLoading) {
    return null;
  } // console.log(data)


  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(TopSlide, {
      items: data.banner,
      config: data.config
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "container",
      children: data.cols.map((col, i) => {
        return /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
          children: /*#__PURE__*/jsx_runtime_.jsx(WeekTop/* WeekTop */.j, {
            col: col
          })
        }, col.id);
      })
    })]
  });
}

/***/ })

};
;