exports.id = 963;
exports.ids = [963];
exports.modules = {

/***/ 3919:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "lP": () => (/* binding */ APIClient),
  "XL": () => (/* binding */ AppConfig),
  "v": () => (/* binding */ ImageBase),
  "Eh": () => (/* binding */ queryClient),
  "ib": () => (/* binding */ setAppConfig),
  "u1": () => (/* binding */ setImageBase)
});

// EXTERNAL MODULE: external "react-query"
var external_react_query_ = __webpack_require__(2585);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: external "unstated-next"
var external_unstated_next_ = __webpack_require__(9892);
// EXTERNAL MODULE: ./mod/api/api.ts + 1 modules
var api = __webpack_require__(2529);
;// CONCATENATED MODULE: ./mod/api/index.ts

;// CONCATENATED MODULE: ./pages/api.ts

let imageBase = '';
let ImageBase = img => {
  if (imageBase == '') {
    return '';
  }

  return imageBase + img;
};
const setImageBase = n => imageBase = n;
let AppConfig = {
  kfUrl: '',
  resUrl: ''
};
const setAppConfig = n => AppConfig = n;
const queryClient = new external_react_query_.QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      refetchOnMount: false
    }
  }
});




const useAPIClient = () => {
  const _client = (0,external_react_.useMemo)(() => {
    api/* client.interceptors.request.use */.L.interceptors.request.use(config => {
      return config;
    });
    return api/* client */.L;
  }, []);

  return _client;
};

const APIClient = (0,external_unstated_next_.createContainer)(useAPIClient);

/***/ }),

/***/ 2431:
/***/ (() => {

/* (ignored) */

/***/ })

};
;