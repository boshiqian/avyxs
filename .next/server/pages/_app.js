(() => {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 8011:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "mQ": () => (/* binding */ genTitle)
/* harmony export */ });
/* unused harmony exports appname, title */
const appname = '嘿嘿视频';
const title = {
  '/': '首页',
  '/video': '视频播放',
  '/search': '搜索',
  '/tag/category': '分类',
  '/member': '我的主页',
  '/member/store': '商店',
  '/member/store/coin': '金币购买',
  '/member/store/vip': '会员购买',
  '/member/store/vip2': '会员购买'
};
const genTitle = (pathname, extitle = '') => {
  let names = [appname];
  return appname;

  if (pathname === '/') {
    names.push(title['/']);
  } else {
    pathname.split('/').slice(1).reduce((t, e) => {
      t = t + '/' + e;
      let v = title[t];

      if (v) {
        names.push(v);
      }

      return t;
    }, '');
  }

  if (extitle) {
    names.push(extitle);
  }

  names.reverse();
  return names.join(' - ');
};

/***/ }),

/***/ 9927:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "I": () => (/* binding */ useLazyloadRef)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony default export */ var __WEBPACK_DEFAULT_EXPORT__ = ('es6');



if (false) {}

const useLazyloadRef = () => {
  const ref = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)();
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    if (!ref.current) {
      return;
    }

    if (false) {} // @ts-ignore


    jQuery('.lazyload').lazyload();
  }, [ref.current]);
  return ref;
};

/***/ }),

/***/ 1488:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ _app_page)
});

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(701);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: external "react-query"
var external_react_query_ = __webpack_require__(2585);
// EXTERNAL MODULE: ./pages/api.ts + 1 modules
var api = __webpack_require__(3919);
// EXTERNAL MODULE: ./mod/layzload.ts
var layzload = __webpack_require__(9927);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: ./components/Site/Header/Header.module.css
var Header_module = __webpack_require__(8775);
var Header_module_default = /*#__PURE__*/__webpack_require__.n(Header_module);
// EXTERNAL MODULE: ./components/iconfont/helper.ts
var helper = __webpack_require__(4531);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/iconfont/IconSearch.tsx
const _excluded = ["size", "color", "style"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconSearch = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = _objectWithoutProperties(_ref, _excluded);

  const style = _style ? _objectSpread(_objectSpread({}, DEFAULT_STYLE), _style) : DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", _objectSpread(_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M469.333 192c153.174 0 277.334 124.16 277.334 277.333 0 68.054-24.534 130.411-65.216 178.688L846.336 818.24l-48.341 49.877L630.4 695.125a276.053 276.053 0 0 1-161.067 51.542C316.16 746.667 192 622.507 192 469.333S316.16 192 469.333 192z m0 64C351.51 256 256 351.51 256 469.333s95.51 213.334 213.333 213.334 213.334-95.51 213.334-213.334S587.157 256 469.333 256z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconSearch.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconSearch = (IconSearch);
;// CONCATENATED MODULE: ./components/iconfont/IconHome.tsx
const IconHome_excluded = ["size", "color", "style"];

function IconHome_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function IconHome_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { IconHome_ownKeys(Object(source), true).forEach(function (key) { IconHome_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { IconHome_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function IconHome_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function IconHome_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = IconHome_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function IconHome_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const IconHome_DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconHome = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = IconHome_objectWithoutProperties(_ref, IconHome_excluded);

  const style = _style ? IconHome_objectSpread(IconHome_objectSpread({}, IconHome_DEFAULT_STYLE), _style) : IconHome_DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", IconHome_objectSpread(IconHome_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M556.586667 159.36l288.490666 183.914667A64 64 0 0 1 874.666667 397.248v392.746667a64 64 0 0 1-64 64H555.456l0.021333-196.992H490.666667v196.992H234.666667a64 64 0 0 1-64-64v-398.293334a64 64 0 0 1 30.272-54.4l287.530666-178.346666a64 64 0 0 1 68.138667 0.426666zM810.666667 790.016V397.226667L522.197333 213.333333 234.666667 391.68v398.336h192v-197.013333h192.810666v196.992H810.666667z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconHome.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconHome = (IconHome);
// EXTERNAL MODULE: ./components/iconfont/IconVip.tsx
var IconVip = __webpack_require__(4754);
;// CONCATENATED MODULE: ./components/iconfont/IconMobilePhone.tsx
const IconMobilePhone_excluded = ["size", "color", "style"];

function IconMobilePhone_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function IconMobilePhone_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { IconMobilePhone_ownKeys(Object(source), true).forEach(function (key) { IconMobilePhone_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { IconMobilePhone_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function IconMobilePhone_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function IconMobilePhone_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = IconMobilePhone_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function IconMobilePhone_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const IconMobilePhone_DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconMobilePhone = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = IconMobilePhone_objectWithoutProperties(_ref, IconMobilePhone_excluded);

  const style = _style ? IconMobilePhone_objectSpread(IconMobilePhone_objectSpread({}, IconMobilePhone_DEFAULT_STYLE), _style) : IconMobilePhone_DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", IconMobilePhone_objectSpread(IconMobilePhone_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M704 149.333333a64 64 0 0 1 64 64v597.333334a64 64 0 0 1-64 64H320a64 64 0 0 1-64-64V213.333333a64 64 0 0 1 64-64h384z m0 64H320v597.333334h384V213.333333z m-192 469.333334a42.666667 42.666667 0 1 1 0 85.333333 42.666667 42.666667 0 0 1 0-85.333333z m85.333333-437.333334v64h-170.666666v-64h170.666666z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconMobilePhone.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconMobilePhone = (IconMobilePhone);
// EXTERNAL MODULE: ./components/iconfont/IconConsumption.tsx
var IconConsumption = __webpack_require__(3269);
;// CONCATENATED MODULE: ./components/iconfont/IconAccount.tsx
const IconAccount_excluded = ["size", "color", "style"];

function IconAccount_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function IconAccount_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { IconAccount_ownKeys(Object(source), true).forEach(function (key) { IconAccount_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { IconAccount_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function IconAccount_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function IconAccount_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = IconAccount_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function IconAccount_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const IconAccount_DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconAccount = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = IconAccount_objectWithoutProperties(_ref, IconAccount_excluded);

  const style = _style ? IconAccount_objectSpread(IconAccount_objectSpread({}, IconAccount_DEFAULT_STYLE), _style) : IconAccount_DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", IconAccount_objectSpread(IconAccount_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M644.8 581.568l160.64 187.456a64 64 0 0 1-48.597 105.643H267.157a64 64 0 0 1-48.597-105.643l160.661-187.435a253.813 253.813 0 0 0 61.206 26.944l-173.27 202.134h489.686l-173.27-202.134a254.613 254.613 0 0 0 61.227-26.965zM512 149.333c117.824 0 213.333 95.51 213.333 213.334S629.824 576 512 576s-213.333-95.51-213.333-213.333S394.176 149.333 512 149.333z m0 64A149.333 149.333 0 1 0 512 512a149.333 149.333 0 0 0 0-298.667z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconAccount.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconAccount = (IconAccount);
;// CONCATENATED MODULE: ./components/iconfont/IconLink.tsx
const IconLink_excluded = ["size", "color", "style"];

function IconLink_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function IconLink_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { IconLink_ownKeys(Object(source), true).forEach(function (key) { IconLink_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { IconLink_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function IconLink_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function IconLink_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = IconLink_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function IconLink_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const IconLink_DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconLink = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = IconLink_objectWithoutProperties(_ref, IconLink_excluded);

  const style = _style ? IconLink_objectSpread(IconLink_objectSpread({}, IconLink_DEFAULT_STYLE), _style) : IconLink_DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", IconLink_objectSpread(IconLink_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M618.24 439.381333a152.746667 152.746667 0 0 1 0 216l-135.893333 135.893334a163.370667 163.370667 0 1 1-231.04-231.04l66.922666-66.944 45.269334 45.269333-66.944 66.944a99.370667 99.370667 0 1 0 140.522666 140.522667l135.893334-135.893334a88.746667 88.746667 0 0 0 0-125.482666z m182.528-197.589333a163.370667 163.370667 0 0 1 0 231.04L733.866667 539.776l-45.269334-45.248 66.944-66.944a99.370667 99.370667 0 1 0-140.522666-140.522667l-135.893334 135.893334a88.746667 88.746667 0 0 0 0 125.482666l-45.269333 45.269334a152.746667 152.746667 0 0 1 0-216l135.893333-135.893334a163.370667 163.370667 0 0 1 231.04 0z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconLink.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconLink = (IconLink);
;// CONCATENATED MODULE: ./components/iconfont/IconAtm.tsx
const IconAtm_excluded = ["size", "color", "style"];

function IconAtm_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function IconAtm_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { IconAtm_ownKeys(Object(source), true).forEach(function (key) { IconAtm_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { IconAtm_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function IconAtm_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function IconAtm_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = IconAtm_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function IconAtm_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const IconAtm_DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconAtm = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = IconAtm_objectWithoutProperties(_ref, IconAtm_excluded);

  const style = _style ? IconAtm_objectSpread(IconAtm_objectSpread({}, IconAtm_DEFAULT_STYLE), _style) : IconAtm_DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", IconAtm_objectSpread(IconAtm_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M225.173333 149.397333L227.264 149.333333c2.261333 0.042667 4.181333 0.213333 6.101333 0.597334l2.986667 0.725333 3.968 0.938667c23.893333 6.186667 70.186667 26.645333 97.792 45.653333a363.754667 363.754667 0 0 1 173.653333-44.458667c200.106667 0 362.901333 161.92 362.901334 360.96C874.666667 712.746667 711.872 874.666667 511.786667 874.666667 308.544 874.666667 149.333333 712.256 149.333333 504.938667c0-15.146667 7.530667-82.026667 14.186667-100.778667 1.813333-8.661333 12.565333-51.413333 53.717333-103.509333 1.493333-20.202667 3.626667-71.786667-14.208-108.672a33.984 33.984 0 0 1-2.453333-9.813334l-0.256-3.072-0.298667-2.666666a25.344 25.344 0 0 1 4.821334-15.744 27.306667 27.306667 0 0 1 19.370666-11.264l0.96-0.021334 2.090667-0.064zM512 213.333333c-66.410667 0-127.786667 21.674667-177.344 58.304-9.344-6.677333-18.773333-13.098667-28.266667-19.285333-11.84-7.68-20.906667-13.077333-27.2-16.192 2.197333 12.714667 3.285333 23.189333 3.285334 31.445333 0 7.701333-0.96 25.301333-2.858667 52.757334l-0.298667 4.373333A297.408 297.408 0 0 0 213.333333 512c0 164.949333 133.717333 298.666667 298.666667 298.666667s298.666667-133.717333 298.666667-298.666667-133.717333-298.666667-298.666667-298.666667z m-10.666667 170.666667a32 32 0 0 1 32 32v64a32 32 0 0 1-64 0v-64a32 32 0 0 1 32-32z m170.666667 0a32 32 0 0 1 32 32v64a32 32 0 0 1-64 0v-64a32 32 0 0 1 32-32zM263.765333 170.88c0.085333 0.789333 0.149333 1.557333 0.192 2.346667l0.192 1.429333-0.213333-3.413333-0.170667-0.362667z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconAtm.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconAtm = (IconAtm);
// EXTERNAL MODULE: ./pages/member/Settings.tsx + 1 modules
var Settings = __webpack_require__(1124);
;// CONCATENATED MODULE: ./components/iconfont/IconShare.tsx
const IconShare_excluded = ["size", "color", "style"];

function IconShare_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function IconShare_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { IconShare_ownKeys(Object(source), true).forEach(function (key) { IconShare_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { IconShare_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function IconShare_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function IconShare_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = IconShare_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function IconShare_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const IconShare_DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconShare = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = IconShare_objectWithoutProperties(_ref, IconShare_excluded);

  const style = _style ? IconShare_objectSpread(IconShare_objectSpread({}, IconShare_DEFAULT_STYLE), _style) : IconShare_DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", IconShare_objectSpread(IconShare_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M395.946667 234.666667v64H256v469.333333h512V522.666667h64V768a64 64 0 0 1-64 64H256a64 64 0 0 1-64-64V298.666667a64 64 0 0 1 64-64h139.946667z m335.850666-87.914667l150.848 150.826667-158.378666 158.4-45.269334-45.248L748.394667 341.333333H672c-121.685333 0-220.714667 97.024-223.914667 217.941334L448 565.333333v85.333334h-64v-85.333334C384 406.272 512.938667 277.333333 672 277.333333h99.861333l-85.312-85.333333 45.248-45.248z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconShare.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconShare = (IconShare);
// EXTERNAL MODULE: ./components/Site/Modal/index.ts + 1 modules
var Modal = __webpack_require__(3117);
// EXTERNAL MODULE: ./pages/user.state.tsx
var user_state = __webpack_require__(2298);
// EXTERNAL MODULE: ./components/Site/Header/User.module.css
var User_module = __webpack_require__(6389);
var User_module_default = /*#__PURE__*/__webpack_require__.n(User_module);
// EXTERNAL MODULE: external "react-ga"
var external_react_ga_ = __webpack_require__(9831);
var external_react_ga_default = /*#__PURE__*/__webpack_require__.n(external_react_ga_);
// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__(2470);
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);
;// CONCATENATED MODULE: ./components/Site/Header/User.tsx


















const User = () => {
  var _data$headUrl;

  const {
    isError,
    data
  } = user_state/* UserQuery.useContainer */.fE.useContainer();
  const [_x, dispatch] = Modal/* ModalState.useContainer */.XK.useContainer();

  if (isError) {
    return /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "settings",
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
        href: "#",
        children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
          className: "rounded-circle",
          src: (0,api/* ImageBase */.v)(user_state/* placeholderUser.headUrl */.Wv.headUrl),
          width: "35"
        }), /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "ml-2 fs-3 f-w-600 d-none d-lg-block",
          children: "\u8BF7\u5148\u767B\u9646"
        })]
      })
    });
  } // process.browser && console.log(data)


  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "settings",
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
      href: "#",
      "data-toggle": "dropdown",
      children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
        className: "rounded-circle",
        src: (0,api/* ImageBase */.v)((_data$headUrl = data === null || data === void 0 ? void 0 : data.headUrl) !== null && _data$headUrl !== void 0 ? _data$headUrl : user_state/* placeholderUser */.Wv === null || user_state/* placeholderUser */.Wv === void 0 ? void 0 : user_state/* placeholderUser.headUrl */.Wv.headUrl),
        width: "35"
      }), /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "ml-2 fs-3 f-w-600 d-none d-lg-block",
        children: data.nickName
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: `dropdown-menu dropdown-menu-right pb-3 ${(User_module_default()).menu}`,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "cover"
      }), /*#__PURE__*/jsx_runtime_.jsx("img", {
        className: "avatar",
        src: (0,api/* ImageBase */.v)(data.headUrl)
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("h5", {
        children: [data.nickName, /*#__PURE__*/jsx_runtime_.jsx(IconVip/* default */.Z, {
          className: "mx-1 active-color",
          size: 1.3,
          color: data.isVip ? 'currentColor' : '#b8babc'
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
        href: {
          pathname: '/member'
        },
        passHref: true,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
          className: "dropdown-item pl-4",
          children: [/*#__PURE__*/jsx_runtime_.jsx(iconfont_IconHome, {
            className: "mr-3",
            size: 1.3,
            color: "currentColor"
          }), /*#__PURE__*/jsx_runtime_.jsx("span", {
            children: "\u6211\u7684\u4E3B\u9801"
          })]
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
        href: {
          pathname: '/agent'
        },
        passHref: true,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
          className: "dropdown-item pl-4",
          children: [/*#__PURE__*/jsx_runtime_.jsx(iconfont_IconShare, {
            className: "mr-3",
            size: 1.3,
            color: "currentColor"
          }), /*#__PURE__*/jsx_runtime_.jsx("span", {
            children: "\u63A8\u5E7F\u5206\u4EAB"
          })]
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
        href: {
          pathname: '/member/store/coin'
        },
        passHref: true,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
          className: "dropdown-item pl-4",
          children: [/*#__PURE__*/jsx_runtime_.jsx(IconConsumption/* default */.Z, {
            className: "mr-3",
            size: 1.3,
            color: "currentColor"
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("span", {
            children: ["\u91D1\u5E01\u8D2D\u4E70 (\u73B0\u6709:", data.balance, ")"]
          })]
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
        href: {
          pathname: '/member/store/vip2'
        },
        passHref: true,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
          className: "dropdown-item pl-4",
          children: [/*#__PURE__*/jsx_runtime_.jsx(IconVip/* default */.Z, {
            className: "mr-3",
            size: 1.3,
            color: "currentColor"
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("span", {
            children: ["\u4F1A\u5458\u8D2D\u4E70", data.isVip && ` (有效:~ ${external_moment_default()(data.vipTimp).format('YYYY-MM-DD')})`]
          })]
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
        href: "#",
        className: "dropdown-item pl-4",
        onClick: e => {
          e.preventDefault();
          external_react_ga_default().event({
            category: '用户资料',
            action: '绑定手机',
            label: '顶部用户菜单'
          });
          dispatch({
            type: 'show',
            content: /*#__PURE__*/jsx_runtime_.jsx(Settings/* Phone */.LP, {
              oldPhone: data.phone
            })
          });
        },
        children: [/*#__PURE__*/jsx_runtime_.jsx(iconfont_IconMobilePhone, {
          className: "mr-3",
          size: 1.3,
          color: "currentColor"
        }), /*#__PURE__*/jsx_runtime_.jsx("span", {
          children: data.phone ? `${data.phone}` : '绑定手机号'
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
        href: "#",
        className: "dropdown-item pl-4",
        onClick: e => {
          e.preventDefault();
          external_react_ga_default().event({
            category: '用户资料',
            action: '切换账号',
            label: '顶部用户菜单'
          });
          dispatch({
            type: 'show',
            content: /*#__PURE__*/jsx_runtime_.jsx(Settings/* SwitchAccount */.c6, {})
          });
        },
        children: [/*#__PURE__*/jsx_runtime_.jsx(iconfont_IconAccount, {
          className: "mr-3",
          size: 1.3,
          color: "currentColor"
        }), /*#__PURE__*/jsx_runtime_.jsx("span", {
          children: "\u5207\u6362\u5E10\u53F7"
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
        href: {
          pathname: '/h5/backurl'
        },
        passHref: true,
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
          className: "dropdown-item pl-4",
          children: [/*#__PURE__*/jsx_runtime_.jsx(iconfont_IconLink, {
            className: "mr-3",
            size: 1.3,
            color: "currentColor"
          }), /*#__PURE__*/jsx_runtime_.jsx("span", {
            children: "\u5907\u7528\u7F51\u5740"
          })]
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
        className: "dropdown-item pl-4",
        href: api/* AppConfig.kfUrl */.XL.kfUrl,
        target: "_blank",
        children: [/*#__PURE__*/jsx_runtime_.jsx(iconfont_IconAtm, {
          className: "mr-3",
          size: 1.3,
          color: "currentColor"
        }), /*#__PURE__*/jsx_runtime_.jsx("span", {
          children: "\u5BA2\u670D\u70ED\u7EBF"
        })]
      })]
    })]
  });
};
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
;// CONCATENATED MODULE: ./components/Site/Header/Header.tsx









const Header = () => {
  (0,external_react_.useEffect)(() => {
    return () => {
      jQuery('body').removeClass('open-app-nav');
    };
  }, []);
  const {
    data
  } = HeaderQuery.useContainer();
  const items = data !== null && data !== void 0 ? data : []; // console.log(data)

  return /*#__PURE__*/jsx_runtime_.jsx("header", {
    id: "site-header",
    className: (Header_module_default())["site-header"],
    children: /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "container",
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "row header-wrap",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "col-auto col-md-5",
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            className: `${(Header_module_default())["app-nav-toggle"]}`,
            onClick: () => jQuery('body').toggleClass('open-app-nav'),
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: "lines",
              children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
                className: "line-1"
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: "line-2"
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: "line-3"
              })]
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("nav", {
            className: "navbar navbar-expand-lg",
            children: /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: "collapse navbar-collapse",
              children: /*#__PURE__*/jsx_runtime_.jsx("ul", {
                className: "navbar-nav",
                children: items.map(l => {
                  return /*#__PURE__*/jsx_runtime_.jsx("li", {
                    className: "nav-item d-none d-md-block",
                    children: /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
                      href: {
                        pathname: '/tag/category',
                        query: {
                          id: l.id
                        }
                      },
                      passHref: true,
                      children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                        children: l.title
                      })
                    })
                  }, l.id);
                })
              })
            })
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "col-auto col-md-2 justify-content-center p-0",
          children: /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "/",
            passHref: true,
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              className: "logo",
              children: /*#__PURE__*/jsx_runtime_.jsx("img", {
                height: "30",
                src: "/img/logo.png"
              })
            })
          })
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "col-auto header-right",
          children: [/*#__PURE__*/jsx_runtime_.jsx(Search, {}), /*#__PURE__*/jsx_runtime_.jsx(User, {})]
        })]
      })
    })
  });
};

const Lang = () => {
  return /*#__PURE__*/_jsxs("div", {
    className: "lang",
    children: [/*#__PURE__*/_jsx("a", {
      href: "#",
      "data-toggle": "dropdown",
      children: /*#__PURE__*/_jsx("img", {
        src: "/img/flag/zh.svg",
        width: "22"
      })
    }), /*#__PURE__*/_jsxs("div", {
      className: "dropdown-menu dropdown-menu-right mt-3",
      children: [/*#__PURE__*/_jsxs("a", {
        href: "?lang=jp",
        className: "dropdown-item mr-2 mb-1",
        children: [/*#__PURE__*/_jsx("img", {
          className: "mr-2 mb-1",
          src: "/img/flag/jp.svg",
          width: "20"
        }), " \u65E5\u672C\u8A9E"]
      }), /*#__PURE__*/_jsxs("a", {
        href: "?lang=en",
        className: "dropdown-item mr-2 mb-1",
        children: [/*#__PURE__*/_jsx("img", {
          className: "mr-2 mb-1",
          src: "/img/flag/en.svg",
          width: "20"
        }), ' ', "English"]
      })]
    })]
  });
};

const Search = () => {
  const router = (0,router_.useRouter)();
  const {
    0: val,
    1: setVal
  } = (0,external_react_.useState)('');

  const handleSubmit = () => {
    router.push({
      pathname: '/search',
      query: {
        q: val
      }
    });
  };

  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "search",
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("form", {
      className: "h-100 d-flex align-items-center",
      id: "search_form",
      action: "/search",
      method: "get",
      onSubmit: e => (e.preventDefault(), handleSubmit()),
      children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
        htmlFor: "inputSearch",
        className: "m-0",
        children: /*#__PURE__*/jsx_runtime_.jsx(iconfont_IconSearch, {
          size: 1.5,
          color: "currentColor"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("input", {
        id: "inputSearch",
        className: "h-100 fs-3",
        name: "q",
        type: "search",
        placeholder: "\u641C\u5C0B..",
        value: val,
        onChange: e => setVal(e.target.value)
      })]
    })
  });
};
// EXTERNAL MODULE: external "unstated-next"
var external_unstated_next_ = __webpack_require__(9892);
// EXTERNAL MODULE: ./mod/api/video.ts
var video = __webpack_require__(8612);
;// CONCATENATED MODULE: ./components/Site/Header/Header.state.ts




const useHeaderQuery = () => {
  return (0,external_react_query_.useQuery)('header-nav', () => {
    return video/* listCategory */.pk().then(r => r.data.result);
  }, {
    placeholderData: []
  });
};

const HeaderQuery = (0,external_unstated_next_.createContainer)(useHeaderQuery);
;// CONCATENATED MODULE: ./components/Site/Header/index.ts


// EXTERNAL MODULE: ./components/Site/Footer/index.ts + 2 modules
var Footer = __webpack_require__(3752);
// EXTERNAL MODULE: ./components/Site/Content/index.ts + 2 modules
var Content = __webpack_require__(8229);
// EXTERNAL MODULE: ./components/Site/Nav/Nav.module.css
var Nav_module = __webpack_require__(9731);
var Nav_module_default = /*#__PURE__*/__webpack_require__.n(Nav_module);
;// CONCATENATED MODULE: external "perfect-scrollbar"
const external_perfect_scrollbar_namespaceObject = require("perfect-scrollbar");
var external_perfect_scrollbar_default = /*#__PURE__*/__webpack_require__.n(external_perfect_scrollbar_namespaceObject);
;// CONCATENATED MODULE: ./components/iconfont/IconHotFill.tsx
const IconHotFill_excluded = ["size", "color", "style"];

function IconHotFill_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function IconHotFill_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { IconHotFill_ownKeys(Object(source), true).forEach(function (key) { IconHotFill_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { IconHotFill_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function IconHotFill_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function IconHotFill_objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = IconHotFill_objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function IconHotFill_objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

/* tslint:disable */

/* eslint-disable */



const IconHotFill_DEFAULT_STYLE = {
  display: 'inline-block'
};

const IconHotFill = _ref => {
  let {
    size,
    color,
    style: _style
  } = _ref,
      rest = IconHotFill_objectWithoutProperties(_ref, IconHotFill_excluded);

  const style = _style ? IconHotFill_objectSpread(IconHotFill_objectSpread({}, IconHotFill_DEFAULT_STYLE), _style) : IconHotFill_DEFAULT_STYLE;
  return /*#__PURE__*/jsx_runtime_.jsx("svg", IconHotFill_objectSpread(IconHotFill_objectSpread({
    viewBox: "0 0 1024 1024",
    width: size + 'rem',
    height: size + 'rem',
    style: style
  }, rest), {}, {
    children: /*#__PURE__*/jsx_runtime_.jsx("path", {
      d: "M448.533333 155.797333c67.968 53.098667 115.498667 104.618667 142.592 154.602667 25.664 47.36 35.968 95.808 30.912 145.344l-0.981333 8.256 5.034667-4.117333a143.829333 143.829333 0 0 0 40.533333-57.536l2.218667-6.122667 6.336-19.029333c67.776 46.037333 101.674667 124.010667 101.674666 233.898666 0 164.821333-144.426667 270.037333-263.125333 270.037334-118.72 0-253.866667-70.506667-275.733333-218.069334-21.845333-147.541333 68.757333-216.426667 130.474666-312.533333 41.173333-64.064 67.84-128.981333 80.064-194.730667z",
      fill: (0,helper/* getIconColor */.m)(color, 0, '#333333')
    })
  }));
};

IconHotFill.defaultProps = {
  size: 1
};
/* harmony default export */ const iconfont_IconHotFill = (IconHotFill);
// EXTERNAL MODULE: ./node_modules/next/dynamic.js
var dynamic = __webpack_require__(5152);
;// CONCATENATED MODULE: ./components/Site/Nav/Nav.tsx







const Nav = () => {
  const ref = (0,external_react_.useRef)();
  const {
    data
  } = HeaderQuery.useContainer();
  let items = data !== null && data !== void 0 ? data : [];
  (0,external_react_.useEffect)(() => {
    if (!ref.current) {
      return;
    }

    let ps = new (external_perfect_scrollbar_default())(ref.current);
    return () => {
      ps.destroy();
    };
  }, []);
  return /*#__PURE__*/jsx_runtime_.jsx("nav", {
    ref: ref,
    className: `${(Nav_module_default())["app-nav"]}`,
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "container",
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "title-box d-lg-none",
        children: /*#__PURE__*/jsx_runtime_.jsx("h2", {
          className: "h3-md",
          children: "\u63A8\u8350\u7AD9\u70B9"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "row gutter-20 pb-2 d-lg-none",
        children: /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "col-6 mb-3",
          children: /*#__PURE__*/jsx_runtime_.jsx("a", {
            className: "h4 text-light",
            href: "https://zb.ayqy.org/",
            target: "_blank",
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("span", {
              style: {
                color: 'hsl(338deg,85%,66%)'
              },
              children: ["\u563F\u563F-1\u5BF91\u89C6\u9891", /*#__PURE__*/jsx_runtime_.jsx(iconfont_IconHotFill, {
                className: "mb-1",
                color: "currentColor",
                size: 1.5
              })]
            })
          })
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "title-box",
        children: /*#__PURE__*/jsx_runtime_.jsx("h2", {
          className: "h3-md",
          children: "\u9078\u7247"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "row gutter-20 pb-4",
        children: items.map(l => {
          // const hClass = l.hiddenOnPC ? 'd-block d-lg-none' : ''
          const hClass = '';
          return /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: `col-6 col-sm-4 col-lg-3 mb-3 ${hClass}`,
            children: /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
              href: {
                pathname: '/tag/category',
                query: {
                  id: l.id
                }
              },
              passHref: true,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                className: "h5 text-light",
                onClick: () => {
                  external_react_ga_default().event({
                    category: '分类导航',
                    action: '点击'
                  });
                },
                children: l.title
              })
            })
          }, l.id);
        })
      }), /*#__PURE__*/jsx_runtime_.jsx(StaticCategoryClientOnly, {})]
    })
  });
};



const StaticCategoryClientOnly = (0,dynamic.default)(() => __webpack_require__.e(/* import() */ 829).then(__webpack_require__.bind(__webpack_require__, 1829)), {
  ssr: false,
  loadableGenerated: {
    webpack: () => [/*require.resolve*/(1829)],
    modules: ["..\\components\\Site\\Nav\\Nav.tsx -> " + './StaticCategory']
  }
});
;// CONCATENATED MODULE: ./components/Site/Nav/index.ts

// EXTERNAL MODULE: ./components/Site/Store/index.ts + 3 modules
var Store = __webpack_require__(9284);
// EXTERNAL MODULE: ./mod/utils/index.ts + 1 modules
var utils = __webpack_require__(3863);
;// CONCATENATED MODULE: ./pages/app/CheckInitUser.tsx








const CheckInitUser = () => {
  const [_x, dispatch] = Modal/* ModalState.useContainer */.XK.useContainer();
  const q = user_state/* UserQuery.useContainer */.fE.useContainer();
  (0,external_react_.useEffect)(() => {
    if (!q.isError) {
      return;
    }

    dispatch({
      type: 'show',
      content: /*#__PURE__*/jsx_runtime_.jsx(Settings/* SwitchAccount */.c6, {
        login: (0,utils/* isRespError */.Vn)(q.error) ? q.error.message : '登陆后方可继续'
      })
    }); // ga

    external_react_ga_default().event({
      category: '切换账号',
      action: '登录出错, 重新登录',
      label: (0,utils/* isRespError */.Vn)(q.error) ? q.error.message : '未知错误'
    });
  }, [q.error]);
  return null;
};

const CheckInitUserWrapper = () => {
  return /*#__PURE__*/jsx_runtime_.jsx(Modal/* ModalProvider */.DY, {
    children: /*#__PURE__*/jsx_runtime_.jsx(CheckInitUser, {})
  });
};


// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__(2034);
// EXTERNAL MODULE: ./mod/app.title.ts
var app_title = __webpack_require__(8011);
// EXTERNAL MODULE: ./pages/home/home.state.ts + 1 modules
var home_state = __webpack_require__(9933);
// EXTERNAL MODULE: ./mod/ec.ts
var ec = __webpack_require__(4280);
;// CONCATENATED MODULE: ./pages/app/init/ga.ts




async function initReactGA() {
  // @ts-ignore
  // window.GA_XHR_PATH =
  //   process.env.NODE_ENV === 'development'
  //     ? 'https://www.google-analytics.com'
  //     : '/ga-proxy'
  external_react_ga_default().initialize('UA-215389368-1', {// src: https://www.google-analytics.com/analytics.js
    // gaAddress: '/js/analytics.js?v=20211215',
    // debug: true,
  });
  external_react_ga_default().pageview(window.location.pathname + window.location.search);
  ec.Ec.init();
}
const useReactGA = () => {
  const router = (0,router_.useRouter)();
  (0,external_react_.useEffect)(() => {
    const handleRouteChange = url => {
      ec.ec.clear();
      external_react_ga_default().pageview(url);
    };

    router.events.on('routeChangeComplete', handleRouteChange);
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, []);
};
;// CONCATENATED MODULE: ./pages/app/init/user.ts


const registerUser = (0,utils/* withGAEvent */.v)(user_state/* registerUser */.a$)({
  category: '用户注册',
  action: '自动注册',
  label: '初始化'
});
const initUser = async () => {
  await registerUser();
};
;// CONCATENATED MODULE: ./pages/app/init/index.ts


const initApp = () => {
  return Promise.all([initReactGA(), initUser() // initReactGA(),
  ]);
};
const useAppInit = () => {
  useReactGA();
};
;// CONCATENATED MODULE: ./pages/app/index.ts

;// CONCATENATED MODULE: ./pages/_app.page.tsx
function _app_page_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _app_page_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { _app_page_ownKeys(Object(source), true).forEach(function (key) { _app_page_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { _app_page_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _app_page_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




















const Providers = props => {
  let providers = props.providers;
  providers = providers.reverse();
  return providers.reduce((children, Provider) => {
    return /*#__PURE__*/jsx_runtime_.jsx(Provider, {
      children: children
    });
  }, props.children);
};

function MyApp({
  Component,
  pageProps,
  router
}) {
  const ComponentHead = Component === null || Component === void 0 ? void 0 : Component.Head;
  useAppInit();
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(Providers, {
    providers: [props => /*#__PURE__*/jsx_runtime_.jsx(api/* APIClient.Provider */.lP.Provider, {
      children: props.children
    }), ({
      children
    }) => /*#__PURE__*/jsx_runtime_.jsx(external_react_query_.QueryClientProvider, {
      client: api/* queryClient */.Eh,
      children: children
    }), props => /*#__PURE__*/jsx_runtime_.jsx(home_state/* HomeState.Provider */.K.Provider, {
      children: props.children
    }), props => /*#__PURE__*/jsx_runtime_.jsx(user_state/* UserQuery.Provider */.fE.Provider, {
      children: props.children
    }), Store/* PayProvider */.K9, Modal/* ModalProvider */.DY],
    children: [/*#__PURE__*/jsx_runtime_.jsx(external_react_toastify_.ToastContainer, {
      autoClose: 3e3,
      position: "bottom-center",
      hideProgressBar: true,
      closeButton: false
    }), /*#__PURE__*/jsx_runtime_.jsx(CheckInitUserWrapper, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)((head_default()), {
        children: [/*#__PURE__*/jsx_runtime_.jsx("title", {
          children: (0,app_title/* genTitle */.mQ)(router.pathname)
        }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
          charSet: "utf-8"
        }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
          name: "viewport",
          content: "width=device-width, initial-scale=1, shrink-to-fit=no"
        })]
      }), ComponentHead && /*#__PURE__*/jsx_runtime_.jsx(ComponentHead, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(HeaderQuery.Provider, {
        children: [/*#__PURE__*/jsx_runtime_.jsx(Nav, {}), /*#__PURE__*/jsx_runtime_.jsx(Header, {})]
      }), /*#__PURE__*/jsx_runtime_.jsx(user_state/* UserProvider */.dr, {
        children: pageProps.noWrap ? /*#__PURE__*/jsx_runtime_.jsx(Component, _app_page_objectSpread({}, pageProps)) : /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
          children: [/*#__PURE__*/jsx_runtime_.jsx(Content/* SiteContent */.z, {
            children: /*#__PURE__*/jsx_runtime_.jsx(Component, _app_page_objectSpread({}, pageProps))
          }), /*#__PURE__*/jsx_runtime_.jsx(Footer/* Footer */.$, {})]
        })
      })]
    })]
  });
}






const NoSSRApp = (0,dynamic.default)(async () => {
  await initApp();
  return MyApp;
}, {
  ssr: false
});
/* harmony default export */ const _app_page = (NoSSRApp);

/***/ }),

/***/ 9933:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "K": () => (/* binding */ HomeState)
});

// EXTERNAL MODULE: ./pages/api.ts + 1 modules
var pages_api = __webpack_require__(3919);
// EXTERNAL MODULE: ./mod/api/api.ts + 1 modules
var api = __webpack_require__(2529);
;// CONCATENATED MODULE: ./mod/api/home.ts

const info = params => {
  return api/* client.post */.L.post('/h5/getIndexData', params);
};
// EXTERNAL MODULE: external "react-query"
var external_react_query_ = __webpack_require__(2585);
// EXTERNAL MODULE: external "unstated-next"
var external_unstated_next_ = __webpack_require__(9892);
;// CONCATENATED MODULE: ./pages/home/home.state.ts





const useHomeState = () => {
  const api = pages_api/* APIClient.useContainer */.lP.useContainer();
  return (0,external_react_query_.useQuery)('home', () => {
    return info().then(res => res.data.result).then(d => {
      (0,pages_api/* setAppConfig */.ib)(d.config);
      (0,pages_api/* setImageBase */.u1)(d.config.resUrl);
      return d;
    });
  });
};

const HomeState = (0,external_unstated_next_.createContainer)(useHomeState);

/***/ }),

/***/ 8775:
/***/ ((module) => {

// Exports
module.exports = {
	"site-header": "Header_site-header__2TEFI",
	"hide": "Header_hide__1lomc",
	"app-nav-toggle": "Header_app-nav-toggle__5rqHe"
};


/***/ }),

/***/ 6389:
/***/ ((module) => {

// Exports
module.exports = {
	"menu": "User_menu__1XkdV"
};


/***/ }),

/***/ 9731:
/***/ ((module) => {

// Exports
module.exports = {
	"app-nav": "Nav_app-nav__3GRfr"
};


/***/ }),

/***/ 2376:
/***/ ((module) => {

"use strict";
module.exports = require("axios");

/***/ }),

/***/ 419:
/***/ ((module) => {

"use strict";
module.exports = require("crypto-js");

/***/ }),

/***/ 5385:
/***/ ((module) => {

"use strict";
module.exports = require("js-base64");

/***/ }),

/***/ 2470:
/***/ ((module) => {

"use strict";
module.exports = require("moment");

/***/ }),

/***/ 9325:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 5378:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 2307:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/loadable.js");

/***/ }),

/***/ 7162:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 8773:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 2248:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 9372:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 665:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 2747:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 333:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 3456:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 7620:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 701:
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ 6731:
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ 9297:
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ 9831:
/***/ ((module) => {

"use strict";
module.exports = require("react-ga");

/***/ }),

/***/ 2585:
/***/ ((module) => {

"use strict";
module.exports = require("react-query");

/***/ }),

/***/ 2034:
/***/ ((module) => {

"use strict";
module.exports = require("react-toastify");

/***/ }),

/***/ 5282:
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 9892:
/***/ ((module) => {

"use strict";
module.exports = require("unstated-next");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [167,142,678,117,529,963,298,124,480,612,332,244,284], () => (__webpack_exec__(1488)));
module.exports = __webpack_exports__;

})();