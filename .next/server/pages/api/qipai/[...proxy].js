"use strict";
(() => {
var exports = {};
exports.id = 286;
exports.ids = [286];
exports.modules = {

/***/ 3540:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "config": () => (/* binding */ config),
  "default": () => (/* binding */ _proxy_api)
});

;// CONCATENATED MODULE: external "http-proxy"
const external_http_proxy_namespaceObject = require("http-proxy");
;// CONCATENATED MODULE: ./pages/api/qipai/[...proxy].api.ts

const config = {
  api: {
    bodyParser: false,
    externalResolver: true
  }
};
const target = 'http://qp01api.lsxtdlzpt.com/api';
const proxy = (0,external_http_proxy_namespaceObject.createProxy)({
  target: target,
  ignorePath: true
});
/* harmony default export */ const _proxy_api = ((req, res) => {
  let proxyStr = [].concat(req.query.proxy).join('/');
  let url = req.url.slice(req.url.indexOf(proxyStr) - 1);
  url = `${target}${url}`;
  delete req.headers.host;
  return proxy.web(req, res, {
    target: url
  }, (e, req, res, target) => {
    res.writeHead(500);
    res.end('api backend has error');
  });
});

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(3540));
module.exports = __webpack_exports__;

})();