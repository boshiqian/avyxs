_exit:
	exit 0

dev:
	yarn next

export:
	yarn next build && yarn next export

upload:
	tar -czf - -C out/ . | ssh eb-jable 'tar -xzvf - -C /www/wwwroot/avyxs.com'

sync:
	rsync --exclude-from=.rsync.ignore -avzP ./out/ eb-jable:/www/wwwroot/avyxs.comx/
