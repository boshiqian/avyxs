import { useRouter } from 'next/router'
import React from 'react'
import { useQuery } from 'react-query'
import { ad, adClick, ADType } from '~/mod/api/ad'
import { APIClient, ImageBase } from '~/pages/api'

export const SiteColAD: React.FC<{ position: string; className?: string }> = ({
  position,
  className = '',
}) => {
  const router = useRouter()
  const api = APIClient.useContainer()
  const { isLoading, data } = useQuery(
    ['ad', router.pathname, position],
    () => {
      return ad({ type: ADType.Col }).then((r) => r.data.result)
    },
  )
  const click = () => {
    adClick({ id: data.id })
  }
  return (
    <div className={`text-center ${className}`}>
      {data && (
        <a href={data.hrefUrl} target="_blank" onClick={click}>
          <img src={ImageBase(data.imgUrl)} />
        </a>
      )}
    </div>
  )
}
