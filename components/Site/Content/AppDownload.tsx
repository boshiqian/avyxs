import styles from './AppDownload.module.css'
import IconClose from '~/components/iconfont/IconClose'
import { useEffect, useState } from 'react'

const k = 'close-bottom-app-download'

export const AppDownload = () => {
  const [show, setShow] = useState(true)
  useEffect(() => {
    setShow(sessionStorage.getItem(k) !== '0')
  }, [])
  const sC = show ? '' : 'd-none'
  return (
    <div className={`${styles.root} ${sC} d-lg-none`}>
      <div className="container">
        <div className="d-flex py-2 align-items-center">
          <div
            className="no-font p-1 mr-1 pointer"
            onClick={() => {
              sessionStorage.setItem(k, '0')
              setShow(false)
            }}
          >
            <IconClose size={1.5} color="currentColor" />
          </div>
          <div className="no-font">
            <img className="rounded" height="50" src="/img/ico.png" alt="" />
          </div>
          <div className="mx-2">
            <h5 className={`mb-0 ${styles.title}`}>下载APP，免费看视频</h5>
            <p className={`mb-0 ${styles['sub-title']}`}>体验极致，玩游戏，赢取千万现金！！！</p>
          </div>
          <div className="flex-grow-1"></div>
          <div className="ml-2">
            <a href="http://z.mifen517.com/avyxs.apk" download>
              <button className={`btn btn-submit ${styles.btn}`}>下载</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}
