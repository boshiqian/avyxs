import React from 'react'
import styles from './SiteContent.module.css'
import { AppDownload } from './AppDownload'

export const SiteContent: React.FC = (props) => {
  return (
    <>
      <div id="site-content" className={styles['site-content']}>
        {props.children}
      </div>
      <AppDownload />
    </>
  )
}
