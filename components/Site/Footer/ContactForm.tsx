import { Form } from '~/components/Site/Modal/Form'
import { ModalState } from '../Modal'
import styles from './ContactForm.module.css'

export const ContactForm = () => {
  const [_x, dispatch] = ModalState.useContainer()
  return (
    <div className={styles.root}>
      <Form
        title="联系我们"
        subtitle="无论您遇到任何问题，或者想投放广告，皆欢迎使用下方联系方式联络我们。"
        close={() => dispatch({ type: 'close' })}
      >
        <div className="form-group-attached">
          <div className="form-group required">
            <label>Telegram搜索</label>
            <div className="form-control">
              <a href="https://t.me/AVYXS007" target="_blank">
                @AVYXS007
              </a>
            </div>
          </div>
          <div className="form-group required">
            <label>TG下载</label>
            <div className="form-control">
              <a href="https://telegram.org/" target="_blank">
                https://telegram.org/
              </a>
            </div>
          </div>
        </div>
        <div>
          <a href="https://t.me/heiheishipin" target="_blank">
            <button className="btn btn-block btn-submit">
              点击添加 @AVYXS007
            </button>
          </a>
        </div>
      </Form>
    </div>
  )
}
