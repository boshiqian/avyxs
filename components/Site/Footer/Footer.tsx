import Link from 'next/link'
import { ModalState } from '../Modal'
import { ContactForm } from './ContactForm'

export const footerRightLinks = [
  { title: '18 U.S.C. 2257', link: '/h5/2257', stitle: '2257' },
  { title: '使用條款', link: '/h5/terms' },
  { title: '濫用報告', link: '/h5/dmca' },
]

export const Footer = () => {
  return (
    <footer id="site-footer" className="site-footer py-5">
      <div className="container">
        <div className="row">
          <div className="col-lg-8 order-2 order-lg-1">
            <img src="/img/logo.png" className="mb-4" height="28" />
            <div>
              <a href="//instagram.com" target="_blank">
                <svg height="18" width="18">
                  <use xlink-href="#icon-ig"></use>
                </svg>
              </a>
              <a href="#" target="_blank">
                <svg height="18" width="18">
                  <use xlink-href="#icon-fb"></use>
                </svg>
              </a>
              <p className="pt-2 m-0">
                Copyright © 2021 avyxs.com All rights reserved
              </p>
            </div>
          </div>
          <div className="col-lg-4 order-1">
            <div className="row">
              <div className="col-6">
                <LeftLinks />
              </div>
              <div className="col-6">
                <RightLinks />
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer

const LeftLinks = () => {
  const [_x, dispatch] = ModalState.useContainer()
  const showContactForm = (e) => {
    e.preventDefault()
    dispatch({ type: 'show', content: <ContactForm /> })
  }
  return (
    <div className="widget">
      <h5 className="widget-title">關於</h5>
      <ul className="list-inline vertical-list">
        <li>
          <a href="#" onClick={showContactForm}>
            投放广告
          </a>
        </li>
        <li>
          <a href="#" onClick={showContactForm}>
            联系我们
          </a>
        </li>
        <li>
          <Link href="/h5/backurl" passHref>
            <a>备用网址</a>
          </Link>
        </li>
      </ul>
    </div>
  )
}
const RightLinks = () => {
  return (
    <div className="widget">
      <h5 className="widget-title">政策</h5>
      <ul className="list-inline vertical-list">
        {footerRightLinks.map((t) => {
          return (
            <li key={t.title}>
              <Link href={t.link} passHref>
                <a>{t.title}</a>
              </Link>
            </li>
          )
        })}
      </ul>
    </div>
  )
}
