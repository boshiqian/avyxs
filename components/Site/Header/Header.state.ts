import { useQuery } from 'react-query'
import { createContainer } from 'unstated-next'
import * as video from '~/mod/api/video'
import { APIClient } from '~/pages/api'

const useHeaderQuery = () => {
  return useQuery(
    'header-nav',
    () => {
      return video.listCategory().then((r) => {
        return r.data.result
      })
    },
    {
      placeholderData: [],
    },
  )
}

export const HeaderQuery = createContainer(useHeaderQuery)
