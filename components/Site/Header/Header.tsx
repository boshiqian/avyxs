import Link from 'next/link'
import styles from './Header.module.css'
import IconSearch from '~/components/iconfont/IconSearch'
import { HeaderQuery } from '.'
import { User } from './User'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'

export const Header = () => {
  useEffect(() => {
    return () => {
      jQuery('body').removeClass('open-app-nav')
    }
  }, [])
  const { data } = HeaderQuery.useContainer()
  const items = data ?? []
  // console.log(data)
  return (
    <header id="site-header" className={styles['site-header']}>
      <div className="container">
        <div className="row header-wrap">
          <div className="col-auto col-md-1">
            <div
              className={`${styles['app-nav-toggle']}`}
              onClick={() => jQuery('body').toggleClass('open-app-nav')}
            >
              <div className="lines">
                <div className="line-1"></div>
                <div className="line-2"></div>
                <div className="line-3"></div>
              </div>
            </div>
           {/* <nav className="navbar navbar-expand-lg">
              <div className="collapse navbar-collapse">
                <ul className="navbar-nav">
                  {items.map((l) => {
                    return (
                      <li key={l.id} className="nav-item d-none d-md-block">
                        <Link
                          href={{
                            pathname: '/tag/category',
                            query: { id: l.id },
                          }}
                          passHref
                        >
                          <a>{l.title}</a>
                        </Link>
                      </li>
                    )
                  })}
                </ul>
              </div>
            </nav>*/}
          </div>
          <div className="col-auto col-md-8 justify-content-center p-0">
            <Link href="/" passHref>
              <a className="logo">
                <img height="30" src="/img/logo.png" />
              </a>
            </Link>
            <a style={{marginLeft:'10px'}} className="card-logo" href={'../qp'}>
              <img height={30} src='/img/icon_main_game.png'/>
              <span>现金大礼</span>
            </a>
          </div>
          <div className="col-auto header-right">
            <Search />
            {/* <Lang /> */}
            <User />
          </div>
        </div>
      </div>
    </header>
  )
}

const Lang = () => {
  return (
    <div className="lang">
      <a href="#" data-toggle="dropdown">
        <img src="/img/flag/zh.svg" width="22" />
      </a>
      <div className="dropdown-menu dropdown-menu-right mt-3">
        <a href="?lang=jp" className="dropdown-item mr-2 mb-1">
          <img className="mr-2 mb-1" src="/img/flag/jp.svg" width="20" /> 日本語
        </a>
        <a href="?lang=en" className="dropdown-item mr-2 mb-1">
          <img className="mr-2 mb-1" src="/img/flag/en.svg" width="20" />{' '}
          English
        </a>
      </div>
    </div>
  )
}

const Search = () => {
  const router = useRouter()
  const [val, setVal] = useState('')
  const handleSubmit = () => {
    router.push({ pathname: '/search', query: { q: val } })
  }
  return (
    <div className="search">
      <form
        className="h-100 d-flex align-items-center"
        id="search_form"
        action="/search"
        method="get"
        onSubmit={(e) => (e.preventDefault(), handleSubmit())}
      >
        <label htmlFor="inputSearch" className="m-0">
          <IconSearch size={1.5} color="currentColor" />
        </label>
        <input
          id="inputSearch"
          className="h-100 fs-3"
          name="q"
          type="search"
          placeholder="搜尋.."
          value={val}
          onChange={(e) => setVal(e.target.value)}
        />
      </form>
    </div>
  )
}
