import Link from 'next/link'
import IconHome from '~/components/iconfont/IconHome'
import IconVip from '~/components/iconfont/IconVip'
import IconMobilePhone from '~/components/iconfont/IconMobilePhone'
import IconConsumption from '~/components/iconfont/IconConsumption'
import IconAccount from '~/components/iconfont/IconAccount'
import IconLink from '~/components/iconfont/IconLink'
import IconSupplierFeatures from '~/components/iconfont/IconAtm'
import { Phone, SwitchAccount } from '~/pages/member/Settings'
import IconShare from "~/components/iconfont/IconShare";
import { ModalState } from '../Modal'
import { placeholderUser, UserQuery } from '~/pages/user.state'
import { AppConfig, ImageBase } from '~/pages/api'
import styles from './User.module.css'
import ReactGA from 'react-ga'
import moment from 'moment'

export const User = () => {
  const { isError, data } = UserQuery.useContainer()
  const [_x, dispatch] = ModalState.useContainer()
  if (isError) {
    return (
      <div className="settings">
        <a href="#">
          <img
            className="rounded-circle"
            src={ImageBase(placeholderUser.headUrl)}
            width="35"
          />
          <span className="ml-2 fs-3 f-w-600 d-none d-lg-block">请先登陆</span>
        </a>
      </div>
    )
  }
  // process.browser && console.log(data)
  return (
    <div className="settings">
      <a href="#" data-toggle="dropdown">
        <img
          className="rounded-circle"
          src={ImageBase(data?.headUrl ?? placeholderUser?.headUrl)}
          width="35"
        />
        <span className="ml-2 fs-3 f-w-600 d-none d-lg-block">
          {data.nickName}
        </span>
      </a>
      <div className={`dropdown-menu dropdown-menu-right pb-3 ${styles.menu}`}>
        <div className="cover"></div>
        <img className="avatar" src={ImageBase(data.headUrl)} />
        <h5>
          {data.nickName}
          <IconVip
            className="mx-1 active-color"
            size={1.3}
            color={data.isVip ? 'currentColor' : '#b8babc'}
          />
        </h5>
        <Link href={{ pathname: '/member' }} passHref>
          <a className="dropdown-item pl-4">
            <IconHome className="mr-3" size={1.3} color="currentColor" />
            <span>我的主頁</span>
          </a>
        </Link>
        <Link href={{ pathname: '/agent' }} passHref>
          <a className="dropdown-item pl-4">
            <IconShare className="mr-3" size={1.3} color="currentColor" />
            <span>推广分享</span>
          </a>
        </Link>
        <Link href={{ pathname: '/member/store/coin' }} passHref>
          <a className="dropdown-item pl-4">
            <IconConsumption className="mr-3" size={1.3} color="currentColor" />
            <span>金币购买 (现有:{data.balance})</span>
          </a>
        </Link>
        <Link href={{ pathname: '/member/store/vip2' }} passHref>
          <a className="dropdown-item pl-4">
            <IconVip className="mr-3" size={1.3} color="currentColor" />
            <span>
              会员购买
              {data.isVip &&
                ` (有效:~ ${moment(data.vipTimp).format('YYYY-MM-DD')})`}
            </span>
          </a>
        </Link>
        <a
          href="#"
          className="dropdown-item pl-4"
          onClick={(e) => {
            e.preventDefault()
            ReactGA.event({
              category: '用户资料',
              action: '绑定手机',
              label: '顶部用户菜单',
            })
            dispatch({ type: 'show', content: <Phone oldPhone={data.phone} /> })
          }}
        >
          <IconMobilePhone className="mr-3" size={1.3} color="currentColor" />
          <span>{data.phone ? `${data.phone}` : '绑定手机号'}</span>
        </a>
        <a
          href="#"
          className="dropdown-item pl-4"
          onClick={(e) => {
            e.preventDefault()
            ReactGA.event({
              category: '用户资料',
              action: '切换账号',
              label: '顶部用户菜单',
            })
            dispatch({ type: 'show', content: <SwitchAccount /> })
          }}
        >
          <IconAccount className="mr-3" size={1.3} color="currentColor" />
          <span>切换帐号</span>
        </a>
        <Link href={{ pathname: '/h5/backurl' }} passHref>
          <a className="dropdown-item pl-4">
            <IconLink className="mr-3" size={1.3} color="currentColor" />
            <span>备用网址</span>
          </a>
        </Link>
        <a
          className="dropdown-item pl-4"
          href={AppConfig.kfUrl}
          target="_blank"
        >
          <IconSupplierFeatures
            className="mr-3"
            size={1.3}
            color="currentColor"
          />
          <span>客服热线</span>
        </a>
      </div>
      {/* <Modal show>
        <FindAccount />
      </Modal> */}
    </div>
  )
}
