import React from 'react'

import IconClose from '~/components/iconfont/IconClose'

type Props = {
  title: string
  subtitle: string
  close?: () => any
}

export const Form: React.FC<Props> = (props) => {
  return (
    <div className="modal-content">
      <div className="modal-header">
        {props.close && (
          <button className="close" onClick={() => props.close()}>
            <IconClose size={1.3} color="currentColor" />
          </button>
        )}
        <h4>{props.title}</h4>
        <p>{props.subtitle}</p>
      </div>
      <div className="modal-body">{props.children}</div>
    </div>
  )
}
