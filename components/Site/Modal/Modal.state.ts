import React, { useEffect, useReducer } from 'react'
import { createContainer } from 'unstated-next'

type State = {
  loading: boolean
  show: boolean
  content: React.ReactNode
}
export type Action =
  | { type: 'loading' }
  | { type: 'loaded' }
  | { type: 'show'; content: React.ReactNode; loading?: boolean }
  | { type: 'close' }

function dispatch(s: State, a: Action): State {
  let ns = { ...s }
  switch (a.type) {
    case 'loading':
      ns.loading = true
      ns.show = true
      break
    case 'loaded':
      ns.loading = false
      ns.show = true
      break
    case 'close':
      ns.show = false
      break
    case 'show':
      ns.show = true
      ns.content = a.content
      ns.loading = a?.loading ?? false
      break
  }
  return ns
}

const defaultState: State = {
  loading: false,
  show: false,
  content: null,
}

const useModalState = () => {
  return useReducer(dispatch, defaultState)
}

export const ModalState = createContainer(useModalState)

export const usePageLoading = (loading: boolean) => {
  const [_x, dispatch] = ModalState.useContainer()
  useEffect(() => {
    dispatch({ type: loading ? 'loading' : 'close' })
  }, [loading])
}
