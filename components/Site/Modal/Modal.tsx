import { ModalState } from './Modal.state'
import React, { useEffect } from 'react'
import styles from './Modal.module.css'

export const Modal: React.FC<{
  show: boolean
  loading?: boolean
  className?: string
}> = (props) => {
  const activeC = props.show ? `show ${styles.show}` : ''
  useEffect(() => {
    if (props.show) {
      jQuery('body').css('overflow', 'hidden')
    } else {
      if (jQuery('.xmodal.show').length === 0) {
        jQuery('body').css('overflow', '')
      }
    }
  }, [props.show])
  return (
    <div
      className={`xmodal ${styles.root} ${activeC} ${props.className ?? ''}`}
    >
      {props.loading && (
        <div>
          <div className={styles.loading}></div>
        </div>
      )}
      {props.children && (
        <div
          className="modal-dialog"
          style={props.loading ? { display: 'none' } : {}}
        >
          <div className="modal-content-wrapper">
            <div className="modal-content">{props.children}</div>
          </div>
        </div>
      )}
    </div>
  )
}
const ModalWrapper: React.FC = (props) => {
  const [state] = ModalState.useContainer()
  return (
    <Modal show={state.show} loading={state.loading}>
      {state.content}
    </Modal>
  )
}

export const ModalProvider: React.FC = (props) => {
  return (
    <ModalState.Provider>
      <ModalWrapper />
      {props.children}
    </ModalState.Provider>
  )
}
