import styles from './Nav.module.css'
import { category } from './api.fake'
import {rankBy} from "./api.fake";
import Link from 'next/link'
import { Fragment, useEffect, useRef } from 'react'
import PerfectScrollbar from 'perfect-scrollbar'
import { HeaderQuery } from '../Header'
import IconHot from '~/components/iconfont/IconHot'
import IconHotFill from '~/components/iconfont/IconHotFill'
import ReactGA from 'react-ga'

export const Nav = () => {
  const ref = useRef<HTMLElement>()
  const { data } = HeaderQuery.useContainer()
  let items = data ?? []
  useEffect(() => {
    if (!ref.current) {
      return
    }
    let ps = new PerfectScrollbar(ref.current)
    return () => {
      ps.destroy()
    }
  }, [])
  return (
    <nav ref={ref} className={`${styles['app-nav']}`}>
      <div className="container">
        <div className="title-box d-lg-none">
          <h2 className="h3-md">推荐站点</h2>
        </div>
        <div className="row gutter-20 pb-2 d-lg-none">
          <div className="col-6 mb-3 topic-link">
            <a
              className="h4 text-light"
              href="https://www.abty5.com/"
              target="_blank"
            >
              <span style={{ color: 'hsl(338deg,85%,66%)' }}>
                爱博体育 -  2022世界杯
                <IconHotFill className="mb-1" color="currentColor" size={1.5} />
              </span>
            </a>
          </div>
        </div>
        <div className="title-box">
          <h2 className="h3-md">選片</h2>
        </div>
        <div className="row gutter-20 pb-4">
          {rankBy.map((l) => {
            // const hClass = l.hiddenOnPC ? 'd-block d-lg-none' : ''
            const hClass = ''
            return (
                <div
                    key={l.id}
                    className={`col-6 topic-link col-sm-4 col-lg-3 mb-3 ${hClass}`}
                >
                  {l.id=='1492513071521878018'? (<a
                      className="h5 text-light"
                  >
                    {l.title}
                  </a>): (<Link
                      href={{ pathname: '/tag/indexcol', query: { id: l.id } }}
                      passHref
                  >
                      {l.title}
                  </Link>)}
                </div>
            )
          })}
        </div>
        <div className="title-box">
          <h2 className="h3-md">分类</h2>
        </div>
        <div className="row gutter-20 pb-4">
          {items.map((l) => {
            // const hClass = l.hiddenOnPC ? 'd-block d-lg-none' : ''
            const hClass = ''
            return (
                <div
                    key={l.id}
                    className={`col-6 col-sm-4 col-lg-3 mb-3 ${hClass}`}
                >
                  <Link
                      href={{ pathname: '/tag/category', query: { id: l.id } }}
                      passHref
                  >
                    <a
                        className="h5 text-light"
                        onClick={() => {
                          ReactGA.event({
                            category: '分类导航',
                            action: '点击',
                          })
                        }}
                    >
                      {l.title}
                    </a>
                  </Link>
                </div>
            )
          })}
        </div>
        {/* 分类 */}
        <StaticCategoryClientOnly />
      </div>
    </nav>
  )
}

import dynamic from 'next/dynamic'
import { access } from 'fs'
const StaticCategoryClientOnly = dynamic(() => import('./StaticCategory'), {
  ssr: false,
})
