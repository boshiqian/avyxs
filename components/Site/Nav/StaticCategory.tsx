import { category } from './api.fake'
import { Fragment } from 'react'
import Link from 'next/link'

export default () => {
  return (
    <>
      {category.map((c) => {
        return (
          <Fragment key={c.name}>
            <div className="title-box">
              <h2 className="h3-md">{c.name}</h2>
            </div>
            <div className="row gutter-20 pb-3">
              {c.children.map((c2) => {
                return (
                  <div key={c2} className="col-6 col-sm-4 col-lg-3 mb-3">
                    <Link
                      href={{ pathname: '/search', query: { q: c2 } }}
                      passHref
                    >
                      <a className="tag text-light">{c2}</a>
                    </Link>
                  </div>
                )
              })}
            </div>
          </Fragment>
        )
      })}
    </>
  )
}
