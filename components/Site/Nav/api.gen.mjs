import data from './api.fake.src.mjs'
import { encode } from 'js-base64'

let d = JSON.stringify(data)
d = encode(d)
let mod = `
import { decode } from 'js-base64'
export const category = JSON.parse(decode(\`${d}\`))
`

import fs from 'fs/promises'

await fs.writeFile('./api.fake.ts', mod)
