import React, { useEffect } from 'react'
import { useQuery } from 'react-query'
import { APIClient, ImageBase } from '~/pages/api'
import { Action, StoreState, useStoreLoading } from './Store.state'
import * as store from '~/mod/api/store'
import { Form } from '../Modal'
import styles from './Buy.module.css'
import { useRouter } from 'next/router'

import ReactGA from 'react-ga'
import { ec } from '~/mod/ec'

export const Buy: React.FC = () => {
  const { state, dispatch } = StoreState.useContainer()

  const { isLoading, data } = useQuery('payways', () => {
    return store.payChannels().then((r) => r.data.result)
  })

  useEffect(() => {
    ec.addProduct({
      id: state.params.gid,
      name: state.params.gtitle,
      price: state.params.gprice,
    })
    ec.detail({ step: 1 })
  }, [])

  useStoreLoading(isLoading)
  if (isLoading) {
    return null
  }

  // ReactGA.event({})

  return (
    <Form title="购买" subtitle="" close={() => dispatch({ type: 'close' })}>
      <form onSubmit={(e) => e.preventDefault()}>
        <div className="form-group-attached">
          <div className="form-group required">
            <label htmlFor="p-name">产品</label>
            <input
              readOnly
              id="p-name"
              className="form-control"
              value={state.params.gtitle}
            />
          </div>
          <div className="form-group required">
            <label htmlFor="p-price">价格</label>
            <input
              readOnly
              id="p-price"
              className="form-control"
              value={state.params.gprice}
            />
          </div>
        </div>
        <div className={styles['buy-btns']}>
          {data.map((item) => {
            return (
              <div key={item.id}>
                <button
                  className="btn btn-block"
                  onClick={() => {
                    dispatch({
                      type: 'order',
                      dispatch: dispatch,
                      params: { cid: item.id },
                    })
                    // ga
                    ec.checkout_option({ option: item.title, step: 2 })
                  }}
                >
                  <img
                    width="20"
                    className="mr-2"
                    src={ImageBase(item.imgUrl)}
                  />
                  <span>{item.title}</span>
                </button>
              </div>
            )
          })}
        </div>
      </form>
    </Form>
  )
}
