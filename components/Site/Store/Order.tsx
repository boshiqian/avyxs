import React, { useEffect } from 'react'
import { useMutation, useQuery } from 'react-query'
import { Action, StoreState, useStoreLoading } from './Store.state'
import * as store from '~/mod/api/store'
import { Form } from '../Modal'
import { useRouter } from 'next/router'
import { UserQuery } from '~/pages/user.state'
import { toast } from 'react-toastify'
import { ec } from '~/mod/ec'

export const Order: React.FC = () => {
  const { state, dispatch } = StoreState.useContainer()
  const orderid = state.params.orderid
  const { refetch: refetchUser } = UserQuery.useContainer()

  const { isLoading, data, refetch, isFetching, isFetched } = useQuery(
    ['order-check', orderid],
    ({ queryKey: [_n, orderid] }) => {
      return store
        .checkOrder({ orderNum: orderid })
        .then((r) => r.data.result)
        .then(async (paid) => {
          if (paid) {
            await refetchUser()
          }
          return paid
        })
    },
  )
  useEffect(() => {
    if (!isFetched) {
      return
    }
    let goNext = true
    const f = () => {
      Promise.resolve(1)
        .then(() => refetch())
        .then((r) => {
          if (r.data) {
            goNext = false
            toast.success('支付成功')
            return Promise.reject(0)
          }
        })
        .then(() => new Promise((rl) => setTimeout(rl, 2e3)))
        .then(() => {
          if (goNext) {
            f()
          }
        })
    }
    f()

    return () => {
      goNext = false
    }
  }, [isFetched])

  // ga
  useEffect(() => {
    ec.checkout({
      id: orderid,
      step: 3,
    })
  }, [])
  useEffect(() => {
    if (!data) {
      return
    }
    ec.purchase({
      id: orderid,
      step: 4,
    })
  }, [!!data])

  useStoreLoading(isLoading)

  const pay = () => {
    store.pay({ orderNum: orderid })
  }
  let orderStatus = '加载中'
  if (typeof data !== 'undefined') {
    orderStatus = data ? '支付完成' : '待支付'
  }
  return (
    <Form
      title="订单状态"
      subtitle=""
      close={() => dispatch({ type: 'close' })}
    >
      <div className="form-group-attached">
        <div className="form-group required">
          <label htmlFor="p-name">产品</label>
          <input
            readOnly
            id="p-name"
            className="form-control"
            value={state.params.gtitle}
          />
        </div>
        <div className="form-group required">
          <label htmlFor="p-price">价格</label>
          <input
            readOnly
            id="p-price"
            className="form-control"
            value={state.params.gprice}
          />
        </div>
      </div>
      <div className="form-group-attached">
        <div className="form-group required">
          <label htmlFor="p-price">订单状态</label>
          <input
            readOnly
            id="p-price"
            className="form-control"
            value={orderStatus}
          />
        </div>
      </div>
      <div>
        {data ? (
          <button
            className="btn btn-submit btn-block"
            onClick={() => dispatch({ type: 'close' })}
          >
            关闭
          </button>
        ) : (
          <a href={store.pay({ orderNum: orderid })} target="_blank">
            <button className="btn btn-submit btn-block">去支付</button>
          </a>
        )}
      </div>
    </Form>
  )
}
