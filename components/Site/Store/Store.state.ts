import { NextRouter, useRouter } from 'next/router'
import React, { useEffect, useReducer } from 'react'
import { createContainer } from 'unstated-next'
import { GoodType } from '~/mod/api/store'
import { APIClient } from '~/pages/api'
import * as store from '~/mod/api/store'
import { isRespError } from '~/mod/utils'
import { toast } from 'react-toastify'
import { getUid } from '~/pages/user.state'
import { ec } from '~/mod/ec'

export enum OrderStep {
  Hidden,
  /**商品展示页面 */
  Goods,
  /**打开购买页面 */
  Buy,
  /**展示订单 */
  Order,
  /**支付 */
  Pay,
}

type State = {
  show: boolean
  loading: boolean
  step: OrderStep
  params: any
  goods?: React.ReactNode
}

type ActionBuy = {
  type: 'buy'
  router?: NextRouter
  params: {
    /**商品id */
    gid: string
    /**商品类型 */
    gtype: GoodType
    /**商品名称 */
    gtitle: string
    gprice: string
  }
}

type ActionOrder = {
  type: 'order'
  router?: NextRouter
  dispatch: React.Dispatch<Action>
  params: {
    /**通道id */
    cid: string
  }
}
type ActionOrderPay = {
  type: 'order-pay'
  router?: NextRouter
  params: {
    /**订单号 */
    orderid: string
  }
}

type ActionShowGoods = {
  type: 'goods'
  goods: React.ReactNode
}

export type Action =
  | { type: 'close' }
  | { type: 'loaded' }
  | { type: 'loading' }
  | ActionShowGoods
  | ActionBuy
  | ActionOrder
  | ActionOrderPay

const defaultState: State = {
  show: false,
  loading: false,
  // @ts-ignore
  step: null,
  params: {},
}

function StateReducer(s: State, a: Action): State {
  let ns = { ...s }
  switch (a.type) {
    case 'loading':
      ns.loading = true
      break
    case 'loaded':
      ns.loading = false
      break
    case 'close':
      ns.show = false
      ns.step = OrderStep.Hidden
      ec.clear()
      break
    case 'goods':
      ns.show = true
      ns.step = OrderStep.Goods
      ns.goods = a.goods
      break
    case 'buy':
      ns.loading = true
      ns.show = true
      ns.step = OrderStep.Buy

      ns.params = { ...ns.params, ...a.params }
      break
    case 'order':
      ns.show = true
      ns.loading = true
      Promise.resolve()
        .then(() => {
          return store.buy({
            userId: getUid(),
            channelId: a.params.cid,
            itemId: ns.params.gid,
            type: ns.params.gtype,
          })
        })
        .then((r) => r.data.result)
        .then(
          (orderNum) => {
            a.dispatch({
              type: 'order-pay',
              params: { orderid: orderNum },
            })
          },
          (err) => {
            toast.error(isRespError(err) ? err.message : '网络波动请重试')
            a.dispatch({ type: 'loaded' })
          },
        )
      break
    case 'order-pay':
      ns.show = true
      ns.loading = false
      ns.step = OrderStep.Order
      ns.params = { ...ns.params, ...a.params }
      break
  }
  return ns
}

const useStoreState = () => {
  const api = APIClient.useContainer()
  const [state, dispatch] = useReducer(StateReducer, defaultState)
  return { state, dispatch }
}

export const StoreState = createContainer(useStoreState)

export const useStoreLoading = (loading: boolean) => {
  const { dispatch } = StoreState.useContainer()
  useEffect(() => {
    dispatch({ type: loading ? 'loading' : 'loaded' })
  }, [loading])
}
