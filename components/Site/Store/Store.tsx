import React, { useEffect, useReducer } from 'react'
import { Action, OrderStep, StoreState } from './Store.state'
import { Modal, Form } from '../Modal'
import { useRouter } from 'next/router'
import styles from './Store.module.css'

import { Buy } from './Buy'
import { Order } from './Order'

export const Store = () => {
  const { state } = StoreState.useContainer()
  return (
    <Modal className={styles.root} show={state.show} loading={state.loading}>
      {state.step === OrderStep.Buy && <Buy />}
      {state.step === OrderStep.Order && <Order />}
      {state.step === OrderStep.Goods && state.goods}
    </Modal>
  )
}

export const StoreProvider: React.FC = (props) => {
  return (
    <StoreState.Provider>
      <Store />
      {props.children}
    </StoreState.Provider>
  )
}
