import Head from 'next/head'
import { useRouter } from 'next/router'
import React from 'react'
import { genTitle } from '~/mod/app.title'

type Props = {
  title: string
}

export const SiteTitle: React.FC<Props> = (props) => {
  const router = useRouter()
  let title = genTitle(router.pathname, props.title)
  return (
    <Head>
      <title>{title}</title>
    </Head>
  )
}
