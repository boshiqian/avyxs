import React,{ useRef, useEffect} from 'react'
import {BannerItem, getImg} from '~/mod/api/home'
export default function LazyImg({src,className,width,height,lazysrc}){
const imageRef=useRef(null)
    useEffect(()=>{
        const ob = new IntersectionObserver(
        (entries) =>{
        if(entries[0].isIntersecting){
            if (imageRef.current.src.indexOf('img/placeholder-md.jpg')!=-1||!imageRef.current.src)
            {
                getImg({data:lazysrc}).then((res)=>{
                    imageRef.current.src=res
                }).catch((res)=>{
                    console.log('catch发生错误')
                })
            }
        }
        },
            )
        ob.observe(imageRef.current)
        return ()=>{
            ob.disconnect()
        }
    },[])
  return (
      <img width={width} height={height} ref={imageRef}/>
)
}