/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconAddAccount: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M175.893333 769.024l146.602667-171.050667c18.666667 11.669333 38.826667 21.184 60.117333 28.202667L224.490667 810.666667H554.666667v64H224.490667a64 64 0 0 1-48.597334-105.642667l146.602667-171.050667zM746.666667 640v85.333333h85.333333v64h-85.333333v85.333334h-64v-85.333334h-85.333334v-64h85.333334v-85.333333h64zM469.333333 149.333333c117.824 0 213.333333 95.509333 213.333334 213.333334s-95.509333 213.333333-213.333334 213.333333-213.333333-95.509333-213.333333-213.333333S351.509333 149.333333 469.333333 149.333333z m0 64a149.333333 149.333333 0 1 0 0 298.666667 149.333333 149.333333 0 0 0 0-298.666667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconAddAccount.defaultProps = {
  size: 1,
};

export default IconAddAccount;
