/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconAddProducts: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M682.666667 490.666667a192 192 0 1 1 0 384 192 192 0 0 1 0-384z m50.816-320a64 64 0 0 1 57.237333 35.370666l55.850667 111.68A64 64 0 0 1 853.333333 346.346667v175.274666a235.456 235.456 0 0 0-64-48.021333L789.333333 384h-554.666666L234.666667 789.333333h238.933333a235.456 235.456 0 0 0 48 64H234.666667a64 64 0 0 1-64-64V351.552a64 64 0 0 1 6.378666-27.84l56.469334-116.906667A64 64 0 0 1 291.136 170.666667h442.346667zM682.666667 554.666667a128 128 0 1 0 0 256 128 128 0 0 0 0-256z m32 53.333333V661.333333H768v64h-53.333333v53.333334h-64V725.333333H597.333333v-64h53.333334v-53.333333h64zM469.333333 448v64h-170.666666v-64h170.666666z m264.149334-213.333333H291.136l-41.258667 85.333333h526.272l-42.666666-85.333333z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconAddProducts.defaultProps = {
  size: 1,
};

export default IconAddProducts;
