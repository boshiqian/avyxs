/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconApple: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M798.293333 832c-35.413333 52.906667-72.96 104.533333-130.133333 105.386667-57.173333 1.28-75.52-33.706667-140.373333-33.706667-65.28 0-85.333333 32.853333-139.52 34.986667-55.893333 2.133333-98.133333-56.32-133.973334-107.946667C181.333333 725.333333 125.44 531.2 200.533333 400.64c37.12-64.853333 103.68-105.813333 175.786667-107.093333 54.613333-0.853333 106.666667 37.12 140.373333 37.12 33.28 0 96.426667-45.653333 162.56-38.826667 27.733333 1.28 105.386667 11.093333 155.306667 84.48-3.84 2.56-92.586667 54.613333-91.733333 162.56 1.28 128.853333 113.066667 171.946667 114.346666 172.373333-1.28 2.986667-17.92 61.44-58.88 120.746667M554.666667 149.333333c31.146667-35.413333 82.773333-62.293333 125.44-64 5.546667 49.92-14.506667 100.266667-44.373334 136.106667-29.44 36.266667-78.08 64.426667-125.866666 60.586667-6.4-49.066667 17.493333-100.266667 44.8-132.693334z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconApple.defaultProps = {
  size: 1,
};

export default IconApple;
