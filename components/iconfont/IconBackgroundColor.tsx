/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconBackgroundColor: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M789.333333 170.666667a64 64 0 0 1 64 64v554.666666a64 64 0 0 1-64 64H234.666667a64 64 0 0 1-64-64V234.666667a64 64 0 0 1 64-64h554.666666z m-234.730666 96h-63.872L336.256 704H298.666667v64h128v-64h-22.528l45.205333-128h146.624l45.205333 128H618.666667v64h128v-64h-37.589334l-154.474666-437.333333zM522.666667 368.426667L573.376 512h-101.418667l50.709334-143.573333z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconBackgroundColor.defaultProps = {
  size: 1,
};

export default IconBackgroundColor;
