/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconBlock: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M512 149.333333c200.533333 0 362.666667 162.133333 362.666667 362.666667s-162.133333 362.666667-362.666667 362.666667S149.333333 712.533333 149.333333 512 311.466667 149.333333 512 149.333333z m232.533333 174.933334L324.266667 744.533333c51.2 40.533333 117.333333 66.133333 187.733333 66.133334 164.266667 0 298.666667-134.4 298.666667-298.666667 0-70.4-25.6-136.533333-66.133334-187.733333zM512 213.333333c-164.266667 0-298.666667 134.4-298.666667 298.666667 0 70.4 25.6 136.533333 66.133334 187.733333l420.266666-420.266666C648.533333 238.933333 582.4 213.333333 512 213.333333z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconBlock.defaultProps = {
  size: 1,
};

export default IconBlock;
