/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconCascades: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M832 341.333333v554.666667H405.333333v-64h362.666667V341.333333h64z m-106.666667-106.666666v554.666666H298.666667v-64h362.666666V234.666667h64z m-106.666666-106.666667v554.666667H192V128h426.666667z m-64 64H256v426.666667h298.666667V192z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconCascades.defaultProps = {
  size: 1,
};

export default IconCascades;
