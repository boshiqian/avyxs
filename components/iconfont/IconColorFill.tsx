/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconColorFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M883.84 211.498667v186.410666a96 96 0 0 1-96 96H534.826667v49.173334h7.637333a64 64 0 0 1 64 64V810.666667a64 64 0 0 1-64 64h-79.253333a64 64 0 0 1-64-64v-203.584a64 64 0 0 1 64-64h7.616v-113.173334H787.84a32 32 0 0 0 32-32V211.498667h64zM727.765333 149.333333a64 64 0 0 1 64 64v120.682667a64 64 0 0 1-64 64H213.333333a64 64 0 0 1-64-64V213.333333a64 64 0 0 1 64-64h514.432z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconColorFill.defaultProps = {
  size: 1,
};

export default IconColorFill;
