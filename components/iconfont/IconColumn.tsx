/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconColumn: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M234.666667 853.333333a64 64 0 0 1-64-64V234.666667a64 64 0 0 1 64-64h554.666666a64 64 0 0 1 64 64v554.666666a64 64 0 0 1-64 64H234.666667z m138.666666-618.666666H234.666667v554.666666h138.666666V234.666667z m213.333334 0h-149.333334v554.666666h149.333334V234.666667z m202.666666 0h-138.666666v554.666666H789.333333V234.666667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconColumn.defaultProps = {
  size: 1,
};

export default IconColumn;
