/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconContacts: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M746.666667 170.666667a64 64 0 0 1 64 64v554.666666a64 64 0 0 1-64 64H320a64 64 0 0 1-64-64v-42.666666h-42.666667v-64h149.333334v64h-42.666667v42.666666h426.666667V234.666667H320v42.666666h42.666667v64h-149.333334v-64h42.666667v-42.666666a64 64 0 0 1 64-64h426.666667z m-56.597334 106.666666v469.333334h-64V277.333333h64zM320 384v256h-64V384h64z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconContacts.defaultProps = {
  size: 1,
};

export default IconContacts;
