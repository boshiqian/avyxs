/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconContactsFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M746.666667 170.666667a64 64 0 0 1 64 64v554.666666a64 64 0 0 1-64 64H320a64 64 0 0 1-64-64v-42.666666h-42.666667v-64h149.333334v-42.666667h-106.666667V384h106.666667v-42.666667h-149.333334v-64h42.666667v-42.666666a64 64 0 0 1 64-64h426.666667z m-56.597334 106.666666h-64v469.333334h64V277.333333z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconContactsFill.defaultProps = {
  size: 1,
};

export default IconContactsFill;
