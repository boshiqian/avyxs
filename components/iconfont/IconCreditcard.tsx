/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconCreditcard: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M789.333333 234.666667a64 64 0 0 1 64 64v426.666666a64 64 0 0 1-64 64H234.666667a64 64 0 0 1-64-64V298.666667a64 64 0 0 1 64-64h554.666666z m0 64H234.666667v426.666666h554.666666V298.666667zM384 618.666667v64h-106.666667v-64h106.666667z m362.666667-213.333334v64H277.333333v-64h469.333334z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconCreditcard.defaultProps = {
  size: 1,
};

export default IconCreditcard;
