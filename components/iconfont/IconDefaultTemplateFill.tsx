/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconDefaultTemplateFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M277.333333 298.666667v490.666666h437.333334v64h-405.333334a96 96 0 0 1-95.893333-91.477333L213.333333 757.333333V298.666667h64z m472.789334-127.786667a64 64 0 0 1 64 64v451.242667a64 64 0 0 1-64 64H319.786667V234.88a64 64 0 0 1 64-64h366.336z"
        fill={getIconColor(color, 0, '#333333')}
      />
      <path
        d="M491.946667 441.578667l-45.226667 45.269333 88.341333 88.298667 170.112-168.405334-45.013333-45.482666-124.885333 123.626666z"
        fill={getIconColor(color, 1, '#333333')}
      />
    </svg>
  );
};

IconDefaultTemplateFill.defaultProps = {
  size: 1,
};

export default IconDefaultTemplateFill;
