/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconDiscountFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1045 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M599.253333 134.72l220.842667 23.808a64 64 0 0 1 56.789333 56.768l23.786667 220.842667a64 64 0 0 1-18.368 52.096L510.933333 859.584a64 64 0 0 1-90.517333 0L175.808 614.954667a64 64 0 0 1 0-90.496L547.157333 153.109333a64 64 0 0 1 52.117334-18.389333zM652.352 277.333333a96 96 0 1 0 0 192 96 96 0 0 0 0-192z m0 64a32 32 0 1 1 0 64 32 32 0 0 1 0-64z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconDiscountFill.defaultProps = {
  size: 1,
};

export default IconDiscountFill;
