/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconEraser: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M647.850667 213.205333l3.370666 3.157334 221.290667 222.72a64 64 0 0 1 0 90.24L592.938667 810.624 789.333333 810.666667v64H297.216l-150.613333-151.594667a64 64 0 0 1 0-90.218667L560.426667 216.362667a64 64 0 0 1 87.424-3.157334z m-385.066667 393.493334L192 677.973333 323.84 810.666667h142.890667l-203.946667-203.946667z m343.04-345.237334L307.904 561.301333l221.994667 221.994667 297.216-299.093333-221.290667-222.72z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconEraser.defaultProps = {
  size: 1,
};

export default IconEraser;
