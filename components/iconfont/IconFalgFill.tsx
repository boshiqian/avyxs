/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconFalgFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M256 138.666667v746.666666H192v-746.666666h64z m588.544 0L644.544 341.333333l200 202.666667H298.666667v-405.333333h545.877333z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconFalgFill.defaultProps = {
  size: 1,
};

export default IconFalgFill;
