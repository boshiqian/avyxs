/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconFeedsFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M810.666667 298.666667a64 64 0 0 1 64 64v426.666666a64 64 0 0 1-64 64l-81.130667 0.021334a127.274667 127.274667 0 0 0 16.981333-57.706667L746.666667 789.333333V298.666667h64z m-170.666667-128a64 64 0 0 1 64 64v554.666666a64 64 0 0 1-64 64H213.333333a64 64 0 0 1-64-64V234.666667a64 64 0 0 1 64-64h426.666667z m-192 320h-170.666667v64h170.666667v-64z m128-128H277.333333v64h298.666667v-64z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconFeedsFill.defaultProps = {
  size: 1,
};

export default IconFeedsFill;
