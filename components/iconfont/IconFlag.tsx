/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconFlag: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M844.544 149.333333l-200 202.666667 200 202.666667H256v341.333333H192V149.333333h652.544z m-153.088 64H266.666667v277.333334h424.789333l-136.832-138.666667 136.832-138.666667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconFlag.defaultProps = {
  size: 1,
};

export default IconFlag;
