/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconFolderFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M460.650667 192a64 64 0 0 1 62.485333 50.133333l3.093333 13.866667H789.333333a64 64 0 0 1 64 64v448a64 64 0 0 1-64 64H234.666667a64 64 0 0 1-64-64V256a64 64 0 0 1 64-64h225.984zM746.666667 640H277.333333v64h469.333334v-64z m42.666666-320H540.437333l9.493334 42.666667H789.333333v-42.666667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconFolderFill.defaultProps = {
  size: 1,
};

export default IconFolderFill;
