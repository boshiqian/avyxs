/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconFurniture: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M810.666667 170.666667a64 64 0 0 1 64 64v490.666666a64 64 0 0 1-64 64h-64v64h-64v-64H362.666667v64h-64v-64h-64a64 64 0 0 1-64-64V234.666667a64 64 0 0 1 64-64h576z m-320 64H234.666667v490.666666h256V234.666667z m320 0H554.666667v490.666666h256V234.666667z m-362.666667 170.666666v134.250667h-64V405.333333h64z m213.333333 0v134.250667h-64V405.333333h64z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconFurniture.defaultProps = {
  size: 1,
};

export default IconFurniture;
