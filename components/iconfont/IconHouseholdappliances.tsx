/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconHouseholdappliances: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M832 832a64 64 0 0 1-64 64H256a64 64 0 0 1-64-64V192a64 64 0 0 1 64-64h512a64 64 0 0 1 64 64v640z m-64-320H256v320h512V512z m-384 64v170.666667h-64v-170.666667h64zM768 192H256v256h512V192z m-384 64v128h-64v-128h64z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconHouseholdappliances.defaultProps = {
  size: 1,
};

export default IconHouseholdappliances;
