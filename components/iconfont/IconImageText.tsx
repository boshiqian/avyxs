/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconImageText: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M789.333333 170.666667a64 64 0 0 1 64 64v554.666666a64 64 0 0 1-64 64H234.666667a64 64 0 0 1-64-64V234.666667a64 64 0 0 1 64-64h554.666666z m0 64H234.666667v554.666666h554.666666V234.666667z m-42.666666 384v64H277.333333v-64h469.333334zM469.333333 320a64 64 0 0 1 64 64v106.666667a64 64 0 0 1-64 64h-128a64 64 0 0 1-64-64v-106.666667a64 64 0 0 1 64-64h128z m277.333334 170.666667v64h-170.666667v-64h170.666667z m-277.333334-106.666667h-128v106.666667h128v-106.666667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconImageText.defaultProps = {
  size: 1,
};

export default IconImageText;
