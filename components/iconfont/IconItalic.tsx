/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconItalic: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M729.173333 149.333333v64h-111.829333l-165.781333 618.666667h128.298666v64H277.333333v-64h107.968l165.76-618.666667H426.666667V149.333333h302.506666z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconItalic.defaultProps = {
  size: 1,
};

export default IconItalic;
