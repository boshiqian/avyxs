/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconJizhuangxiang: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M762.325333 213.333333l112.298667 144.405334 0.042667 0.021333v469.333333H149.333333v-469.333333l644.224-0.021333L731.008 277.333333H292.992l-62.549333 80.405334h-81.066667L261.674667 213.333333h500.650666zM810.666667 421.76H213.333333v341.333333h597.333334v-341.333333z m-426.666667 64v213.333333h-64v-213.333333h64z m170.666667 0v213.333333h-64v-213.333333h64z m170.666666 0v213.333333h-64v-213.333333h64z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconJizhuangxiang.defaultProps = {
  size: 1,
};

export default IconJizhuangxiang;
