/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconNavList: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M789.333333 576v85.333333h85.333334v64h-85.333334v85.333334h-64v-85.333334h-85.333333v-64h85.333333v-85.333333h64z m-533.333333 158.101333v64H170.666667v-64h85.333333z m341.333333 0v64H298.666667v-64h298.666666zM256 493.162667v64H170.666667v-64h85.333333z m426.666667 0v64H298.666667v-64h384zM256 252.224v64H170.666667v-64h85.333333z m597.333333 0v64H298.666667v-64h554.666666z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconNavList.defaultProps = {
  size: 1,
};

export default IconNavList;
