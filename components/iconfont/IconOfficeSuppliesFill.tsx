/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconOfficeSuppliesFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M789.333333 620.309333a64 64 0 0 1 64 64v192l-128-48-128 48v-192a64 64 0 0 1 64-64h128z m-570.773333 148.693334l146.602667-171.029334A276.053333 276.053333 0 0 0 512 640a279.466667 279.466667 0 0 0 42.666667-3.264V874.666667H267.157333a64 64 0 0 1-48.597333-105.642667l146.602667-171.050667zM789.333333 684.330667h-128v99.626666l64-23.978666 64 24v-99.648zM512 149.333333c117.824 0 213.333333 95.509333 213.333333 213.333334s-95.509333 213.333333-213.333333 213.333333-213.333333-95.509333-213.333333-213.333333S394.176 149.333333 512 149.333333z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconOfficeSuppliesFill.defaultProps = {
  size: 1,
};

export default IconOfficeSuppliesFill;
