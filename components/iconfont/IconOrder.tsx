/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconOrder: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M298.667 192h42.666v63.979l-42.666 0.021v533.333h426.666V256h-42.666v-64h42.666a64 64 0 0 1 64 64v533.333a64 64 0 0 1-64 64H298.667a64 64 0 0 1-64-64V256a64 64 0 0 1 64-64z m256 341.333v64h-192v-64h192z m106.666-128v64H362.667v-64h298.666zM576 128a64 64 0 0 1 64 64v42.667a64 64 0 0 1-64 64H448a64 64 0 0 1-64-64V192a64 64 0 0 1 64-64h128z m0 64H448v42.667h128V192z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconOrder.defaultProps = {
  size: 1,
};

export default IconOrder;
