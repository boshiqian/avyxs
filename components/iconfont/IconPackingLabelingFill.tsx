/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconPackingLabelingFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M853.333333 384l-0.021333 245.013333-123.392-123.392-106.794667 106.816 33.322667 33.322667h-143.061333v151.04h142.186666L623.146667 829.226667 647.168 853.333333 234.666667 853.333333a64 64 0 0 1-64-64V384h682.666666z m-384 64h-170.666666v64h170.666666v-64zM733.482667 170.666667a64 64 0 0 1 57.237333 35.370666l55.850667 111.68 1.024 2.282667H178.816l54.698667-113.173333A64 64 0 0 1 291.136 170.666667h442.346667z"
        fill={getIconColor(color, 0, '#333333')}
      />
      <path
        d="M556.053333 692.394667l203.392-0.021334-75.989333-75.989333 46.464-46.464 154.88 154.88-154.88 154.88-46.464-46.464 75.114667-75.136h-202.517334z"
        fill={getIconColor(color, 1, '#333333')}
      />
    </svg>
  );
};

IconPackingLabelingFill.defaultProps = {
  size: 1,
};

export default IconPackingLabelingFill;
