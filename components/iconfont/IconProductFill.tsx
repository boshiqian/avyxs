/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconProductFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M853.333333 384v405.333333a64 64 0 0 1-64 64H234.666667a64 64 0 0 1-64-64V384h682.666666z m-384 64h-170.666666v64h170.666666v-64zM733.482667 170.666667a64 64 0 0 1 57.237333 35.370666l55.850667 111.68 1.024 2.282667H178.816l54.698667-113.173333A64 64 0 0 1 291.136 170.666667h442.346667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconProductFill.defaultProps = {
  size: 1,
};

export default IconProductFill;
