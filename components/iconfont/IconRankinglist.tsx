/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconRankinglist: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M512 320c153.173333 0 277.333333 124.16 277.333333 277.333333s-124.16 277.333333-277.333333 277.333334-277.333333-124.16-277.333333-277.333334 124.16-277.333333 277.333333-277.333333z m0 64c-117.824 0-213.333333 95.509333-213.333333 213.333333s95.509333 213.333333 213.333333 213.333334 213.333333-95.509333 213.333333-213.333334-95.509333-213.333333-213.333333-213.333333z m0 64a149.333333 149.333333 0 1 1 0 298.666667 149.333333 149.333333 0 0 1 0-298.666667z m0 64a85.333333 85.333333 0 1 0 0 170.666667 85.333333 85.333333 0 0 0 0-170.666667zM736 160a64 64 0 0 1 64 64v109.248l-52.053333 52.053333-45.226667-45.269333 33.28-33.28V224h-85.333333V277.333333h-64v-53.333333h-149.333334V277.333333h-64v-53.333333h-85.333333v82.752l33.28 33.28-45.226667 45.269333-52.053333-52.053333V224a64 64 0 0 1 64-64z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconRankinglist.defaultProps = {
  size: 1,
};

export default IconRankinglist;
