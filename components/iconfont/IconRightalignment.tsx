/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconRightalignment: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M810.666667 768v64H213.333333v-64h597.333334z m0-192v64H554.666667v-64h256z m0-192v64H384v-64h426.666667z m0-192v64H213.333333V192h597.333334z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconRightalignment.defaultProps = {
  size: 1,
};

export default IconRightalignment;
