/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconRmb1: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M675.861333 192l45.610667 44.906667-166.997333 169.6h237.525333v64H544v144.597333h248v64H544v176.597333h-64v-176.597333H232v-64H480v-144.597333H232v-64h237.482667l-166.954667-169.6L348.138667 192 512 358.464 675.861333 192z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconRmb1.defaultProps = {
  size: 1,
};

export default IconRmb1;
