/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconSalescenterFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M682.666667 149.333333a128 128 0 1 1-91.093334 217.941334l-89.173333 66.858666c5.973333 17.024 9.344 35.264 9.6 54.250667l146.517333 48.853333A106.666667 106.666667 0 1 1 640 598.272l-0.042667 0.234667-139.349333-46.421334a170.944 170.944 0 0 1-44.074667 64.512l66.048 88.064a85.333333 85.333333 0 1 1-56.213333 31.765334l-64.554667-86.122667a170.666667 170.666667 0 1 1 68.138667-271.829333l89.322667-66.986667A128 128 0 0 1 682.666667 149.333333z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconSalescenterFill.defaultProps = {
  size: 1,
};

export default IconSalescenterFill;
