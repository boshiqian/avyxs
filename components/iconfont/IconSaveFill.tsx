/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconSaveFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M320 170.666667v213.333333h384V191.381333l149.333333 153.322667V789.333333a64 64 0 0 1-64 64h-85.333333V533.333333H320v320h-85.333333a64 64 0 0 1-64-64V234.666667a64 64 0 0 1 64-64h85.333333z m320 448v234.666666H384V618.666667h256z m-42.666667 42.666666h-170.666666v64h170.666666v-64z m42.666667-490.666666v149.333333H384V170.666667h256z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconSaveFill.defaultProps = {
  size: 1,
};

export default IconSaveFill;
