/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconScanning: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M234.666667 640v149.333333h159.978666v64H234.666667a64 64 0 0 1-64-64v-149.333333h64z m618.666666 0v149.333333a64 64 0 0 1-64 64h-160.021333v-64H789.333333v-149.333333h64z m-85.333333-160v64H256v-64h512zM789.333333 170.666667a64 64 0 0 1 64 64v149.333333h-64v-149.333333h-160V170.666667H789.333333zM394.666667 170.666667v64H234.666667v149.333333H170.666667v-149.333333a64 64 0 0 1 64-64h160z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconScanning.defaultProps = {
  size: 1,
};

export default IconScanning;
