/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconShangchuanbeiandanzheng: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M341.333333 192v63.978667L298.666667 256v533.333333h213.333333v64H298.666667a64 64 0 0 1-64-64V256a64 64 0 0 1 64-64h42.666666z m344.469334 402.517333l109.696 109.696-45.248 45.248-32.853334-32.874666-0.021333 134.122666h-64V717.44l-32 32.021333-45.269333-45.226666 109.696-109.717334zM554.666667 533.333333v64h-192v-64h192z m170.666666-341.333333a64 64 0 0 1 64 64l-0.021333 298.666667h-64L725.333333 256h-42.666666V192h42.666666z m-64 213.333333v64H362.666667v-64h298.666666zM576 128a64 64 0 0 1 64 64v42.666667a64 64 0 0 1-64 64h-128a64 64 0 0 1-64-64V192a64 64 0 0 1 64-64h128z m0 64h-128v42.666667h128V192z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconShangchuanbeiandanzheng.defaultProps = {
  size: 1,
};

export default IconShangchuanbeiandanzheng;
