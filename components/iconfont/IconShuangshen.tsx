/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconShuangshen: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M814.122667 273.877333a64 64 0 0 1 64 64V789.12a64 64 0 0 1-64 64H341.12a64 64 0 0 1-64-64V273.877333z m0 64H341.12V789.12h473.002667V337.877333zM672 170.666667v64H234.666667v490.666666H170.666667V266.666667A96 96 0 0 1 266.666667 170.666667h405.333333z m-122.88 234.88a133.12 133.12 0 0 1 121.386667 187.904l77.44 77.44-45.269334 45.269333-72.149333-72.149333A133.12 133.12 0 1 1 549.12 405.546667z m0 64a69.12 69.12 0 1 0 0 138.24 69.12 69.12 0 0 0 0-138.24z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconShuangshen.defaultProps = {
  size: 1,
};

export default IconShuangshen;
