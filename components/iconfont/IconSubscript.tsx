/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconSubscript: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M842.666667 586.666667a42.666667 42.666667 0 0 1 42.666666 42.666666v213.333334a42.666667 42.666667 0 0 1-42.666666 42.666666h-213.333334a42.666667 42.666667 0 0 1-42.666666-42.666666v-213.333334a42.666667 42.666667 0 0 1 42.666666-42.666666h213.333334zM736 170.666667v117.333333h-64V234.666667H490.666667v565.312l53.333333 0.021333v42.666667c0 7.381333 0.938667 14.506667 2.688 21.333333H352v-64l74.666667-0.021333V234.666667h-192v53.333333H170.666667V170.666667h565.333333z m85.333333 480h-170.666666v170.666666h170.666666v-170.666666z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconSubscript.defaultProps = {
  size: 1,
};

export default IconSubscript;
