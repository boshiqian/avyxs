/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconSuperscript: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M725.333333 160V277.333333h-64v-53.333333h-181.333333V789.333333H554.666667v64H341.333333v-64h74.666667V224h-192V277.333333h-64V160H725.333333z m106.666667 160a42.666667 42.666667 0 0 1 42.666667 42.666667v213.333333a42.666667 42.666667 0 0 1-42.666667 42.666667H618.666667a42.666667 42.666667 0 0 1-42.666667-42.666667V362.666667a42.666667 42.666667 0 0 1 42.666667-42.666667h213.333333z m-21.333333 64h-170.666667v170.666667h170.666667v-170.666667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconSuperscript.defaultProps = {
  size: 1,
};

export default IconSuperscript;
