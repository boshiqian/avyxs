/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconSuspendedFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M512 128c200.298667 0 362.666667 162.368 362.666667 362.666667s-162.368 362.666667-362.666667 362.666666S149.333333 690.965333 149.333333 490.666667 311.701333 128 512 128z m-42.666667 234.666667h-64v277.333333h64V362.666667z m138.666667 0h-64v277.333333h64V362.666667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconSuspendedFill.defaultProps = {
  size: 1,
};

export default IconSuspendedFill;
