/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconTaskManagementFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M512 243.712c164.949333 0 298.666667 133.717333 298.666667 298.666667s-133.717333 298.666667-298.666667 298.666666-298.666667-133.717333-298.666667-298.666666 133.717333-298.666667 298.666667-298.666667z m27.477333 123.157333h-64v203.626667c0 28.522667 34.474667 42.794667 54.613334 22.634667l124.458666-124.437334-45.248-45.269333-69.824 69.824v-126.378667zM620.586667 139.306667v67.882666H394.346667V139.306667H620.586667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconTaskManagementFill.defaultProps = {
  size: 1,
};

export default IconTaskManagementFill;
