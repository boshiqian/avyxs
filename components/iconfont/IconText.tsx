/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconText: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M725.333333 224V341.333333h-64v-53.333333h-181.333333V768H554.666667v64H341.333333v-64h74.666667V288h-192V341.333333h-64v-117.333333H725.333333zM853.333333 768v64H597.333333v-64h256z m0-128v64H597.333333v-64h256z m0-128v64H597.333333v-64h256z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconText.defaultProps = {
  size: 1,
};

export default IconText;
