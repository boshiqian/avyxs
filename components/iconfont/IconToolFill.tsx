/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconToolFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M874.666667 533.333333v256a64 64 0 0 1-64 64H213.333333a64 64 0 0 1-64-64V533.333333h256v128h213.333334v-128h256z m-320 0v64h-85.333334v-64h85.333334z m85.333333-384a64 64 0 0 1 64 64v170.666667h42.666667v-85.333333h64a64 64 0 0 1 64 64v106.666666H149.333333v-106.666666a64 64 0 0 1 64-64h64v85.333333h42.666667v-170.666667a64 64 0 0 1 64-64h256z m0 64H384v85.333334h256v-85.333334z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconToolFill.defaultProps = {
  size: 1,
};

export default IconToolFill;
