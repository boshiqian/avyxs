/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconTradingdata: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M224 224V768H832v64H256a96 96 0 0 1-95.893333-91.477333L160 736v-512h64z m160 149.333333v325.333334h-64V373.333333h64z m298.666667 133.333334v192h-64v-192h64zM533.333333 245.333333v453.333334h-64V245.333333h64z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconTradingdata.defaultProps = {
  size: 1,
};

export default IconTradingdata;
