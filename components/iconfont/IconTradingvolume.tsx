/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconTradingvolume: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M245.333333 245.333333V768H810.666667v64H277.333333a96 96 0 0 1-96-96v-490.666667h64z m581.141334-42.666666V426.666667h-64v-111.082667l-194.688 199.701333-61.653334-63.872-141.781333 146.816-46.037333-44.458666 187.797333-194.517334 61.909333 64.128 152.746667-156.714666h-102.464v-64h208.170667z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconTradingvolume.defaultProps = {
  size: 1,
};

export default IconTradingvolume;
