/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconTraining1: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M874.666667 170.666667v64h-42.666667v426.666666c0 35.349333-30.72 64-68.565333 64h-149.354667l113.749333 128h-85.632l-113.770666-128h-11.562667l-113.749333 128h-85.610667l113.728-128h-170.666667C222.72 725.333333 192 696.682667 192 661.333333V234.666667H149.333333V170.666667h725.333334zM405.333333 469.333333h-64v64h64v-64z m277.333334 0H448v64h234.666667v-64z m0-106.666666H448v64h234.666667v-64z m-277.333334 0h-64v64h64v-64z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconTraining1.defaultProps = {
  size: 1,
};

export default IconTraining1;
