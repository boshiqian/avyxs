/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconVideo1: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M658.069333 256a64 64 0 0 1 64 64l-0.021333 33.664 49.28-38.4A64 64 0 0 1 874.666667 365.781333v338.368a64 64 0 0 1-103.338667 50.474667l-49.28-38.4v26.496a64 64 0 0 1-64 64H213.333333a64 64 0 0 1-64-64V320a64 64 0 0 1 64-64h444.736z"
        fill={getIconColor(color, 0, '#333333')}
      />
      <path
        d="M376.106667 638.933333l132.224-88.981333a21.333333 21.333333 0 0 0-0.170667-35.498667l-132.202667-87.274666a21.333333 21.333333 0 0 0-33.088 17.792v176.277333a21.333333 21.333333 0 0 0 33.237334 17.706667z"
        fill={getIconColor(color, 1, '#333333')}
      />
    </svg>
  );
};

IconVideo1.defaultProps = {
  size: 1,
};

export default IconVideo1;
