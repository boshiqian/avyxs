/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconViewgallery: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M426.666667 170.666667a64 64 0 0 1 64 64v554.666666a64 64 0 0 1-64 64h-192a64 64 0 0 1-64-64V234.666667a64 64 0 0 1 64-64h192z m362.666666 362.666666a64 64 0 0 1 64 64v192a64 64 0 0 1-64 64h-192a64 64 0 0 1-64-64v-192a64 64 0 0 1 64-64h192zM426.666667 234.666667h-192v554.666666h192V234.666667z m362.666666 362.666666h-192v192h192v-192z m0-426.666666a64 64 0 0 1 64 64v192a64 64 0 0 1-64 64h-192a64 64 0 0 1-64-64v-192a64 64 0 0 1 64-64h192z m0 64h-192v192h192v-192z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconViewgallery.defaultProps = {
  size: 1,
};

export default IconViewgallery;
