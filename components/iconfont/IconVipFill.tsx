/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconVipFill: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M552.96 200.128a64 64 0 0 1 8.213333 8.213333L682.666667 354.133333l134.016-98.965333a64 64 0 0 1 100.117333 66.986667L789.333333 832H234.666667L107.2 322.176a64 64 0 0 1 100.117333-67.008L341.333333 354.133333l121.493334-145.792a64 64 0 0 1 90.154666-8.213333zM661.333333 649.962667H362.666667v68.266666h298.666666v-68.266666z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconVipFill.defaultProps = {
  size: 1,
};

export default IconVipFill;
