/* tslint:disable */
/* eslint-disable */

import React, { CSSProperties, SVGAttributes, FunctionComponent } from 'react';
import { getIconColor } from './helper';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  size?: number;
  color?: string | string[];
}

const DEFAULT_STYLE: CSSProperties = {
  display: 'inline-block',
};

const IconWallet1: FunctionComponent<Props> = ({ size, color, style: _style, ...rest }) => {
  const style = _style ? { ...DEFAULT_STYLE, ..._style } : DEFAULT_STYLE;

  return (
    <svg viewBox="0 0 1024 1024" width={size + 'rem'} height={size + 'rem'} style={style} {...rest}>
      <path
        d="M639.42656 204.8a61.44 61.44 0 0 1 61.44 61.44l-0.02048 41.86112h62.54592a61.44 61.44 0 0 1 61.44 61.44l-0.02048 103.85408H535.57248v165.29408h289.23904v103.85408a61.44 61.44 0 0 1-61.44 61.44H245.76a61.44 61.44 0 0 1-61.44-61.44V307.2a102.4 102.4 0 0 1 102.4-102.4h352.70656z m182.86592 330.0352v42.41408h-225.28v-42.3936h225.28zM639.3856 266.24H286.72a40.96 40.96 0 0 0-40.96 40.96l-0.02048 0.90112h393.66656V266.24z"
        fill={getIconColor(color, 0, '#333333')}
      />
    </svg>
  );
};

IconWallet1.defaultProps = {
  size: 1,
};

export default IconWallet1;
