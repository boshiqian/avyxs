/* tslint:disable */
/* eslint-disable */

import React, { SVGAttributes, FunctionComponent } from 'react';
import IconAndorid from './IconAndorid';
import IconApple from './IconApple';
import IconSport from './IconSport';
import IconSubscribe from './IconSubscribe';
import IconCreditcard from './IconCreditcard';
import IconBecomeagoldsupplier from './IconBecomeagoldsupplier';
import IconContacts from './IconContacts';
import IconNew from './IconNew';
import IconCheckstand from './IconCheckstand';
import IconFree from './IconFree';
import IconAviation from './IconAviation';
import IconCadFill from './IconCadFill';
import IconDaytimemode from './IconDaytimemode';
import IconRobot from './IconRobot';
import IconInfantmom from './IconInfantmom';
import IconInspection1 from './IconInspection1';
import IconDiscounts from './IconDiscounts';
import IconBlock from './IconBlock';
import IconInvoice from './IconInvoice';
import IconShouhuoicon from './IconShouhuoicon';
import IconInsurance from './IconInsurance';
import IconNightmode from './IconNightmode';
import IconUsercenter from './IconUsercenter';
import IconUnlock from './IconUnlock';
import IconVip from './IconVip';
import IconWallet from './IconWallet';
import IconLandtransportation from './IconLandtransportation';
import IconVoice from './IconVoice';
import IconExchangerate from './IconExchangerate';
import IconContactsFill from './IconContactsFill';
import IconAddAccount1 from './IconAddAccount1';
import IconYearsFill from './IconYearsFill';
import IconAddCartFill from './IconAddCartFill';
import IconAddFill from './IconAddFill';
import IconAllFill1 from './IconAllFill1';
import IconAshbinFill from './IconAshbinFill';
import IconCalendarFill from './IconCalendarFill';
import IconBadFill from './IconBadFill';
import IconBussinessManFill from './IconBussinessManFill';
import IconAtmFill from './IconAtmFill';
import IconCartFullFill from './IconCartFullFill';
import IconCartEmptyFill from './IconCartEmptyFill';
import IconCameraswitchingFill from './IconCameraswitchingFill';
import IconAtmAwayFill from './IconAtmAwayFill';
import IconCertifiedSupplierFill from './IconCertifiedSupplierFill';
import IconCalculatorFill from './IconCalculatorFill';
import IconClockFill from './IconClockFill';
import IconAliClouldFill from './IconAliClouldFill';
import IconColorFill from './IconColorFill';
import IconCouponsFill from './IconCouponsFill';
import IconCecurityProtectionFill from './IconCecurityProtectionFill';
import IconCreditLevelFill from './IconCreditLevelFill';
import IconDefaultTemplateFill from './IconDefaultTemplateFill';
import IconAll from './IconAll';
import IconCurrencyConverterFill from './IconCurrencyConverterFill';
import IconBussinessMan from './IconBussinessMan';
import IconCustomermanagementFill from './IconCustomermanagementFill';
import IconComponent from './IconComponent';
import IconDiscountsFill from './IconDiscountsFill';
import IconCode from './IconCode';
import IconDaytimemodeFill from './IconDaytimemodeFill';
import IconCopy from './IconCopy';
import IconExlFill from './IconExlFill';
import IconDollar from './IconDollar';
import IconCryFill from './IconCryFill';
import IconHistory from './IconHistory';
import IconEmailFill from './IconEmailFill';
import IconEditor from './IconEditor';
import IconFilterFill from './IconFilterFill';
import IconData from './IconData';
import IconFolderFill from './IconFolderFill';
import IconGift from './IconGift';
import IconFeedsFill from './IconFeedsFill';
import IconIntegral from './IconIntegral';
import IconGoldSupplieFill from './IconGoldSupplieFill';
import IconNavList from './IconNavList';
import IconFormFill from './IconFormFill';
import IconPic from './IconPic';
import IconCameraFill from './IconCameraFill';
import IconNotvisible from './IconNotvisible';
import IconGoodFill from './IconGoodFill';
import IconPlay from './IconPlay';
import IconImageTextFill from './IconImageTextFill';
import IconRising from './IconRising';
import IconInspectionFill from './IconInspectionFill';
import IconQRcode from './IconQRcode';
import IconHotFill from './IconHotFill';
import IconRmb from './IconRmb';
import IconCompanyFill from './IconCompanyFill';
import IconSimilarProduct from './IconSimilarProduct';
import IconDiscountFill from './IconDiscountFill';
import IconExportservices from './IconExportservices';
import IconInsuranceFill from './IconInsuranceFill';
import IconSendinquiry from './IconSendinquiry';
import IconInquiryTemplateFill from './IconInquiryTemplateFill';
import IconAllFill from './IconAllFill';
import IconLeftbuttonFill from './IconLeftbuttonFill';
import IconFavoritesFill from './IconFavoritesFill';
import IconIntegralFill1 from './IconIntegralFill1';
import IconIntegralFill from './IconIntegralFill';
import IconHelp1 from './IconHelp1';
import IconNamecardFill from './IconNamecardFill';
import IconListingContentFill from './IconListingContentFill';
import IconPicFill from './IconPicFill';
import IconLogisticLogoFill from './IconLogisticLogoFill';
import IconPlayFill from './IconPlayFill';
import IconMoneymanagementFill from './IconMoneymanagementFill';
import IconPromptFill from './IconPromptFill';
import IconManageOrderFill from './IconManageOrderFill';
import IconStopFill from './IconStopFill';
import IconMultiLanguageFill from './IconMultiLanguageFill';
import IconColumn from './IconColumn';
import IconLogisticsIconFill from './IconLogisticsIconFill';
import IconAddAccount from './IconAddAccount';
import IconNewuserzoneFill from './IconNewuserzoneFill';
import IconColumn1 from './IconColumn1';
import IconNightmodeFill from './IconNightmodeFill';
import IconAdd from './IconAdd';
import IconOfficeSuppliesFill from './IconOfficeSuppliesFill';
import IconAgriculture from './IconAgriculture';
import IconNoticeFill from './IconNoticeFill';
import IconYears from './IconYears';
import IconMute from './IconMute';
import IconAddCart from './IconAddCart';
import IconOrderFill from './IconOrderFill';
import IconArrowRight from './IconArrowRight';
import IconPassword1 from './IconPassword1';
import IconArrowLeft from './IconArrowLeft';
import IconMap1 from './IconMap1';
import IconApparel from './IconApparel';
import IconPaylaterFill from './IconPaylaterFill';
import IconAll1 from './IconAll1';
import IconPhoneFill from './IconPhoneFill';
import IconArrowUp from './IconArrowUp';
import IconOnlineTrackingFill from './IconOnlineTrackingFill';
import IconAscending from './IconAscending';
import IconPlayFill1 from './IconPlayFill1';
import IconAshbin from './IconAshbin';
import IconPdfFill from './IconPdfFill';
import IconAtm from './IconAtm';
import IconPhone1 from './IconPhone1';
import IconBad from './IconBad';
import IconPinFill from './IconPinFill';
import IconAttachent from './IconAttachent';
import IconProductFill from './IconProductFill';
import IconBrowse from './IconBrowse';
import IconRankinglistFill from './IconRankinglistFill';
import IconBeauty from './IconBeauty';
import IconReduceFill from './IconReduceFill';
import IconAtmAway from './IconAtmAway';
import IconReeorFill from './IconReeorFill';
import IconAssessedBadge from './IconAssessedBadge';
import IconPicFill1 from './IconPicFill1';
import IconAuto from './IconAuto';
import IconRankinglist from './IconRankinglist';
import IconBags from './IconBags';
import IconProduct1 from './IconProduct1';
import IconCalendar from './IconCalendar';
import IconPromptFill1 from './IconPromptFill1';
import IconCartFull from './IconCartFull';
import IconResonserateFill from './IconResonserateFill';
import IconCalculator from './IconCalculator';
import IconRemindFill from './IconRemindFill';
import IconCameraswitching from './IconCameraswitching';
import IconRightbuttonFill from './IconRightbuttonFill';
import IconCecurityProtection from './IconCecurityProtection';
import IconRfqLogoFill from './IconRfqLogoFill';
import IconCategory from './IconCategory';
import IconRfqWordFill from './IconRfqWordFill';
import IconClose from './IconClose';
import IconSearchcartFill from './IconSearchcartFill';
import IconCertifiedSupplier from './IconCertifiedSupplier';
import IconSalescenterFill from './IconSalescenterFill';
import IconCartEmpty from './IconCartEmpty';
import IconSaveFill from './IconSaveFill';
import IconCode1 from './IconCode1';
import IconSecurityFill from './IconSecurityFill';
import IconColor from './IconColor';
import IconSimilarproductsFill from './IconSimilarproductsFill';
import IconConditions from './IconConditions';
import IconSignboardFill from './IconSignboardFill';
import IconConfirm from './IconConfirm';
import IconServiceFill from './IconServiceFill';
import IconCompany from './IconCompany';
import IconShufflingBannerFill from './IconShufflingBannerFill';
import IconAliClould from './IconAliClould';
import IconSupplierFeaturesFill from './IconSupplierFeaturesFill';
import IconCopy1 from './IconCopy1';
import IconStoreFill from './IconStoreFill';
import IconCreditLevel from './IconCreditLevel';
import IconSmileFill from './IconSmileFill';
import IconCoupons from './IconCoupons';
import IconSuccessFill from './IconSuccessFill';
import IconConnections from './IconConnections';
import IconSoundFillingFill from './IconSoundFillingFill';
import IconCry from './IconCry';
import IconSoundMute1 from './IconSoundMute1';
import IconCostomsAlearance from './IconCostomsAlearance';
import IconSuspendedFill from './IconSuspendedFill';
import IconClock from './IconClock';
import IconToolFill from './IconToolFill';
import IconCurrencyConverter from './IconCurrencyConverter';
import IconTaskManagementFill from './IconTaskManagementFill';
import IconCut from './IconCut';
import IconUnlockFill from './IconUnlockFill';
import IconData1 from './IconData1';
import IconTrustFill from './IconTrustFill';
import IconCustomermanagement from './IconCustomermanagement';
import IconVipFill from './IconVipFill';
import IconDescending from './IconDescending';
import IconSet1 from './IconSet1';
import IconDoubleArroRight from './IconDoubleArroRight';
import IconTopFill from './IconTopFill';
import IconCustomization from './IconCustomization';
import IconViewlarger1 from './IconViewlarger1';
import IconDoubleArrowLeft from './IconDoubleArrowLeft';
import IconVoiceFill from './IconVoiceFill';
import IconDiscount from './IconDiscount';
import IconWarningFill from './IconWarningFill';
import IconDownload from './IconDownload';
import IconWarehouseFill from './IconWarehouseFill';
import IconDollar1 from './IconDollar1';
import IconZipFill from './IconZipFill';
import IconDefaultTemplate from './IconDefaultTemplate';
import IconTradeAssuranceFill from './IconTradeAssuranceFill';
import IconEditor1 from './IconEditor1';
import IconVsFill from './IconVsFill';
import IconEletrical from './IconEletrical';
import IconVideo1 from './IconVideo1';
import IconElectronics from './IconElectronics';
import IconTemplateFill from './IconTemplateFill';
import IconEtricalEquipm from './IconEtricalEquipm';
import IconWallet1 from './IconWallet1';
import IconEllipsis from './IconEllipsis';
import IconTraining1 from './IconTraining1';
import IconEmail from './IconEmail';
import IconPackingLabelingFill from './IconPackingLabelingFill';
import IconFalling from './IconFalling';
import IconExportservicesFill from './IconExportservicesFill';
import IconEarth from './IconEarth';
import IconBrandFill from './IconBrandFill';
import IconFilter from './IconFilter';
import IconCollection from './IconCollection';
import IconFurniture from './IconFurniture';
import IconConsumptionFill from './IconConsumptionFill';
import IconFolder from './IconFolder';
import IconCollectionFill from './IconCollectionFill';
import IconFeeds from './IconFeeds';
import IconBrand from './IconBrand';
import IconHistory1 from './IconHistory1';
import IconRejectedOrderFill from './IconRejectedOrderFill';
import IconHardware from './IconHardware';
import IconHomepageAdsFill from './IconHomepageAdsFill';
import IconHelp from './IconHelp';
import IconHomepageAds from './IconHomepageAds';
import IconGood from './IconGood';
import IconScenesFill from './IconScenesFill';
import IconHouseholdappliances from './IconHouseholdappliances';
import IconScenes from './IconScenes';
import IconGift1 from './IconGift1';
import IconSimilarProductFill from './IconSimilarProductFill';
import IconForm from './IconForm';
import IconTopraningFill from './IconTopraningFill';
import IconImageText from './IconImageText';
import IconConsumption from './IconConsumption';
import IconHot from './IconHot';
import IconTopraning from './IconTopraning';
import IconInspection from './IconInspection';
import IconGoldSupplier from './IconGoldSupplier';
import IconLeftbutton from './IconLeftbutton';
import IconMessagecenterFill from './IconMessagecenterFill';
import IconJewelry from './IconJewelry';
import IconQuick from './IconQuick';
import IconIpad from './IconIpad';
import IconWriting from './IconWriting';
import IconLeftarrow from './IconLeftarrow';
import IconDocjpgeFill from './IconDocjpgeFill';
import IconIntegral1 from './IconIntegral1';
import IconJpgeFill from './IconJpgeFill';
import IconKitchen from './IconKitchen';
import IconGifjpgeFill from './IconGifjpgeFill';
import IconInquiryTemplate from './IconInquiryTemplate';
import IconBmpjpgeFill from './IconBmpjpgeFill';
import IconLink from './IconLink';
import IconTifjpgeFill from './IconTifjpgeFill';
import IconLibra from './IconLibra';
import IconPngjpgeFill from './IconPngjpgeFill';
import IconLoading from './IconLoading';
import IconHometextile from './IconHometextile';
import IconListingContent from './IconListingContent';
import IconHome from './IconHome';
import IconLights from './IconLights';
import IconSendinquiryFill from './IconSendinquiryFill';
import IconLogisticsIcon from './IconLogisticsIcon';
import IconCommentsFill from './IconCommentsFill';
import IconMessagecenter from './IconMessagecenter';
import IconAccountFill from './IconAccountFill';
import IconMobilePhone from './IconMobilePhone';
import IconFeedLogoFill from './IconFeedLogoFill';
import IconManageOrder from './IconManageOrder';
import IconFeedLogo from './IconFeedLogo';
import IconMove from './IconMove';
import IconHomeFill from './IconHomeFill';
import IconMoneymanagement from './IconMoneymanagement';
import IconAddSelect from './IconAddSelect';
import IconNamecard from './IconNamecard';
import IconSamiSelect from './IconSamiSelect';
import IconMap from './IconMap';
import IconCamera from './IconCamera';
import IconNewuserzone from './IconNewuserzone';
import IconArrowDown from './IconArrowDown';
import IconMultiLanguage from './IconMultiLanguage';
import IconAccount from './IconAccount';
import IconOffice from './IconOffice';
import IconComments from './IconComments';
import IconNotice from './IconNotice';
import IconCartEmpty1 from './IconCartEmpty1';
import IconOntimeshipment from './IconOntimeshipment';
import IconFavorites from './IconFavorites';
import IconOfficeSupplies from './IconOfficeSupplies';
import IconOrder from './IconOrder';
import IconPassword from './IconPassword';
import IconSearch from './IconSearch';
import IconNotvisible1 from './IconNotvisible1';
import IconTradeAssurance from './IconTradeAssurance';
import IconOperation from './IconOperation';
import IconUsercenter1 from './IconUsercenter1';
import IconPackaging from './IconPackaging';
import IconTradingdata from './IconTradingdata';
import IconOnlineTracking from './IconOnlineTracking';
import IconMicrophone from './IconMicrophone';
import IconPackingLabeling from './IconPackingLabeling';
import IconTxt from './IconTxt';
import IconPhone from './IconPhone';
import IconXlsx from './IconXlsx';
import IconPic1 from './IconPic1';
import IconBanzhengfuwu from './IconBanzhengfuwu';
import IconPin from './IconPin';
import IconCangku from './IconCangku';
import IconPlay1 from './IconPlay1';
import IconDaibancaishui from './IconDaibancaishui';
import IconLogisticLogo from './IconLogisticLogo';
import IconJizhuangxiang from './IconJizhuangxiang';
import IconPrint from './IconPrint';
import IconJiaobiao from './IconJiaobiao';
import IconProduct from './IconProduct';
import IconKehupandian from './IconKehupandian';
import IconMachinery from './IconMachinery';
import IconDongtai from './IconDongtai';
import IconProcess from './IconProcess';
import IconDaikuan from './IconDaikuan';
import IconPrompt from './IconPrompt';
import IconShengyijing from './IconShengyijing';
import IconQRcode1 from './IconQRcode1';
import IconJiehui from './IconJiehui';
import IconReeor from './IconReeor';
import IconFencengpeizhi from './IconFencengpeizhi';
import IconReduce from './IconReduce';
import IconShenqingjilu from './IconShenqingjilu';
import IconNonStaplefood from './IconNonStaplefood';
import IconShangchuanbeiandanzheng from './IconShangchuanbeiandanzheng';
import IconRejectedOrder from './IconRejectedOrder';
import IconShangchuan from './IconShangchuan';
import IconResonserate from './IconResonserate';
import IconKehuquanyi from './IconKehuquanyi';
import IconRemind from './IconRemind';
import IconSuoxiao from './IconSuoxiao';
import IconResponsetime from './IconResponsetime';
import IconQuanyipeizhi from './IconQuanyipeizhi';
import IconReturn from './IconReturn';
import IconShuangshen from './IconShuangshen';
import IconPaylater from './IconPaylater';
import IconTongguan from './IconTongguan';
import IconRising1 from './IconRising1';
import IconTuishui from './IconTuishui';
import IconRightarrow from './IconRightarrow';
import IconTongguanshuju from './IconTongguanshuju';
import IconRmb1 from './IconRmb1';
import IconKuaidiwuliu from './IconKuaidiwuliu';
import IconRfqLogo from './IconRfqLogo';
import IconWuliuchanpin from './IconWuliuchanpin';
import IconSave from './IconSave';
import IconWaihuishuju from './IconWaihuishuju';
import IconScanning from './IconScanning';
import IconXinxibarShouji from './IconXinxibarShouji';
import IconSecurity from './IconSecurity';
import IconXinwaizongyewu from './IconXinwaizongyewu';
import IconSalescenter from './IconSalescenter';
import IconWuliudingdan from './IconWuliudingdan';
import IconSeleted from './IconSeleted';
import IconZhongjianren from './IconZhongjianren';
import IconSearchcart from './IconSearchcart';
import IconXinxibarZhanghu from './IconXinxibarZhanghu';
import IconRaw from './IconRaw';
import IconYidatong from './IconYidatong';
import IconService from './IconService';
import IconZhuanyequanwei from './IconZhuanyequanwei';
import IconShare from './IconShare';
import IconZhanghucaozuo from './IconZhanghucaozuo';
import IconSignboard from './IconSignboard';
import IconXuanzhuandu from './IconXuanzhuandu';
import IconShufflingBanner from './IconShufflingBanner';
import IconTuishuirongzi from './IconTuishuirongzi';
import IconRightbutton from './IconRightbutton';
import IconAddProducts from './IconAddProducts';
import IconSorting from './IconSorting';
import IconZiyingyewu from './IconZiyingyewu';
import IconSoundMute from './IconSoundMute';
import IconAddcell from './IconAddcell';
import IconSimilarproducts from './IconSimilarproducts';
import IconBackgroundColor from './IconBackgroundColor';
import IconSoundFilling from './IconSoundFilling';
import IconCascades from './IconCascades';
import IconSuggest from './IconSuggest';
import IconBeijing from './IconBeijing';
import IconStop from './IconStop';
import IconBold from './IconBold';
import IconSuccess from './IconSuccess';
import IconZijin from './IconZijin';
import IconSupplierFeatures from './IconSupplierFeatures';
import IconEraser from './IconEraser';
import IconSwitch from './IconSwitch';
import IconCenteralignment from './IconCenteralignment';
import IconSurvey from './IconSurvey';
import IconClick from './IconClick';
import IconTemplate from './IconTemplate';
import IconAspjiesuan from './IconAspjiesuan';
import IconText from './IconText';
import IconFlag from './IconFlag';
import IconSuspended from './IconSuspended';
import IconFalgFill from './IconFalgFill';
import IconTaskManagement from './IconTaskManagement';
import IconFee from './IconFee';
import IconTool from './IconTool';
import IconFilling from './IconFilling';
import IconTop from './IconTop';
import IconForeigncurrency from './IconForeigncurrency';
import IconSmile from './IconSmile';
import IconGuanliyuan from './IconGuanliyuan';
import IconTextileProducts from './IconTextileProducts';
import IconLanguage from './IconLanguage';
import IconTradealert from './IconTradealert';
import IconLeftalignment from './IconLeftalignment';
import IconTopsales from './IconTopsales';
import IconExtraInquiries from './IconExtraInquiries';
import IconTradingvolume from './IconTradingvolume';
import IconItalic from './IconItalic';
import IconTraining from './IconTraining';
import IconPcm from './IconPcm';
import IconUpload from './IconUpload';
import IconReducecell from './IconReducecell';
import IconRfqWord from './IconRfqWord';
import IconRightalignment from './IconRightalignment';
import IconViewlarger from './IconViewlarger';
import IconPointerleft from './IconPointerleft';
import IconViewgallery from './IconViewgallery';
import IconSubscript from './IconSubscript';
import IconVehivles from './IconVehivles';
import IconSquare from './IconSquare';
import IconTrust from './IconTrust';
import IconSuperscript from './IconSuperscript';
import IconWarning from './IconWarning';
import IconTagSubscript from './IconTagSubscript';
import IconWarehouse from './IconWarehouse';
import IconDanjuzhuanhuan from './IconDanjuzhuanhuan';
import IconShoes from './IconShoes';
import IconTransfermoney from './IconTransfermoney';
import IconVideo from './IconVideo';
import IconUnderLine from './IconUnderLine';
import IconViewlist from './IconViewlist';
import IconXiakuangxian from './IconXiakuangxian';
import IconSet from './IconSet';
import IconShouqi from './IconShouqi';
import IconStore from './IconStore';
import IconZhankai from './IconZhankai';
import IconToolHardware from './IconToolHardware';
import IconTongxunlu from './IconTongxunlu';
import IconVs from './IconVs';
import IconYiguanzhugongyingshang from './IconYiguanzhugongyingshang';
import IconToy from './IconToy';
import IconGoumaipianhao from './IconGoumaipianhao';

export type IconNames = 'andorid' | 'apple' | 'sport' | 'Subscribe' | 'creditcard' | 'becomeagoldsupplier' | 'contacts' | 'new' | 'checkstand' | 'free' | 'aviation' | 'cad-fill' | 'Daytimemode' | 'robot' | 'infantmom' | 'inspection1' | 'discounts' | 'block' | 'invoice' | 'shouhuoicon' | 'insurance' | 'nightmode' | 'usercenter' | 'unlock' | 'vip' | 'wallet' | 'landtransportation' | 'voice' | 'exchangerate' | 'contacts-fill' | 'add-account1' | 'years-fill' | 'add-cart-fill' | 'add-fill' | 'all-fill1' | 'ashbin-fill' | 'calendar-fill' | 'bad-fill' | 'bussiness-man-fill' | 'atm-fill' | 'cart-full-fill' | 'cart-Empty-fill' | 'cameraswitching-fill' | 'atm-away-fill' | 'certified-supplier-fill' | 'calculator-fill' | 'clock-fill' | 'ali-clould-fill' | 'color-fill' | 'coupons-fill' | 'cecurity-protection-fill' | 'credit-level-fill' | 'default-template-fill' | 'all' | 'CurrencyConverter-fill' | 'bussiness-man' | 'Customermanagement-fill' | 'component' | 'discounts-fill' | 'code' | 'Daytimemode-fill' | 'copy' | 'exl-fill' | 'dollar' | 'cry-fill' | 'history' | 'email-fill' | 'editor' | 'filter-fill' | 'data' | 'folder-fill' | 'gift' | 'feeds-fill' | 'integral' | 'gold-supplie-fill' | 'nav-list' | 'form-fill' | 'pic' | 'camera-fill' | 'Notvisible' | 'good-fill' | 'play' | 'image-text-fill' | 'rising' | 'inspection-fill' | 'QRcode' | 'hot-fill' | 'rmb' | 'company-fill' | 'similar-product' | 'discount-fill' | 'Exportservices' | 'insurance-fill' | 'sendinquiry' | 'inquiry-template-fill' | 'all-fill' | 'leftbutton-fill' | 'favorites-fill' | 'integral-fill1' | 'integral-fill' | 'help1' | 'namecard-fill' | 'listing-content-fill' | 'pic-fill' | 'logistic-logo-fill' | 'play-fill' | 'Moneymanagement-fill' | 'prompt-fill' | 'manage-order-fill' | 'stop-fill' | 'multi-language-fill' | 'column' | 'logistics-icon-fill' | 'add-account' | 'Newuserzone-fill' | 'column1' | 'nightmode-fill' | 'add' | 'office-supplies-fill' | 'agriculture' | 'notice-fill' | 'years' | 'mute' | 'add-cart' | 'order-fill' | 'arrow-right' | 'password1' | 'arrow-left' | 'map1' | 'apparel' | 'paylater-fill' | 'all1' | 'phone-fill' | 'arrow-up' | 'online-tracking-fill' | 'ascending' | 'play-fill1' | 'ashbin' | 'pdf-fill' | 'atm' | 'phone1' | 'bad' | 'pin-fill' | 'attachent' | 'product-fill' | 'browse' | 'rankinglist-fill' | 'beauty' | 'reduce-fill' | 'atm-away' | 'reeor-fill' | 'assessed-badge' | 'pic-fill1' | 'auto' | 'rankinglist' | 'bags' | 'product1' | 'calendar' | 'prompt-fill1' | 'cart-full' | 'resonserate-fill' | 'calculator' | 'remind-fill' | 'cameraswitching' | 'Rightbutton-fill' | 'cecurity-protection' | 'RFQ-logo-fill' | 'category' | 'RFQ-word-fill' | 'close' | 'searchcart-fill' | 'certified-supplier' | 'salescenter-fill' | 'cart-Empty' | 'save-fill' | 'code1' | 'security-fill' | 'color' | 'Similarproducts-fill' | 'conditions' | 'signboard-fill' | 'confirm' | 'service-fill' | 'company' | 'shuffling-banner-fill' | 'ali-clould' | 'supplier-features-fill' | 'copy1' | 'store-fill' | 'credit-level' | 'smile-fill' | 'coupons' | 'success-fill' | 'connections' | 'sound-filling-fill' | 'cry' | 'sound-Mute1' | 'costoms-alearance' | 'suspended-fill' | 'clock' | 'tool-fill' | 'CurrencyConverter' | 'task-management-fill' | 'cut' | 'unlock-fill' | 'data1' | 'trust-fill' | 'Customermanagement' | 'vip-fill' | 'descending' | 'set1' | 'double-arro-right' | 'Top-fill' | 'customization' | 'viewlarger1' | 'double-arrow-left' | 'voice-fill' | 'discount' | 'warning-fill' | 'download' | 'warehouse-fill' | 'dollar1' | 'zip-fill' | 'default-template' | 'trade-assurance-fill' | 'editor1' | 'vs-fill' | 'eletrical' | 'video1' | 'electronics' | 'template-fill' | 'etrical-equipm' | 'wallet1' | 'ellipsis' | 'training1' | 'email' | 'packing-labeling-fill' | 'falling' | 'Exportservices-fill' | 'earth' | 'brand-fill' | 'filter' | 'collection' | 'furniture' | 'consumption-fill' | 'folder' | 'collection-fill' | 'feeds' | 'brand' | 'history1' | 'rejected-order-fill' | 'hardware' | 'homepage-ads-fill' | 'help' | 'homepage-ads' | 'good' | 'scenes-fill' | 'Householdappliances' | 'scenes' | 'gift1' | 'similar-product-fill' | 'form' | 'topraning-fill' | 'image-text' | 'consumption' | 'hot' | 'topraning' | 'inspection' | 'gold-supplier' | 'leftbutton' | 'messagecenter-fill' | 'jewelry' | 'quick' | 'ipad' | 'writing' | 'leftarrow' | 'docjpge-fill' | 'integral1' | 'jpge-fill' | 'kitchen' | 'gifjpge-fill' | 'inquiry-template' | 'bmpjpge-fill' | 'link' | 'tifjpge-fill' | 'libra' | 'pngjpge-fill' | 'loading' | 'Hometextile' | 'listing-content' | 'home' | 'lights' | 'sendinquiry-fill' | 'logistics-icon' | 'comments-fill' | 'messagecenter' | 'account-fill' | 'mobile-phone' | 'feed-logo-fill' | 'manage-order' | 'feed-logo' | 'move' | 'home-fill' | 'Moneymanagement' | 'add-select' | 'namecard' | 'sami-select' | 'map' | 'camera' | 'Newuserzone' | 'arrow-down' | 'multi-language' | 'account' | 'office' | 'comments' | 'notice' | 'cart-Empty1' | 'ontimeshipment' | 'favorites' | 'office-supplies' | 'order' | 'password' | 'search' | 'Notvisible1' | 'trade-assurance' | 'operation' | 'usercenter1' | 'packaging' | 'tradingdata' | 'online-tracking' | 'microphone' | 'packing-labeling' | 'txt' | 'phone' | 'xlsx' | 'pic1' | 'banzhengfuwu' | 'pin' | 'cangku' | 'play1' | 'daibancaishui' | 'logistic-logo' | 'jizhuangxiang' | 'print' | 'jiaobiao' | 'product' | 'kehupandian' | 'machinery' | 'dongtai' | 'process' | 'daikuan' | 'prompt' | 'shengyijing' | 'QRcode1' | 'jiehui' | 'reeor' | 'fencengpeizhi' | 'reduce' | 'shenqingjilu' | 'Non-staplefood' | 'shangchuanbeiandanzheng' | 'rejected-order' | 'shangchuan' | 'resonserate' | 'kehuquanyi' | 'remind' | 'suoxiao' | 'responsetime' | 'quanyipeizhi' | 'return' | 'shuangshen' | 'paylater' | 'tongguan' | 'rising1' | 'tuishui' | 'Rightarrow' | 'tongguanshuju' | 'rmb1' | 'kuaidiwuliu' | 'RFQ-logo' | 'wuliuchanpin' | 'save' | 'waihuishuju' | 'scanning' | 'xinxibar_shouji' | 'security' | 'xinwaizongyewu' | 'salescenter' | 'wuliudingdan' | 'seleted' | 'zhongjianren' | 'searchcart' | 'xinxibar_zhanghu' | 'raw' | 'yidatong' | 'service' | 'zhuanyequanwei' | 'share' | 'zhanghucaozuo' | 'signboard' | 'xuanzhuandu' | 'shuffling-banner' | 'tuishuirongzi' | 'Rightbutton' | 'AddProducts' | 'sorting' | 'ziyingyewu' | 'sound-Mute' | 'addcell' | 'Similarproducts' | 'background-color' | 'sound-filling' | 'cascades' | 'suggest' | 'beijing' | 'stop' | 'bold' | 'success' | 'zijin' | 'supplier-features' | 'eraser' | 'switch' | 'centeralignment' | 'survey' | 'click' | 'template' | 'aspjiesuan' | 'text' | 'flag' | 'suspended' | 'falg-fill' | 'task-management' | 'Fee' | 'tool' | 'filling' | 'Top' | 'Foreigncurrency' | 'smile' | 'guanliyuan' | 'textile-products' | 'language' | 'tradealert' | 'leftalignment' | 'topsales' | 'extra-inquiries' | 'tradingvolume' | 'Italic' | 'training' | 'pcm' | 'upload' | 'reducecell' | 'RFQ-word' | 'rightalignment' | 'viewlarger' | 'pointerleft' | 'viewgallery' | 'subscript' | 'vehivles' | 'square' | 'trust' | 'superscript' | 'warning' | 'tag-subscript' | 'warehouse' | 'danjuzhuanhuan' | 'shoes' | 'Transfermoney' | 'video' | 'under-line' | 'viewlist' | 'xiakuangxian' | 'set' | 'shouqi' | 'store' | 'zhankai' | 'tool-hardware' | 'tongxunlu' | 'vs' | 'yiguanzhugongyingshang' | 'toy' | 'goumaipianhao';

interface Props extends Omit<SVGAttributes<SVGElement>, 'color'> {
  name: IconNames;
  size?: number;
  color?: string | string[];
}

const IconFont: FunctionComponent<Props> = ({ name, ...rest }) => {
  switch (name) {
    case 'andorid':
      return <IconAndorid {...rest} />;
    case 'apple':
      return <IconApple {...rest} />;
    case 'sport':
      return <IconSport {...rest} />;
    case 'Subscribe':
      return <IconSubscribe {...rest} />;
    case 'creditcard':
      return <IconCreditcard {...rest} />;
    case 'becomeagoldsupplier':
      return <IconBecomeagoldsupplier {...rest} />;
    case 'contacts':
      return <IconContacts {...rest} />;
    case 'new':
      return <IconNew {...rest} />;
    case 'checkstand':
      return <IconCheckstand {...rest} />;
    case 'free':
      return <IconFree {...rest} />;
    case 'aviation':
      return <IconAviation {...rest} />;
    case 'cad-fill':
      return <IconCadFill {...rest} />;
    case 'Daytimemode':
      return <IconDaytimemode {...rest} />;
    case 'robot':
      return <IconRobot {...rest} />;
    case 'infantmom':
      return <IconInfantmom {...rest} />;
    case 'inspection1':
      return <IconInspection1 {...rest} />;
    case 'discounts':
      return <IconDiscounts {...rest} />;
    case 'block':
      return <IconBlock {...rest} />;
    case 'invoice':
      return <IconInvoice {...rest} />;
    case 'shouhuoicon':
      return <IconShouhuoicon {...rest} />;
    case 'insurance':
      return <IconInsurance {...rest} />;
    case 'nightmode':
      return <IconNightmode {...rest} />;
    case 'usercenter':
      return <IconUsercenter {...rest} />;
    case 'unlock':
      return <IconUnlock {...rest} />;
    case 'vip':
      return <IconVip {...rest} />;
    case 'wallet':
      return <IconWallet {...rest} />;
    case 'landtransportation':
      return <IconLandtransportation {...rest} />;
    case 'voice':
      return <IconVoice {...rest} />;
    case 'exchangerate':
      return <IconExchangerate {...rest} />;
    case 'contacts-fill':
      return <IconContactsFill {...rest} />;
    case 'add-account1':
      return <IconAddAccount1 {...rest} />;
    case 'years-fill':
      return <IconYearsFill {...rest} />;
    case 'add-cart-fill':
      return <IconAddCartFill {...rest} />;
    case 'add-fill':
      return <IconAddFill {...rest} />;
    case 'all-fill1':
      return <IconAllFill1 {...rest} />;
    case 'ashbin-fill':
      return <IconAshbinFill {...rest} />;
    case 'calendar-fill':
      return <IconCalendarFill {...rest} />;
    case 'bad-fill':
      return <IconBadFill {...rest} />;
    case 'bussiness-man-fill':
      return <IconBussinessManFill {...rest} />;
    case 'atm-fill':
      return <IconAtmFill {...rest} />;
    case 'cart-full-fill':
      return <IconCartFullFill {...rest} />;
    case 'cart-Empty-fill':
      return <IconCartEmptyFill {...rest} />;
    case 'cameraswitching-fill':
      return <IconCameraswitchingFill {...rest} />;
    case 'atm-away-fill':
      return <IconAtmAwayFill {...rest} />;
    case 'certified-supplier-fill':
      return <IconCertifiedSupplierFill {...rest} />;
    case 'calculator-fill':
      return <IconCalculatorFill {...rest} />;
    case 'clock-fill':
      return <IconClockFill {...rest} />;
    case 'ali-clould-fill':
      return <IconAliClouldFill {...rest} />;
    case 'color-fill':
      return <IconColorFill {...rest} />;
    case 'coupons-fill':
      return <IconCouponsFill {...rest} />;
    case 'cecurity-protection-fill':
      return <IconCecurityProtectionFill {...rest} />;
    case 'credit-level-fill':
      return <IconCreditLevelFill {...rest} />;
    case 'default-template-fill':
      return <IconDefaultTemplateFill {...rest} />;
    case 'all':
      return <IconAll {...rest} />;
    case 'CurrencyConverter-fill':
      return <IconCurrencyConverterFill {...rest} />;
    case 'bussiness-man':
      return <IconBussinessMan {...rest} />;
    case 'Customermanagement-fill':
      return <IconCustomermanagementFill {...rest} />;
    case 'component':
      return <IconComponent {...rest} />;
    case 'discounts-fill':
      return <IconDiscountsFill {...rest} />;
    case 'code':
      return <IconCode {...rest} />;
    case 'Daytimemode-fill':
      return <IconDaytimemodeFill {...rest} />;
    case 'copy':
      return <IconCopy {...rest} />;
    case 'exl-fill':
      return <IconExlFill {...rest} />;
    case 'dollar':
      return <IconDollar {...rest} />;
    case 'cry-fill':
      return <IconCryFill {...rest} />;
    case 'history':
      return <IconHistory {...rest} />;
    case 'email-fill':
      return <IconEmailFill {...rest} />;
    case 'editor':
      return <IconEditor {...rest} />;
    case 'filter-fill':
      return <IconFilterFill {...rest} />;
    case 'data':
      return <IconData {...rest} />;
    case 'folder-fill':
      return <IconFolderFill {...rest} />;
    case 'gift':
      return <IconGift {...rest} />;
    case 'feeds-fill':
      return <IconFeedsFill {...rest} />;
    case 'integral':
      return <IconIntegral {...rest} />;
    case 'gold-supplie-fill':
      return <IconGoldSupplieFill {...rest} />;
    case 'nav-list':
      return <IconNavList {...rest} />;
    case 'form-fill':
      return <IconFormFill {...rest} />;
    case 'pic':
      return <IconPic {...rest} />;
    case 'camera-fill':
      return <IconCameraFill {...rest} />;
    case 'Notvisible':
      return <IconNotvisible {...rest} />;
    case 'good-fill':
      return <IconGoodFill {...rest} />;
    case 'play':
      return <IconPlay {...rest} />;
    case 'image-text-fill':
      return <IconImageTextFill {...rest} />;
    case 'rising':
      return <IconRising {...rest} />;
    case 'inspection-fill':
      return <IconInspectionFill {...rest} />;
    case 'QRcode':
      return <IconQRcode {...rest} />;
    case 'hot-fill':
      return <IconHotFill {...rest} />;
    case 'rmb':
      return <IconRmb {...rest} />;
    case 'company-fill':
      return <IconCompanyFill {...rest} />;
    case 'similar-product':
      return <IconSimilarProduct {...rest} />;
    case 'discount-fill':
      return <IconDiscountFill {...rest} />;
    case 'Exportservices':
      return <IconExportservices {...rest} />;
    case 'insurance-fill':
      return <IconInsuranceFill {...rest} />;
    case 'sendinquiry':
      return <IconSendinquiry {...rest} />;
    case 'inquiry-template-fill':
      return <IconInquiryTemplateFill {...rest} />;
    case 'all-fill':
      return <IconAllFill {...rest} />;
    case 'leftbutton-fill':
      return <IconLeftbuttonFill {...rest} />;
    case 'favorites-fill':
      return <IconFavoritesFill {...rest} />;
    case 'integral-fill1':
      return <IconIntegralFill1 {...rest} />;
    case 'integral-fill':
      return <IconIntegralFill {...rest} />;
    case 'help1':
      return <IconHelp1 {...rest} />;
    case 'namecard-fill':
      return <IconNamecardFill {...rest} />;
    case 'listing-content-fill':
      return <IconListingContentFill {...rest} />;
    case 'pic-fill':
      return <IconPicFill {...rest} />;
    case 'logistic-logo-fill':
      return <IconLogisticLogoFill {...rest} />;
    case 'play-fill':
      return <IconPlayFill {...rest} />;
    case 'Moneymanagement-fill':
      return <IconMoneymanagementFill {...rest} />;
    case 'prompt-fill':
      return <IconPromptFill {...rest} />;
    case 'manage-order-fill':
      return <IconManageOrderFill {...rest} />;
    case 'stop-fill':
      return <IconStopFill {...rest} />;
    case 'multi-language-fill':
      return <IconMultiLanguageFill {...rest} />;
    case 'column':
      return <IconColumn {...rest} />;
    case 'logistics-icon-fill':
      return <IconLogisticsIconFill {...rest} />;
    case 'add-account':
      return <IconAddAccount {...rest} />;
    case 'Newuserzone-fill':
      return <IconNewuserzoneFill {...rest} />;
    case 'column1':
      return <IconColumn1 {...rest} />;
    case 'nightmode-fill':
      return <IconNightmodeFill {...rest} />;
    case 'add':
      return <IconAdd {...rest} />;
    case 'office-supplies-fill':
      return <IconOfficeSuppliesFill {...rest} />;
    case 'agriculture':
      return <IconAgriculture {...rest} />;
    case 'notice-fill':
      return <IconNoticeFill {...rest} />;
    case 'years':
      return <IconYears {...rest} />;
    case 'mute':
      return <IconMute {...rest} />;
    case 'add-cart':
      return <IconAddCart {...rest} />;
    case 'order-fill':
      return <IconOrderFill {...rest} />;
    case 'arrow-right':
      return <IconArrowRight {...rest} />;
    case 'password1':
      return <IconPassword1 {...rest} />;
    case 'arrow-left':
      return <IconArrowLeft {...rest} />;
    case 'map1':
      return <IconMap1 {...rest} />;
    case 'apparel':
      return <IconApparel {...rest} />;
    case 'paylater-fill':
      return <IconPaylaterFill {...rest} />;
    case 'all1':
      return <IconAll1 {...rest} />;
    case 'phone-fill':
      return <IconPhoneFill {...rest} />;
    case 'arrow-up':
      return <IconArrowUp {...rest} />;
    case 'online-tracking-fill':
      return <IconOnlineTrackingFill {...rest} />;
    case 'ascending':
      return <IconAscending {...rest} />;
    case 'play-fill1':
      return <IconPlayFill1 {...rest} />;
    case 'ashbin':
      return <IconAshbin {...rest} />;
    case 'pdf-fill':
      return <IconPdfFill {...rest} />;
    case 'atm':
      return <IconAtm {...rest} />;
    case 'phone1':
      return <IconPhone1 {...rest} />;
    case 'bad':
      return <IconBad {...rest} />;
    case 'pin-fill':
      return <IconPinFill {...rest} />;
    case 'attachent':
      return <IconAttachent {...rest} />;
    case 'product-fill':
      return <IconProductFill {...rest} />;
    case 'browse':
      return <IconBrowse {...rest} />;
    case 'rankinglist-fill':
      return <IconRankinglistFill {...rest} />;
    case 'beauty':
      return <IconBeauty {...rest} />;
    case 'reduce-fill':
      return <IconReduceFill {...rest} />;
    case 'atm-away':
      return <IconAtmAway {...rest} />;
    case 'reeor-fill':
      return <IconReeorFill {...rest} />;
    case 'assessed-badge':
      return <IconAssessedBadge {...rest} />;
    case 'pic-fill1':
      return <IconPicFill1 {...rest} />;
    case 'auto':
      return <IconAuto {...rest} />;
    case 'rankinglist':
      return <IconRankinglist {...rest} />;
    case 'bags':
      return <IconBags {...rest} />;
    case 'product1':
      return <IconProduct1 {...rest} />;
    case 'calendar':
      return <IconCalendar {...rest} />;
    case 'prompt-fill1':
      return <IconPromptFill1 {...rest} />;
    case 'cart-full':
      return <IconCartFull {...rest} />;
    case 'resonserate-fill':
      return <IconResonserateFill {...rest} />;
    case 'calculator':
      return <IconCalculator {...rest} />;
    case 'remind-fill':
      return <IconRemindFill {...rest} />;
    case 'cameraswitching':
      return <IconCameraswitching {...rest} />;
    case 'Rightbutton-fill':
      return <IconRightbuttonFill {...rest} />;
    case 'cecurity-protection':
      return <IconCecurityProtection {...rest} />;
    case 'RFQ-logo-fill':
      return <IconRfqLogoFill {...rest} />;
    case 'category':
      return <IconCategory {...rest} />;
    case 'RFQ-word-fill':
      return <IconRfqWordFill {...rest} />;
    case 'close':
      return <IconClose {...rest} />;
    case 'searchcart-fill':
      return <IconSearchcartFill {...rest} />;
    case 'certified-supplier':
      return <IconCertifiedSupplier {...rest} />;
    case 'salescenter-fill':
      return <IconSalescenterFill {...rest} />;
    case 'cart-Empty':
      return <IconCartEmpty {...rest} />;
    case 'save-fill':
      return <IconSaveFill {...rest} />;
    case 'code1':
      return <IconCode1 {...rest} />;
    case 'security-fill':
      return <IconSecurityFill {...rest} />;
    case 'color':
      return <IconColor {...rest} />;
    case 'Similarproducts-fill':
      return <IconSimilarproductsFill {...rest} />;
    case 'conditions':
      return <IconConditions {...rest} />;
    case 'signboard-fill':
      return <IconSignboardFill {...rest} />;
    case 'confirm':
      return <IconConfirm {...rest} />;
    case 'service-fill':
      return <IconServiceFill {...rest} />;
    case 'company':
      return <IconCompany {...rest} />;
    case 'shuffling-banner-fill':
      return <IconShufflingBannerFill {...rest} />;
    case 'ali-clould':
      return <IconAliClould {...rest} />;
    case 'supplier-features-fill':
      return <IconSupplierFeaturesFill {...rest} />;
    case 'copy1':
      return <IconCopy1 {...rest} />;
    case 'store-fill':
      return <IconStoreFill {...rest} />;
    case 'credit-level':
      return <IconCreditLevel {...rest} />;
    case 'smile-fill':
      return <IconSmileFill {...rest} />;
    case 'coupons':
      return <IconCoupons {...rest} />;
    case 'success-fill':
      return <IconSuccessFill {...rest} />;
    case 'connections':
      return <IconConnections {...rest} />;
    case 'sound-filling-fill':
      return <IconSoundFillingFill {...rest} />;
    case 'cry':
      return <IconCry {...rest} />;
    case 'sound-Mute1':
      return <IconSoundMute1 {...rest} />;
    case 'costoms-alearance':
      return <IconCostomsAlearance {...rest} />;
    case 'suspended-fill':
      return <IconSuspendedFill {...rest} />;
    case 'clock':
      return <IconClock {...rest} />;
    case 'tool-fill':
      return <IconToolFill {...rest} />;
    case 'CurrencyConverter':
      return <IconCurrencyConverter {...rest} />;
    case 'task-management-fill':
      return <IconTaskManagementFill {...rest} />;
    case 'cut':
      return <IconCut {...rest} />;
    case 'unlock-fill':
      return <IconUnlockFill {...rest} />;
    case 'data1':
      return <IconData1 {...rest} />;
    case 'trust-fill':
      return <IconTrustFill {...rest} />;
    case 'Customermanagement':
      return <IconCustomermanagement {...rest} />;
    case 'vip-fill':
      return <IconVipFill {...rest} />;
    case 'descending':
      return <IconDescending {...rest} />;
    case 'set1':
      return <IconSet1 {...rest} />;
    case 'double-arro-right':
      return <IconDoubleArroRight {...rest} />;
    case 'Top-fill':
      return <IconTopFill {...rest} />;
    case 'customization':
      return <IconCustomization {...rest} />;
    case 'viewlarger1':
      return <IconViewlarger1 {...rest} />;
    case 'double-arrow-left':
      return <IconDoubleArrowLeft {...rest} />;
    case 'voice-fill':
      return <IconVoiceFill {...rest} />;
    case 'discount':
      return <IconDiscount {...rest} />;
    case 'warning-fill':
      return <IconWarningFill {...rest} />;
    case 'download':
      return <IconDownload {...rest} />;
    case 'warehouse-fill':
      return <IconWarehouseFill {...rest} />;
    case 'dollar1':
      return <IconDollar1 {...rest} />;
    case 'zip-fill':
      return <IconZipFill {...rest} />;
    case 'default-template':
      return <IconDefaultTemplate {...rest} />;
    case 'trade-assurance-fill':
      return <IconTradeAssuranceFill {...rest} />;
    case 'editor1':
      return <IconEditor1 {...rest} />;
    case 'vs-fill':
      return <IconVsFill {...rest} />;
    case 'eletrical':
      return <IconEletrical {...rest} />;
    case 'video1':
      return <IconVideo1 {...rest} />;
    case 'electronics':
      return <IconElectronics {...rest} />;
    case 'template-fill':
      return <IconTemplateFill {...rest} />;
    case 'etrical-equipm':
      return <IconEtricalEquipm {...rest} />;
    case 'wallet1':
      return <IconWallet1 {...rest} />;
    case 'ellipsis':
      return <IconEllipsis {...rest} />;
    case 'training1':
      return <IconTraining1 {...rest} />;
    case 'email':
      return <IconEmail {...rest} />;
    case 'packing-labeling-fill':
      return <IconPackingLabelingFill {...rest} />;
    case 'falling':
      return <IconFalling {...rest} />;
    case 'Exportservices-fill':
      return <IconExportservicesFill {...rest} />;
    case 'earth':
      return <IconEarth {...rest} />;
    case 'brand-fill':
      return <IconBrandFill {...rest} />;
    case 'filter':
      return <IconFilter {...rest} />;
    case 'collection':
      return <IconCollection {...rest} />;
    case 'furniture':
      return <IconFurniture {...rest} />;
    case 'consumption-fill':
      return <IconConsumptionFill {...rest} />;
    case 'folder':
      return <IconFolder {...rest} />;
    case 'collection-fill':
      return <IconCollectionFill {...rest} />;
    case 'feeds':
      return <IconFeeds {...rest} />;
    case 'brand':
      return <IconBrand {...rest} />;
    case 'history1':
      return <IconHistory1 {...rest} />;
    case 'rejected-order-fill':
      return <IconRejectedOrderFill {...rest} />;
    case 'hardware':
      return <IconHardware {...rest} />;
    case 'homepage-ads-fill':
      return <IconHomepageAdsFill {...rest} />;
    case 'help':
      return <IconHelp {...rest} />;
    case 'homepage-ads':
      return <IconHomepageAds {...rest} />;
    case 'good':
      return <IconGood {...rest} />;
    case 'scenes-fill':
      return <IconScenesFill {...rest} />;
    case 'Householdappliances':
      return <IconHouseholdappliances {...rest} />;
    case 'scenes':
      return <IconScenes {...rest} />;
    case 'gift1':
      return <IconGift1 {...rest} />;
    case 'similar-product-fill':
      return <IconSimilarProductFill {...rest} />;
    case 'form':
      return <IconForm {...rest} />;
    case 'topraning-fill':
      return <IconTopraningFill {...rest} />;
    case 'image-text':
      return <IconImageText {...rest} />;
    case 'consumption':
      return <IconConsumption {...rest} />;
    case 'hot':
      return <IconHot {...rest} />;
    case 'topraning':
      return <IconTopraning {...rest} />;
    case 'inspection':
      return <IconInspection {...rest} />;
    case 'gold-supplier':
      return <IconGoldSupplier {...rest} />;
    case 'leftbutton':
      return <IconLeftbutton {...rest} />;
    case 'messagecenter-fill':
      return <IconMessagecenterFill {...rest} />;
    case 'jewelry':
      return <IconJewelry {...rest} />;
    case 'quick':
      return <IconQuick {...rest} />;
    case 'ipad':
      return <IconIpad {...rest} />;
    case 'writing':
      return <IconWriting {...rest} />;
    case 'leftarrow':
      return <IconLeftarrow {...rest} />;
    case 'docjpge-fill':
      return <IconDocjpgeFill {...rest} />;
    case 'integral1':
      return <IconIntegral1 {...rest} />;
    case 'jpge-fill':
      return <IconJpgeFill {...rest} />;
    case 'kitchen':
      return <IconKitchen {...rest} />;
    case 'gifjpge-fill':
      return <IconGifjpgeFill {...rest} />;
    case 'inquiry-template':
      return <IconInquiryTemplate {...rest} />;
    case 'bmpjpge-fill':
      return <IconBmpjpgeFill {...rest} />;
    case 'link':
      return <IconLink {...rest} />;
    case 'tifjpge-fill':
      return <IconTifjpgeFill {...rest} />;
    case 'libra':
      return <IconLibra {...rest} />;
    case 'pngjpge-fill':
      return <IconPngjpgeFill {...rest} />;
    case 'loading':
      return <IconLoading {...rest} />;
    case 'Hometextile':
      return <IconHometextile {...rest} />;
    case 'listing-content':
      return <IconListingContent {...rest} />;
    case 'home':
      return <IconHome {...rest} />;
    case 'lights':
      return <IconLights {...rest} />;
    case 'sendinquiry-fill':
      return <IconSendinquiryFill {...rest} />;
    case 'logistics-icon':
      return <IconLogisticsIcon {...rest} />;
    case 'comments-fill':
      return <IconCommentsFill {...rest} />;
    case 'messagecenter':
      return <IconMessagecenter {...rest} />;
    case 'account-fill':
      return <IconAccountFill {...rest} />;
    case 'mobile-phone':
      return <IconMobilePhone {...rest} />;
    case 'feed-logo-fill':
      return <IconFeedLogoFill {...rest} />;
    case 'manage-order':
      return <IconManageOrder {...rest} />;
    case 'feed-logo':
      return <IconFeedLogo {...rest} />;
    case 'move':
      return <IconMove {...rest} />;
    case 'home-fill':
      return <IconHomeFill {...rest} />;
    case 'Moneymanagement':
      return <IconMoneymanagement {...rest} />;
    case 'add-select':
      return <IconAddSelect {...rest} />;
    case 'namecard':
      return <IconNamecard {...rest} />;
    case 'sami-select':
      return <IconSamiSelect {...rest} />;
    case 'map':
      return <IconMap {...rest} />;
    case 'camera':
      return <IconCamera {...rest} />;
    case 'Newuserzone':
      return <IconNewuserzone {...rest} />;
    case 'arrow-down':
      return <IconArrowDown {...rest} />;
    case 'multi-language':
      return <IconMultiLanguage {...rest} />;
    case 'account':
      return <IconAccount {...rest} />;
    case 'office':
      return <IconOffice {...rest} />;
    case 'comments':
      return <IconComments {...rest} />;
    case 'notice':
      return <IconNotice {...rest} />;
    case 'cart-Empty1':
      return <IconCartEmpty1 {...rest} />;
    case 'ontimeshipment':
      return <IconOntimeshipment {...rest} />;
    case 'favorites':
      return <IconFavorites {...rest} />;
    case 'office-supplies':
      return <IconOfficeSupplies {...rest} />;
    case 'order':
      return <IconOrder {...rest} />;
    case 'password':
      return <IconPassword {...rest} />;
    case 'search':
      return <IconSearch {...rest} />;
    case 'Notvisible1':
      return <IconNotvisible1 {...rest} />;
    case 'trade-assurance':
      return <IconTradeAssurance {...rest} />;
    case 'operation':
      return <IconOperation {...rest} />;
    case 'usercenter1':
      return <IconUsercenter1 {...rest} />;
    case 'packaging':
      return <IconPackaging {...rest} />;
    case 'tradingdata':
      return <IconTradingdata {...rest} />;
    case 'online-tracking':
      return <IconOnlineTracking {...rest} />;
    case 'microphone':
      return <IconMicrophone {...rest} />;
    case 'packing-labeling':
      return <IconPackingLabeling {...rest} />;
    case 'txt':
      return <IconTxt {...rest} />;
    case 'phone':
      return <IconPhone {...rest} />;
    case 'xlsx':
      return <IconXlsx {...rest} />;
    case 'pic1':
      return <IconPic1 {...rest} />;
    case 'banzhengfuwu':
      return <IconBanzhengfuwu {...rest} />;
    case 'pin':
      return <IconPin {...rest} />;
    case 'cangku':
      return <IconCangku {...rest} />;
    case 'play1':
      return <IconPlay1 {...rest} />;
    case 'daibancaishui':
      return <IconDaibancaishui {...rest} />;
    case 'logistic-logo':
      return <IconLogisticLogo {...rest} />;
    case 'jizhuangxiang':
      return <IconJizhuangxiang {...rest} />;
    case 'print':
      return <IconPrint {...rest} />;
    case 'jiaobiao':
      return <IconJiaobiao {...rest} />;
    case 'product':
      return <IconProduct {...rest} />;
    case 'kehupandian':
      return <IconKehupandian {...rest} />;
    case 'machinery':
      return <IconMachinery {...rest} />;
    case 'dongtai':
      return <IconDongtai {...rest} />;
    case 'process':
      return <IconProcess {...rest} />;
    case 'daikuan':
      return <IconDaikuan {...rest} />;
    case 'prompt':
      return <IconPrompt {...rest} />;
    case 'shengyijing':
      return <IconShengyijing {...rest} />;
    case 'QRcode1':
      return <IconQRcode1 {...rest} />;
    case 'jiehui':
      return <IconJiehui {...rest} />;
    case 'reeor':
      return <IconReeor {...rest} />;
    case 'fencengpeizhi':
      return <IconFencengpeizhi {...rest} />;
    case 'reduce':
      return <IconReduce {...rest} />;
    case 'shenqingjilu':
      return <IconShenqingjilu {...rest} />;
    case 'Non-staplefood':
      return <IconNonStaplefood {...rest} />;
    case 'shangchuanbeiandanzheng':
      return <IconShangchuanbeiandanzheng {...rest} />;
    case 'rejected-order':
      return <IconRejectedOrder {...rest} />;
    case 'shangchuan':
      return <IconShangchuan {...rest} />;
    case 'resonserate':
      return <IconResonserate {...rest} />;
    case 'kehuquanyi':
      return <IconKehuquanyi {...rest} />;
    case 'remind':
      return <IconRemind {...rest} />;
    case 'suoxiao':
      return <IconSuoxiao {...rest} />;
    case 'responsetime':
      return <IconResponsetime {...rest} />;
    case 'quanyipeizhi':
      return <IconQuanyipeizhi {...rest} />;
    case 'return':
      return <IconReturn {...rest} />;
    case 'shuangshen':
      return <IconShuangshen {...rest} />;
    case 'paylater':
      return <IconPaylater {...rest} />;
    case 'tongguan':
      return <IconTongguan {...rest} />;
    case 'rising1':
      return <IconRising1 {...rest} />;
    case 'tuishui':
      return <IconTuishui {...rest} />;
    case 'Rightarrow':
      return <IconRightarrow {...rest} />;
    case 'tongguanshuju':
      return <IconTongguanshuju {...rest} />;
    case 'rmb1':
      return <IconRmb1 {...rest} />;
    case 'kuaidiwuliu':
      return <IconKuaidiwuliu {...rest} />;
    case 'RFQ-logo':
      return <IconRfqLogo {...rest} />;
    case 'wuliuchanpin':
      return <IconWuliuchanpin {...rest} />;
    case 'save':
      return <IconSave {...rest} />;
    case 'waihuishuju':
      return <IconWaihuishuju {...rest} />;
    case 'scanning':
      return <IconScanning {...rest} />;
    case 'xinxibar_shouji':
      return <IconXinxibarShouji {...rest} />;
    case 'security':
      return <IconSecurity {...rest} />;
    case 'xinwaizongyewu':
      return <IconXinwaizongyewu {...rest} />;
    case 'salescenter':
      return <IconSalescenter {...rest} />;
    case 'wuliudingdan':
      return <IconWuliudingdan {...rest} />;
    case 'seleted':
      return <IconSeleted {...rest} />;
    case 'zhongjianren':
      return <IconZhongjianren {...rest} />;
    case 'searchcart':
      return <IconSearchcart {...rest} />;
    case 'xinxibar_zhanghu':
      return <IconXinxibarZhanghu {...rest} />;
    case 'raw':
      return <IconRaw {...rest} />;
    case 'yidatong':
      return <IconYidatong {...rest} />;
    case 'service':
      return <IconService {...rest} />;
    case 'zhuanyequanwei':
      return <IconZhuanyequanwei {...rest} />;
    case 'share':
      return <IconShare {...rest} />;
    case 'zhanghucaozuo':
      return <IconZhanghucaozuo {...rest} />;
    case 'signboard':
      return <IconSignboard {...rest} />;
    case 'xuanzhuandu':
      return <IconXuanzhuandu {...rest} />;
    case 'shuffling-banner':
      return <IconShufflingBanner {...rest} />;
    case 'tuishuirongzi':
      return <IconTuishuirongzi {...rest} />;
    case 'Rightbutton':
      return <IconRightbutton {...rest} />;
    case 'AddProducts':
      return <IconAddProducts {...rest} />;
    case 'sorting':
      return <IconSorting {...rest} />;
    case 'ziyingyewu':
      return <IconZiyingyewu {...rest} />;
    case 'sound-Mute':
      return <IconSoundMute {...rest} />;
    case 'addcell':
      return <IconAddcell {...rest} />;
    case 'Similarproducts':
      return <IconSimilarproducts {...rest} />;
    case 'background-color':
      return <IconBackgroundColor {...rest} />;
    case 'sound-filling':
      return <IconSoundFilling {...rest} />;
    case 'cascades':
      return <IconCascades {...rest} />;
    case 'suggest':
      return <IconSuggest {...rest} />;
    case 'beijing':
      return <IconBeijing {...rest} />;
    case 'stop':
      return <IconStop {...rest} />;
    case 'bold':
      return <IconBold {...rest} />;
    case 'success':
      return <IconSuccess {...rest} />;
    case 'zijin':
      return <IconZijin {...rest} />;
    case 'supplier-features':
      return <IconSupplierFeatures {...rest} />;
    case 'eraser':
      return <IconEraser {...rest} />;
    case 'switch':
      return <IconSwitch {...rest} />;
    case 'centeralignment':
      return <IconCenteralignment {...rest} />;
    case 'survey':
      return <IconSurvey {...rest} />;
    case 'click':
      return <IconClick {...rest} />;
    case 'template':
      return <IconTemplate {...rest} />;
    case 'aspjiesuan':
      return <IconAspjiesuan {...rest} />;
    case 'text':
      return <IconText {...rest} />;
    case 'flag':
      return <IconFlag {...rest} />;
    case 'suspended':
      return <IconSuspended {...rest} />;
    case 'falg-fill':
      return <IconFalgFill {...rest} />;
    case 'task-management':
      return <IconTaskManagement {...rest} />;
    case 'Fee':
      return <IconFee {...rest} />;
    case 'tool':
      return <IconTool {...rest} />;
    case 'filling':
      return <IconFilling {...rest} />;
    case 'Top':
      return <IconTop {...rest} />;
    case 'Foreigncurrency':
      return <IconForeigncurrency {...rest} />;
    case 'smile':
      return <IconSmile {...rest} />;
    case 'guanliyuan':
      return <IconGuanliyuan {...rest} />;
    case 'textile-products':
      return <IconTextileProducts {...rest} />;
    case 'language':
      return <IconLanguage {...rest} />;
    case 'tradealert':
      return <IconTradealert {...rest} />;
    case 'leftalignment':
      return <IconLeftalignment {...rest} />;
    case 'topsales':
      return <IconTopsales {...rest} />;
    case 'extra-inquiries':
      return <IconExtraInquiries {...rest} />;
    case 'tradingvolume':
      return <IconTradingvolume {...rest} />;
    case 'Italic':
      return <IconItalic {...rest} />;
    case 'training':
      return <IconTraining {...rest} />;
    case 'pcm':
      return <IconPcm {...rest} />;
    case 'upload':
      return <IconUpload {...rest} />;
    case 'reducecell':
      return <IconReducecell {...rest} />;
    case 'RFQ-word':
      return <IconRfqWord {...rest} />;
    case 'rightalignment':
      return <IconRightalignment {...rest} />;
    case 'viewlarger':
      return <IconViewlarger {...rest} />;
    case 'pointerleft':
      return <IconPointerleft {...rest} />;
    case 'viewgallery':
      return <IconViewgallery {...rest} />;
    case 'subscript':
      return <IconSubscript {...rest} />;
    case 'vehivles':
      return <IconVehivles {...rest} />;
    case 'square':
      return <IconSquare {...rest} />;
    case 'trust':
      return <IconTrust {...rest} />;
    case 'superscript':
      return <IconSuperscript {...rest} />;
    case 'warning':
      return <IconWarning {...rest} />;
    case 'tag-subscript':
      return <IconTagSubscript {...rest} />;
    case 'warehouse':
      return <IconWarehouse {...rest} />;
    case 'danjuzhuanhuan':
      return <IconDanjuzhuanhuan {...rest} />;
    case 'shoes':
      return <IconShoes {...rest} />;
    case 'Transfermoney':
      return <IconTransfermoney {...rest} />;
    case 'video':
      return <IconVideo {...rest} />;
    case 'under-line':
      return <IconUnderLine {...rest} />;
    case 'viewlist':
      return <IconViewlist {...rest} />;
    case 'xiakuangxian':
      return <IconXiakuangxian {...rest} />;
    case 'set':
      return <IconSet {...rest} />;
    case 'shouqi':
      return <IconShouqi {...rest} />;
    case 'store':
      return <IconStore {...rest} />;
    case 'zhankai':
      return <IconZhankai {...rest} />;
    case 'tool-hardware':
      return <IconToolHardware {...rest} />;
    case 'tongxunlu':
      return <IconTongxunlu {...rest} />;
    case 'vs':
      return <IconVs {...rest} />;
    case 'yiguanzhugongyingshang':
      return <IconYiguanzhugongyingshang {...rest} />;
    case 'toy':
      return <IconToy {...rest} />;
    case 'goumaipianhao':
      return <IconGoumaipianhao {...rest} />;

  }

  return null;
};

export default IconFont;
