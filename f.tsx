import ReactGA from 'react-ga'
import { tapError, tapOk, tapRespErr } from './mod/utils'

let ec = { category: '验证码', label: '绑定手机号' }
ReactGA.event({ ...ec, action: '发送' })

Promise.resolve(1).then(
  tapOk(() => {
    ReactGA.event({ ...ec, action: '发送成功' })
  }),
  tapRespErr((l) => {
    l = ec.label + '-' + l
    ReactGA.event({ ...ec, action: '发送失败', label: l })
  }),
)
