import { AxiosInstance, AxiosResponse } from 'axios'
import { client } from './api'

export enum ADType {
  /**竖屏广告 */
  Row = 1,
  /**横屏广告 */
  Col = 2,
}
export type ADResponse = {
  advType: ADType
  clickCount: number
  hrefUrl: string
  id: string
  imgUrl: string
  sort: number
  status: number
  title: string
}

export const ad = (params: { type: ADType }) => {
  type t = AxiosResponse<{ result: ADResponse }>
  return client.post<any, t>('/getAdv', params)
}

export const adClick = (params: { id: string }) => {
  type t = AxiosResponse<{ result: ADResponse }>
  return client.post<any, t>('/addAdvCount', params)
}
