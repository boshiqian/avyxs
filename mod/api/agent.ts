import axios, { AxiosInstance, AxiosResponse } from 'axios'
import { BaseAPI as BaseAPI, client } from './api'
type agentParams={
    userId:string
}
type listParams={
    userId:string,
    page:1,
    limit:20
}
export type agentTotal = object
export type headerTotal =object
export const GetAgentStatic = (params: agentParams) => {
    type t = AxiosResponse<{ result: agentTotal}>

    return client.post<any, t>('/user/getAgentTotal', params)
}
export const GetPromoteRecord = (params: { limit: number; page: number; userId: string }) => {
    type t = AxiosResponse<{ result: agentTotal}>

    return client.post<any, t>('/user/getTgUserLog', params)
}
export const GetUserInfoe = (params: agentParams) => {
    type t = AxiosResponse<{ result: agentTotal}>

    return client.post<any, t>('/user/getUserInfo', params)
}
export const GetAgentTotal = (params: agentParams) => {
    type t = AxiosResponse<{ result: headerTotal}>

    return client.post<any, t>('/user/getTgLogCount', params)
}
