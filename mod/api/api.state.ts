import { AxiosInstance } from 'axios'
import { useReducer, useState } from 'react'
import { createContainer } from 'unstated-next'
import { client } from './api'

export const useAPIClient = () => {
  const [_client] = useState<AxiosInstance>(client)
  return _client
}

export const APIClient = createContainer(useAPIClient)
