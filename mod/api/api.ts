import axios, { AxiosResponse } from 'axios'
import { Decrypt, Encrypt } from './crypto'
import { Base64 } from 'js-base64'

export const BaseAPI =
  process.env.NODE_ENV === 'development'
    ? '//api.mifen517.com/api'
    : '//api.mifen517.com/api'

export const client = axios.create({
  baseURL: BaseAPI,
})

client.interceptors.request.use(function (req) {
  if (req.url.indexOf('/img/')!=-1)
  {
    req.responseType='arraybuffer';
    let decodedData = Base64.encode(req.data);
    req.url=req.url+decodedData+'.data'
    return req
  }
  else {
    let d = req.data ?? {}
    let w = JSON.stringify(d)
    let arr=JSON.stringify([
      {
        "id": "1492513071521878018",
        "title": "📚 按主題",
        "categoryType": 1,
        "url": "/"
      },
      {
        "id": "1403624711820746754",
        "title": "💡 新片優先",
        "categoryType": 1,
        "url":''
      },
      {
        "id": "1403624348594020354",
        "title": "🔥 熱度優先",
        "categoryType": 1,
        "url":''
      }
    ])
    w = Encrypt(w)
    req.data = { data: w }
    return req
  }
})

export interface APIResponse<T = any> {
  success: boolean
  message: string
  code: number
  result: T
}

client.interceptors.response.use(function (res: AxiosResponse<APIResponse>) {
  if (!res.data.success) {
   // return Promise.reject(res.data)
  }
  return res
})
type Response = {
  code: number
  result: string
}

client.interceptors.response.use(function (res) {
  let d: Response = res.data
  if (res.config.url.indexOf('/img/') == -1)
  {
    if (d.result === null) {
      return res
    }
    if (typeof d.result !== 'string') {
      return Promise.reject(d)
    }
    let result = Decrypt(d.result)
    res.data.result = JSON.parse(result)
    return res
  }
  else {
    return get_image(d)
  }
})
function get_image(buffer){
  var binary = '';
  var url=''
  var bytes = new Uint8Array(buffer);
  for (var len = bytes.byteLength, i = 2; i < len; i++) {
    binary += String.fromCharCode(bytes[i]);
  }
  url="data:image/png;base64,"+Base64.btoa(binary);
  return url;
}
