import CryptoJS from 'crypto-js'

const KEY = CryptoJS.enc.Utf8.parse('56+0x4d8=8c56fd5')
const IV = CryptoJS.enc.Utf8.parse('')

export function Encrypt(word: string, key = KEY, iv = IV) {
  let srcs = CryptoJS.enc.Utf8.parse(word)
  var encrypted = CryptoJS.AES.encrypt(srcs, key, {
    iv: iv,
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  })
  // console.log("-=-=-=-", encrypted.ciphertext)
  return CryptoJS.enc.Base64.stringify(encrypted.ciphertext)
}

export function Decrypt(word: string, key = KEY, iv = IV) {
  let base64 = CryptoJS.enc.Base64.parse(word)

  let src = CryptoJS.enc.Base64.stringify(base64)

  var decrypt = CryptoJS.AES.decrypt(src, key, {
    iv: iv,
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  })

  var decryptedStr = decrypt.toString(CryptoJS.enc.Utf8)
  return decryptedStr.toString()
}
