import { AxiosInstance, AxiosResponse } from 'axios'
import { SortType, Config, VideoDesc } from './types'
import { client } from './api'

export type BannerItem = {
  hrefUrl: string
  id: string
  imgUrl: string
  sort: number
  status: number
}

export type HomeCol = {
  id: string
  title: string
  subtitle?: string
  list: VideoDesc[]
}
export type BannerList=any[]
export type colList=any[]
export type imgData= object
export type topicArr=Array<any>
export const info = (params?: {}) => {
  type t = AxiosResponse<{ result }>
  return client.post<any, t>('/h5/getConfig', params)
}
export const getBnnerList = (params?: {}) => {
  type t = AxiosResponse<{ result:BannerList}>
  return client.post<any, t>('/h5/getBanner', params)
}
export const getIndexCol = (params?: {}) => {
  type t = AxiosResponse<{ result:colList}>
  return client.post<any, t>('/h5/getIndexCol', params)
}
export const listByIndexCol = (params?: {}) => {
  type t = AxiosResponse<{ result}>
  console.log(params,'params')
  return client.post<any, t>('/listByIndexCol', params)
}
export const getImg = (params?: {}) => {
  type t = AxiosResponse<{ result: imgData }>
  return client.get<any, t>('/img/', params)
}
export const getTopicList = (params?: {}) => {
  type t = AxiosResponse<{ result: topicArr }>
  return client.post<any, t>('/h5/getZtList', params)
}
