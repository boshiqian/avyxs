import axios, { AxiosInstance, AxiosResponse } from 'axios'
import { BaseAPI as BaseAPI, client } from './api'

export type Good = {
  /**商品ID */
  id: string
  /**商品标题 */
  title: string
  /**底图 */
  imgUrl: string
  /**副标题 */
  subTitle: string
  /**标签 */
  tag: string
  /**原价 */
  price: string
  /**折扣价 */
  disCount: string
  /**介绍 */
  note: string
}

export enum GoodType {
  /**VIP */
  VIP = 1,
  /**金币 */
  Coin = 2,
}

export type VIPGood = Good & {
  /**购片折扣 */
  payDiscount: string
  /**充值月数 */
  payMonth: number
  params: { title: string; note: string }[]
}

/**VIP商品列表 */
export const vipGoodList = (params: {} = {}) => {
  type t = AxiosResponse<{ result: VIPGood[] }>

  return client.post<any, t>('/getVipItem', params).then((r) => {
    let nr = r.data.result.map((i) => {
      let params = JSON.parse(i.params as any as string)
      return { ...i, params }
    })
    r.data.result = nr
    return r
  })
}

export type CoinGood = Good & {
  /**充值金币 */
  coins: string
}

/**金币商品列表 */
export const coinGoodList = (params: {} = {}) => {
  type t = AxiosResponse<{ result: CoinGood[] }>

  return client.post<any, t>('/getCoinItem', params)
}

/**支付通道 */
export type PayChannel = {
  id: string
  title: string
  /**图标 */
  imgUrl: string
}

/**支付通道列表 */
export const payChannels = (params: {} = {}) => {
  type t = AxiosResponse<{ result: PayChannel[] }>

  return client.post<any, t>('/getChannelList', params)
}

export type BuyParams = {
  userId: string
  type: GoodType
  /**商品ID */
  itemId: string
  /**支付通道ID */
  channelId: string
}
/**订单号，下一步调用  */
export type BuyResp = string
/**商品支付下单 */
export const buy = (params: BuyParams) => {
  type t = AxiosResponse<{ result: BuyResp }>

  return client.post<any, t>('/pay/toPay', params)
}

export type PayParams = {
  /**订单号  */
  orderNum: string
}
export type PayResp = any
/**支付订单 */
export const pay = (params: PayParams) => {
  type t = AxiosResponse<{ result: PayResp }>

  return `${BaseAPI}/pay/page?token=${params.orderNum}`
}

export type CheckOrderParams = {
  /**订单号  */
  orderNum: string
}
export type CheckOrderResp = boolean
/**检查订单是否完成 */
export const checkOrder = (params: CheckOrderParams) => {
  type t = AxiosResponse<{ result: CheckOrderResp }>

  return client.post<any, t>(`/pay/checkOrderStatus`, params)
}

export type buyVideoParams = {
  userId: string
  /**视频ID */
  videoId: string
}
/**用户余额 */
export type buyVideoResp = number
/**视频购买 */
export const buyVideo = (params: buyVideoParams) => {
  type t = AxiosResponse<{ result: buyVideoResp }>

  return client.post<any, t>(`/user/coinPay`, params)
}
