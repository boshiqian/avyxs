export enum SortType {
  /**默认 */
  Default = '-1',
  /**最新 */
  Newest = '1',
  /**排行 */
  Top = '2',
  /**推荐 */
  Recommend = '3',
}

export enum PayMode {
  /**免费 */
  Free = 1,
  /**付费 */
  RequirePay = 2,
  /**会员免费 */
  VIPFree = 3,
}

export const PayMode2Str = (p: PayMode) => {
  switch (p) {
    case PayMode.Free:
      return '免费'
    case PayMode.RequirePay:
      return '付费'
    case PayMode.VIPFree:
      return 'VIP'
  }
}

export type VideoDesc = {
  id: string
  imgUrl: string
  payCoin: number
  payMode: PayMode
  playCount: number
  playTime: string
  serverCode: string
  title: string
  userId: string
  videoType: string
  advType?: number
}

export type Page = {
  page?: number
  limit?: number
  sort?: SortType
}

export type PageResp = {
  /**数据总数 */
  total: number
  /**当前页 */
  current: number
  /**总页数 */
  pages: number
}

export type Config = {
  resUrl: string
  kfUrl: string
}

export enum SexType {
  Secret = 0,
  Man = 1,
  Women = 2,
}

export type UserInfo = {
  id: string
  /**折扣, 默认为 10 不打折 */
  discount: number
  /**登录帐号 */
  account: string
  /**头像地址，默认无 */
  headUrl: string
  nickName: string
  /**金币余额 */
  balance: number
  /**vip到期时间 */
  vipTimp: string
  /**是否VIP */
  isVip: boolean
  sex: SexType
  /**个人简介 */
  introduction: string
  /**手机号 */
  phone?: string
  devCode: string
  devType: 3
  createTime: string
}
