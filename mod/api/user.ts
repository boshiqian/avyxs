import { AxiosInstance, AxiosResponse } from 'axios'
import { UserInfo, SexType } from './types'

import { client } from './api'

type RegisterParams = {
  // devCode: string
  /**h5 固定 3 */
  devType?: 3
  regType?: 'h5'
  tgCode?: string
}
export const register = (params: RegisterParams) => {
  type t = AxiosResponse<{ result: UserInfo }>

  params.devType = 3
  params.regType = 'h5'
  let link = new URL(window.location.search, 'http://noop')
  let tgCode = link.searchParams.get('t')
  if (tgCode) {
    params.tgCode = tgCode
  }
  return client.post<any, t>('/user/regUser', params)
}

type LoginParams = {
  userId: string
  isUpdate?: true
}
export const login = (params: LoginParams) => {
  type t = AxiosResponse<{ result: UserInfo }>

  params.isUpdate = true
  return client.post<any, t>('/user/getUserInfo', params)
}

export type UpdateParams = {
  id: string
  nickName?: string
  introduction?: string
  headUrl?: string
  sex?: SexType
}
export const updateInfo = (params: UpdateParams) => {
  type t = AxiosResponse<{ result: UserInfo }>

  return client.post<any, t>('/user/updateUserInfo', params)
}

type uploadAvatarParams = {
  file: string
  token: string
}
export const uploadAvatar = (params: uploadAvatarParams) => {
  type t = AxiosResponse<{ result: string }>

  let token = params.token
  delete params.token
  return client.post<any, t>('/upload/uploadUserHead', params, {
    headers: { token },
  })
}

export enum SendCodeType {
  /**绑定手机 */
  bindPhone = 1,
  /**找回帐号 */
  findAccount = 2,
}
type SendCodeParams = {
  mobile: string
  type: SendCodeType
}
export const sendCode = (params: SendCodeParams) => {
  type t = AxiosResponse<{ result: UserInfo }>

  return client.post<any, t>('/user/sendCode', params)
}

type BindPhoneParams = {
  mobile: string
  userId: string
  /**验证码 */
  code: string
}
export const bindPhone = (params: BindPhoneParams) => {
  type t = AxiosResponse<{ result: UserInfo }>

  return client.post<any, t>('/user/bindPhone', params)
}

export type SwitchAccountParams = {
  mobile: string
  /**验证码 */
  code: string
}
export const switchAccount = (params: SwitchAccountParams) => {
  type t = AxiosResponse<{ result: UserInfo }>

  return client.post<any, t>('/user/findUserByPhone', params)
}
