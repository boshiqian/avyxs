import { AxiosInstance, AxiosResponse } from 'axios'
import { SortType, Page, PayMode, VideoDesc, PageResp } from './types'
import { client } from './api'

type SearchParams = Page & { key: string; sort: SortType }
type TopicParams = Page & { id: string; sort: SortType }
export type SearchResponse = PageResp & {
  records: any[]
}
export type ClassResponse = PageResp & {
  records:any[],
  total:number
}
export type TopicResponse = PageResp & {
  ext: {records:any[]}
}
export const search = (params: SearchParams) => {
  type t = AxiosResponse<{ result: SearchResponse }>

  params.limit = params.limit ?? 24
  params.page = params.page ?? 1
  return client.post<any, t>('/searchVideo', params)
}
export const getVideoByZhuanTi = (params) => {
  type t = AxiosResponse<{ result }>
  params.limit = params.limit ?? 24
  params.page = params.page ?? 1
  return client.post<any, t>('/getVideoByZhuanTi', params)
}
export const listTags =
  (client: AxiosInstance) =>
  (params: {} = {}) => {
    type t = AxiosResponse<{ result: any }>

    return client.post<any, t>('/getTagList', params)
  }

export type Category = {
  id: string
  title: string
  sort: string
  status: number
  categoryType: number
}
export const listCategory = (params: {} = {}) => {
  type t = AxiosResponse<{ result: Category[] }>

  return client.post<any, t>('/getVideoCategory', params)
}

type listByCategoryIDParams = Page & { categoryId: string }
export const listByCategoryID = (params: listByCategoryIDParams) => {
  type t = AxiosResponse<{
    result: { categoryName: string; data: SearchResponse }
  }>

  params.limit = params.limit ?? 24
  params.page = params.page ?? 1
  return client.post<any, t>('/h5/getListByCategory', params)
}

type listByColIDParams = Page & { id: string }
export const listByColID = (params: listByColIDParams) => {
  type t = AxiosResponse<{ result: SearchResponse }>

  params.limit = params.limit ?? 24
  params.page = params.page ?? 1
  return client.post<any, t>('/listByIndexCol', params)
}

type infoParams = {
  id: string
  /**用户ID 权限判断 */
  userId: string
}
export type VideoInfo = {
  id: string
  /**2021-08-17 00:22:22 */
  createTime: string
  /**视频标题 */
  title: string
  /**封面图 */
  imgUrl: string
  /**时长 */
  playTime: string
  /**视频地址（未满足播放条件为试看地址） */
  videoUrl: string
  /**免费视频也为 true */
  isPay: boolean
  /**收费模式: 1免费，2收费，3 VIP免费 */
  payMode: PayMode
  /**收费金额 */
  payCoin: number
  /**分类ID */
  categoryId: string
  /**首页栏目ID */
  indexCol: string
  /**标签 */
  tagIds: string
  /**播放数 */
  playCount: string
  /**点赞数 */
  zan: number
}
export const info = (params: infoParams) => {
  type t = AxiosResponse<{ result: VideoInfo }>
  return client.post<any, t>('/getVideoInfo', params)
}

export const randRecommend = (params: { limit: number }) => {
  type t = AxiosResponse<{ result: VideoDesc[] }>
  return client.post<any, t>('/h5/listVideoByRand', params)
}

export const zan = (params: { id: string; count?: 1 | -1 }) => {
  type t = AxiosResponse<{ result: VideoDesc[] }>
  return client.post<any, t>('/updateVideoZanCount', params)
}
