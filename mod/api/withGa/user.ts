import { withGAEvent } from '~/mod/utils'
import * as user from '../user'
export * from '../user'

export const register = withGAEvent(user.register)({
  category: '用户自动注册',
  action: '注册',
})

export const login = withGAEvent(user.login)({
  category: '用户登录',
  action: 'token登录',
})

export const updateInfo = withGAEvent(user.updateInfo)({
  category: '修改资料',
  action: '修改',
})

export const uploadAvatar = withGAEvent(user.uploadAvatar)({
  category: '上传头像',
  action: '上传',
})

export const sendCode = withGAEvent(user.sendCode)({
  category: '验证码',
  label: '绑定手机号',
  action: '发送',
})

export const bindPhone = withGAEvent(user.bindPhone)({
  category: '用户资料',
  action: '绑定手机号',
})

export const switchAccount = withGAEvent(user.switchAccount)({
  category: '用户登录',
  action: '切换账号',
})
