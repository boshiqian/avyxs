export * from '../video'
import { withGAEvent } from '~/mod/utils'
import * as video from '../video'

export const zan = withGAEvent(video.zan)({
  category: '视频',
  action: '点赞',
})
