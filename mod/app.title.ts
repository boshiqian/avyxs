export const appname = 'AV研习社'
export const title = {
  '/': '首页',
  '/video': '视频播放',
  '/search': '搜索',
  '/tag/category': '分类',
  '/member': '我的主页',
  '/member/store': '商店',
  '/member/store/coin': '金币购买',
  '/member/store/vip': '会员购买',
  '/member/store/vip2': '会员购买',
}

export const genTitle = (pathname: string, extitle: string = '') => {
  let names = [appname]
  return appname
  if (pathname === '/') {
    names.push(title['/'])
  } else {
    pathname
      .split('/')
      .slice(1)
      .reduce((t, e) => {
        t = t + '/' + e
        let v = title[t]
        if (v) {
          names.push(v)
        }
        return t
      }, '')
  }
  if (extitle) {
    names.push(extitle)
  }
  names.reverse()
  return names.join(' - ')
}
