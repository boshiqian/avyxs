import ReactGA from 'react-ga'

/**
 * 展示数据
 * > 表示被查看的产品的相关信息
 */
export interface ImpressionData {
  id: string
  name?: string
  /**产品所在的列表或集合（例如搜索结果） */
  list?: string
  /**与产品关联的品牌（例如 Google）。 */
  brand?: string
  /**产品所属的类别（例如服装）。可以使用 / 作为分隔符来指定最多 5 个层级（例如服装/男装/T 恤）。 */
  category?: string
  /**	产品的细分款式（例如黑色）。 */
  variant?: string
  /**产品在列表或集合中的位置（例如 2）。 */
  position?: number
  /**产品的价格（例如 29.20）。 */
  price?: number
}

/**
 * 产品数据
 * > 产品数据表示被查看（或添加到购物车等操作）的具体产品。
 */
export interface ProductData {
  id: string
  name?: string
  /**与产品关联的品牌（例如 Google）。 */
  brand?: string
  /**产品所属的类别（例如服装）。可以使用 / 作为分隔符来指定最多 5 个层级（例如服装/男装/T 恤）。 */
  category?: string
  /**	产品的细分款式（例如黑色）。 */
  variant?: string
  /**产品的价格（例如 29.20）。 */
  price?: number
  /**产品的数量（例如 2）。 */
  quantity?: number
  /**与产品关联的优惠券代码（例如 SUMMER_SALE13） */
  coupon?: string
  /**产品在列表或集合中的位置（例如 2）。 */
  position?: number
}

/**
 * 操作数据
 * > 表示发生的电子商务相关操作的信息。
 */
export interface ActionData {
  /**
   * 交易 ID（例如 T1234）。
   * > 如果操作类型是 purchase 或 refund，则必须提供此值
   */
  id?: string
  /**发生此交易的商店或关联商户（例如 Google Store）。 */
  affiliation?: string
  /**
   * 与这笔交易关联的总收入或总计金额（例如 11.99）。此值可能包含运费、税费或其他要计入 revenue 的总收入调整值。
   * > 注意：如果不设置 revenue 的值，系统将自动根据同一命中中所有产品的 quantity 和 price 字段来计算此值。
   */
  revenue?: number
  /**交易对应的总税费。 */
  tax?: number
  /**交易关联的运费。 */
  shipping?: number
  /**在交易中使用的优惠券。 */
  coupon?: string
  /**关联产品所属的列表。可选。 */
  list?: string
  /**表示结帐流程中某个步骤的数字。对于 `checkout` 操作，可自由选择是否提供此值。 */
  step?: number
  /**	checkout 和 checkout_option 操作的附加字段，用于描述结帐页面上的选项信息，例如所选的付款方式。 */
  option?: string
}

export class Ec {
  static PluginName = 'ec'
  static init = () => {
    ReactGA.plugin.require('ec')
  }
  /**衡量展示 */
  addImpression = (items: ImpressionData) => {
    ReactGA.plugin.execute(Ec.PluginName, 'addImpression', items)
  }
  addProduct = (items: ProductData) => {
    ReactGA.plugin.execute(Ec.PluginName, 'addProduct', items)
  }
  clear = () => {
    ReactGA.plugin.execute(Ec.PluginName, 'clear', {})
  }
  private _action = (action: string, params: any) => {
    ReactGA.plugin.execute(Ec.PluginName, 'setAction', action, params)
  }
  /**对某个产品的点击，或是对一个或多个产品的链接的点击。 */
  click = (action: ActionData = {}) => {
    this._action('click', action)
  }
  /**查看商品详情。 */
  detail = (action: ActionData = {}) => {
    this._action('detail', action)
  }
  /**	将一个或多个产品添加到购物车。 */
  add = (action: ActionData = {}) => {
    this._action('add', action)
  }
  /**从购物车中移除一个或多个产品。 */
  remove = (action: ActionData = {}) => {
    this._action('remove', action)
  }
  /**开始一个或多个产品的结帐流程。 */
  checkout = (action: ActionData & { step: number }) => {
    this._action('checkout', action)
  }
  /**发送某个结帐步骤的选项值。 */
  checkout_option = (action: ActionData & { option: string }) => {
    this._action('checkout_option', action)
  }
  /**购买一个或多个产品。 */
  purchase = (action: ActionData & { id: string }) => {
    this._action('click', action)
  }
  /**为一个或多个产品退款。 */
  refund = (action: ActionData = {}) => {
    this._action('click', action)
  }
  /**对内部促销信息的点击。 */
  promo_click = (action: ActionData = {}) => {
    this._action('click', action)
  }
}

export const ec = new Ec()
