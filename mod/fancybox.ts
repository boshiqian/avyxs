import '@fancyapps/fancybox/dist/jquery.fancybox.min.css'
import './jquery'
if (process.browser) {
  require('@fancyapps/fancybox')
  // @ts-ignore
  Object.assign(jQuery.fancybox.defaults, {
    buttons: [],
    smallBtn: !1,
    touch: !1,
    closeExisting: !0,
  })
}
