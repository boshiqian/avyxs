export default 'es6'
import { useEffect, useRef } from 'react'
import './jquery'
if (process.browser) {
  require('lazyload')
}

export const useLazyloadRef = () => {
  const ref = useRef()
  useEffect(() => {
    if (!ref.current) {
      return
    }
    if (process.env.NODE_ENV === 'development') {
      return
    }
    // @ts-ignore
    jQuery('.lazyload').lazyload()
  }, [ref.current])
  return ref
}
