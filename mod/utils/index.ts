import { APIResponse } from '../api/api'
import ReactGA from 'react-ga'

export { beforeNow } from './time'
export const isRespError = (err: any): err is APIResponse => {
  if ((err as APIResponse).success == false) {
    return true
  }
  return false
}

export const tapError = (fn: (err: any) => any) => (err: any) => {
  fn(err)
  return Promise.reject(err)
}

export const tapRespErr = (fn: (err: string) => any) => (err: any) => {
  let l = isRespError(err) ? err.message : '未知错误'
  fn(l)
  return Promise.reject(err)
}

export const tapOk =
  <T = any>(fn: (r: T) => any) =>
  (r: T) => {
    fn(r)
    return r
  }

export const withGAEvent =
  <T>(f: T) =>
  (ec: ReactGA.EventArgs): T => {
    // @ts-ignore
    return (...r) => {
      ReactGA.event(ec)
      // @ts-ignore
      return f(...r).then(
        tapOk(() => {
          ReactGA.event({ ...ec, action: ec.action + '成功' })
        }),
        tapRespErr((l) => {
          if (ec.label) {
            l = ec.label + '-' + l
          }
          ReactGA.event({ ...ec, action: ec.action + '失败', label: l })
        }),
      )
    }
  }
