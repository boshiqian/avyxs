import moment, { Moment } from 'moment'
export const beforeNow = (t: Moment, n = moment()) => {
  let r: number
  r = n.diff(t, 'years')
  if (r > 0) {
    return r + '年前'
  }
  r = n.diff(t, 'months')
  if (r > 0) {
    return r + '月前'
  }
  r = n.diff(t, 'weeks')
  if (r > 0) {
    return r + '星期前'
  }
  r = n.diff(t, 'days')
  if (r > 0) {
    return r + '天前'
  }
  r = n.diff(t, 'hours')
  if (r > 0) {
    return r + '小时前'
  }
  r = Math.max(1, n.diff(t, 'minutes'))
  if (r > 0) {
    return r + '分钟前'
  }
  return r
}
