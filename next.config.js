/**@type {import("next").NextConfig} */
module.exports = {
  pageExtensions: ['page.tsx', 'api.ts'],
  images: { loader: 'imgix', path: 'https://noop/' },
}
