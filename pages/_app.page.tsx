import '~/mod/bootstrap'
import '../styles/_globals.css'
import Head from 'next/head'
import type { AppProps } from 'next/app'

import { QueryClientProvider } from 'react-query'
import { queryClient, APIClient } from './api'

import '~/mod/layzload'

import React, { useEffect, useRef } from 'react'
import { Header as SiteHeader, HeaderQuery } from '~/components/Site/Header'
import { Footer as SiteFooter } from '~/components/Site/Footer'
import { SiteContent } from '~/components/Site/Content'
import { Nav as SiteNav } from '~/components/Site/Nav'
import { PayProvider } from '~/components/Site/Store'
import { ModalProvider } from '~/components/Site/Modal'
import { UserProvider, UserQuery } from './user.state'
import { CheckInitUser } from './app/CheckInitUser'
import { ToastContainer } from 'react-toastify'
import { appname, genTitle } from '~/mod/app.title'
import { HomeState } from './home/home.state'

const Providers: React.FC<{ providers: React.FC[] }> = (props) => {
  let providers = props.providers
  providers = providers.reverse()
  return providers.reduce((children, Provider) => {
    return <Provider>{children}</Provider>
  }, props.children as any)
}

function MyApp({ Component, pageProps, router }: AppProps) {
  const ComponentHead: React.FC = (Component as any)?.Head

  useAppInit()
  return (
    <Providers
      providers={[
        (props) => <APIClient.Provider>{props.children}</APIClient.Provider>,
        ({ children }) => (
          <QueryClientProvider client={queryClient}>
            {children}
          </QueryClientProvider>
        ),
        (props) => <HomeState.Provider>{props.children}</HomeState.Provider>,
        (props) => <UserQuery.Provider>{props.children}</UserQuery.Provider>,
        PayProvider,
        ModalProvider,
      ]}
    >
      <ToastContainer
        autoClose={3e3}
        position="bottom-center"
        hideProgressBar
        closeButton={false}
      />
      <CheckInitUser />
      <>
        <Head>
          <title>{genTitle(router.pathname)}</title>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no"
          />
        </Head>
        {ComponentHead && <ComponentHead />}
        <HeaderQuery.Provider>
          <SiteNav />
          <SiteHeader />
        </HeaderQuery.Provider>

        <UserProvider>
          {pageProps.noWrap ? (
            <Component {...pageProps} />
          ) : (
            <>
              <SiteContent>
                <Component {...pageProps} />
              </SiteContent>
              <SiteFooter />
            </>
          )}
        </UserProvider>
      </>
    </Providers>
  )
}

import { initApp, useAppInit } from './app'
import dynamic from 'next/dynamic'
const NoSSRApp = dynamic(
  async () => {
    await initApp()
    return MyApp
  },
  { ssr: false },
)
export default NoSSRApp
