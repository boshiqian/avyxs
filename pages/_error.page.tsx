// export const getStaticProps = () => ({ props: { noWrap: true } })
import { useRouter } from 'next/router'
import styles from './_error.module.css'

export default function ErrorPage() {
  const router = useRouter()
  return (
    <div className="container">
      <div className={styles.root}>
        <div className="h3">出错了</div>
        <div className={`h5 ${styles.content}`}>网络波动, 请稍候重试</div>
        <div>
          <button className="btn btn-submit" onClick={() => router.reload()}>
            重新加载
          </button>
        </div>
      </div>
    </div>
  )
}
