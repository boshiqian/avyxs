import { useRouter } from 'next/router'
import { getUid, setUid, UserQuery } from '../user.state'
import * as agent from '~/mod/api/agent'
import Link from "~/node_modules/next/dist/client/link";
import CopyToClipboard from 'react-copy-to-clipboard';
import { toast } from 'react-toastify';
export default function AgentPage() {
    const router = useRouter();
    const data =agent.GetAgentStatic({userId:getUid()}).then((r) => r.data.result);
    const domains=window.location.protocol+'//'+window.location.host+'/';
   const onCopy=()=>{
       toast.success('复制成功')
    }
    return (
        <>
            <div className="container">
                <div className="main-wrap">
                    <div className="t-view">
                        <div className="t-v-menu">
                            <div className="t-v-menu-item">
                                <div className="u-img">
                                    <div
                                        className="icon1"></div>
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAADuklEQVRIS7WVX0xbVRzHP+e2lD8DCmVUJijQIbhFE0rVmDE3HjTL1EwxMKM+TJzRN6LRiIlGxuKDMzFLFh90ibIlZuogW4wmRKcZWzYelkCbbNn4NygO6OxaaLkT2g7uMfe2QEGKM8bzdHLzO9/zO9/v7/u9gv9piVS4LS6XNWNB1gO7pcAlNex6rVDwC0kv0BUxidOHenvDa2H8DbgRTFuczr0g2orvEY5KB6ayYkFObvy4OgPeCcngCAsTf8gRkK3X3O6THbCQfMEK4LdLSjJzCgoP5eaIN5/aoViqHAJLhoT0GKQlzt0xQdRCLCIYGJH8cl6Lqar8Ug3eajk8Pj63CL4E3FpXZ9amw58V2UXz3mcUCmzAphA84AfrLCJj3jgjI2YIZ8GQHXx5BKfh5E8aN/3yiJJvfaetu9soXAJuczpf2pAt2l9tUNJtNgk1Y4jywLrSytGN0FfK1JTgWKcW/fO2bGp1u79dAjaE0rRLL+w2VT68BajxIhwBkIACaKvwF78JkCM6eBmXr8GpLm0ooohHdUGNjj+qrt5XXKR8va/BrFhKpxC1wwbSwoLgxLubcdUH2LozZFw0E0zjuxYHz384hr0sEqfnYgWxMRvHO+e1yZvsb/P0HTOAW6udHXXblIadtQJqBxB2Nd6iAoMXrZw9uommLwaxZGmc+fxeZkNm9nzwOyLxEunPgQtVnOuRdPdonW0ed2O8Y2fNcFOjsrm0MoZ48ipY4kLpa14KOt4vp2p7mPLHVL55q4JXDl/HVhRd5idmRv66lbFBC+0d2vWD7r6KBBU14eb9Sq7tvghi15U4t4mlbycHsvjh4zLsjjk23h9hx+s+lKQafQTkzw8xdSODI19pMwc9fdb1gQVM9mdx9WweQz1WQj4LrucCFJZHcO4JLlsiJfA6VMypJmZVM4PnrHg92exqnsBk1rAW3Vl+WSoqlsTbJmB7kngCfP2ZXPnNRnAsg8CNdB58IkxeUZRHGgJLHacWL8W4GTTrZKXB5R/z6b+QR+Mno/G51vVN2CvluOkGydS0S/VrGSQBHvCmE/KlU/H4zLKy/2QQvfJAdfXLG7KV9qZGxRK3tBdRHlwBYnSY5EI5WmC4Trf08U4tqqraawc8nhMrsiI5hF58VsGWnwihCj/krQqhUBYMx0Noahq+Xy+E9Fv02MwusH9qzeGNu43NM+e1WFjl6O2g/701Y3PxzWsGfcmqoB//l0GfnGGLvyaJfBqES0oKDe4Et0D2IkVX1CxO3fWvKRn8v+z/Ank93ibf1scWAAAAAElFTkSuQmCC" />
                                    </div>
                                <p><span>奖励</span></p>
                            </div>
                            <div className="t-v-menu-item">
                                <div className="u-img">
                                    <div></div>
                                    <img
                                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAF3klEQVRIS8WWe1BUZRTAf3ffuyK28goNHygoTUiK+chKpPGRWlLa1IBOxUgzaaPY02pqSrOU7KVZymiZ2muYkR6EOaOiAzWZBlLyUBR5KArLsrz2dVnube4mhrjuUv7RN7Mz+8095/zO+c75zvkE/qclBOBGAKnALCABCAcUnWbgT+AAsAc4/2/9vx54IPCGUa9etvC+4fqUWVFMjA9hSLgRQRC41OykpKyF7w+c55u8Go/d3rVTgpcuO9QvH3yBbwO+S1swMjprdSKR4Ua/hqytbl597wRbvzjdACwEfu0PuS84QaMWCraum2JOf3h0f/SvyOTuryNtVZHD5e6eAxQGUu4NHgyUfPbOncMee2hUID2f3/MOnWfBk4ebZVkeHyjvvcHZTzwck7Fj/ZSrjO5z3M8YbTnR2rP9cmZ1VjFZ2eXfIcsp/hR6wCOMJn1VzZEHNGGDDVfkDzjnsNfxqHcfo6kkyXCAeF0JGqH7ujbtTg/R03OxWLsToav4eoI94NfSHxnz+va3Jnn3HdJAcuxpHBenXqNnEuyM0xWToCsmVluBUXBeI/PyxhOs31a2BUl6OhC4cM+2lLvmJkdR5JpBgWsWTtkU8GhVdBOlqWWEppoodQ2R6gZC1BbKSs9xV+qhWll0jQgEbsnIqzRrwmOQvf3hxlbR2WpOpsXKSJKSN9GXtR6KtLTQLmj0/u9sf9yRZZnCugtULolDctmVTmfxB3anH7LpdEGD+mPbr0ynKPJ7QyOVqaORPV2KwXZ/4NOLdh2PCR074YbBtW3tVJ2ppDpzhii5nfpAOd41NfO9xQmpmVcluDw3m5jZqWhNQT71u7tEynK2EP/ICgS1GkmWOXbhEo0FOTTtXnfM02qZFAi8ICQ2IXfR7mJBGQI9q2BtOq3nKrl37R6Ch0ZfZcNhvcThNekoOZ374Y8Igor69g6qbW3UrUnFbal/3nOpdmMgsBoom521N3ZkUsoVsixJFO98m9I97zI0MYmwuERvZNaqP6j75SfGPpDO5OXrUGv1dLhFTjRa6Cw/Sv3bjzukcXdHcnSfz/wqzvQ+2jkDwobkK1EbByvF+M9ytbVQW5SH7Vw5yDKDhsUwfNo8TKGRXqFOsYs/Gi247O3UvDgfQaN/xl1f8b6/gul7abPC4iY+N2/zfsEQbA5YaDLQ2GnnTEsrXS4H9RvS8TTW7xeb6pQJ5Xf1BSv7jwZExS6b9sZuom5NRKtSXWNAKSKrw0l9eycdooh4sYYLHyzH02Y95ImfNY/DO12XlZRrMgw4qHTi3oau16YyUGs2mu99NPiW2WmYx05Eo1J5q9bp8XiPVvnvqjtF68GvaCvI6daYw98VG84qrxDpMiBTr1O9Hx5ioP6ioxpQnGjrgfvrj8nAPrXeqFOHDME4ehwacwQIAh6bBde5k3iaG5BFl4zO8KbkaH+tz9FU7lg/dcySB6MZlZSrwJUx900gcAyC8PPdL3wceuuDS4Xm06UoP2dLo/f66M2hhIyKJyIuEVtNJfmZ82R70/mngG3As4Ayi+/I/zRZP2f6UMbPz6O0wqYM9BLgZaDKV8Qa4LcpKzbefvviZ/o1MZqry+S9T0zplpz2DeZBulfWrEpggEnLzGmRmEJDSd4VR5Otm87ig3T8mn8SiPdhWJ0RET8pO2V7obcpBFpKrsssVs7kbKbx87XS9MkRqoIvldcwtHsMPF61lFPOv6+d/c8ipbko00rvK6LiGVm548ckLQjExC52ccpq81a2JLqpenIikr3NGTd6kDE4SItu8Qaah8+kYXMm7oZqxAtnkJydm4CVfcE3qwymi7E7T2LSG7jJoGeAToterUatEkAGj1LZXR5aXS5sLvdVzjVsWknb0fwNiO7fgS1RL30eFjRhBtXPzcZdW7EV+Bo40rdzKftkY+yEgyPWfRswWl8C1h+yadr15ifAMuBw8LT7pw+cOh/FIVl03dP72ds3YiU5+/8T9R+l7UAGMBbYcbmBZANr+9NAbpAdWP0vKdJSPf3vu20AAAAASUVORK5CYII=" />
                                    </div>
                                <p><span>邀请人数</span></p>
                            </div>
                        </div>
                        <div className="t-v-list">
                            <div className="t-v-list-item">
                                <div className="t-v-list-col">
                                    <div className="t-v-jb u-img">

                                    </div>
                                    300
                                </div>
                                <div className="t-v-list-col">累计邀请3人</div>
                                </div>
                            <div className="t-v-list-item">
                                <div className="t-v-list-col">
                                    <div className="t-v-jb u-img">

                                    </div>
                                    1000
                                </div>
                                <div className="t-v-list-col">累计邀请10人</div>
                                </div>
                            <div className="t-v-list-item">
                                <div className="t-v-list-col">
                                    <div className="t-v-vip u-img">

                                    </div>
                                    10天
                                </div>
                                <div className="t-v-list-col">累计邀请15人</div>
                                </div>
                            <div className="t-v-list-item">
                                <div className="t-v-list-col">
                                    <div className="t-v-vip u-img">

                                        </div>
                                    30天
                                </div>
                                <div className="t-v-list-col">累计邀请20人</div>
                                </div>
                        </div>
                    </div>
                    <div className="share-view">
                        <div className="app_url" id="urllink">官网地址：http://avyxs.com/</div>
                        <div className="code-view">
                            <div className="code-l">
                                <div className="code-txt">
                                    <p><span>截图保存二维码</span>
                                    </p>
                                </div>
                                <div className="code-img">
                                    <div className="tki-qrcode">
                                        <div className="u-img">
                                            <img src={'http://vapi.pipe-welding.cn/api/user/getTgCode?domain='+window.btoa(domains+'?t='+getUid())} id="codeImg"/>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div className="code-r">
                                <CopyToClipboard className="share-btn" id="copybtn" text={domains+'?t='+getUid()}
                                                 onCopy={onCopy}>
                                    <span>复制推广链接</span>
                                </CopyToClipboard>
                                <div className="share-btn" id="myPromote"><Link href={{ pathname: '/agent/static' }} passHref>
                                        <span>我的推广</span>
                                </Link></div>
                            </div>
                        </div>
                        <div className="foot_url">若扫码失败，请在浏览器输入以下地址进入
                            http://avyxs.com/
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
