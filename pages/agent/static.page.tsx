import * as store from '~/mod/api/store'
import { useRouter } from 'next/router'
import { UserState } from '../game/user.state'
import { useQuery } from 'react-query'
import * as agent from '~/mod/api/agent'
import React, { useEffect,useMemo, useState } from 'react'
import {getUid} from "~/pages/user.state";
import {usePageLoading} from '../../components/Site/Modal/Modal.state'
import {list} from "postcss";
export default function StaticPage() {
    const router = useRouter();
    let agentData:any={total:'',today:''};
    const [statistics, setSs] = useState({});
    const [list, setSlist] = useState([]);
    const [pages, setSpages] = useState({page:1,limit:20});
    const [hasMore, setSmore] = useState(true);
    const [isLoading, setSLoading] = useState(true);
    let hasData =true;
    let totalPage=1;
    usePageLoading(isLoading)
    useEffect(() => {
        agent.GetAgentStatic({userId:getUid()}).then((r) =>{
            setSs(r.data.result)
        })
        loadList();
    }, [])
     agentData= useQuery(['agents'], () => {
        return agent.GetAgentTotal({userId:getUid()}).then((r) => r.data.result)
    }).data;
    let userInfo=useQuery(['userInfo'], () => {
        return agent.GetUserInfoe({userId:getUid()}).then((r) => r.data.result)
    }).data;
    const refresh = () => {
        setSLoading(true)
        agent.GetAgentStatic({userId:getUid()}).then((r) =>{
            setSs(r.data.result)
            setSLoading(false)
        })
        setSs({})
    };
    const loadList=()=>{
        hasData=false;
        if (pages.page<=totalPage)
        {
            setSLoading(true)
            agent.GetPromoteRecord({userId:getUid(),...pages}).then((r) => {
                r.data.result;
                totalPage=parseInt(String((r.data.result as any).total / pages.limit))+1;
                if (totalPage==1)
                {
                    setSmore(false)
                }
                let page=pages.page+1;
                let arr=list;
                (r.data.result as any).records.forEach((item)=>{
                    arr.push(item)
                });
                setSpages({page:page,limit:20});
                setSlist(arr);
                setSLoading(false)
            })
        }
        else {

        }
    };
    return (
        <>
            <div className="container">
                <div className="user-header">
                    <div className="user-img">
                        <div className="img-file">

                        </div>
                    </div>
                    <div data-v-bbbefac2="" className="user-name">
                        <p><span id="username">{userInfo?(userInfo as any).account:''}</span> <span className="gold-color" id="level" style={{marginLeft:' 10px',fontSize:' 11px'}}>{userInfo?(userInfo as any).agentLevel:''}</span>
                        </p>
                        <div className="v-info">
                            <div>总推广 <span id="totalPromote">{agentData?agentData.total:0}</span></div>
                            <div>今日推广 <span id="todayPromote">{agentData?agentData.today:0}</span></div>
                        </div>
                    </div>
                </div>
                <div className="total">
                    <div className="total-item">
                        <div className="t-i-t">
                            <p className="t-i-t-l"><span>今日业绩</span></p>
                            <p className="t-i-reload"><span id="refresh1" onClick={refresh}>刷新</span></p>
                        </div>
                        <div className="total-main">
                            <div className="total-row">
                                <div className="total-row-l">充值金额</div>
                                <div className="total-row-r" id="totalAmount">￥{statistics?(statistics as any).todayMoney:''}</div>
                            </div>
                            <div className="total-row">
                                <div className="total-row-l">充值人数</div>
                                <div className="total-row-r" id="totalMember">{statistics?(statistics as any).todayCount:''}人</div>
                            </div>
                        </div>
                    </div>
                    <div className="total-item">
                        <div className="t-i-t">
                            <p className="t-i-t-l">
                                <span>总业绩</span></p>
                            <p className="t-i-reload">
                                <span id="refresh2" onClick={refresh}>刷新</span>
                            </p>
                        </div>
                        <div className="total-main">
                            <div className="total-row">
                                <div className="total-row-l">充值金额</div>
                                <div className="total-row-r" id="todayAmount">￥{statistics?(statistics as any).totalMoney:''}</div>
                            </div>
                            <div className="total-row">
                                <div className="total-row-l">充值人数</div>
                                <div className="total-row-r" id="todayMember">{statistics?(statistics as any).totalCount:''}人</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tab-list">
                    <ul className="list" id="list">
                        {list?list.map((item,i) => {
                            return (
                                    <li key={'x' + i}>
                                        <h4><span className="fl">{item.agentLogType_dictText}</span>
                                            <span className="fr color-gold">{item.money}</span>
                                        </h4>
                                        <p className="charge-time clearfix"><span className="fl">订单号</span><span className="fr">{item.orderNum}</span></p>
                                        <p className="charge-time clearfix"><span className="fl">创建时间</span><span className="fr">{item.createTime}</span></p>
                                    </li>
                            )
                        }):null}
                    </ul>
                    <a className="load-more" id="load" onClick={loadList}>{(hasMore&&hasData||!list.length)?'暂无数据':(hasMore?'加载更多':'已加载全部')}</a>
                </div>
            </div>
        </>
    )
}