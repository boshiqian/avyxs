import { QueryClient } from 'react-query'

let imageBase = ''
export let ImageBase = (img: string) => {
  if (imageBase == '') {
    return ''
  }
  return imageBase + img
}
export const setImageBase = (n: string) => (imageBase = n)

export let AppConfig: Config = {
  kfUrl: '',
  resUrl: '',
}
export const setAppConfig = (n: Config) => (AppConfig = n)

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      refetchOnMount: false,
    },
  },
})

import { AxiosInstance } from 'axios'
import { useMemo, useReducer, useState } from 'react'
import { createContainer } from 'unstated-next'
import { client } from '~/mod/api'
import { Config } from '~/mod/api/types'

const useAPIClient = () => {
  const _client = useMemo(() => {
    client.interceptors.request.use((config) => {
      return config
    })
    return client
  }, [])
  return _client
}

export const APIClient = createContainer(useAPIClient)
