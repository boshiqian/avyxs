import { PageConfig, NextApiResponse, NextApiRequest } from 'next'
import { createProxy } from 'http-proxy'

export const config: PageConfig = {
  api: { bodyParser: false, externalResolver: true },
}

const target = 'http://qp01api.lsxtdlzpt.com/api'

const proxy = createProxy({
  target: target,
  ignorePath: true,
})

export default (req: NextApiRequest, res: NextApiResponse) => {
  let proxyStr = [].concat(req.query.proxy).join('/')
  let url = req.url.slice(req.url.indexOf(proxyStr) - 1)
  url = `${target}${url}`
  delete req.headers.host
  return proxy.web(req, res, { target: url }, (e, req, res, target) => {
    res.writeHead(500)
    res.end('api backend has error')
  })
}
