import { useEffect } from 'react'
import { ModalState, ModalProvider } from '~/components/Site/Modal'
import { isRespError } from '~/mod/utils'
import { SwitchAccount } from '~/pages/member/Settings'
import { UserQuery } from '../user.state'
import ReactGA from 'react-ga'

const CheckInitUser = () => {
  const [_x, dispatch] = ModalState.useContainer()
  const q = UserQuery.useContainer()

  useEffect(() => {
    if (!q.isError) {
      return
    }
    dispatch({
      type: 'show',
      content: (
        <SwitchAccount
          login={isRespError(q.error) ? q.error.message : '登陆后方可继续'}
        />
      ),
    })
    // ga
    ReactGA.event({
      category: '切换账号',
      action: '登录出错, 重新登录',
      label: isRespError(q.error) ? q.error.message : '未知错误',
    })
  }, [q.error])
  return null
}

const CheckInitUserWrapper = () => {
  return (
    <ModalProvider>
      <CheckInitUser />
    </ModalProvider>
  )
}

export { CheckInitUserWrapper as CheckInitUser }
