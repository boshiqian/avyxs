import { useRouter } from 'next/router'
import { useEffect } from 'react'
import ReactGA from 'react-ga'
import { ec, Ec } from '~/mod/ec'

export async function initReactGA() {
  // @ts-ignore
  // window.GA_XHR_PATH =
  //   process.env.NODE_ENV === 'development'
  //     ? 'https://www.google-analytics.com'
  //     : '/ga-proxy'
  ReactGA.initialize('UA-215389368-1', {
    // src: https://www.google-analytics.com/analytics.js
    // gaAddress: '/js/analytics.js?v=20211215',
    // debug: true,
  })
  ReactGA.pageview(window.location.pathname + window.location.search)
  Ec.init()
}

export const useReactGA = () => {
  const router = useRouter()
  useEffect(() => {
    const handleRouteChange = (url: string) => {
      ec.clear()
      ReactGA.pageview(url)
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [])
}
