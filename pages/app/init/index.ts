import { initReactGA, useReactGA } from './ga'
import { initUser } from './user'

export const initApp = () => {
  return Promise.all([
    initReactGA(),
    initUser(),
    // initReactGA(),
  ])
}

export const useAppInit = () => {
  useReactGA()
}
