import { withGAEvent } from '~/mod/utils'
import { registerUser as _registerUser } from '~/pages/user.state'

const registerUser = withGAEvent(_registerUser)({
  category: '用户注册',
  action: '自动注册',
  label: '初始化',
})
export const initUser = async () => {
  await registerUser()
}
