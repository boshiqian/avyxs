import { Dispatch, useEffect, useReducer } from 'react'
import { useQuery } from 'react-query'
import { createContainer } from 'unstated-next'
import * as gameApi from '../api/game'

type State = {
  game: string
  opening: boolean
  show: boolean
  iframe: string
  isFullscreen: boolean
  panelShow: boolean
}

type Action =
  | { type: 'open'; game: string }
  | { type: 'opened'; iframe: string }
  | { type: 'opening'; value: boolean }
  | { type: 'close' }
  | { type: 'removed' }
  | { type: 'fullscreen'; value: boolean }
  | { type: 'panel'; show: boolean }

const StateReducer = (s: State, action: Action): State => {
  let ns: Partial<State> = {}
  switch (action.type) {
    case 'open':
      ns.game = action.game
      ns.opening = true
      ns.show = true
      break
    case 'close':
      ns.show = false
      ns.panelShow = false
      ns.isFullscreen = false
      if (document.fullscreenElement) {
        document.exitFullscreen()
      }
      break
    case 'opened':
      ns.opening = false
      ns.iframe = action.iframe
      ns.panelShow = false
      break
    case 'opening':
      ns.opening = action.value
      break
    case 'fullscreen':
      ns.isFullscreen = action.value
      break
    case 'removed':
      ns.game = ''
      break
    case 'panel':
      ns.panelShow = action.show
      break
  }
  return { ...s, ...ns }
}

const initState: State = {
  game: null,
  opening: false,
  show: false,
  iframe: null,
  isFullscreen: false,
  panelShow: false,
}

const useGameState = () => {
  const [state, dispatch] = useReducer(StateReducer, initState)
  const game = useQuery(
    ['game', state.game],
    ({ queryKey: [_name, game] }) => {
      if (!game) {
        return Promise.reject('no game id')
      }
      return gameApi
        .openGame({ gameId: game })
        .then((r) => {
          dispatch({ type: 'opened', iframe: r.data.result })
          return r.data.result
        })
        .catch((err) => {
          window.alert('打开失败')
          dispatch({ type: 'close' })
          console.error(err)
          return Promise.reject(err)
        })
    },
    {
      retry: 0,
    },
  )
  useEffect(() => {
    dispatch({ type: 'opening', value: game.isFetching })
  }, [game.isFetching])
  return { state, dispatch, game }
}

export const GameState = createContainer(useGameState)
