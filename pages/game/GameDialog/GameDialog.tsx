import { Modal } from '~/components/Site/Modal'
import { GameState } from './Game.state'
import { GameIframe } from './GameIframe'
import styles from './GameDialog.module.css'

export const GameDialog = () => {
  const { state, game } = GameState.useContainer()
  return (
    <Modal show={state.show} loading={state.opening} className={styles.root}>
      {state.show && state.iframe && <GameIframe link={state.iframe} />}
    </Modal>
  )
}
