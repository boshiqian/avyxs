import React, { useEffect, useRef } from 'react'
import { GameState } from '.'
import styles from './GameIframe.module.css'

export const GameIframe: React.FC<{ link: string }> = (props) => {
  const { dispatch, state, game } = GameState.useContainer()
  const ref = useRef<HTMLIFrameElement>()
  useEffect(() => {
    if (!ref.current) {
      return
    }
    ref.current.onfullscreenchange = (e) => {
      const isFullscreen = document.fullscreenElement === ref.current
      if (!isFullscreen) {
        dispatch({ type: 'panel', show: true })
      }
      dispatch({ type: 'fullscreen', value: isFullscreen })
    }
    ref.current.requestFullscreen()
    return () => {
      // ref.current.onfullscreenchange = () => 0
    }
  }, [ref.current])
  useEffect(() => {
    return () => dispatch({ type: 'removed' })
  }, [])
  const toggleFullscreen = () => {
    if (state.isFullscreen) {
      document.exitFullscreen()
    } else {
      ref.current.requestFullscreen()
    }
  }
  const showPanelC = state.panelShow ? styles.panelShow : ''
  return (
    <div ref={ref} className={styles.root}>
      <iframe src={props.link} />
      <button
        className={styles.toggleBtn}
        onClick={() => dispatch({ type: 'panel', show: !state.panelShow })}
      >
        点我
      </button>
      <div className={`${styles.panel} ${showPanelC}`}>
        <div>
          <button onClick={() => dispatch({ type: 'panel', show: false })}>
            继续游戏
          </button>
          <button onClick={() => game.refetch()}>重新登录</button>
          <br />
          <button onClick={() => toggleFullscreen()}>
            {state.isFullscreen ? '退出全屏' : '全屏'}
          </button>
          <button onClick={() => dispatch({ type: 'close' })}>退出</button>
        </div>
      </div>
    </div>
  )
}
