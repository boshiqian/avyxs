import axios, { AxiosResponse } from 'axios'
import { Decrypt, Encrypt, Sign } from './crypto'

const client = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? '/api/qipai' : '/qipai-api',
})
export interface APIResponse<T = any> {
  success: boolean
  message: string
  code: number
  result: T
}

client.interceptors.request.use(function (req) {
  let d = req.data ?? {}
  d.timeStamp = d.timeStamp ?? Date.now()
  let w = JSON.stringify(d)
  w = Encrypt(w)
  req.data = { data: w }
  req.headers.sign = Sign(d)
  return req
})
client.interceptors.response.use(function (res) {
  let d: APIResponse = res.data
  if (!d.result) {
    return res
  }
  let result = Decrypt(d.result)
  res.data.result = JSON.parse(result)
  return res
})

client.interceptors.response.use(function (res: AxiosResponse<APIResponse>) {
  if (!res.data.success) {
    console.error(res.data)
    return Promise.reject(res.data)
  }
  return res
})

export { client }
