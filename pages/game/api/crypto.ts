import CryptoJS from 'crypto-js'

const KEY = CryptoJS.enc.Utf8.parse('83c4d4869f6as795')
const IV = CryptoJS.enc.Utf8.parse('')
const AppID = '218BD21F6F6F27B21CC78F344A3CEAC8'

export function Encrypt(word: string, key = KEY, iv = IV) {
  let srcs = CryptoJS.enc.Utf8.parse(word)
  var encrypted = CryptoJS.AES.encrypt(srcs, key, {
    iv: iv,
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  })
  // console.log("-=-=-=-", encrypted.ciphertext)
  return CryptoJS.enc.Base64.stringify(encrypted.ciphertext)
}

export function Decrypt(word: string, key = KEY, iv = IV) {
  let base64 = CryptoJS.enc.Base64.parse(word)

  let src = CryptoJS.enc.Base64.stringify(base64)

  var decrypt = CryptoJS.AES.decrypt(src, key, {
    iv: iv,
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7,
  })

  var decryptedStr = decrypt.toString(CryptoJS.enc.Utf8)
  return decryptedStr.toString()
}

export function Sign(s: { [k: string]: any }) {
  const u = new URL('https://noop')
  let ks = Object.keys(s)
  ks = ks.sort()
  let ss = ''
  for (let k of ks) {
    // u.searchParams.set(k, s[k])
    ss += `&${k}=${s[k]}`
  }
  // let ss = u.search.slice(1)
  ss = ss.slice(1)
  ss = ss + AppID
  return CryptoJS.MD5(ss).toString().toUpperCase()
}
