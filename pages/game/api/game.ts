import { AxiosResponse } from 'axios'
import { client, APIResponse } from './api'
import { User, PageParams, Page } from './types'

export type Game = {
  id: string
  /**分类id */
  categoryId: string
  /**平台游戏编号 */
  gameCode: string
  /**游戏平台编码 */
  gamePlatform: number
  /**游戏图片 */
  pic: string
  /**标题 */
  title: string
  sort: number
  status: string
}

export type getGamesParams = PageParams & { categoryId?: string }
export const getGames = (params: getGamesParams = {}) => {
  type r = Page<Game>
  type t = APIResponse<r>
  return client.post<any, AxiosResponse<t>>('/open/game/list', params)
}

export type openGameParams = { gameId?: string; gamePlatform?: string }
export const openGame = (params: openGameParams) => {
  /**游戏跳转链接 */
  type r = string
  type t = APIResponse<r>
  return client.post<any, AxiosResponse<t>>('/game/openGame', params)
}
