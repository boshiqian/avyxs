export interface Page<T = any> {
  current: number
  orders: string[]
  pages: number
  records: T[]
  size: number
  total: number
}

export interface PageParams {
  /**默认1 */
  page?: number
  /**默认20 */
  limit?: number
}

export enum Status {
  Allow = 1,
  Disabled = 0,
}

export type User = {
  account: string
  createTime: number
  id: string
  lastLoginIp: string
  lastLoginTime: number
  /**登录token */
  lastLoginToken: string
  userLevel: string
  level: {
    createTime: number
    id: string
    isDef: 1
    /**允许游戏 */
    isGame: Status
    /**允许充值游戏币 */
    isPay: Status
    /**允许提现 */
    isWithdrawal: Status
    /**提现手续费 % */
    profit: number
    /**层级名称 */
    title: string
    /**每日提现次数 */
    withdrawalCount: 3
    /**单次提现上限 */
    withdrawalMax: 300
  }
  /**钱包余额 */
  moneyBalance: number
  nickName: string
  phone: string
  /**真实姓名 */
  realName: string
  /**账号状态 */
  status: Status
  /**注册来源 */
  regCode: string
}
