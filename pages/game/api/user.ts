import { AxiosResponse } from 'axios'
import { client, APIResponse } from './api'
import { User } from './types'

export type loginParams = { account: string; pwd: string }
export const login = (params: loginParams) => {
  type t = APIResponse<User>
  return client.post<any, AxiosResponse<t>>('/open/user/login', params)
}

export const getUserInfo = (params: {} = {}) => {
  type t = APIResponse<User>
  return client.post<any, AxiosResponse<t>>('/user/getUserInfo', params)
}

export const getUserInfoTry = (params: {} = {}) => {
  type t = APIResponse<User>
  return client.post<any, AxiosResponse<t>>('/user/getUserInfo', params, {
    headers: {
      notLoginRequired: true,
    },
  })
}
