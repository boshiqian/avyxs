import { useQuery } from 'react-query'
import { usePageLoading } from '~/components/Site/Modal'
import * as gamesApi from './api/game'
import { GameState } from './GameDialog'

export const Games = () => {
  const r = useQuery('games', () => {
    return gamesApi.getGames().then((r) => r.data.result)
  })
  const { dispatch } = GameState.useContainer()
  usePageLoading(r.isLoading)
  if (r.isLoading) {
    return null
  }
  if (r.isError) {
    return (
      <div>
        <button onClick={() => r.refetch()}>出错了, 重新加载</button>
      </div>
    )
  }
  let items = r.data.records ?? []
  return (
    <div>
      {items.map((t) => {
        return (
          <div key={t.id}>
            <button onClick={() => dispatch({ type: 'open', game: t.id })}>
              {t.title}
            </button>
          </div>
        )
      })}
    </div>
  )
}
