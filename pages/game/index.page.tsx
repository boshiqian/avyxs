import { UserState } from './user.state'
import { Games } from './games'
import { GameDialog, GameState } from './GameDialog'

const GamePage = () => {
  const { state, user } = UserState.useContainer()
  if (state.isTokenChecking) {
    return (
      <div>
        <button>登录中</button>
      </div>
    )
  }
  if (!state.isAliveToken) {
    return (
      <div>
        <button onClick={() => user.login()}>点击登录</button>
      </div>
    )
  }
  return <Games />
}

export default function GamePageWithProvider() {
  return (
    <UserState.Provider>
      <GameState.Provider>
        <GamePage />
        <GameDialog />
      </GameState.Provider>
    </UserState.Provider>
  )
}
