import { useRouter } from 'next/router'
import { useEffect, useMemo, useReducer, useState } from 'react'
import { useMutation } from 'react-query'
import { createContainer } from 'unstated-next'
import { client } from './api'
import { User } from './api/types'
import * as userApi from './api/user'

type State = {
  isTokenChecking: boolean
  /**登录token是否有效 */
  isAliveToken: boolean
}

const QiPaiTokenKey = 'qipai-user-token'

const UserStateReducer = (s: State, ns: Partial<State>) => ({ ...s, ...ns })

let loginPromise: Promise<string> = null

const useUserState = () => {
  const [state, setState] = useReducer(UserStateReducer, {
    isTokenChecking: true,
    isAliveToken: false,
  } as State)
  const router = useRouter()
  useEffect(() => {}, [])
  const login = useMutation((params: userApi.loginParams): Promise<User> => {
    return userApi.login(params).then((r) => r.data.result)
  })
  const user = {
    checkToken: async () => {
      setState({ isTokenChecking: true })
      let p = userApi.getUserInfoTry()
      p.then(() => {
        setState({ isAliveToken: true })
      }).catch(() => {
        setState({ isAliveToken: false })
        localStorage.removeItem(QiPaiTokenKey)
      })
      p.then(() => {
        setState({ isTokenChecking: false })
      })
      return p
    },
    login: () => {
      let user = window.prompt('用户名')
      let cancelLoginErr = Promise.reject('取消登录')
      if (!user) {
        return cancelLoginErr
      }
      let pwd = window.prompt('密码')
      if (!pwd) {
        return cancelLoginErr
      }
      let p = login.mutateAsync({ account: user, pwd: pwd })
      p.then((user) => {
        setState({ isTokenChecking: false, isAliveToken: true })
        localStorage.setItem(QiPaiTokenKey, user.lastLoginToken)
      })
      p.then(() => {
        window.alert('登录成功')
      }).catch((err) => {
        window.alert('登录失败')
        return Promise.reject(err)
      })
      return p
    },
    logout: async () => {
      setState({ isTokenChecking: false, isAliveToken: false })
      localStorage.removeItem(QiPaiTokenKey)
    },
  }
  let _x = useMemo(() => {
    client.interceptors.request.use((c) => {
      if (c.url.startsWith('/open')) {
        return c
      }
      let token = localStorage.getItem(QiPaiTokenKey) ?? ''
      c.headers['token'] = token
      return c
    })
  }, [])
  useEffect(() => {
    if (typeof localStorage.getItem(QiPaiTokenKey) === 'string') {
      user.checkToken()
    } else {
      setState({
        isAliveToken: false,
        isTokenChecking: false,
      })
    }
  }, [])
  return { state, setState, user }
}

export const UserState = createContainer(useUserState)
