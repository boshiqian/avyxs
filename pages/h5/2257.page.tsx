import { ASideWrapper } from './ASide'
export { getStaticProps } from './ASide'
import { footerRightLinks } from '~/components/Site/Footer'
export default function X2257Page() {
  return (
    <ASideWrapper links={footerRightLinks}>
      <div className="content-header pt-0">
        <div className="container">
          <div className="title-with-avatar center">
            <div className="title-box">
              <h6 className="sub-title mb-1">政策</h6>
              <h2 className="h3-md mb-1">18 USC 2257 Statement</h2>
              <span className="inactive-color fs-2 mb-0">
                最後修訂: 2019-03-28
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <section className="pb-3 pb-e-lg-40">
          <p>
            avyxs.com is not a producer (primary or secondary) of any and all of
            the content found on the website (avyxs.com). With respect to the
            records as per 18 USC 2257 for any and all content found on this
            site, please kindly direct your request to the site for which the
            content was produced.
          </p>
          <p>
            avyxs.com is a video sharing site in which allows for the uploading,
            sharing and general viewing of various types of adult content and
            while avyxs.com does the best it can with verifying compliance, it may
            not be 100% accurate.
          </p>
          <p>
            avyxs.com abides by the following procedures to ensure compliance:
          </p>
          <ol>
            <li>Requiring all users to be 18 years of age to upload videos.</li>
            <li>
              When uploading, user must verify the content; assure he/she is 18
              years of age; certify that he/she keeps records of the models in
              the content and that they are over 18 years of age.
            </li>
          </ol>
          <p>
            For further assistance and/or information in finding the content's
            originating site, please contact avyxs.com compliance via Support Form
            .
          </p>
          <p>
            avyxs.com allows content to be flagged as inappropriate. Should any
            content be flagged as illegal, unlawful, harassing, harmful,
            offensive or various other reasons, avyxs.com shall remove it from the
            site without delay.
          </p>
          <p>
            Users of avyxs.com who come across such content are urged to flag it
            as inappropriate by clicking 'flag as inappropriate' link found
            below each video.
          </p>
        </section>
      </div>
    </ASideWrapper>
  )
}
