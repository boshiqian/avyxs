import React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { SiteContent } from '~/components/Site/Content'
import { Footer } from '~/components/Site/Footer'

type ASideProps = {
  links: { title: string; link: string; stitle?: string }[]
}
export const ASide: React.FC<ASideProps> = ({ links }) => {
  const router = useRouter()
  return (
    <aside className="col-lg-2 left-aside menu d-none d-lg-block">
      <nav className="navbar navbar-expand pl-3 pl-xl-5">
        <div className="collapse navbar-collapse">
          <ul className="navbar-nav flex-column">
            {links.map(({ title, link, stitle }) => {
              if (link.startsWith('http')) {
                return (
                  <li key={title} className="nav-item">
                    <a className="nav-link" target="_blank" href={link}>
                      <span className="separator">•</span>
                      {stitle ?? title}
                    </a>
                  </li>
                )
              }
              let aC = router.pathname === link ? 'active-color' : ''
              return (
                <li key={title} className="nav-item">
                  <Link href={link} passHref>
                    <a className={`nav-link ${aC}`}>
                      <span className="separator">•</span>
                      {stitle ?? title}
                    </a>
                  </Link>
                </li>
              )
            })}
          </ul>
        </div>
      </nav>
    </aside>
  )
}

export const getStaticProps = () => ({ props: { noWrap: true } })
export const ASideWrapper: React.FC<ASideProps> = (props) => {
  return (
    <SiteContent>
      <div className="container">
        <div className="row with-left-aside">
          <ASide links={props.links} />
          <div className="col-12 col-lg-10">
            {props.children}
            <Footer />
          </div>
        </div>
      </div>
    </SiteContent>
  )
}
