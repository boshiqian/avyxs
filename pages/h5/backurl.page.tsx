import IconApple from '~/components/iconfont/IconApple'
import IconAndorid from '~/components/iconfont/IconAndorid'
import React, { Fragment, useEffect, useMemo, useState } from 'react'
import styles from './backurl.module.css'
import { useQuery } from 'react-query'
import axios from 'axios'

export default function ShowPage() {
  return (
    <>
      <div className="content-header pt-0 mb-0">
        <div className="container">
          <div className="title-with-avatar center">
            <div className="title-box">
              <h6 className="sub-title mb-1"></h6>
              <h2 className="h3-md mb-1">备用网址</h2>
              <span className="inactive-color fs-2 mb-0">截图保存</span>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <section className="pb-3 pb-e-lg-40">
          <Links />
          <div className="py-2"></div>
          <div className="py-2 d-none d-md-block"></div>
          <Footer />
        </section>
      </div>
    </>
  )
}

const Links = () => {
  const { data: links } = useQuery('backurl-json', () => {
    return axios.get('/config/backurl.json').then((r) => r.data)
  })
  return (
    <div
      className="row gutter-10 gutter-lg-20 justify-content-center"
      style={{ margin: '0 auto' }}
    >
      {(links || []).map((l, i) => {
        return <Item key={'x' + i} l={l} />
      })}
    </div>
  )
}

const Item: React.FC<{ l: string }> = ({ l }) => {
  const [s, setS] = useState('30')
  useEffect(() => {
    setS((Math.random() * 40 + 20).toFixed(0))
  }, [])
  return (
    <Fragment>
      <div className={`col-12 col-lg-8`}>
        <div className={`d-flex mb-3 p-3 rounded ${styles.item}`}>
          <div className="col-8">{l}</div>
          <div className="col-2 text-center" style={{ color: '#46d041' }}>
            {s}ms
          </div>
          <a className="col-2 text-center text-nowrap" href={l}>
            点击进入
          </a>
        </div>
      </div>
      <div className="col-12"></div>
    </Fragment>
  )
}

const Footer = () => {
  return (
    <div className="row justify-content-center gutter-20 text-center d-lg-none">
      <div className="col-12 col-lg-6">
        <div className=" btn btn-submit btn-block">
          <a>截图保存</a>
        </div>
      </div>
      <div className="col-12"></div>
      <div className="col-6 col-lg-3">
        <a href="https://hhxx2.com/" target="_blank">
          <div className="btn btn-submit btn-block">
            <IconApple color="currentColor" />
            苹果下载
          </div>
        </a>
      </div>
      <div className="col-6 col-lg-3">
        <a href="https://hhxx2.com/" target="_blank">
          <div className="btn btn-submit btn-block">
            <IconAndorid color="currentColor" />
            安卓下载
          </div>
        </a>
      </div>
    </div>
  )
}
