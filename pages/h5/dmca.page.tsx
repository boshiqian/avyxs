import { ASideWrapper } from './ASide'
export { getStaticProps } from './ASide'
import { footerRightLinks } from '~/components/Site/Footer'

export default function DMCAPage() {
  return (
    <ASideWrapper links={footerRightLinks}>
      <div className="content-header pt-0">
        <div className="container">
          <div className="title-with-avatar center">
            <div className="title-box">
              <h6 className="sub-title mb-1">政策</h6>
              <h2 className="h3-md mb-1">使用條款</h2>
              <span className="inactive-color fs-2 mb-0">
                最後修訂: 2019-03-28
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <section className="pb-3 pb-e-lg-40">
          <p>
            In accordance with the Digital Millennium Copyright Act of 1998 (the
            text of which may be found on the U.S. Copyright Office website at
            http://lcweb.loc.gov/copyright/), avyxs.com will respond
            expeditiously to claims of copyright infringement that are reported
            to avyxs.com designated copyright agent identified below. Please also
            note that under Section 512(f) any person who knowingly materially
            misrepresents that material or activity is infringing may be subject
            to liability. avyxs.com reserves the right at its sole and entire
            discretion, to remove content and terminate the accounts of avyxs.com
            users who infringe, or appear to infringe, the intellectual property
            or other rights of third parties.
          </p>
          <p>
            If you believe that your copywriten work has been copied in a way
            that constitutes copyright infringement, please provide avyxs.com
            copyright agent the following information:
          </p>
          <ol>
            <li>
              A physical or electronic signature of a person authorized to act
              on behalf of the owner of an exclusive right that is allegedly
              infringed; Identification of the copyright work claimed to have
              been infringed, or, if multiple copyrighted works at a single
              online site are covered by a single notification, a representative
              list of such works at the Website;
            </li>
            <li>
              Identification of the material that is claimed to be infringing or
              to be the subject of infringing activity and that is to be removed
              or access to which is to be disabled, and information reasonably
              sufficient to permit avyxs.com to locate the material;
            </li>
            <li>
              Information reasonably sufficient to permit avyxs.com to contact
              the complaining party, including a name, address, telephone number
              and, if available, an email address at which the complaining party
              may be contacted;
            </li>
            <li>
              A statement that the complaining party has a good-faith belief
              that use of the material in the manner complained of is not
              authorized by the copyright owner, its agent or the law; and
            </li>
            <li>
              A statement that the information in the notification is accurate
              and, under penalty of perjury, that the complaining party is
              authorized to act on behalf of the owner of an exclusive right
              that is allegedly infringed.
            </li>
          </ol>
          <p>
            All claims of copyright infringement on or regarding this Website
            should be delivered to avyxs.com designated copyright agent at the
            following email address:
          </p>
          {/* <p className="active-color">abuse[at]avyxs.com</p> */}
          <p>
            We apologize for any kind of misuse of our service and promise to do
            our best to find and terminate abusive files.
          </p>
        </section>
      </div>
    </ASideWrapper>
  )
}
