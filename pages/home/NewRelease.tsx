import IconRightarrow from '~/components/iconfont/IconRightarrow'
import Link from 'next/link'
import { useEffect, useRef } from 'react'
import styles1 from './TopSlide.module.css'
export const NewRelease = () => {
  const ref = useRef<HTMLDivElement>()
  useEffect(() => {
    if (!ref.current) {
      return
    }
    // @ts-ignore
    jQuery(ref.current).owlCarousel({
      autoWidth: false,
      dots: true,
      responsive: {
        0: { items: 3 },
        576: { items: 4 },
        992: { items: 6 },
      },
    })
    // @ts-ignore
    jQuery(ref.current).find('.lazyload').lazyload()
  }, [ref])
  return (
    <section className="pb-3 pb-e-lg-40">
      <div className="title-with-more">
        <div className="title-box">
          <h6 className="sub-title inactive-color">出爐</h6>
          <h2 className="h3-md">全新上市</h2>
        </div>
        <div className="more">
          <Link href="/tag?id=1">
            <a>
              更多
              <IconRightarrow size={1.3} color="currentColor" />
            </a>
          </Link>
        </div>
      </div>
      <div className="jable-carousel">
        <div className="gutter-20">
          <div className="owl-carousel owl-theme-jable" ref={ref}>
            {fakeData.map((item) => {
              return (
                <div className="item" key={item.id}>
                  <div className={'video-img-box'}>
                    <div className="img-box cover-half">
                      <Link
                        href={{ pathname: '/video', query: { id: item.id } }}
                        passHref
                      >
                        <a
                          style={{ backgroundImage: `url(${item.cover})` }}
                        ></a>
                      </Link>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </section>
  )
}

const fakeData = [
  {
    id: '1',
    cover: '/video/14677/m-preview.jpg',
  },
  {
    id: '2',
    cover: '/video/14677/m-preview.jpg',
  },
  {
    id: '3',
    cover: '/video/14677/m-preview.jpg',
  },
  {
    id: '4',
    cover: '/video/14677/m-preview.jpg',
  },
  {
    id: '5',
    cover: '/video/14677/m-preview.jpg',
  },
  {
    id: '6',
    cover: '/video/14677/m-preview.jpg',
  },
]
