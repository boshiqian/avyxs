import { useRef, useEffect, Fragment } from 'react'
import '~/mod/owl.carousel'
import chunk from 'lodash/chunk'
import IconRightarrow from '~/components/iconfont/IconRightarrow'
import { VideoCard } from './VideoCard'

export const RecentlyUpdated = () => {
  const ref = useRef<HTMLDivElement>()
  useEffect(() => {
    if (!ref.current) {
      return
    }
    let o = jQuery(ref.current)
    // @ts-ignore
    o.owlCarousel({
      autoWidth: false,
      dots: true,
      responsive: {
        0: { items: 2 },
        576: { items: 3 },
        992: { items: 4 },
      },
    })
    return () => {
      o.trigger('destroy.owl.carousel')
      o = null
    }
  }, [ref])
  return (
    <section className="py-3 pb-e-lg-40">
      <div className="title-with-more">
        <div className="title-box">
          <h6 className="sub-title inactive-color">新鮮</h6>
          <h2 className="h3-md">最近更新</h2>
        </div>
        <div className="more">
          <a href="">
            更多
            <IconRightarrow size={1.3} color="currentColor" />
          </a>
        </div>
      </div>
      <div className="jable-carousel">
        <div className="gutter-20">
          <div className="owl-carousel owl-theme-jable" ref={ref}>
            {chunk(fakeData, 2).map((items, i) => {
              return (
                <div className="item" key={'x' + i}>
                  {items.map((item) => {
                    return <VideoCard item={item as any} key={item.id} />
                  })}
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </section>
  )
}

const fakeData = [
  {
    id: '1',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '2',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '3',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '4',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '5',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '6',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '7',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '8',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '9',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '10',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '11',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
  {
    id: '12',
    view: 877,
    like: 52,
    duration: '2:02:03',
    name: `FSDSS-273
                              エッチした過ぎて即ちんぺろフェラチオしてくる絶倫カノジョと同棲性活
                              夏木鈴`,
    link: '/video',
    img: 'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/320x180/1.jpg',
    preview:
      'https://assetscdn2.jable.tv/contents/videos_screenshots/18000/18240/18240_preview.mp4',
  },
]
