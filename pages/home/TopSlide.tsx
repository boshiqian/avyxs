import Link from 'next/link'
import React, { useEffect, useRef } from 'react'
import '~/mod/owl.carousel'
import styles from './TopSlide.module.css'
import '~/mod/jquery'
import '~/mod/layzload'
import { BannerItem,getImg } from '~/mod/api/home'
import { Config } from '~/mod/api/types'

export const TopSlide: React.FC<{ items: BannerItem[]; config: Config }> = ({
  items,
  config,
}) => {
  const ref = useRef<HTMLDivElement>()
  useEffect(() => {
    if (!ref.current) {
      return
    }
    const lazyload = () => {
      // @ts-ignore
      jQuery('#home-owl .lazyload').lazyload()
    }
    let o = jQuery('#home-owl')
    o.one('initialized.owl.carousel', lazyload)
    o.on('resized.owl.carousel', lazyload)
    // @ts-ignore
    o.owlCarousel({
      autoplay: true,
      autoWidth: false,
      dots: false,
      loop: true,
      center: true,
      responsive: {
        0: { items: 2 },
        992: { items: 4 },
      },
    })
    return () => {
      o.trigger('destroy.owl.carousel')
      o = null
    }
  }, [ref])
 function getLoad(item,index){
    getImg({data:config.resUrl + item.imgUrl}).then((res)=>{
       // @ts-ignore
      $('.lazybanner'+index).attr('src',res)
    })
  }
  return (
    <div
      className="jable-carousel jable-animate overflow-h"
      data-animation="slideRight"
      data-animation-item=".item"
    >
      <div className="gutter-20 gutter-xl-30 pb-3">
        <div id="home-owl" className="owl-carousel" ref={ref}>
          {items.map((item, index) => {
            return (
              <div className="item" key={item.id}>
                <div className="video-img-box">
                  <div className="img-box">
                    <a href={item.hrefUrl} target="_blank">
                      <img
                        className={'lazybanner'+index}
                        width="690"
                        height="300"
                        src="/img/placeholder-lg.jpg"
                        data-src={getLoad(item,index)}
                      />
                      {(item as any)?.re && (
                        <div className="ribbon-top-left">精選</div>
                      )}
                    </a>
                  </div>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}
