import Link from 'next/link'
import IconGoodFill from '~/components/iconfont/IconGoodFill'
import IconTopraningFill from '~/components/iconfont/IconTopraningFill'
import IconRightarrow from '~/components/iconfont/IconRightarrow'
import {BannerItem, getTopicList} from '~/mod/api/home'
import React, {useEffect, useRef, useState} from 'react'
import {Config} from "~/mod/api/types";
import LazyImg from "~/components/Site/lazyImg/lazyImg";
export const Topics:React.FC<{config: Config }> = ({config,}) => {
  const ref = useRef<HTMLDivElement>()
  const [topicList, setTopic] = useState([]);
  useEffect(() => {
     getTopicList().then((res)=>{
       setTopic(res.data.result)
     })
  }, [ref])
  return (
    <section className="pb-3 pb-e-lg-40">
      <div className="row">
        <div className="col-lg-7 pb-3 pb-md-0">
          <div className="title-with-more">
            <div className="title-box">
              <h6 className="sub-title inactive-color">選台</h6>
              <h2 className="h3-md">影片主題</h2>
            </div>
            <div className="more">
              <Link
                  href={{ pathname: '/topic', query: { id: 'more'} }}
                  passHref
              >
                <a>
                  更多
                  <IconRightarrow size={1.3} color="currentColor" />
                </a>
              </Link>
            </div>
          </div>
          <div className="row gutter-20">
            {topicList.map((item) => {
              return (
                <div className="col-6" key={item.id}>
                  <div className="horizontal-img-box mb-3">
                    <Link href={{ pathname: '/topic', query: { id: item.id } }} passHref>
                      <a>
                        <div className="media">
                          <LazyImg src={''} className={'rounded'} height={''} width={'50'} lazysrc={config.resUrl + item.imgUrl}/>
                          <div className="detail">
                            <h6 className="title">{item.title}</h6>
                            <span>{item.ext} 部影片</span>
                          </div>
                        </div>
                      </a>
                    </Link>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
        <div className="col-lg-5 d-none d-lg-block">
          <div className="title-with-more">
            <div className="title-box">
              <h6 className="sub-title inactive-color">焦點</h6>
              <h2 className="h3-md">專題合集</h2>
            </div>
          </div>
          <div className="row gutter-20">
            {fakeData2.map((item, i) => {
              let bgColor = i === 0 ? 'bg-pink' : 'bg-gray'
              let Icon = i === 0 ? IconTopraningFill : IconGoodFill
              return (
                <div className="col-6" key={item.id}>
                  <Link href={item.link} passHref>
                    <a className={`card ${bgColor} text-light`}>
                      <img
                        className="overlay-image"
                        src="https://assetscdn.jable.tv/assets/images/card-overlay.png"
                      />
                      <div className="card-body with-icon-title">
                        <div className="icon-title">
                          <Icon size={1.5} color="currentColor" />
                        </div>
                        <div>
                          <h4 className="mb-3"># {item.name}</h4>
                          <span className="text-white">
                            現實中絕對不能做的事情，滿足您的佔有慾望。
                          </span>
                        </div>
                      </div>
                    </a>
                  </Link>
                </div>
              )
            })}
          </div>
        </div>
      </div>
    </section>
  )
}

const fakeData1 = 'x'
  .repeat(12)
  .split('')
  .map((_x, i) => {
    return {
      id: '' + i,
      link: '/tag',
      name: '無碼解放',
      count: '369',
      img: '/video/14677/s1_uncensored.jpg',
    }
  })

const fakeData2 = [
  {
    id: '1',
    name: '強姦',
    link: '/search?q=強姦',
    desc: '現實中絕對不能做的事情，滿足您的佔有慾望。',
  },
  {
    id: '2',
    name: '不倫',
    link: '/search?q=不倫',
    desc: '禁忌的關係，從中體驗偷歡的快感。',
  },
]
