import React, { useEffect, useRef, useState } from 'react'
import Link from 'next/link'
import IconBrowse from '~/components/iconfont/IconBrowse'
import IconFavorites from '~/components/iconfont/IconFavorites'
import IconFavoritesFill from '~/components/iconfont/IconFavoritesFill'
import { PayMode, PayMode2Str, VideoDesc } from '~/mod/api/types'
import { useLazyloadRef } from '~/mod/layzload'
import { adClick, ADResponse } from '~/mod/api/ad'
import { APIClient, ImageBase } from '../api'
import LazyImg from "~/components/Site/lazyImg/lazyImg";
import ReactGA from 'react-ga'

export type VideoInfo = {
  id: string
  view: number
  like: number
  duration: string
  name: string
  link: string
  img: string
  preview: string
}

const VideoCardWrapper: React.FC<{ item: VideoDesc }> = ({ item }) => {
  if (typeof item.advType !== 'undefined') {
    return <VideoCardAD item={item as any} />
  }
  return <VideoCard item={item} />
}
export { VideoCardWrapper as VideoCard }
const VideoCard: React.FC<{ item: VideoDesc }> = ({ item }) => {
  const [show, setShow] = useState(false)
  let top: string = PayMode2Str(item.payMode)
  const ref = useLazyloadRef()
  return (
    <div
      ref={ref}
      className="video-img-box mb-e-20"
      key={item.id}
      title={item.title}
    >
      <div className="img-box">
        <Link href={{ pathname: '/video', query: { id: item.id } }} passHref>
          <a
            onMouseEnter={() => setShow(true)}
            onTouchStart={() => setShow(true)}
            onMouseLeave={() => setShow(false)}
            onClick={() => {
              ReactGA.event({
                category: '视频播放',
                action: '点击',
                label: item.title,
              })
            }}
          >
            <div className="ximg">
              <img src="/img/placeholder-md.jpg" />
              <div className="p">
                <LazyImg src={''} className={''} height={''} width={''} lazysrc={item.imgUrl}/>
              </div>
            </div>
            <div className="absolute-bottom-left">
              <span
                className="action hover-state d-none d-sm-flex"
                data-fav-video-id="18241"
                data-fav-type="0"
              >
                <IconFavoritesFill color="currentColor" />
              </span>
            </div>
            <div className="absolute-bottom-right">
              <span className="label">{item.playTime}</span>
            </div>
            {/* {item?.preview && show && (
              <video
                autoPlay
                loop
                src={item?.preview}
                style={{
                  position: 'absolute',
                  left: '0px',
                  top: '0px',
                  visibility: 'visible',
                }}
              />
            )} */}
          </a>
        </Link>
      </div>
      <div className="detail">
        <Link href={{ pathname: '/video', query: { id: item.id } }} passHref>
          <a>
            <h6 className="title">{item.title}</h6>
          </a>
        </Link>
        <p className="sub-title">
          <IconBrowse className="mr-1" />
          {item.playCount}
          <IconFavorites className="ml-3 mr-1" />
          {item.payCoin}
        </p>
      </div>
    </div>
  )
}

const VideoCardAD: React.FC<{ item: ADResponse }> = ({ item }) => {
  let top: string = PayMode2Str(PayMode.Free)
  const api = APIClient.useContainer()
  const click = () => {
    adClick({ id: item.id })
  }
  const ref = useLazyloadRef()
  return (
    <div
      ref={ref}
      className="video-img-box mb-e-20"
      key={item.id}
      title={item.title}
    >
      <div className="img-box">
        <a href={item.hrefUrl} target="_blank" onClick={click}>
          <div className="ximg">
            <img src="/img/placeholder-md.jpg" />
            <div className="p">
              <img
                className="lazyload"
                src="/img/placeholder-md.jpg"
                data-src={ImageBase(item.imgUrl)}
              />
            </div>
          </div>
          <div className="ribbon-top-left">{top}</div>
        </a>
      </div>
      <div className="detail">
        <a href={item.hrefUrl} target="_blank" onClick={click}>
          <h6 className="title">{item.title}</h6>
        </a>
        <p className="sub-title">
          <IconBrowse className="mr-1" />
          {item.clickCount}
          <IconFavorites className="ml-3 mr-1" />
          {38}
        </p>
      </div>
    </div>
  )
}
