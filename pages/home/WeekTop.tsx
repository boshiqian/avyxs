import React, {useEffect, useState} from 'react'
import { VideoCard } from './VideoCard'
import IconRightarrow from '~/components/iconfont/IconRightarrow'
import {listByIndexCol,HomeCol} from '~/mod/api/home'
import Link from 'next/link'

export const WeekTop: React.FC<{ col: HomeCol,index:number }> = ({ col: props,index }) => {
    const [list, setList] = useState([]);
    useEffect(() => {
        listByIndexCol({"id":props.id,"page":"1","limit":index?"8":"4"})
            .then((res) => {
                if (res?.data?.result)
                {
                    return setList(res.data.result.records||[])
                }

            })
    }, [])
  return (
    <section className="pb-3 pb-e-lg-40">
      <div className="title-with-more">
        <div className="title-box">
          <h6 className="sub-title inactive-color">
            {props?.subtitle ?? props.title}
          </h6>
          <h2 className="h3-md">{props.title}</h2>
        </div>
        {props.id && (
          <div className="more">
            <Link
              href={{ pathname: '/tag/indexcol', query: { id: props.id } }}
              passHref
            >
              <a>
                更多
                <IconRightarrow size={1.3} color="currentColor" />
              </a>
            </Link>
          </div>
        )}
      </div>
      <div className="row gutter-20">
        {list.map((item) => {
          return (
            <div className="col-6 col-sm-4 col-lg-3" key={item.id}>
              <VideoCard item={item} />
            </div>
          )
        })}
      </div>
    </section>
  )
}
