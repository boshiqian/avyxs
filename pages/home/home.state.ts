import { APIClient, setImageBase, setAppConfig } from '~/pages/api'
import * as home from '~/mod/api/home'
import { useQuery } from 'react-query'
import { createContainer } from 'unstated-next'

const useHomeState = () => {
  const api = APIClient.useContainer()
  return useQuery('home', () => {
    return home
      .info()
      .then((res) => {
          return res.data.result
      })
      .then((d) => {
        setAppConfig(d)
        setImageBase(d.resUrl)
        return d
      })
  })
}

export const HomeState = createContainer(useHomeState)
