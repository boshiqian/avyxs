import { TopSlide } from './TopSlide'
import { RecentlyUpdated } from './RecentlyUpdated'
import { NewRelease } from './NewRelease'
import { Topics } from './Topics'
import { WeekTop } from './WeekTop'
import { APIClient } from '~/pages/api'
import { useQuery } from 'react-query'
import * as home from '~/mod/api/home'
import { usePageLoading } from '~/components/Site/Modal'
import {Fragment, useEffect, useState} from 'react'
import { SiteColAD } from '~/components/Site/AD'
import { HomeState } from './home.state'
import {getBnnerList,getIndexCol} from "~/mod/api/home";
import {ec} from "~/mod/ec";

export default function HomePage() {
  const api = APIClient.useContainer()
    const [bannerItems, setBanner] = useState([]);
    const [colList, setColList] = useState([]);
    const [colListTop, setColListTop] = useState([]);
    const [colListFoot, setColListFoot] = useState([]);
  const { isLoading, isError, data } = HomeState.useContainer()
    useEffect(() => {
        getBnnerList()
            .then((res) => {
                return setBanner(res.data.result)
            })
        getIndexCol()
            .then((res) => {
                setColListTop(res.data.result.splice(0,2))
                setColListFoot(res.data.result)
                return setColList(res.data.result)
            })
    }, [])
  if (isLoading) {
    return null
  }
  return (
    <>
        {bannerItems.length&&(<TopSlide items={bannerItems} config={data} />)}
      <div className="container">
          {colListTop.map((col, i) => {
              return (
                  <Fragment key={col.id}>
                      <WeekTop index={i} col={col} />
                  </Fragment>
              )
          })}
      </div>
        <div className="container">
            <Topics config={data}/>
        </div>
        <div className="container">
            {colListFoot.map((col, i) => {
                return (
                    <Fragment key={col.id}>
                        <WeekTop index={i+2} col={col} />
                    </Fragment>
                )
            })}
        </div>
    </>
  )
}
