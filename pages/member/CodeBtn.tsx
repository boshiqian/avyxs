import { Component } from 'react'
import { isRespError } from '~/mod/utils'
import { toast } from 'react-toastify'

type Props = {
  sendCode: () => Promise<any>
}

type State = {
  wait: number
  sending: boolean
}

export class CodeBtn extends Component<Props, State> {
  state: State = {
    wait: 0,
    sending: false,
  }
  timer = null
  sendCode = () => {
    if (this.state.sending) {
      return
    }
    this.setState((s) => ({ ...s, sending: true }))
    Promise.resolve(1)
      .then(() => this.props.sendCode())
      .then(() => {
        this.setState((s) => ({ ...s, wait: 60 }))
        this.timer = setInterval(() => {
          this.setState((s) => {
            let nwait = s.wait - 1
            if (nwait <= 0) {
              clearInterval(this.timer)
            }
            return { ...s, wait: nwait }
          })
        }, 1e3)
      })
      .then(() => {
        toast.success('验证码发送成功, 请查收')
      })
      .catch((err) => {
        toast.error(isRespError(err) ? err.message : '网络波动请重试')
      })
      .finally(() => {
        this.setState((s) => ({ ...s, sending: false }))
      })
  }
  componentWillUnmount() {
    clearInterval(this.timer)
  }
  render() {
    return (
      <div>
        <button
          type="button"
          className="btn p-2"
          disabled={this.state.sending || this.state.wait > 0}
          onClick={this.sendCode}
        >
          {this.timer !== null ? '重新发送' : '发送验证码'}
          {this.state.wait > 0 && `${Math.max(this.state.wait, 0)}s`}
        </button>
      </div>
    )
  }
}
