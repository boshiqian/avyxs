import IconPlay from '~/components/iconfont/IconPlay'
import IconFavorites from '~/components/iconfont/IconFavorites'
import IconJiaobiao from '~/components/iconfont/IconJiaobiao'
import IconSet from '~/components/iconfont/IconSet'
import IconConsumption from '~/components/iconfont/IconConsumption'
import IconVip from '~/components/iconfont/IconVip'
import Link from 'next/link'
import { NextRouter, useRouter } from 'next/router'
import { UrlObject } from 'url'
import { useQuery } from 'react-query'
import { APIClient, ImageBase } from '../api'
import * as user from '~/mod/api/withGa/user'
import { UserInfo } from '~/mod/api/types'
import { beforeNow } from '~/mod/utils'
import moment from 'moment'
import { Modal, ModalState, usePageLoading } from '~/components/Site/Modal'
import { Phone, Profile } from './Settings'
import { UserQuery } from '../user.state'
import ReactGA from 'react-ga'

const placeholderUser = {
  account: 'guest',
  balance: 0,
  devCode: '',
  devType: 3,
  headUrl: 'user/def_head.png',
  id: '',
  isVip: false,
  nickName: '游客',
  sex: 0,
} as UserInfo

export const Header = () => {
  const router = useRouter()
  const nowTab = router.pathname
  const isActive = (tab: string) => {
    if (nowTab === tab) {
      return 'active'
    }
    return ''
  }
  const [_x1, dispatch] = ModalState.useContainer()
  const { isLoading, data } = UserQuery.useContainer()
  const isSelf = true
  const joinBefore = beforeNow(moment(data.createTime))
  return (
    <section
      className="content-header pt-0"
      style={{
        background: '#10121d',
        backgroundImage: 'linear-gradient(to bottom, #090812, #111520)',
      }}
    >
      <div className="container">
        <div className="profile-info title-with-avatar">
          <img className="avatar" src={ImageBase(data.headUrl)} width="100" />
          <div className="title-box">
            <h3>{data.nickName}</h3>
            <div className="inactive-color">
              <span>加入於 {joinBefore}</span>
              {data.isVip && (
                <span className="ml-2">
                  VIP过期时间:
                  {moment(data.vipTimp).format('YYYY-MM-DD HH:MM')}
                </span>
              )}
            </div>
          </div>
        </div>
        <nav className="profile-nav">
          <ul>
            <li className={isActive('/member')}>
              <Link href="/member" passHref>
                <a>
                  <IconPlay
                    className="ml-2 ml-sm-0 mr-2"
                    color="currentColor"
                  />
                  <span>上傳作品</span>
                  <span className="count">0</span>
                </a>
              </Link>
            </li>
            <li className={isActive('/member/store/coin')}>
              <Link href="/member/store/coin" passHref>
                <a>
                  <IconConsumption
                    className="ml-2 ml-sm-0 mr-2"
                    color="currentColor"
                  />
                  <span>金币购买</span>
                  <span className="count"></span>
                </a>
              </Link>
            </li>
            <li className={isActive('/member/store/vip2')}>
              <Link href="/member/store/vip2" passHref>
                <a>
                  <IconVip className="ml-2 ml-sm-0 mr-2" color="currentColor" />
                  <span>会员购买</span>
                  <span className="count"></span>
                </a>
              </Link>
            </li>
          </ul>
          {isSelf && (
            <>
              <a className="right text-nowrap" href="#" data-toggle="dropdown">
                <IconSet className="mr-2" color="currentColor" />
                <span>設定</span>
              </a>
              <div className="dropdown-menu dropdown-menu-right">
                <a
                  className="dropdown-item"
                  href="#"
                  onClick={(e) => {
                    e.preventDefault()
                    ReactGA.event({
                      category: '用户资料',
                      action: '修改资料',
                      label: '用户中心',
                    })
                    dispatch({ type: 'show', content: <Profile info={data} /> })
                  }}
                >
                  個人資料
                </a>
                <a
                  className="dropdown-item"
                  href="#"
                  onClick={(e) => {
                    e.preventDefault()
                    ReactGA.event({
                      category: '用户资料',
                      action: '绑定手机',
                      label: '用户中心',
                    })
                    dispatch({
                      type: 'show',
                      content: <Phone oldPhone={data.phone} />,
                    })
                  }}
                >
                  绑定手机
                </a>
              </div>
            </>
          )}
        </nav>
      </div>
    </section>
  )
}
