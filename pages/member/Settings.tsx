import React, { useEffect, useState } from 'react'
import { useMutation, useQuery } from 'react-query'
import { Form, ModalState, usePageLoading } from '~/components/Site/Modal'
import * as user from '~/mod/api/withGa/user'
import { UserInfo } from '~/mod/api/types'
import { getUid, setUid, UserQuery } from '../user.state'
import { isRespError, tapError, withGAEvent } from '~/mod/utils'
import { toast } from 'react-toastify'
import { CodeBtn } from './CodeBtn'
import ReactGA from 'react-ga'

import { registerUser as _registerUser } from '~/pages/user.state'
const registerUser = withGAEvent(_registerUser)({
  category: '用户注册',
  action: '自动注册',
  label: '登录出错',
})

export const Profile: React.FC<{ info: UserInfo }> = ({ info }) => {
  const [_x, dispatch] = ModalState.useContainer()
  const { refetch } = UserQuery.useContainer()
  const [state, setState] = useState({
    nickName: info.nickName ?? '',
    introduction: info.introduction ?? '',
  })
  useEffect(() => {
    setState((s) => ({
      ...s,
      nickName: info.nickName ?? '',
      introduction: info.introduction ?? '',
    }))
  }, [info.nickName, info.introduction])
  const updateProfile = useMutation((params: user.UpdateParams) => {
    return user.updateInfo(params)
  })
  const updateProfileFn = () => {
    dispatch({ type: 'loading' })
    updateProfile
      .mutateAsync({
        id: getUid(),
        nickName: state.nickName,
        introduction: state.introduction,
      })
      .then(
        async (r) => {
          await refetch()
          toast.success('用户资料更新成功')
          dispatch({ type: 'close' })
        },
        (err) => {
          toast.error(isRespError(err) ? err.message : '网络波动请重试')
          dispatch({ type: 'loaded' })
        },
      )
  }
  return (
    <Form
      title="個人資料"
      subtitle="您的名稱及頭像將在留言時展示，自我介紹則可在個人主頁裡看到。"
      close={() => dispatch({ type: 'close' })}
    >
      <form
        onSubmit={(e) => {
          e.preventDefault()
          updateProfileFn()
        }}
      >
        <div
          className="alert alert-danger w-100"
          style={{ display: 'none' }}
          role="alert"
        ></div>
        <div className="form-group-attached">
          <div className="form-group required">
            <label htmlFor="display_name">昵称</label>
            <input
              id="display_name"
              name="display_name"
              className="form-control"
              type="text"
              maxLength={20}
              value={state.nickName}
              onChange={(e) => {
                setState((s) => ({ ...s, nickName: e.target.value }))
              }}
            />
          </div>
          <div className="form-group">
            <label htmlFor="about_me">自我介紹</label>
            <input
              id="about_me"
              name="about_me"
              className="form-control"
              type="text"
              value={state.introduction}
              onChange={(e) => {
                setState((s) => ({ ...s, introduction: e.target.value }))
              }}
            />
          </div>
        </div>
        <button className="btn btn-submit btn-block" type="submit">
          更新资料
        </button>
      </form>
    </Form>
  )
}

/**绑定手机 */
export const Phone: React.FC<{ oldPhone?: string }> = (props) => {
  const [_x, dispatch] = ModalState.useContainer()
  const [state, setState] = useState({
    phone: props?.oldPhone ?? '',
    code: '',
  })
  useEffect(() => {
    setState((s) => ({ ...s, phone: props?.oldPhone ?? '' }))
  }, [props.oldPhone])
  const sendCode = (params: { phone: string }) => {
    return Promise.resolve(1)
      .then(() => {
        return user.sendCode({
          type: user.SendCodeType.bindPhone,
          mobile: params.phone,
        })
      })
      .then((r) => r.data.result)
  }
  const bindPhone = (params: { phone: string; code: string }) => {
    return user
      .bindPhone({
        userId: getUid(),
        mobile: params.phone,
        code: params.code,
      })
      .then((r) => r.data.result)
  }

  const { refetch: refetchUser } = UserQuery.useContainer()
  const sendCodeFn = () => {
    return sendCode({ phone: state.phone })
  }
  const bindPhoneFn = () => {
    dispatch({ type: 'loading' })
    Promise.resolve(1)
      .then(() => bindPhone({ phone: state.phone, code: state.code }))
      .then(
        async (r) => {
          await refetchUser()
          toast.success('手机号绑定成功')
          dispatch({ type: 'close' })
        },
        (err) => {
          toast.error(isRespError(err) ? err.message : '网络波动请重试')
          dispatch({ type: 'loaded' })
        },
      )
  }
  return (
    <Form
      title="绑定手机"
      subtitle="绑定手机后你将可以在不同设备中同步用户"
      close={() => dispatch({ type: 'close' })}
    >
      <form
        onSubmit={(e) => {
          e.preventDefault()
          bindPhoneFn()
        }}
      >
        <div
          className="alert alert-danger w-100"
          style={{ display: 'none' }}
          role="alert"
        ></div>
        <div className="form-group-attached">
          <div className="form-group required">
            <label htmlFor="bindphone">手机号</label>
            <div className="d-flex">
              <input
                id="bindphone"
                className="form-control"
                type="tel"
                maxLength={20}
                value={state.phone}
                required
                onChange={(e) =>
                  setState((s) => ({ ...s, phone: e.target.value }))
                }
              />
              <div>
                <CodeBtn sendCode={sendCodeFn} />
              </div>
            </div>
          </div>
          <div className="form-group required">
            <label htmlFor="vierfy_code">验证码</label>
            <input
              id="vierfy_code"
              className="form-control"
              type="text"
              value={state.code}
              required
              onChange={(e) =>
                setState((s) => ({ ...s, code: e.target.value }))
              }
            />
          </div>
        </div>
        <div>
          <button className="btn btn-submit btn-block" type="submit">
            绑定
          </button>
        </div>
      </form>
    </Form>
  )
}

/**切换帐号 */
export const SwitchAccount: React.FC<{ login?: string }> = (props) => {
  const [_x, dispatch] = ModalState.useContainer()
  const [state, setState] = useState({
    phone: '',
    code: '',
  })

  const sendCode = (params: { phone: string }) => {
    return user
      .sendCode({
        type: user.SendCodeType.findAccount,
        mobile: params.phone,
      })
      .then((r) => r.data.result)
  }
  const switchAccount = useMutation((params: user.SwitchAccountParams) => {
    return user.switchAccount(params).then((r) => r.data.result)
  })
  const userQ = UserQuery.useContainer()
  const sendCodeFn = () => {
    return sendCode({ phone: state.phone })
  }

  const isLoading = switchAccount.isLoading || userQ.isLoading
  const switchAccountFn = () => {
    dispatch({ type: 'loading' })
    Promise.resolve(1)
      .then(() =>
        switchAccount.mutateAsync({ mobile: state.phone, code: state.code }),
      )
      .then((r) => {
        setUid(r.id)
        return userQ.refetch()
      })
      .then(
        (r) => {
          toast.success('切换帐号成功')
          dispatch({ type: 'close' })
        },
        (err) => {
          toast.error(isRespError(err) ? err.message : '网络波动请重试')
          dispatch({ type: 'loaded' })
        },
      )
  }
  return (
    <Form
      title={props.login ? '登陆帐号' : '切换帐号'}
      subtitle={props.login ?? '通过已绑定的手机号登陆帐号'}
      close={() => {
        if (!props.login) {
          dispatch({ type: 'close' })
          // ga
          ReactGA.event({
            category: '切换账号',
            action: '取消切换',
          })
          return
        }
        setUid('')
        dispatch({ type: 'loading' })
        registerUser()
          .then(() => {
            return userQ.refetch()
          })
          .then(() => {
            dispatch({ type: 'close' })
          })
          .catch(
            tapError((err) => {
              toast.error(isRespError(err) ? err.message : '网络波动请重试')
              dispatch({ type: 'loaded' })
            }),
          )
        return
      }}
    >
      <form
        onSubmit={(e) => {
          e.preventDefault()
          switchAccountFn()
        }}
      >
        <div
          className="alert alert-danger w-100"
          style={{ display: 'none' }}
          role="alert"
        ></div>
        <div className="form-group-attached">
          <div className="form-group required">
            <label htmlFor="bindphone">手机号</label>
            <div className="d-flex">
              <input
                id="bindphone"
                className="form-control"
                required
                type="tel"
                maxLength={20}
                value={state.phone}
                disabled={isLoading}
                onChange={(e) =>
                  setState((s) => ({ ...s, phone: e.target.value }))
                }
              />
              <div>
                <CodeBtn sendCode={sendCodeFn} />
              </div>
            </div>
          </div>
          <div className="form-group required">
            <label htmlFor="vierfy_code">验证码</label>
            <input
              id="vierfy_code"
              className="form-control"
              type="text"
              required
              value={state.code}
              disabled={isLoading}
              onChange={(e) =>
                setState((s) => ({ ...s, code: e.target.value }))
              }
            />
          </div>
        </div>
        <div>
          <button
            className="btn btn-submit btn-block"
            type="submit"
            disabled={isLoading}
          >
            切换帐号
          </button>
        </div>
      </form>
    </Form>
  )
}
