import { useRouter } from 'next/router'
import { useMemo } from 'react'
import { Header as MemberHeader } from './Header'
import { VideoCard } from '~/pages/home/VideoCard'
import { Pagination } from '~/pages/tag/Pagination'

export default function MemberPage() {
  const router = useRouter()
  const data = []
  return (
    <>
      <MemberHeader />
      <div className="container">
        <section className="pb-3 pb-e-lg-40">
          {!data.length ? (
            <section className="pb-3 pb-e-lg-40">
              <div className="text-center">
                <img
                  className="mt-5 mb-3 opacity-8"
                  src="/img/doge.png"
                  height="110"
                />
                <h5 className="inactive-color">暫無相關內容</h5>
              </div>
            </section>
          ) : (
            <>
              <div id="list_videos_my_favourite_videos">
                <section className="pb-3 pb-e-lg-40">
                  <div className="row gutter-20">
                    {data.map((item) => {
                      return (
                        <div key={item.id} className="col-6 col-sm-4 col-lg-3">
                          {/* <VideoCard item={item} /> */}
                        </div>
                      )
                    })}
                  </div>
                </section>
              </div>
              {/* <Pagination /> */}
            </>
          )}
        </section>
      </div>
    </>
  )
}
