import { useEffect } from 'react'
import { ModalState } from '~/components/Site/Modal'
import { SwitchAccount } from './Settings'

export default function LoginPage() {
  const [_x, dispatch] = ModalState.useContainer()

  useEffect(() => {
    dispatch({ type: 'show', content: <SwitchAccount /> })
  }, [])
  return (
    <>
      <div className="container text-center">
        <div className="row justify-content-center">
          <div className="col-12 col-md-8 col-lg-6 col-xl-6">
            <h5 className="mb-4">当前IP达到注册上限, 请使用找回帐号登陆</h5>
            <div>
              <button
                className="btn btn-submit w-75"
                onClick={() => {
                  dispatch({ type: 'show', content: <SwitchAccount /> })
                }}
              >
                找回帐号
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
