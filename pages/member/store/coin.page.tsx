import { Header } from '../Header'
import * as store from '~/mod/api/store'
import { useQuery } from 'react-query'
import { APIClient, ImageBase } from '~/pages/api'
import React, { Dispatch, useEffect } from 'react'
import { PayState } from '~/components/Site/Store'
import styles from './coin.module.css'
import { ec } from '~/mod/ec'
import { useRouter } from 'next/router'

export default function StoreCoinPage() {
  return (
    <div>
      <Header />
      <div className="container">
        <div id="list_videos_my_favourite_videos">
          <section className="pb-3 pb-e-lg-40">
            <div className="row gutter-20">
              <CoinGoods />
            </div>
          </section>
        </div>
      </div>
    </div>
  )
}

export type SAction =
  | { type: 'loading' }
  | { type: 'loaded' }
  | { type: 'close' }

export const CoinGoods: React.FC<{
  className?: string
  dispatch?: Dispatch<SAction>
}> = (props) => {
  const { isLoading, data } = useQuery(['store-coin'], () => {
    return store.coinGoodList().then((r) => r.data.result)
  })
  useEffect(() => {
    if (props.dispatch) {
      props.dispatch({ type: isLoading ? 'loading' : 'loaded' })
    }
  }, [isLoading])
  const router = useRouter()
  useEffect(() => {
    if (!data) {
      return
    }
    let i = 0
    for (let t of data) {
      ec.addImpression({
        id: t.id,
        name: `${t.coins} 金币`,
        position: i++,
        list: router.pathname.startsWith('/video')
          ? '视频页弹窗购买列表'
          : '金币页购买列表',
      })
    }
  }, [isLoading])
  if (isLoading) {
    return null
  }
  return (
    <>
      {data.map((item, i) => {
        return (
          <div
            key={item.id}
            className={props?.className ?? 'col-6 col-sm-4 col-lg-2'}
          >
            <Item
              good={item}
              onBuy={() => {
                ec.addProduct({
                  id: item.id,
                  name: `${item.coins} 金币`,
                  position: i,
                })
                ec.click()
              }}
            />
          </div>
        )
      })}
    </>
  )
}

const Item: React.FC<{ good: store.CoinGood; onBuy?: () => any }> = ({
  good: item,
  onBuy,
}) => {
  const { dispatch } = PayState.useContainer()
  const buy = () => {
    if (onBuy) {
      onBuy()
    }
    dispatch({
      type: 'buy',
      params: {
        gid: item.id,
        gtype: store.GoodType.Coin,
        gtitle: `${item.coins} 金币`,
        gprice: item.price,
      },
    })
  }
  let bgColor = 50 + Math.ceil((Number(item.price) / 1000) * 280)
  bgColor = Math.ceil(bgColor)
  return (
    <div
      className={`rounded p-2 mb-3 ${styles.item}`}
      style={{ backgroundColor: `hsl(${bgColor}, 38%, 35%)` }}
    >
      <div className="p-2">
        <h4 className="mb-1">
          金币: <span className="float-right">{item.coins}</span>
        </h4>
        <h4 className="mb-1">
          价格: <span className="float-right">{item.price}</span>
        </h4>
      </div>
      <div>
        <button className="btn btn-block mb-0" onClick={buy}>
          购买
        </button>
      </div>
    </div>
  )
}
