import { Dispatch, useEffect } from 'react'
import { useQuery } from 'react-query'
import { APIClient, ImageBase } from '~/pages/api'
import { SAction } from './coin.page'
import * as store from '~/mod/api/store'
import { PayState } from '~/components/Site/Store'
import styles from './vip.good.module.css'
import { ec } from '~/mod/ec'
import { useRouter } from 'next/router'

export const VipGoods: React.FC<{
  className?: string
  dispatch?: Dispatch<SAction>
}> = (props) => {
  const router = useRouter()
  const { isLoading, data } = useQuery(['store-vip'], () => {
    return store.vipGoodList().then((r) => r.data.result)
  })
  useEffect(() => {
    if (props.dispatch) {
      props.dispatch({ type: isLoading ? 'loading' : 'loaded' })
    }
  }, [isLoading])
  useEffect(() => {
    if (!data) {
      return
    }
    let i = 0
    for (let t of data) {
      ec.addImpression({
        id: t.id,
        name: t.title,
        position: i++,
        list: router.pathname.startsWith('/video')
          ? '视频弹窗会员购买'
          : '弹窗会员购买',
      })
    }
  }, [!!data])
  if (isLoading) {
    return null
  }
  return (
    <>
      {data.map((item, i) => {
        return (
          <div
            key={item.id}
            className={props?.className ?? 'col-6 col-sm-4 col-lg-2'}
          >
            <Item
              good={item}
              onBuy={() => {
                ec.addProduct({
                  id: item.id,
                  name: item.title,
                  position: i,
                })
                ec.click()
              }}
            />
          </div>
        )
      })}
    </>
  )
}

const Item: React.FC<{ good: store.VIPGood; onBuy?: () => any }> = ({
  good: item,
  onBuy,
}) => {
  const nprice = item?.disCount ?? item.price
  const { dispatch } = PayState.useContainer()
  const buy = () => {
    if (onBuy) {
      onBuy()
    }
    dispatch({
      type: 'buy',
      params: {
        gid: item.id,
        gtype: store.GoodType.VIP,
        gtitle: `${item.title}`,
        gprice: nprice,
      },
    })
  }
  return (
    <div className={`bg-white rounded p-2 mb-3 ${styles.item}`}>
      <div className="p-2 text-center">
        <h4 className="mb-2">{item.title}</h4>
        <h6 className="mb-3">{item.subTitle}</h6>
        {item.disCount && (
          <del className={`${styles.scolor}`}>
            <h6 className={`mb-1 ${styles.scolor}`}>原价: {item.price}</h6>
          </del>
        )}
        <h5 className="mb-1">现价: {nprice}</h5>
      </div>
      <div>
        <button className="btn btn-block btn-submit mb-0 p-2" onClick={buy}>
          购买
        </button>
      </div>
    </div>
  )
}
