import { Header } from '../Header'
import * as store from '~/mod/api/store'
import { useQuery } from 'react-query'
import { APIClient, ImageBase } from '~/pages/api'
import React from 'react'
import styles from './vip.module.css'
import { StoreState } from '~/components/Site/Store/Store.state'

export default function XStoreVIPPage() {
  return <div>404</div>
}
function StoreVIPPage() {
  const api = APIClient.useContainer()
  const { isLoading, data } = useQuery(['store-vip'], () => {
    return store.vipGoodList().then((r) => r.data.result)
  })
  if (isLoading) {
    return null
  }
  return (
    <div>
      <Header />
      <div className="container">
        <section className="pb-3 px-2 px-sm-0 pb-e-lg-40">
          <div className="row gutter-20">
            <div id="list_videos_my_favourite_videos">
              <section className="pb-3 pb-e-lg-40">
                <div className="row gutter-20">
                  {data.map((item) => {
                    return (
                      <div
                        key={item.id}
                        className="col-6 col-sm-6 col-lg-3 mb-2"
                      >
                        <Item key={item.id} good={item} />
                      </div>
                    )
                  })}
                </div>
              </section>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

const Item: React.FC<{ good: store.VIPGood }> = ({ good: item }) => {
  const { dispatch } = StoreState.useContainer()
  const buy = () => {
    dispatch({
      type: 'buy',
      params: {
        gid: item.id,
        gprice: item.disCount ?? item.price,
        gtitle: item.title,
        gtype: store.GoodType.VIP,
      },
    })
  }
  return (
    <div className={`text-center p-2 pt-4 rounded ${styles['item']}`}>
      <h4 className={`mb-0 ${styles.title}`}>{item.title}</h4>
      <p className="mb-2">{item.subTitle}</p>
      <div className={`my-2 ${styles.img} d-none d-lg-block`}>
        <img
          className="h-auto"
          width="1350"
          height="717"
          src={ImageBase(item.imgUrl)}
        />
      </div>
      <div className="my-2 text-left">
        {item.params.map(({ title, note }, i) => {
          return (
            <div className="mb-2 bg-white rounded p-2" key={'x' + i}>
              <h6 className="mb-1">{title}</h6>
              <p className={`mb-0`}>{note}</p>
            </div>
          )
        })}
      </div>
      <div className="my-3">
        {item.disCount ? (
          <>
            <del className={`${styles.scolor}`}>
              <h6 className={`${styles.scolor} mb-1`}>原价: {item.price}</h6>
            </del>
            <h3>现价: {item.disCount}</h3>
          </>
        ) : (
          <>
            <h3>现价: {item.price}</h3>
          </>
        )}
        <div>
          <button className="btn btn-block btn-submit" onClick={buy}>
            购买
          </button>
        </div>
      </div>
    </div>
  )
}
