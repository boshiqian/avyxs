import { Header } from '../Header'
import * as store from '~/mod/api/store'
import { useQuery } from 'react-query'
import { ImageBase } from '~/pages/api'
import React, { useEffect } from 'react'
import styles from './vip2.module.css'
import { StoreState } from '~/components/Site/Store/Store.state'
import IconCollectionFill from '~/components/iconfont/IconCollectionFill'
import { ec } from '~/mod/ec'

export default function StoreVIPPage() {
  const { isLoading, data } = useQuery(['store-vip'], () => {
    return store.vipGoodList().then((r) => r.data.result)
  })
  useEffect(() => {
    if (!data) {
      return
    }
    let i = 0
    for (let t of data) {
      ec.addImpression({
        id: t.id,
        name: t.title,
        position: i++,
        list: '会员购买主页',
      })
    }
  }, [isLoading])
  if (isLoading) {
    return null
  }
  return (
    <div>
      <Header />
      <div className="container">
        <section className="pb-3 px-2 px-sm-0 pb-e-lg-40">
          <div className="row gutter-20">
            <div id="list_videos_my_favourite_videos">
              <section className="pb-3 pb-e-lg-40">
                <div className="row gutter-20">
                  {data.map((item, i) => {
                    const onBuy = () => {
                      ec.addProduct({
                        id: item.id,
                        name: item.title,
                        position: i,
                      })
                      ec.click()
                    }
                    return (
                      <div
                        key={item.id}
                        className="col-12 col-lg-6 mb-2 pb-e-lg-10"
                      >
                        <Item key={item.id} good={item} onBuy={onBuy} />
                      </div>
                    )
                  })}
                </div>
              </section>
            </div>
          </div>
        </section>
      </div>
    </div>
  )
}

const Item: React.FC<{ good: store.VIPGood; onBuy?: () => any }> = ({
  good: item,
  onBuy,
}) => {
  const { dispatch } = StoreState.useContainer()
  const buy = () => {
    if (onBuy) {
      onBuy()
    }
    dispatch({
      type: 'buy',
      params: {
        gid: item.id,
        gprice: item.disCount ?? item.price,
        gtitle: item.title,
        gtype: store.GoodType.VIP,
      },
    })
  }
  return (
    <div className={styles.item}>
      <div className={`${styles.img}`}>
        <div
          className={`d-flex flex-column ${styles.box}`}
          style={{ backgroundImage: `url(${ImageBase(item.imgUrl)})` }}
        >
          <div className="d-flex align-items-center mb-1">
            <h1 className={`mb-0 ${styles.vipt}`}>VIP</h1>
            <div>
              <h2 className={`mb-0 ${styles.ititle}`}>{item.title}</h2>
              <h6 className={`mb-0 ${styles.istitle}`}>{item.subTitle}</h6>
            </div>
          </div>
          <div>
            {item.params.map((elem, i) => {
              return (
                <div
                  key={'x' + i}
                  className={`d-flex align-items-center ${styles.pcol}`}
                >
                  <div className="no-font mr-2">
                    <IconCollectionFill size={1.5} color="currentColor" />
                  </div>
                  <div>
                    <div className={styles.ptitle}>{elem.title}</div>
                    <div className={styles.pstitle}>{elem.note}</div>
                  </div>
                  {/* <div>{elem.note}</div> */}
                </div>
              )
            })}
          </div>
          <div className="pb-3"></div>
          <div className="flex-grow-1"></div>
          <div className="d-flex w-100 flex-row align-items-center">
            <div className={`${styles.price}`}>
              <del>
                <span>原价¥{item.price}</span>
              </del>
            </div>
            <div className={`${styles.dprice}`}>
              <span>¥{item.disCount}</span>
            </div>
            <div className="flex-grow-1"></div>
            <div>
              <button className={`btn ${styles.btn}`} onClick={buy}>
                立即购买
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
  return (
    <div className={`text-center p-2 pt-4 rounded ${styles['item']}`}>
      <h4 className={`mb-0 ${styles.title}`}>{item.title}</h4>
      <p className="mb-2">{item.subTitle}</p>
      <div className={`my-2 ${styles.img} d-none d-lg-block`}>
        <img
          className="h-auto"
          width="1350"
          height="717"
          src={ImageBase(item.imgUrl)}
        />
      </div>
      <div className="my-2 text-left">
        {item.params.map(({ title, note }, i) => {
          return (
            <div className="mb-2 bg-white rounded p-2" key={'x' + i}>
              <h6 className="mb-1">{title}</h6>
              <p className={`mb-0`}>{note}</p>
            </div>
          )
        })}
      </div>
      <div className="my-3">
        {item.disCount ? (
          <>
            <del className={`${styles.scolor}`}>
              <h6 className={`${styles.scolor} mb-1`}>原价: {item.price}</h6>
            </del>
            <h3>现价: {item.disCount}</h3>
          </>
        ) : (
          <>
            <h3>现价: {item.price}</h3>
          </>
        )}
        <div>
          <button className="btn btn-block btn-submit" onClick={buy}>
            购买
          </button>
        </div>
      </div>
    </div>
  )
}
