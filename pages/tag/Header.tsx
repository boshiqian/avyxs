import Link from 'next/link'
import { NextRouter, useRouter } from 'next/router'
import React, { useMemo } from 'react'
import { UrlObject } from 'url'
import { SortType } from '~/mod/api/types'

type Tag = {
  title: string
  subtitle: string
  count: string
}

function makeHref(router: NextRouter, query: { sort: string }): UrlObject {
  let q2 = {}
  for (let k in router.query) {
    if (k === 'page') {
      continue
    }
    q2[k] = router.query[k]
  }
  Object.assign(q2, query)
  return {
    query: q2,
  }
}

type Props = {
  tag: Tag
  noSort?: boolean
}

export const Header: React.FC<Props> = ({ tag, noSort }) => {
  const sorts = useMemo(() => {
    return [
      { name: '默认排序', key: SortType.Default },
      { name: '近期最佳', key: SortType.Top },
      { name: '最近更新', key: SortType.Newest },
      { name: '推荐', key: SortType.Recommend },
    ]
  }, [])
  const router = useRouter()
  const sortBy = router.query?.sort ?? sorts[0].key
  const isActive = (sort: SortType) => {
    if (sort === sortBy) {
      return 'active'
    }
    return ''
  }
  return (
    <section className="content-header pt-0">
      <div className="container">
        <div className="title-with-avatar center">
          <div className="title-box">
            <h6 className="sub-title mb-1">{tag.subtitle}</h6>
            <h2 className="h3-md mb-1">{tag.title}</h2>
            <span className="inactive-color fs-2 mb-0">{tag.count} 部影片</span>
          </div>
        </div>
        {!noSort && (
          <nav className="sort-nav">
            <ul id="list_videos_common_videos_list_sort_list">
              {sorts.map((item) => {
                return (
                  <li key={item.key} className={isActive(item.key)}>
                    <Link
                      href={makeHref(router, { sort: item.key.toString() })}
                      passHref
                      shallow
                    >
                      <a>{item.name}</a>
                    </Link>
                  </li>
                )
              })}
            </ul>
          </nav>
        )}
      </div>
    </section>
  )
}
