import { useRouter } from 'next/router'
import React, { useMemo } from 'react'
import Link from 'next/link'
import { useEffect } from 'react'
import { PageResp } from '~/mod/api/types'

export const Pagination: React.FC<{ page: PageResp }> = ({ page }) => {
  const router = useRouter()
  const nowPage = router.query?.page ?? '1'
  useEffect(() => {
    jQuery('html').animate({ scrollTop: 0 })
  }, [router.query?.page])
  const pages = useMemo(() => {
    let r = []
    for (let i = page.current - 2; i <= page.current + 2; i++) {
      r.push(i)
    }
    r = r.filter((n) => n > 0)
    r = r.filter((n) => n <= page.pages)
    if (r.length < 5) {
      if (r[r.length - 1] == page.pages) {
        let r0 = r[0]
        r = [r0 - 2, r0 - 1].concat(r)
        r = r.filter((n) => n > 0)
        r = r.slice(-5)
        return r
      }
      if (r[0] == 1) {
        let r0 = r[r.length - 1]
        r = r.concat([r0 + 1, r0 + 2])
        r = r.filter((n) => n <= page.pages)
        r = r.slice(0, 5)
        return r
      }
    }
    return r
  }, [page.pages, page.current])
  return (
    <ul className="pagination">
      {pages[0] > 1 && (
        <li className="page-item">
          <Link
            href={{ query: { ...router.query, page: '1' } }}
            passHref
            shallow
            replace
          >
            <a className="page-link">« 首頁</a>
          </Link>
        </li>
      )}
      {pages.map((n) => {
        let nn = '' + n
        if (n < 10) {
          nn = '0' + nn
        }
        if (page.current === n) {
          return (
            <li key={n} className="page-item">
              <span className="page-link active disabled">{nn}</span>
            </li>
          )
        }
        return (
          <li key={n} className="page-item">
            <Link
              href={{ query: { ...router.query, page: n } }}
              passHref
              shallow
              replace
            >
              <a className="page-link">{nn}</a>
            </Link>
          </li>
        )
      })}
      {pages[pages.length - 1] < page.pages && (
        <li className="page-item">
          <Link
            href={{ query: { ...router.query, page: page.pages } }}
            passHref
            shallow
            replace
          >
            <a className="page-link">最後 »</a>
          </Link>
        </li>
      )}
    </ul>
  )
}
