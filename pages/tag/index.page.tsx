import { Header as TagHeader } from '../tag/Header'
import { VideoCard } from '~/pages/home/VideoCard'
import { Pagination } from '../tag/Pagination'
import { useRouter } from 'next/router'
import * as video from '~/mod/api/video'
import { useQuery } from 'react-query'
import { APIClient } from '../api'
import { SortType } from '~/mod/api/types'

export default function ColPage() {
  const api = APIClient.useContainer()
  const router = useRouter()
  const q = router.query?.q as string
  const page = router.query?.page ?? '1'
  const sort: SortType = (router.query?.sort as any) ?? SortType.Newest
  const { isLoading, data } = useQuery(
    ['search', q, sort, page] as [string, string, SortType, string],
    ({ queryKey: [_name, q, sort, page] }) => {
      return video
        .search({ sort, key: q, page: page as any as number })
        .then((r) => r.data.result)
    },
    {},
  )
  if (isLoading) {
    return null
  }
  return (
    <>
      <TagHeader
        tag={{ title: q, subtitle: '搜索结果', count: data?data.total.toString():'' }}
      />
      <div className="container">
        <section className="pb-3 pb-e-lg-40">
          {data.total === 0 ? (
            <div className="text-center">
              <img
                className="mt-5 mb-3 opacity-8"
                src="/img/doge.png"
                height="110"
              />
              <h5 className="inactive-color">暫無相關內容</h5>
            </div>
          ) : (
            <>
              <div className="row gutter-20">
                {data.records.map((item) => {
                  return (
                    <div key={item.id} className="col-6 col-sm-4 col-lg-3">
                      <VideoCard item={item} />
                    </div>
                  )
                })}
              </div>
              <Pagination page={data} />
            </>
          )}
        </section>
      </div>
    </>
  )
}
