import { Header as TagHeader } from '../tag/Header'
import { VideoCard } from '~/pages/home/VideoCard'
import { Pagination } from '../tag/Pagination'
import { useRouter } from 'next/router'
import * as video from '~/mod/api/video'
import { useQuery } from 'react-query'
import { APIClient } from '../api'
import { SortType } from '~/mod/api/types'
import { useReducer, useState } from 'react'
import { usePageLoading } from '~/components/Site/Modal'
import { SiteTitle } from '~/components/Site/Title'

export default function TopicPage() {
  const router = useRouter()
  const q = router.query?.id as string
  const page = router.query?.page ?? '1'
  const sort: SortType = (router.query?.sort as any) ?? SortType.Default

  const api = APIClient.useContainer()
  const { isLoading, data } = useQuery(
    ['topicData', q, sort, page as string] as [string, string, SortType, string],
    ({ queryKey: [_name, id, sort, page] }) => {
        let params={}
        if (q!='more')
        {
            params={ sort, id:id, page: page as any as number }
        }
        else {
            params={sort,id:id, page: page as any as number}
        }
      return video
        .getVideoByZhuanTi(params)
        .then((r) =>r.data.result)
    },
  )
  usePageLoading(isLoading)
  if (isLoading) {
    return null
  }

  return (
    <>
      <SiteTitle title={q} />
      <TagHeader
        tag={{
          title:data?data.title:'',
          subtitle: '搜索结果',
          count:data?data.ext.total.toString():'0',
        }}
      />
      <div className="container">
        <section className="pb-3 pb-e-lg-40">
          {!data ? null : data.ext.total === 0 ? (
            <div className="text-center">
              <img
                className="mt-5 mb-3 opacity-8"
                src="/img/doge.png"
                height="110"
              />
              <h5 className="inactive-color">暫無相關內容</h5>
            </div>
          ) : (
            <>
              <div className="row gutter-20">
                {data.ext.records.map((item) => {
                  return (
                    <div key={item.id} className="col-6 col-sm-4 col-lg-3">
                      <VideoCard item={item} />
                    </div>
                  )
                })}
              </div>
              <Pagination page={data.ext} />
            </>
          )}
        </section>
      </div>
    </>
  )
}
