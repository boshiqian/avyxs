import { register } from '~/mod/api/withGa/user'
import { createContainer } from 'unstated-next'
import React, { useEffect, useState } from 'react'
import { useQuery } from 'react-query'

const UserIDStorageKey = 'user-id'

const hourKey = [0, 1, 2, 3, 4, 5, 6, 7, 8]
const getHourKey = () => {
  const h = new Date().getHours()
  return hourKey[Math.floor(h / 3)]
}

export async function registerUser() {
  let userId = localStorage.getItem(UserIDStorageKey)
  if (!userId) {
    // let d = await import('@fingerprintjs/fingerprintjs')
    // let fp = await d.load()
    // let r = await fp.get()

    // let deviceId = r.visitorId + getHourKey()
    userId = await register({})
      .then((r) => r.data.result)
      .then((r) => r.id)
    localStorage.setItem(UserIDStorageKey, userId)
  }
  return userId
}

export const getUid = () => {
  return localStorage.getItem(UserIDStorageKey)
}
export const setUid = (uid: string) => {
  if (uid === '') {
    return localStorage.removeItem(UserIDStorageKey)
  }
  return localStorage.setItem(UserIDStorageKey, uid)
}

import * as user from '~/mod/api/withGa/user'
import { UserInfo } from '~/mod/api/types'
import { withGAEvent } from '~/mod/utils'
export const placeholderUser = {
  account: 'guest',
  balance: 0,
  devCode: '',
  devType: 3,
  headUrl: 'user/def_head.png',
  id: '',
  isVip: false,
  nickName: '游客',
  sex: 0,
} as UserInfo

const useUserQuery = () => {
  return useQuery(
    ['user-info'],
    () => {
      return user
        .login({ userId: getUid() })
        .then((r) => r.data.result)
        .then((u) => {
          if (!u) {
            return Promise.reject('login fail')
          }
          return u
        })
    },
    {
      placeholderData: placeholderUser,
      retry: false,
    },
  )
}

export const UserQuery = createContainer(useUserQuery)

export const UserProvider: React.FC = (props) => {
  const q = UserQuery.useContainer()
  if (!q?.data?.id) {
    return null
  }
  return <>{props.children}</>
}

export const UserPrice = (user: UserInfo) => (price: number) => {
  let discount = user.discount / 10
  price = price * discount
  price = Math.ceil(price)
  return price
}
