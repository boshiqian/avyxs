import React from 'react'
import { VideoInfo } from '~/mod/api/video'
import { Form, ModalState } from '~/components/Site/Modal'
import * as store from '~/mod/api/store'
import { getUid, UserPrice, UserQuery } from '../user.state'
import IconVip from '~/components/iconfont/IconVip'
import { toast } from 'react-toastify'
import { isRespError } from '~/mod/utils'
import ReactGA from 'react-ga'

export const BuyVideo = () => {
  const { data: info, refetch } = VideoQuery.useContainer()
  const { dispatch } = VideoBuyModalState.useContainer()
  const { dispatch: mdispatch } = StoreState.useContainer()

  const { refetch: refetchUser } = UserQuery.useContainer()
  const buyFn = () => {
    dispatch({ type: 'loading' })
    Promise.resolve(1)
      .then(() => {
        return store
          .buyVideo({ userId: getUid(), videoId: info.id })
          .then((r) => r.data.result)
      })
      .then((r) => {
        return Promise.all([refetchUser(), refetch()]).then(() => r)
      })
      .then(
        (balance) => {
          toast.success('购买成功')
          dispatch({ type: 'close' })
        },
        (err) => {
          if (isRespError(err)) {
            toast.error(err.message)
          } else {
            toast.error('网络波动, 请稍后重试')
          }
          dispatch({ type: 'loaded' })
        },
      )
  }
  const { data: user } = UserQuery.useContainer()
  const payCoins = UserPrice(user)(info.payCoin)
  return (
    <Form
      title="购买影片"
      subtitle={`购买影片: ${info.title}`}
      close={() => dispatch({ type: 'close' })}
    >
      <div className="form-group-attached">
        <div className="form-group">
          <label>价格</label>
          <div className="form-control">
            {user.isVip && (
              <label>
                <del>
                  <span>{info.payCoin}</span>
                </del>
              </label>
            )}
            {!user.isVip && <span>{info.payCoin}</span>}
          </div>
        </div>
        {user.isVip && (
          <div className="form-group">
            <label>会员价</label>
            <div className="form-control">
              <span>{payCoins}</span>
              <IconVip className="mb-1 mx-1" color="currentColor" size={0.9} />
            </div>
          </div>
        )}
        <div className="form-group">
          <label htmlFor="vierfy_code">余额变动</label>
          <div className="d-flex align-items-center">
            <div className="form-control">
              {user.balance} - {payCoins} = {user.balance - payCoins}
            </div>
            <div>
              <button
                className="float-right btn"
                onClick={() => {
                  mdispatch({ type: 'goods', goods: <BuyCoinsWrapper /> })
                  // ga
                  ReactGA.event({
                    category: '视频购买',
                    action: '补充金币',
                    label: info.id,
                  })
                }}
              >
                购买金币
              </button>
            </div>
          </div>
        </div>
      </div>
      <div>
        <button
          className="btn btn-block btn-submit"
          onClick={() => {
            buyFn()
            // ga
            ReactGA.event({
              category: '视频购买',
              action: '购买影片',
              label: info.id,
            })
          }}
        >
          购买影片 {payCoins}
        </button>
      </div>
    </Form>
  )
}

import { CoinGoods } from '../member/store/coin.page'
import { VideoQuery } from './video.state'
import { VideoBuyModalState } from './BuyModal.state'
import { StoreState } from '~/components/Site/Store/Store.state'
import { VipGoods } from '../member/store/vip.goods'
import { ec } from '~/mod/ec'
export const BuyCoinsWrapper = () => {
  const { dispatch } = StoreState.useContainer()
  return (
    <Form
      title="金币购买"
      subtitle=""
      close={() => dispatch({ type: 'close' })}
    >
      <div className="row gutter-20">
        <CoinGoods dispatch={dispatch} className="col-6 col-sm-4 col-lg-4" />
      </div>
    </Form>
  )
}

export const BuyVipWrapper = () => {
  const { dispatch } = VideoBuyModalState.useContainer()
  return (
    <Form
      title="会员购买"
      subtitle=""
      close={() => dispatch({ type: 'close' })}
    >
      <div className="row gutter-20">
        <VipGoods dispatch={dispatch} className="col-6" />
      </div>
    </Form>
  )
}
