import React, { useReducer } from 'react'
import { createContainer } from 'unstated-next'
import { ec } from '~/mod/ec'
import ReactGA from 'react-ga'
import { VideoInfo } from '~/mod/api/video'

export enum BuyWays {
  Hidden = 0,
  Coins = 1,
  Vip = 2,
}

type State = {
  show: boolean
  loading: boolean
  content: BuyWays
  video: VideoInfo
}

type Action =
  | { type: 'show'; show: BuyWays; video: VideoInfo }
  | { type: 'close' }
  | { type: 'loading' }
  | { type: 'loaded' }

const defaultState: State = {
  show: false,
  loading: false,
  content: BuyWays.Hidden,
  video: null,
}
function StateReducer(s: State, a: Action): State {
  let ns = { ...s }
  switch (a.type) {
    case 'loaded':
      ns.loading = false
      break
    case 'loading':
      ns.loading = true
      break
    case 'close':
      ns.show = false
      // ga
      {
        let c = { category: '视频购买', label: s?.video?.id }
        ReactGA.event({ ...c, action: '取消购买' })
      }
      break
    case 'show':
      ns.show = true
      ns.content = a.show
      ns.video = a.video
      // ga
      {
        let c = { category: '视频购买', label: a.video.id }
        switch (a.show) {
          case BuyWays.Coins:
            ReactGA.event({ ...c, action: '金币购买' })
            break
          case BuyWays.Vip:
            ReactGA.event({ ...c, action: '开通会员' })
            break
        }
      }
      break
  }
  return ns
}
const useVideoBuyModalState = () => {
  const [state, dispatch] = useReducer(StateReducer, defaultState)
  return { state, dispatch }
}
export const VideoBuyModalState = createContainer(useVideoBuyModalState)
