import { Modal } from '~/components/Site/Modal'
import { BuyWays, VideoBuyModalState } from './BuyModal.state'
import styles from './BuyModal.module.css'
import React from 'react'

import { BuyVideo, BuyVipWrapper } from './Buy'

export const BuyModal = () => {
  const { state } = VideoBuyModalState.useContainer()
  return (
    <Modal className={styles.root} show={state.show} loading={state.loading}>
      {state.content === BuyWays.Coins && <BuyVideo />}
      {state.content === BuyWays.Vip && <BuyVipWrapper />}
    </Modal>
  )
}

export const BuyModalProvider: React.FC = (props) => {
  return (
    <VideoBuyModalState.Provider>
      <BuyModal />
      {props.children}
    </VideoBuyModalState.Provider>
  )
}
