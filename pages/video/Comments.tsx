import Link from 'next/link'
import { ImageBase } from '../api'
import { placeholderUser } from '../user.state'

export const Comments = () => {
  return (
    <section
      className="comments pb-3 pb-lg-4"
      data-block-id="video_comments_video_comments"
    >
      <div className="new-comment">
        <img
          className="avatar"
          src={ImageBase(placeholderUser.headUrl)}
          width="35"
        />
        <form className="right" action="#" method="post">
          <div className="form-group">
            <div className="info">
              <a href="" data-fancybox="ajax">
                登入
              </a>
              後即可留言，歡迎討論劇情或分享心得。
            </div>
          </div>
        </form>
      </div>
      <div id="video_comments_video_comments" className="comment-list">
        <div
          className="alert alert-success w-100"
          style={{ display: 'none' }}
          role="alert"
        >
          Thank you! Your comment has been submitted for review.
        </div>
        <h6 className="sub-title inactive-color pb-3">留言 (2)</h6>
        <div id="video_comments_video_comments_items">
          {fakeData.map((item) => {
            return (
              <div key={item.id} className="item " data-comment-id={item.id}>
                <Link
                  href={{ pathname: '/member', query: { id: item.user.id } }}
                  passHref
                >
                  <a title="dasdasdad">
                    <img
                      className="avatar mt-1"
                      src={ImageBase(item.user.avatar)}
                      width="35"
                    />
                  </a>
                </Link>
                <div className="right">
                  <div className="title">
                    <span className="pr-2">
                      <Link
                        href={{
                          pathname: '/member',
                          query: { id: item.user.id },
                        }}
                        passHref
                      >
                        <a>dasdasdad</a>
                      </Link>
                    </span>
                    <span className="inactive-color pr-2">{item.datetime}</span>
                  </div>
                  <p className="comment-text">
                    <span className="original-text">{item.content}</span>
                  </p>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </section>
  )
}

const fakeData = [
  {
    id: '2',
    user: {
      id: '1',
      avatar: placeholderUser.headUrl,
      nickiname: 'ilovecurry3000',
    },
    datetime: '1 小時前',
    content: '楼上+1',
  },
  {
    id: '1',
    user: {
      id: '2',
      avatar: placeholderUser.headUrl,
      nickiname: 'ilovecurry3000',
    },
    datetime: '11 小時前',
    content: '男的是不是有點陽痿',
  },
]
