import Plyr from '~/mod/plyr'
import Hls from 'hls.js'
import React, { useEffect, useRef } from 'react'
import { VideoInfo } from '~/mod/api/video'
import ReactGA from 'react-ga'

export const Player: React.FC<{ video: VideoInfo }> = ({ video }) => {
  let videoUrl = video.videoUrl
  const ref = useRef<HTMLVideoElement>()
  useEffect(() => {
    if (!ref.current) {
      return
    }
    var videoElem = document.getElementById('player') as HTMLVideoElement

    let hls: Hls
    if (videoUrl.slice(-4) === 'm3u8') {
      var hlsUrl = videoUrl
      if (Hls.isSupported()) {
        hls = new Hls({
          autoStartLoad: true,
          startPosition: -1,
          maxBufferLength: 0x20,
          maxMaxBufferLength: 0x20,
          maxBufferSize: 0x1e * 0x3e8 * 0x3e8,
          appendErrorMaxRetry: 0x5,
          startFragPrefetch: true,
          capLevelToPlayerSize: true,
          manifestLoadingTimeOut: 0x4e20,
          manifestLoadingMaxRetry: 0x3,
        })
        hls.loadSource(hlsUrl)
        hls.attachMedia(videoElem)
      } else if (videoElem.canPlayType('application/vnd.apple.mpegURL')) {
        let newSource = document.createElement('source')
        newSource.src = videoUrl
        newSource.type = 'application/vnd.apple.mpegURL'
        videoElem.appendChild(newSource)
      } else {
        alert('对不起,你的设备无法播放该m3u8类型视频, 换个视频试试吧.')
      }
    }

    let p = new Plyr(videoElem, {
      ratio: '16:9',
      fullscreen: { enabled: true, fallback: true, iosNative: true },
      // ads: { enabled: false, tagUrl: "" },
      // previewThumbnails: { enabled: true, src: vttUrl },
    })
    return () => {
      p.destroy()
      hls?.destroy()
    }
  }, [videoUrl])
  let ec = { category: '视频播放', label: video.title }
  return (
    <section className="pb-3 pb-e-lg-30">
      <video
        ref={ref}
        poster={video.imgUrl}
        id="player"
        playsInline
        controls
        onPlay={() => {
          ReactGA.event({ ...ec, action: '播放' })
        }}
        onPause={() => {
          ReactGA.event({ ...ec, action: '暂停' })
        }}
      >
        {videoUrl.slice(-4) === '.mp4' && (
          <source src={videoUrl} type="video/mp4" />
        )}
      </video>
    </section>
  )
}
