import IconBrowse from '~/components/iconfont/IconBrowse'
import IconFavoritesFill from '~/components/iconfont/IconFavoritesFill'
import IconClock from '~/components/iconfont/IconClock'
import Link from 'next/link'
import React from 'react'
import { VideoInfo as _VideoInfo } from '~/mod/api/withGa/video'
import * as video from '~/mod/api/video'
import moment from 'moment'
import { beforeNow, isRespError, tapError, tapOk } from '~/mod/utils'
import { PayMode, PayMode2Str } from '~/mod/api/types'
import styles from './VideoInfo.module.css'
import { toast } from 'react-toastify'
import { UserQuery } from '../user.state'
import { BuyWays, VideoBuyModalState } from './BuyModal.state'
import { SiteColAD } from '~/components/Site/AD'
import { useMutation } from 'react-query'
import { ec } from '~/mod/ec'
import ReactGA from 'react-ga'

export const VideoInfo: React.FC<{ video: _VideoInfo }> = ({ video: info }) => {
  const t = moment(info.createTime)

  return (
    <section className="video-info pb-3">
      <div className="info-header">
        <div className="header-left">
          <h4>{info.title}</h4>
          <h6>
            <IconClock color="currentColor" />
            <span className="mr-3">{beforeNow(t)}</span>
            <IconBrowse color="currentColor" />
            <span className="mr-3">{info.playCount}</span>
            <div className="models">
              {/* {info.models.map((model) => {
                return (
                  <Link
                    key={model.id}
                    href={{ pathname: '/tag', query: { id: model.id } }}
                    passHref
                  >
                    <a className="model">
                      {model.avatar ? (
                        <img
                          className="avatar rounded-circle"
                          src={model.avatar}
                          data-toggle="tooltip"
                          data-placement="bottom"
                          title=""
                          data-original-title={model.name}
                        />
                      ) : (
                        <span
                          className="placeholder rounded-circle"
                          data-toggle="tooltip"
                          data-placement="bottom"
                          title={model.name}
                        >
                          {model.name.slice(0, 1)}
                        </span>
                      )}
                    </a>
                  </Link>
                )
              })} */}
            </div>
          </h6>
        </div>
        <div className="header-right d-none d-md-block">
          <h6>
            <span className="text-danger fs-1 mr-2">●</span>
            时长: {info.playTime}
          </h6>
          <span className="inactive-color">
            上市於 {t.format('YYYY-MM-DD')}
          </span>
        </div>
      </div>
      <div className="text-center">
        <div className="my-3">
          <ZanBtn info={info} />
          {/* <button className="btn btn-action">
            <IconJiaobiao color="currentColor" />
          </button>
          <button className="btn btn-action " data-toggle="dropdown">
            <IconEllipsis color="currentColor" />
          </button>
          <div className="dropdown-menu">
            <a
              data-fancybox
              data-src="#flag-form"
              href="#"
              className="dropdown-item"
            >
              問題回報
            </a>
          </div> */}
        </div>
        <h5 className="tags h6-md">
          {info?.tagIds && <Tags id={info.id} tags={info.tagIds.split(',')} />}
        </h5>
      </div>
      <SiteColAD position="1" />
    </section>
  )
}

const ZanBtn: React.FC<{ info: _VideoInfo }> = ({ info }) => {
  const zan = useMutation(() => {
    return video.zan({ id: info.id }).then(
      tapOk(() => {
        toast.success('点赞成功')
      }),
      tapError((err) => {
        toast.error(isRespError(err) ? err.message : '点赞失败, 网络波动请重试')
      }),
    )
  })
  return (
    <button
      className="btn btn-action fav mr-2"
      onClick={() => zan.mutateAsync()}
      disabled={zan.isLoading}
    >
      <IconFavoritesFill className="mr-2" size={1.2} color="currentColor" />
      <span className="count">{info.zan}</span>
    </button>
  )
}

const BuyBtn: React.FC<{ info: _VideoInfo }> = ({ info }) => {
  const { dispatch } = VideoBuyModalState.useContainer()
  const { data: user } = UserQuery.useContainer()
  switch (info.payMode) {
    case PayMode.RequirePay:
      return (
        <button
          className={`btn btn-action mr-2 ${styles['btn']} ${styles['bg-green']}`}
          onClick={() => {
            dispatch({ type: 'show', show: BuyWays.Coins, video: info })
            // ga
            ReactGA.event({
              category: '视频购买',
              action: '点击购买',
              label: info.id,
            })
          }}
        >
          付费购买
        </button>
      )
    case PayMode.VIPFree:
    default:
    case PayMode.Free:
      return (
        <button
          className={`btn btn-action mr-2 ${styles['btn']} ${styles['bg-green']}`}
          onClick={() => {
            dispatch({ type: 'show', show: BuyWays.Vip, video: info })
          }}
        >
          更多精彩, 点击开通VIP
        </button>
      )
  }
  return (
    <button
      className={`btn btn-action mr-2 ${styles['btn']} ${styles['bg-fav']}`}
      onClick={() => toast.success('本影片已购买')}
    >
      免费影片
    </button>
  )
}

const Tags: React.FC<{ tags: string[]; id: string }> = ({ tags, id }) => {
  return (
    <>
      {/* {tagsWithCat.map((tag) => {
        return (
          <Link href={tag.link} key={tag.id} passHref>
            <a className="cat">{tag.name}</a>
          </Link>
        )
      })} */}
      {/* <span className="separator">•</span> */}
      {tags.map((tag) => {
        return (
          <Link
            href={{ pathname: '/search', query: { q: tag } }}
            key={tag}
            passHref
          >
            <a>{tag}</a>
          </Link>
        )
      })}
    </>
  )
}
