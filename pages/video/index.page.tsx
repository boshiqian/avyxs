import { Player } from './Player'
import { VideoInfo } from './VideoInfo'
import { Comments } from './Comments'
import {Recommend} from "./Recommend";
import { RightVideos } from './RightVideos'
import { usePageLoading } from '~/components/Site/Modal'
import { VideoQuery, RandomVideosQuery } from './video.state'
import { BuyModalProvider } from './BuyModal'
import { SiteTitle } from '~/components/Site/Title'

export default function VideoPageWrapper() {
  return (
    <VideoQuery.Provider>
      <RandomVideosQuery.Provider>
        <BuyModalProvider>
          <VieoPage />
        </BuyModalProvider>
      </RandomVideosQuery.Provider>
    </VideoQuery.Provider>
  )
}

function VieoPage() {
  const { isLoading, isError, data, refetch } = VideoQuery.useContainer()
  const { data: rvideos } = RandomVideosQuery.useContainer()
    console.log(rvideos.slice(0, 12),'rvideos')
  usePageLoading(isLoading)
  if (isLoading || isError) {
    return null
  }
  return (
    <div className="container">
      <SiteTitle title={data?.title} />
      <div className="row">
        <div className="col">
          <Player video={data} />
          <VideoInfo video={data} />
          {/* <Comments /> */}
          {/* <WeekTop title="猜你喜欢" subtitle="推荐" /> */}
          <Recommend
            col={{
              id: '',
              list: (rvideos || []).slice(0, 12),
              title: '猜你喜欢',
              subtitle: '推荐',
            }}
          />
        </div>
        <div className="col right-sidebar">
          <RightVideos videos={(rvideos || []).slice(-12)} />
        </div>
      </div>
    </div>
  )
}
