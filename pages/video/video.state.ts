import { useQuery } from 'react-query'
import { getUid, UserQuery } from '~/pages/user.state'
import * as video from '~/mod/api/video'
import { useRouter } from 'next/router'
import { APIClient } from '~/pages/api'
import { createContainer } from 'unstated-next'
import { ad, ADResponse } from '~/mod/api/ad'
import { PayMode, VideoDesc } from '~/mod/api/types'
import { useEffect } from 'react'

const useVideoQuery = () => {
  const router = useRouter()
  const id = router.query.id as string
  const q = useQuery(['video', id], ({ queryKey: [_name, id] }) => {
    return video.info({ userId: getUid(), id: id }).then((r) => r.data.result)
  })
  const u = UserQuery.useContainer()
  useEffect(() => {
    if (u.isLoading) {
      return
    }
    if (!u.data) {
      return
    }
    if (u.isFetching) {
      return
    }
    if (q.isLoading) {
      return
    }
    q.refetch()
  }, [u.isFetching])
  return q
}

export const VideoQuery = createContainer(useVideoQuery)

const useRandomVideosQuery = () => {
  const router = useRouter()
  const id = router.query.id as string
  return useQuery(
    ['video-recommend', id],
    async ({ queryKey: [_name, id] }) => {
      let a = await video
        .randRecommend({ limit: 22 })
        .then((r) => {
          return r.data.result
        })
      let b = await ad({ type: 1 }).then((r) => r.data.result as any)
      a.splice(0, 0, b)
      a.splice(12, 0, b)
      return a
    },
    {
      placeholderData: [],
    },
  )
}
export const RandomVideosQuery = createContainer(useRandomVideosQuery)
